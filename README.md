# Arbitrary Control Dependence

This is a formalization in the Coq proof assistant of a theory about weak
control dependence (Danicic et al., 2011). It also contains a Why3 formalization
of lDFS, an algorithm computing weak control-closure optimizing Danicic's
algorithm, along with OCaml manual implementations of both algorithms for
performance comparisons.

See the [publications](#publications) for more details.

## Contents

This repository contains:
- the Coq formalization and proof of Danicic's algorithm (in subdirectory [`coq/`](coq/)),
- the Why3 formalization of the new algorithm lDFS (in subdirectory [`why3/`](why3/)),
- the OCaml code used to perform some benchmarks (in subdirectory [`ocaml/`](ocaml/)).

A README is present in each subdirectory and explains more precisely
how to use the files.

## License

This project is free software. All files in this distribution are, unless specified
otherwise, licensed under the [MIT license](LICENSE). The exceptions are:
- the graph library in directory [`coq/concrete_graphs/`](coq/concrete_graphs/)
  (GPL 3 or later, see [`coq/concrete_graphs/LICENSE`](coq/concrete_graphs/LICENSE)),
- the result of extracting the Coq formalization of Danicic's algorithm into OCaml
  in directory [`ocaml/coq_extraction/`](ocaml/coq_extraction/)
  (a mix of LPGL 2.1 only, GPL 3 or later and MIT,
  see [`ocaml/coq_extraction/README.md`](ocaml/coq_extraction/README.md)).

## Publications

- [Efficient computation of arbitrary control dependencies](https://doi.org/10.1016/j.tcs.2023.114029) <br>
Jean-Christophe Léchenet, Nikolai Kosmatov, Pascale Le Gall. In Theoretical Computer Science (2023).

- [Fast Computation of Arbitrary Control Dependencies](https://doi.org/10.1007/978-3-319-89363-1_12) <br>
Jean-Christophe Léchenet, Nikolai Kosmatov, Pascale Le Gall. In FASE 2018 (part of ETAPS 2018).
