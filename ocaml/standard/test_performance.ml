open Graph.Pack.Digraph
open MyGraph

let filter g crit w =
  let p = PathCheck.create g in
  S.filter (Danicic.reachable_from p crit) w

type aggregate =
| Sum (* all components are summed *)
| Split (* all components are showned separately *)
| Simple (* just the core calculus is shown *)

(* name, filtering ?, function *)
let l = [
  "coq", (false, fun g crit -> Coq.main g crit);
  "optimized", (true, fun g crit -> Optimized.main g crit);
  "danicic", (false, fun g crit -> Danicic.weak_superset g crit);
  "danicic_opti_step", (false, fun g crit -> Danicic.weak_superset_opti_step g crit);
  "danicic_opti_all", (false, fun g crit -> Danicic.weak_superset_opti_all g crit);
]

let test (reachable, f) g crit =
   let start_time = Unix.gettimeofday () in
   let result = f g crit in
   let end_time = Unix.gettimeofday () in
   let time_exec = end_time -. start_time in
   let size_before_filter = if reachable then S.cardinal result else 0 in
   let start_time = Unix.gettimeofday () in
   let result_final = if reachable then filter g crit result else result in
   let end_time = Unix.gettimeofday () in
   let time_filter = end_time -. start_time in
   (result_final, size_before_filter, (time_exec, time_filter))

let main nb algos =
    let start_time = Unix.gettimeofday () in
    let nb_e = nb * 2 in
    let g = Rand.graph ~loops:true ~v:nb ~e:nb_e () in
    let vertices = S.elements (fold_vertex (fun u acc -> S.add u acc) g S.empty) in
    let crit = [List.nth vertices 0; List.nth vertices 2; List.nth vertices 4] in
    let crit = S.of_list crit in
    let end_time = Unix.gettimeofday () in
    let time_gen = end_time -. start_time in
    let vts = Array.map (fun f -> test f g crit) algos in
    let ts = Array.make (Array.length vts + 3) (0., 0.) in
    ts.(0) <- (let (v, _, _) = vts.(0) in (float (S.cardinal v), 0.));
    let i = ref 0 in
    ignore (Array.fold_left (fun acc (v, n, t) -> if n <> 0 then ts.(1) <- (float n, 0.); assert (S.equal acc v); ts.(!i+3) <- t; incr i; v) (let (v, _, _) = vts.(0) in v) vts);
    ts.(2) <- (time_gen, 0.);
    ts

let print ?(aggregate=Sum) nb repetitions algos =
  for i = 0 to repetitions - 1 do
     Format.printf "%d," nb;
     let ts = main nb algos in
     Array.iteri (fun i t -> if i < 1 then Format.printf "%d," (let (t, _) = t in int_of_float t)
                             else if i = 1 then begin let t = (let (t, _) = t in int_of_float t) in
                                           if t = 0 then Format.printf "N/A," else Format.printf "%d," t end
                             else if i < 3 then Format.printf "%.2e," (let (t, _) = t in t)
                             else begin begin match aggregate with
                                  | Sum -> let (t1, t2) = t in Format.printf "%.2e" (t1 +.t2)
                                  | Split -> let (t1, t2) = t in Format.printf "(%.2e|%.2e)" t1 t2
                                  | Simple -> let (t1, _) = t in Format.printf "%.2e" t1 end;
                             if i < Array.length ts - 1 then Format.printf "," end) ts;
    Format.printf "@."
  done

let () =
  if Array.exists ((=) "--list") Sys.argv
  then
  begin
    List.iter (fun (s, _) -> Format.printf "%s@." s) l;
    exit 0
  end;
  if Array.exists ((=) "--help") Sys.argv
  then
  begin
    Format.printf "Usage: test_performance.native start end step repeat [algorithms]... [options]...@;\
                   A counter is initialized at 'start' and increases by 'step' until it reaches 'end'.@;\
                   At each value of the counter, 'repeat' random graphs are created and each@;\
                   algorithm is called on them.@;\
                   Available options are:@;\
                   %-20s %s@;\
                   %-20s %s@;\
                   %-20s %s@;\
                   %-20s %s@;\
                   %-20s %s@."
                   "--help" "display this help"
                   "--list" "list the available algorithm"
                   "--sum" "sum the times of the algorithm and the filtering step (default)"
                   "--split" "show separately the times of the algorithm and the filtering step"
                   "--simpl" "ignore the filtering time";
    exit 0
  end;
  if Array.length Sys.argv <= 5 then failwith "not enough arguments";
  let start = int_of_string (Sys.argv.(1)) in
  let final = int_of_string (Sys.argv.(2)) in
  let step = int_of_string (Sys.argv.(3)) in
  let repetitions = int_of_string (Sys.argv.(4)) in
  let other_args = Array.sub Sys.argv 5 (Array.length Sys.argv - 5) in
  let aggregate =
    if Array.exists ((=) "--simpl") Sys.argv then Simple
    else if Array.exists ((=) "--split") Sys.argv then Split
    else Sum
  in
  let num_algos = Array.fold_left (fun acc opt -> if opt.[0] = '-' && opt.[1] = '-' then acc else 1+acc) 0 other_args in
  let i = ref 0 in
  let algo_names = Array.init num_algos (fun _ -> while let s = other_args.(!i) in s.[0] = '-' && s.[1] = '-' do incr i done;
                                                  let s = other_args.(!i) in incr i; s)
  in
  if Array.length algo_names = 0 then begin failwith "At least one algo must be specified" end;
  let algos = match Array.map (fun x -> List.assoc x l) algo_names with
              | exception Not_found -> failwith "unknown algo (to print a list, use --list)"
              | algos -> algos
  in
  Random.self_init ();
  Format.printf "#nodes,size,size before filter,generation time,";
  Array.iteri (fun i name -> if not(name.[0] = '-' && name.[1] = '-') then Format.printf "%s" name;
                             if i < Array.length algo_names - 1 then Format.printf ",") algo_names;
  Format.printf "@.";

  let i = ref start in
  while !i <= final do
    print ~aggregate !i repetitions algos;
    i := !i + step
  done
