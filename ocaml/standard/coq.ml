open MyGraph
open Graph.Pack.Digraph

let main ?(debug=false) g (crit: S.t) =
  let g_coq = fold_vertex (fun v g -> Cfg_test.DG.add_vertex (V.label v) g None) g Cfg_test.DG.empty_graph in
  let g_coq : Cfg_test.DG.t = fold_edges (fun u v g -> Cfg_test.DG.add_edge (V.label u, V.label v) g None) g g_coq in
  let crit_coq : int list = List.map V.label (S.elements crit) in
  S.of_list (List.map (fun x -> find_vertex g x) (Cfg_test.test g_coq crit_coq))
