open Equalities

module type COMBINE_TYPE =
 sig
  type coq_Label

  val combine : coq_Label -> coq_Label -> coq_Label
 end

module type ABSTRACT_GRAPH =
 functor (V:UsualEq) ->
 functor (C:COMBINE_TYPE) ->
 sig
  type coq_Graph
 end

module type ABSTRACT_FINITE_GRAPH =
 functor (V:UsualEq) ->
 functor (C:COMBINE_TYPE) ->
 sig
  type coq_Graph
 end

module AbstractGraphProperties :
 functor (V:UsualEq) ->
 functor (C:COMBINE_TYPE) ->
 functor (G:sig
  type coq_Graph
 end) ->
 sig
  
 end

module AbstractGraphPropertiesDec :
 functor (V:UsualDecidableType) ->
 functor (C:COMBINE_TYPE) ->
 functor (G:sig
  type coq_Graph
 end) ->
 sig
  module Coq_lemma53_preresults :
   sig
    
   end
 end

module AbstractGraphPropertiesDecFinite :
 functor (V:UsualDecidableType) ->
 functor (C:COMBINE_TYPE) ->
 functor (G:sig
  type coq_Graph
 end) ->
 sig
  module Coq_lemma53_preresults :
   sig
    
   end
 end
