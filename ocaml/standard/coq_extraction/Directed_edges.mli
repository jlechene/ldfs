open Datatypes
open OrderedTypeEx

module Directed_Edge :
 functor (Vertex:OrderedType.OrderedType) ->
 sig
  module VertexPair :
   sig
    module MO1 :
     sig
      module TO :
       sig
        type t = Vertex.t
       end

      module IsTO :
       sig
        
       end

      module OrderTac :
       sig
        
       end

      val eq_dec : Vertex.t -> Vertex.t -> bool

      val lt_dec : Vertex.t -> Vertex.t -> bool

      val eqb : Vertex.t -> Vertex.t -> bool
     end

    module MO2 :
     sig
      module TO :
       sig
        type t = Vertex.t
       end

      module IsTO :
       sig
        
       end

      module OrderTac :
       sig
        
       end

      val eq_dec : Vertex.t -> Vertex.t -> bool

      val lt_dec : Vertex.t -> Vertex.t -> bool

      val eqb : Vertex.t -> Vertex.t -> bool
     end

    type t = Vertex.t * Vertex.t

    val compare : t -> t -> (Vertex.t * Vertex.t) OrderedType.coq_Compare

    val eq_dec : t -> t -> bool
   end

  module MO1 :
   sig
    module TO :
     sig
      type t = Vertex.t
     end

    module IsTO :
     sig
      
     end

    module OrderTac :
     sig
      
     end

    val eq_dec : Vertex.t -> Vertex.t -> bool

    val lt_dec : Vertex.t -> Vertex.t -> bool

    val eqb : Vertex.t -> Vertex.t -> bool
   end

  module MO2 :
   sig
    module TO :
     sig
      type t = Vertex.t
     end

    module IsTO :
     sig
      
     end

    module OrderTac :
     sig
      
     end

    val eq_dec : Vertex.t -> Vertex.t -> bool

    val lt_dec : Vertex.t -> Vertex.t -> bool

    val eqb : Vertex.t -> Vertex.t -> bool
   end

  type t = Vertex.t * Vertex.t

  val compare : t -> t -> (Vertex.t * Vertex.t) OrderedType.coq_Compare

  val eq_dec : t -> t -> bool

  module OTFacts :
   sig
    module TO :
     sig
      type t = Vertex.t
     end

    module IsTO :
     sig
      
     end

    module OrderTac :
     sig
      
     end

    val eq_dec : Vertex.t -> Vertex.t -> bool

    val lt_dec : Vertex.t -> Vertex.t -> bool

    val eqb : Vertex.t -> Vertex.t -> bool
   end

  module OTPairFacts :
   sig
    module TO :
     sig
      type t = Vertex.t * Vertex.t
     end

    module IsTO :
     sig
      
     end

    module OrderTac :
     sig
      
     end

    val eq_dec : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

    val lt_dec : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

    val eqb : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool
   end

  val fst_end : t -> Vertex.t

  val snd_end : t -> Vertex.t

  val permute : t -> Vertex.t * Vertex.t
 end
