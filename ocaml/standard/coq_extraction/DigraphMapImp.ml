open Datatypes
open Directed_edges
open FMapAVL
open MapFacts

type __ = Obj.t

module Directed_GraphMap =
 functor (Vertex:OrderedType.OrderedType) ->
 functor (Lab:Labels.Labels) ->
 struct
  module VertexMap = Make(Vertex)

  module Edge = Directed_Edge(Vertex)

  module EdgeMap = Make(Edge)

  module EMapFacts = MyMapFacts(EdgeMap)

  module VMapFacts = MyMapFacts(VertexMap)

  type elttype =
    Lab.coq_VertexLabel option * (Lab.coq_EdgeLabel option
    VertexMap.t * Lab.coq_EdgeLabel option VertexMap.t)

  type maptype = elttype VertexMap.t

  (** val adj_map : Vertex.t -> maptype -> elttype **)

  let adj_map x m =
    match VertexMap.find x m with
    | Some wmaps -> wmaps
    | None -> (None, (VertexMap.empty, VertexMap.empty))

  (** val succ :
      Vertex.t -> maptype -> Lab.coq_EdgeLabel option VertexMap.t **)

  let succ x m =
    fst (snd (adj_map x m))

  (** val pred :
      Vertex.t -> maptype -> Lab.coq_EdgeLabel option VertexMap.t **)

  let pred x m =
    snd (snd (adj_map x m))

  type tt =
    maptype
    (* singleton inductive, whose constructor was Make_Graph *)

  (** val map : tt -> maptype **)

  let map t0 =
    t0

  (** val successors :
      Vertex.t -> tt -> Lab.coq_EdgeLabel option VertexMap.t **)

  let successors v g =
    succ v (map g)

  (** val predecessors :
      Vertex.t -> tt -> Lab.coq_EdgeLabel option VertexMap.t **)

  let predecessors v g =
    pred v (map g)

  type t = tt

  (** val coq_V : tt -> Lab.coq_VertexLabel option VertexMap.t **)

  let coq_V g =
    VertexMap.map (fun wsp -> fst wsp) (map g)

  (** val vertex_label :
      VertexMap.key -> tt -> Lab.coq_VertexLabel option option **)

  let vertex_label v g =
    match VertexMap.find v (map g) with
    | Some wsp -> Some (fst wsp)
    | None -> None

  (** val add_neighborhood :
      Vertex.t -> elttype -> Lab.coq_EdgeLabel option EdgeMap.t ->
      Lab.coq_EdgeLabel option EdgeMap.t **)

  let add_neighborhood x wsp s =
    let (_, p) = wsp in
    let (su, _) = p in
    VertexMap.fold (fun y v s' -> EdgeMap.add (x, y) v s') su s

  (** val coq_E : t -> Lab.coq_EdgeLabel option EdgeMap.t **)

  let coq_E g =
    VertexMap.fold (fun x wsp s -> add_neighborhood x wsp s) (map g)
      EdgeMap.empty

  (** val edge_label : Edge.t -> tt -> Lab.coq_EdgeLabel option option **)

  let edge_label e g =
    VertexMap.find (Edge.snd_end e) (successors (Edge.fst_end e) g)

  (** val link :
      tt -> Vertex.t -> VertexMap.key -> Lab.coq_EdgeLabel option option **)

  let link g v1 v2 =
    VertexMap.find v2 (successors v1 g)

  (** val fold_vertices :
      (tt -> VertexMap.key -> Lab.coq_VertexLabel option -> 'a1 -> 'a1) -> tt
      -> 'a1 -> 'a1 **)

  let fold_vertices f g a =
    VertexMap.fold (f g) (coq_V g) a

  (** val fold_edges :
      (t -> EdgeMap.key -> Lab.coq_EdgeLabel option -> 'a1 -> 'a1) -> t ->
      'a1 -> 'a1 **)

  let fold_edges f g a =
    EdgeMap.fold (f g) (coq_E g) a

  (** val fold_succ :
      (tt -> Vertex.t -> VertexMap.key -> Lab.coq_EdgeLabel option -> 'a1 ->
      'a1) -> Vertex.t -> tt -> 'a1 -> 'a1 **)

  let fold_succ f v g a =
    VertexMap.fold (f g v) (successors v g) a

  (** val fold_pred :
      (tt -> Vertex.t -> VertexMap.key -> Lab.coq_EdgeLabel option -> 'a1 ->
      'a1) -> Vertex.t -> tt -> 'a1 -> 'a1 **)

  let fold_pred f v g a =
    VertexMap.fold (f g v) (predecessors v g) a

  (** val fold_succ_ne :
      (tt -> VertexMap.key -> VertexMap.key -> Lab.coq_EdgeLabel option ->
      'a1 -> 'a1) -> VertexMap.key -> tt -> 'a1 -> 'a1 **)

  let fold_succ_ne f v g a =
    VertexMap.fold (f g v) (VertexMap.remove v (successors v g)) a

  (** val fold_pred_ne :
      (tt -> VertexMap.key -> VertexMap.key -> Lab.coq_EdgeLabel option ->
      'a1 -> 'a1) -> VertexMap.key -> tt -> 'a1 -> 'a1 **)

  let fold_pred_ne f v g a =
    VertexMap.fold (f g v) (VertexMap.remove v (predecessors v g)) a

  (** val empty_map : maptype **)

  let empty_map =
    VertexMap.empty

  (** val empty_graph : tt **)

  let empty_graph =
    empty_map

  (** val add_vertex_map :
      maptype -> VertexMap.key -> Lab.coq_VertexLabel option -> maptype **)

  let add_vertex_map m x w =
    match VertexMap.find x m with
    | Some _ -> m
    | None -> VertexMap.add x (w, (VertexMap.empty, VertexMap.empty)) m

  (** val add_vertex :
      VertexMap.key -> tt -> Lab.coq_VertexLabel option -> tt **)

  let add_vertex v g w =
    add_vertex_map (map g) v w

  (** val remove_neighbor :
      VertexMap.key -> elttype -> Lab.coq_VertexLabel
      option * (Lab.coq_EdgeLabel option VertexMap.t * Lab.coq_EdgeLabel
      option VertexMap.t) **)

  let remove_neighbor x = function
  | (w, sp) ->
    let (s, p) = sp in (w, ((VertexMap.remove x s), (VertexMap.remove x p)))

  (** val remove_map : maptype -> VertexMap.key -> maptype **)

  let remove_map m x =
    VertexMap.map (remove_neighbor x) (VertexMap.remove x m)

  (** val remove_vertex : VertexMap.key -> tt -> tt **)

  let remove_vertex v g =
    remove_map (map g) v

  (** val add_edge_map :
      Edge.t -> maptype -> Lab.coq_EdgeLabel option -> (Lab.coq_VertexLabel
      option * (Lab.coq_EdgeLabel option VertexMap.t * Lab.coq_EdgeLabel
      option VertexMap.t)) VertexMap.t **)

  let add_edge_map e map0 w =
    let v1 = Edge.fst_end e in
    let v2 = Edge.snd_end e in
    (match VertexMap.find v1 map0 with
     | Some wsp1 ->
       if Vertex.eq_dec v1 v2
       then let (w1, sp1) = wsp1 in
            let (s1, p1) = sp1 in
            let succ1 = VertexMap.add v2 w s1 in
            let pred1 = VertexMap.add v1 w p1 in
            VertexMap.add v1 (w1, (succ1, pred1)) map0
       else (match VertexMap.find v2 map0 with
             | Some wsp2 ->
               let (w1, sp1) = wsp1 in
               let (s1, p1) = sp1 in
               let (w2, sp2) = wsp2 in
               let (s2, p2) = sp2 in
               let succ1 = VertexMap.add v2 w s1 in
               let pred2 = VertexMap.add v1 w p2 in
               VertexMap.add v2 (w2, (s2, pred2))
                 (VertexMap.add v1 (w1, (succ1, p1)) map0)
             | None -> map0)
     | None -> map0)

  (** val coq_In_ext_dec : Edge.t -> tt -> bool **)

  let coq_In_ext_dec e g =
    let s = VMapFacts.MapFacts.coq_In_dec (map g) (Edge.fst_end e) in
    if s
    then VMapFacts.MapFacts.coq_In_dec (map g) (Edge.snd_end e)
    else false

  (** val add_edge : Edge.t -> tt -> Lab.coq_EdgeLabel option -> tt **)

  let add_edge e g w =
    add_edge_map e (map g) w

  (** val add_edge_w : Edge.t -> tt -> Lab.coq_EdgeLabel option -> tt **)

  let add_edge_w e g w =
    match VertexMap.find (Edge.fst_end e) (map g) with
    | Some _ ->
      (match VertexMap.find (Edge.snd_end e) (map g) with
       | Some _ -> add_edge e g w
       | None -> add_edge e (add_vertex (Edge.snd_end e) g None) w)
    | None ->
      (match VertexMap.find (Edge.snd_end e) (map g) with
       | Some _ -> add_edge e (add_vertex (Edge.fst_end e) g None) w
       | None ->
         add_edge e
           (add_vertex (Edge.fst_end e) (add_vertex (Edge.snd_end e) g None)
             None) w)

  (** val remove_edge_map :
      Edge.t -> maptype -> (Lab.coq_VertexLabel option * (Lab.coq_EdgeLabel
      option VertexMap.t * Lab.coq_EdgeLabel option VertexMap.t)) VertexMap.t **)

  let remove_edge_map e map0 =
    let v1 = Edge.fst_end e in
    let v2 = Edge.snd_end e in
    (match VertexMap.find v1 map0 with
     | Some wsp1 ->
       if Vertex.eq_dec v1 v2
       then let (w1, sp1) = wsp1 in
            let (s1, p1) = sp1 in
            let succ1 = VertexMap.remove v2 s1 in
            let pred1 = VertexMap.remove v1 p1 in
            VertexMap.add v1 (w1, (succ1, pred1)) map0
       else (match VertexMap.find v2 map0 with
             | Some wsp2 ->
               let (w1, sp1) = wsp1 in
               let (s1, p1) = sp1 in
               let (w2, sp2) = wsp2 in
               let (s2, p2) = sp2 in
               let succ1 = VertexMap.remove v2 s1 in
               let pred2 = VertexMap.remove v1 p2 in
               VertexMap.add v2 (w2, (s2, pred2))
                 (VertexMap.add v1 (w1, (succ1, p1)) map0)
             | None -> map0)
     | None -> map0)

  (** val remove_edge : Edge.t -> tt -> tt **)

  let remove_edge e g =
    remove_edge_map e (map g)
 end
