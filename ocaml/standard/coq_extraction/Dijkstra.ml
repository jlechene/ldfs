open Common
open Compare_dec
open Datatypes
open FSetAVL
open FSetFacts
open FSetProperties
open Nat0
open OrderedTypeEx
open Paths

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

module Shortest_path =
 functor (Vertex:OrderedType.OrderedType) ->
 functor (Lab:Labels.Labels_with_weight) ->
 functor (G:sig
  module VertexMap :
   sig
    module E :
     sig
      type t = Vertex.t

      val compare : t -> t -> t OrderedType.coq_Compare

      val eq_dec : t -> t -> bool
     end

    module Raw :
     sig
      type key = Vertex.t

      type 'elt tree =
      | Leaf
      | Node of 'elt tree * key * 'elt * 'elt tree * Int.Z_as_Int.t

      val tree_rect :
        'a2 -> ('a1 tree -> 'a2 -> key -> 'a1 -> 'a1 tree -> 'a2 ->
        Int.Z_as_Int.t -> 'a2) -> 'a1 tree -> 'a2

      val tree_rec :
        'a2 -> ('a1 tree -> 'a2 -> key -> 'a1 -> 'a1 tree -> 'a2 ->
        Int.Z_as_Int.t -> 'a2) -> 'a1 tree -> 'a2

      val height : 'a1 tree -> Int.Z_as_Int.t

      val cardinal : 'a1 tree -> int

      val empty : 'a1 tree

      val is_empty : 'a1 tree -> bool

      val mem : Vertex.t -> 'a1 tree -> bool

      val find : Vertex.t -> 'a1 tree -> 'a1 option

      val create : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val assert_false : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val bal : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val add : key -> 'a1 -> 'a1 tree -> 'a1 tree

      val remove_min :
        'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree * (key * 'a1)

      val merge : 'a1 tree -> 'a1 tree -> 'a1 tree

      val remove : Vertex.t -> 'a1 tree -> 'a1 tree

      val join : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      type 'elt triple = { t_left : 'elt tree; t_opt : 'elt option;
                           t_right : 'elt tree }

      val t_left : 'a1 triple -> 'a1 tree

      val t_opt : 'a1 triple -> 'a1 option

      val t_right : 'a1 triple -> 'a1 tree

      val split : Vertex.t -> 'a1 tree -> 'a1 triple

      val concat : 'a1 tree -> 'a1 tree -> 'a1 tree

      val elements_aux : (key * 'a1) list -> 'a1 tree -> (key * 'a1) list

      val elements : 'a1 tree -> (key * 'a1) list

      val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 tree -> 'a2 -> 'a2

      type 'elt enumeration =
      | End
      | More of key * 'elt * 'elt tree * 'elt enumeration

      val enumeration_rect :
        'a2 -> (key -> 'a1 -> 'a1 tree -> 'a1 enumeration -> 'a2 -> 'a2) ->
        'a1 enumeration -> 'a2

      val enumeration_rec :
        'a2 -> (key -> 'a1 -> 'a1 tree -> 'a1 enumeration -> 'a2 -> 'a2) ->
        'a1 enumeration -> 'a2

      val cons : 'a1 tree -> 'a1 enumeration -> 'a1 enumeration

      val equal_more :
        ('a1 -> 'a1 -> bool) -> Vertex.t -> 'a1 -> ('a1 enumeration -> bool)
        -> 'a1 enumeration -> bool

      val equal_cont :
        ('a1 -> 'a1 -> bool) -> 'a1 tree -> ('a1 enumeration -> bool) -> 'a1
        enumeration -> bool

      val equal_end : 'a1 enumeration -> bool

      val equal : ('a1 -> 'a1 -> bool) -> 'a1 tree -> 'a1 tree -> bool

      val map : ('a1 -> 'a2) -> 'a1 tree -> 'a2 tree

      val mapi : (key -> 'a1 -> 'a2) -> 'a1 tree -> 'a2 tree

      val map_option : (key -> 'a1 -> 'a2 option) -> 'a1 tree -> 'a2 tree

      val map2_opt :
        (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree) ->
        ('a2 tree -> 'a3 tree) -> 'a1 tree -> 'a2 tree -> 'a3 tree

      val map2 :
        ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 tree -> 'a2 tree ->
        'a3 tree

      module Proofs :
       sig
        module MX :
         sig
          module TO :
           sig
            type t = Vertex.t
           end

          module IsTO :
           sig
            
           end

          module OrderTac :
           sig
            
           end

          val eq_dec : Vertex.t -> Vertex.t -> bool

          val lt_dec : Vertex.t -> Vertex.t -> bool

          val eqb : Vertex.t -> Vertex.t -> bool
         end

        module PX :
         sig
          module MO :
           sig
            module TO :
             sig
              type t = Vertex.t
             end

            module IsTO :
             sig
              
             end

            module OrderTac :
             sig
              
             end

            val eq_dec : Vertex.t -> Vertex.t -> bool

            val lt_dec : Vertex.t -> Vertex.t -> bool

            val eqb : Vertex.t -> Vertex.t -> bool
           end
         end

        module L :
         sig
          module MX :
           sig
            module TO :
             sig
              type t = Vertex.t
             end

            module IsTO :
             sig
              
             end

            module OrderTac :
             sig
              
             end

            val eq_dec : Vertex.t -> Vertex.t -> bool

            val lt_dec : Vertex.t -> Vertex.t -> bool

            val eqb : Vertex.t -> Vertex.t -> bool
           end

          module PX :
           sig
            module MO :
             sig
              module TO :
               sig
                type t = Vertex.t
               end

              module IsTO :
               sig
                
               end

              module OrderTac :
               sig
                
               end

              val eq_dec : Vertex.t -> Vertex.t -> bool

              val lt_dec : Vertex.t -> Vertex.t -> bool

              val eqb : Vertex.t -> Vertex.t -> bool
             end
           end

          type key = Vertex.t

          type 'elt t = (Vertex.t * 'elt) list

          val empty : 'a1 t

          val is_empty : 'a1 t -> bool

          val mem : key -> 'a1 t -> bool

          type 'elt coq_R_mem =
          | R_mem_0 of 'elt t
          | R_mem_1 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
          | R_mem_2 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
          | R_mem_3 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
             * bool * 'elt coq_R_mem

          val coq_R_mem_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> 'a1 t ->
            bool -> 'a1 coq_R_mem -> 'a2

          val coq_R_mem_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> 'a1 t ->
            bool -> 'a1 coq_R_mem -> 'a2

          val mem_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val mem_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val coq_R_mem_correct : key -> 'a1 t -> bool -> 'a1 coq_R_mem

          val find : key -> 'a1 t -> 'a1 option

          type 'elt coq_R_find =
          | R_find_0 of 'elt t
          | R_find_1 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
          | R_find_2 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
          | R_find_3 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
             * 'elt option * 'elt coq_R_find

          val coq_R_find_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a1 option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> 'a1
            t -> 'a1 option -> 'a1 coq_R_find -> 'a2

          val coq_R_find_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a1 option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> 'a1
            t -> 'a1 option -> 'a1 coq_R_find -> 'a2

          val find_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val find_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val coq_R_find_correct :
            key -> 'a1 t -> 'a1 option -> 'a1 coq_R_find

          val add : key -> 'a1 -> 'a1 t -> 'a1 t

          type 'elt coq_R_add =
          | R_add_0 of 'elt t
          | R_add_1 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
          | R_add_2 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
          | R_add_3 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
             * 'elt t * 'elt coq_R_add

          val coq_R_add_rect :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1
            -> (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a1 t -> 'a1 coq_R_add -> 'a2 -> 'a2) -> 'a1 t ->
            'a1 t -> 'a1 coq_R_add -> 'a2

          val coq_R_add_rec :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1
            -> (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a1 t -> 'a1 coq_R_add -> 'a2 -> 'a2) -> 'a1 t ->
            'a1 t -> 'a1 coq_R_add -> 'a2

          val add_rect :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1
            -> (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val add_rec :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1
            -> (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val coq_R_add_correct :
            key -> 'a1 -> 'a1 t -> 'a1 t -> 'a1 coq_R_add

          val remove : key -> 'a1 t -> 'a1 t

          type 'elt coq_R_remove =
          | R_remove_0 of 'elt t
          | R_remove_1 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
          | R_remove_2 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
          | R_remove_3 of 'elt t * Vertex.t * 'elt * (Vertex.t * 'elt) list
             * 'elt t * 'elt coq_R_remove

          val coq_R_remove_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a1 t -> 'a1 coq_R_remove -> 'a2 -> 'a2) -> 'a1 t
            -> 'a1 t -> 'a1 coq_R_remove -> 'a2

          val coq_R_remove_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a1 t -> 'a1 coq_R_remove -> 'a2 -> 'a2) -> 'a1 t
            -> 'a1 t -> 'a1 coq_R_remove -> 'a2

          val remove_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val remove_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val coq_R_remove_correct : key -> 'a1 t -> 'a1 t -> 'a1 coq_R_remove

          val elements : 'a1 t -> 'a1 t

          val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2

          type ('elt, 'a) coq_R_fold =
          | R_fold_0 of (key -> 'elt -> 'a -> 'a) * 'elt t * 'a
          | R_fold_1 of (key -> 'elt -> 'a -> 'a) * 'elt t * 'a * Vertex.t
             * 'elt * (Vertex.t * 'elt) list * 'a * ('elt, 'a) coq_R_fold

          val coq_R_fold_rect :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> Vertex.t -> 'a1
            -> (Vertex.t * 'a1) list -> __ -> __ -> ('a1, __) coq_R_fold ->
            'a2 -> 'a2) -> (key -> 'a1 -> 'a3 -> 'a3) -> 'a1 t -> 'a3 -> 'a3
            -> ('a1, 'a3) coq_R_fold -> 'a2

          val coq_R_fold_rec :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> Vertex.t -> 'a1
            -> (Vertex.t * 'a1) list -> __ -> __ -> ('a1, __) coq_R_fold ->
            'a2 -> 'a2) -> (key -> 'a1 -> 'a3 -> 'a3) -> 'a1 t -> 'a3 -> 'a3
            -> ('a1, 'a3) coq_R_fold -> 'a2

          val fold_rect :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> Vertex.t -> 'a1
            -> (Vertex.t * 'a1) list -> __ -> 'a2 -> 'a2) -> (key -> 'a1 ->
            'a3 -> 'a3) -> 'a1 t -> 'a3 -> 'a2

          val fold_rec :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> Vertex.t -> 'a1
            -> (Vertex.t * 'a1) list -> __ -> 'a2 -> 'a2) -> (key -> 'a1 ->
            'a3 -> 'a3) -> 'a1 t -> 'a3 -> 'a2

          val coq_R_fold_correct :
            (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2 -> ('a1, 'a2)
            coq_R_fold

          val equal : ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool

          type 'elt coq_R_equal =
          | R_equal_0 of 'elt t * 'elt t
          | R_equal_1 of 'elt t * 'elt t * Vertex.t * 'elt
             * (Vertex.t * 'elt) list * Vertex.t * 'elt
             * (Vertex.t * 'elt) list * bool * 'elt coq_R_equal
          | R_equal_2 of 'elt t * 'elt t * Vertex.t * 'elt
             * (Vertex.t * 'elt) list * Vertex.t * 'elt
             * (Vertex.t * 'elt) list * Vertex.t OrderedType.coq_Compare
          | R_equal_3 of 'elt t * 'elt t * 'elt t * 'elt t

          val coq_R_equal_rect :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            bool -> 'a1 coq_R_equal -> 'a2 -> 'a2) -> ('a1 t -> 'a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> Vertex.t -> 'a1
            -> (Vertex.t * 'a1) list -> __ -> Vertex.t
            OrderedType.coq_Compare -> __ -> __ -> 'a2) -> ('a1 t -> 'a1 t ->
            'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) -> 'a1 t -> 'a1 t ->
            bool -> 'a1 coq_R_equal -> 'a2

          val coq_R_equal_rec :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            bool -> 'a1 coq_R_equal -> 'a2 -> 'a2) -> ('a1 t -> 'a1 t ->
            Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> Vertex.t -> 'a1
            -> (Vertex.t * 'a1) list -> __ -> Vertex.t
            OrderedType.coq_Compare -> __ -> __ -> 'a2) -> ('a1 t -> 'a1 t ->
            'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) -> 'a1 t -> 'a1 t ->
            bool -> 'a1 coq_R_equal -> 'a2

          val equal_rect :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2 -> 'a2) -> ('a1 t -> 'a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> Vertex.t OrderedType.coq_Compare
            -> __ -> __ -> 'a2) -> ('a1 t -> 'a1 t -> 'a1 t -> __ -> 'a1 t ->
            __ -> __ -> 'a2) -> 'a1 t -> 'a1 t -> 'a2

          val equal_rec :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __
            -> Vertex.t -> 'a1 -> (Vertex.t * 'a1) list -> __ -> __ -> __ ->
            'a2 -> 'a2) -> ('a1 t -> 'a1 t -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> Vertex.t -> 'a1 ->
            (Vertex.t * 'a1) list -> __ -> Vertex.t OrderedType.coq_Compare
            -> __ -> __ -> 'a2) -> ('a1 t -> 'a1 t -> 'a1 t -> __ -> 'a1 t ->
            __ -> __ -> 'a2) -> 'a1 t -> 'a1 t -> 'a2

          val coq_R_equal_correct :
            ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool -> 'a1 coq_R_equal

          val map : ('a1 -> 'a2) -> 'a1 t -> 'a2 t

          val mapi : (key -> 'a1 -> 'a2) -> 'a1 t -> 'a2 t

          val option_cons :
            key -> 'a1 option -> (key * 'a1) list -> (key * 'a1) list

          val map2_l :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a3 t

          val map2_r :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a2 t -> 'a3 t

          val map2 :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t -> 'a3
            t

          val combine : 'a1 t -> 'a2 t -> ('a1 option * 'a2 option) t

          val fold_right_pair :
            ('a1 -> 'a2 -> 'a3 -> 'a3) -> ('a1 * 'a2) list -> 'a3 -> 'a3

          val map2_alt :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t ->
            (key * 'a3) list

          val at_least_one :
            'a1 option -> 'a2 option -> ('a1 option * 'a2 option) option

          val at_least_one_then_f :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 option -> 'a2
            option -> 'a3 option
         end

        type 'elt coq_R_mem =
        | R_mem_0 of 'elt tree
        | R_mem_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * bool * 'elt coq_R_mem
        | R_mem_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_mem_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * bool * 'elt coq_R_mem

        val coq_R_mem_rect :
          Vertex.t -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> bool ->
          'a1 coq_R_mem -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1
          -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1
          tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> 'a1 tree ->
          bool -> 'a1 coq_R_mem -> 'a2

        val coq_R_mem_rec :
          Vertex.t -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> bool ->
          'a1 coq_R_mem -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1
          -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1
          tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> 'a1 tree ->
          bool -> 'a1 coq_R_mem -> 'a2

        type 'elt coq_R_find =
        | R_find_0 of 'elt tree
        | R_find_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt option * 'elt coq_R_find
        | R_find_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_find_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt option * 'elt coq_R_find

        val coq_R_find_rect :
          Vertex.t -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 option -> 'a1 coq_R_find ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 option -> 'a1 coq_R_find -> 'a2

        val coq_R_find_rec :
          Vertex.t -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 option -> 'a1 coq_R_find ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 option -> 'a1 coq_R_find -> 'a2

        type 'elt coq_R_bal =
        | R_bal_0 of 'elt tree * key * 'elt * 'elt tree
        | R_bal_1 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_2 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_3 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 
           'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_4 of 'elt tree * key * 'elt * 'elt tree
        | R_bal_5 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_6 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_7 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 
           'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_8 of 'elt tree * key * 'elt * 'elt tree

        val coq_R_bal_rect :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> __ ->
          'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2)
          -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __
          -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ ->
          __ -> __ -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> __ -> 'a2) -> ('a1 tree -> key
          -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2) -> ('a1 tree
          -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a2) -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_bal -> 'a2

        val coq_R_bal_rec :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> __ ->
          'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2)
          -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __
          -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ ->
          __ -> __ -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> __ -> 'a2) -> ('a1 tree -> key
          -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2) -> ('a1 tree
          -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a2) -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_bal -> 'a2

        type 'elt coq_R_add =
        | R_add_0 of 'elt tree
        | R_add_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_add
        | R_add_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_add_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_add

        val coq_R_add_rect :
          key -> 'a1 -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          tree -> 'a1 coq_R_add -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_add ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_add -> 'a2

        val coq_R_add_rec :
          key -> 'a1 -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          tree -> 'a1 coq_R_add -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_add ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_add -> 'a2

        type 'elt coq_R_remove_min =
        | R_remove_min_0 of 'elt tree * key * 'elt * 'elt tree
        | R_remove_min_1 of 'elt tree * key * 'elt * 'elt tree * 'elt tree
           * key * 'elt * 'elt tree * Int.Z_as_Int.t
           * ('elt tree * (key * 'elt)) * 'elt coq_R_remove_min * 'elt tree
           * (key * 'elt)

        val coq_R_remove_min_rect :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> 'a2) -> ('a1 tree ->
          key -> 'a1 -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> ('a1 tree * (key * 'a1)) -> 'a1
          coq_R_remove_min -> 'a2 -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> ('a1 tree * (key * 'a1)) ->
          'a1 coq_R_remove_min -> 'a2

        val coq_R_remove_min_rec :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> 'a2) -> ('a1 tree ->
          key -> 'a1 -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> ('a1 tree * (key * 'a1)) -> 'a1
          coq_R_remove_min -> 'a2 -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> ('a1 tree * (key * 'a1)) ->
          'a1 coq_R_remove_min -> 'a2

        type 'elt coq_R_merge =
        | R_merge_0 of 'elt tree * 'elt tree
        | R_merge_1 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_merge_2 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * (key * 'elt) * key * 'elt

        val coq_R_merge_rect :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> key -> 'a1
          -> __ -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1
          coq_R_merge -> 'a2

        val coq_R_merge_rec :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> key -> 'a1
          -> __ -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1
          coq_R_merge -> 'a2

        type 'elt coq_R_remove =
        | R_remove_0 of 'elt tree
        | R_remove_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_remove
        | R_remove_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_remove_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_remove

        val coq_R_remove_rect :
          Vertex.t -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree
          -> 'a1 coq_R_remove -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t
          -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_remove -> 'a2 -> 'a2) ->
          'a1 tree -> 'a1 tree -> 'a1 coq_R_remove -> 'a2

        val coq_R_remove_rec :
          Vertex.t -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree
          -> 'a1 coq_R_remove -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t
          -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_remove -> 'a2 -> 'a2) ->
          'a1 tree -> 'a1 tree -> 'a1 coq_R_remove -> 'a2

        type 'elt coq_R_concat =
        | R_concat_0 of 'elt tree * 'elt tree
        | R_concat_1 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_concat_2 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * (key * 'elt)

        val coq_R_concat_rect :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_concat -> 'a2

        val coq_R_concat_rec :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_concat -> 'a2

        type 'elt coq_R_split =
        | R_split_0 of 'elt tree
        | R_split_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt triple * 'elt coq_R_split * 'elt tree
           * 'elt option * 'elt tree
        | R_split_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_split_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt triple * 'elt coq_R_split * 'elt tree
           * 'elt option * 'elt tree

        val coq_R_split_rect :
          Vertex.t -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1 option -> 'a1
          tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1
          tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a1 triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1
          option -> 'a1 tree -> __ -> 'a2) -> 'a1 tree -> 'a1 triple -> 'a1
          coq_R_split -> 'a2

        val coq_R_split_rec :
          Vertex.t -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1 option -> 'a1
          tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1
          tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a1 triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1
          option -> 'a1 tree -> __ -> 'a2) -> 'a1 tree -> 'a1 triple -> 'a1
          coq_R_split -> 'a2

        type ('elt, 'x) coq_R_map_option =
        | R_map_option_0 of 'elt tree
        | R_map_option_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'x * 'x tree * ('elt, 'x) coq_R_map_option
           * 'x tree * ('elt, 'x) coq_R_map_option
        | R_map_option_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'x tree * ('elt, 'x) coq_R_map_option * 
           'x tree * ('elt, 'x) coq_R_map_option

        val coq_R_map_option_rect :
          (key -> 'a1 -> 'a2 option) -> ('a1 tree -> __ -> 'a3) -> ('a1 tree
          -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ ->
          'a2 -> __ -> 'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2
          tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a3) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2 tree -> ('a1,
          'a2) coq_R_map_option -> 'a3 -> 'a3) -> 'a1 tree -> 'a2 tree ->
          ('a1, 'a2) coq_R_map_option -> 'a3

        val coq_R_map_option_rec :
          (key -> 'a1 -> 'a2 option) -> ('a1 tree -> __ -> 'a3) -> ('a1 tree
          -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ ->
          'a2 -> __ -> 'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2
          tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a3) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2 tree -> ('a1,
          'a2) coq_R_map_option -> 'a3 -> 'a3) -> 'a1 tree -> 'a2 tree ->
          ('a1, 'a2) coq_R_map_option -> 'a3

        type ('elt, 'x0, 'x) coq_R_map2_opt =
        | R_map2_opt_0 of 'elt tree * 'x0 tree
        | R_map2_opt_1 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_map2_opt_2 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'x0 tree * key * 'x0 * 'x0 tree
           * Int.Z_as_Int.t * 'x0 tree * 'x0 option * 'x0 tree * 'x * 
           'x tree * ('elt, 'x0, 'x) coq_R_map2_opt * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt
        | R_map2_opt_3 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'x0 tree * key * 'x0 * 'x0 tree
           * Int.Z_as_Int.t * 'x0 tree * 'x0 option * 'x0 tree * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt

        val coq_R_map2_opt_rect :
          (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree)
          -> ('a2 tree -> 'a3 tree) -> ('a1 tree -> 'a2 tree -> __ -> 'a4) ->
          ('a1 tree -> 'a2 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree
          -> key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          'a2 option -> 'a2 tree -> __ -> 'a3 -> __ -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree -> 'a2
          option -> 'a2 tree -> __ -> __ -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3) coq_R_map2_opt
          -> 'a4 -> 'a4) -> 'a1 tree -> 'a2 tree -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4

        val coq_R_map2_opt_rec :
          (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree)
          -> ('a2 tree -> 'a3 tree) -> ('a1 tree -> 'a2 tree -> __ -> 'a4) ->
          ('a1 tree -> 'a2 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree
          -> key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          'a2 option -> 'a2 tree -> __ -> 'a3 -> __ -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree -> 'a2
          option -> 'a2 tree -> __ -> __ -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3) coq_R_map2_opt
          -> 'a4 -> 'a4) -> 'a1 tree -> 'a2 tree -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4

        val fold' : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 tree -> 'a2 -> 'a2

        val flatten_e : 'a1 enumeration -> (key * 'a1) list
       end
     end

    type 'elt bst =
      'elt Raw.tree
      (* singleton inductive, whose constructor was Bst *)

    val this : 'a1 bst -> 'a1 Raw.tree

    type 'elt t = 'elt bst

    type key = E.t

    val empty : 'a1 t

    val is_empty : 'a1 t -> bool

    val add : key -> 'a1 -> 'a1 t -> 'a1 t

    val remove : key -> 'a1 t -> 'a1 t

    val mem : key -> 'a1 t -> bool

    val find : key -> 'a1 t -> 'a1 option

    val map : ('a1 -> 'a2) -> 'a1 t -> 'a2 t

    val mapi : (key -> 'a1 -> 'a2) -> 'a1 t -> 'a2 t

    val map2 :
      ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t -> 'a3 t

    val elements : 'a1 t -> (key * 'a1) list

    val cardinal : 'a1 t -> int

    val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2

    val equal : ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool
   end

  module VMapFacts :
   sig
    module MapFacts :
     sig
      val eqb : Vertex.t -> Vertex.t -> bool

      val coq_In_dec : 'a1 VertexMap.t -> VertexMap.key -> bool
     end

    val eqb : Vertex.t -> Vertex.t -> bool

    val coq_In_dec : 'a1 VertexMap.t -> VertexMap.key -> bool
   end

  module Edge :
   sig
    module VertexPair :
     sig
      module MO1 :
       sig
        module TO :
         sig
          type t = Vertex.t
         end

        module IsTO :
         sig
          
         end

        module OrderTac :
         sig
          
         end

        val eq_dec : Vertex.t -> Vertex.t -> bool

        val lt_dec : Vertex.t -> Vertex.t -> bool

        val eqb : Vertex.t -> Vertex.t -> bool
       end

      module MO2 :
       sig
        module TO :
         sig
          type t = Vertex.t
         end

        module IsTO :
         sig
          
         end

        module OrderTac :
         sig
          
         end

        val eq_dec : Vertex.t -> Vertex.t -> bool

        val lt_dec : Vertex.t -> Vertex.t -> bool

        val eqb : Vertex.t -> Vertex.t -> bool
       end

      type t = Vertex.t * Vertex.t

      val compare : t -> t -> (Vertex.t * Vertex.t) OrderedType.coq_Compare

      val eq_dec : t -> t -> bool
     end

    module MO1 :
     sig
      module TO :
       sig
        type t = Vertex.t
       end

      module IsTO :
       sig
        
       end

      module OrderTac :
       sig
        
       end

      val eq_dec : Vertex.t -> Vertex.t -> bool

      val lt_dec : Vertex.t -> Vertex.t -> bool

      val eqb : Vertex.t -> Vertex.t -> bool
     end

    module MO2 :
     sig
      module TO :
       sig
        type t = Vertex.t
       end

      module IsTO :
       sig
        
       end

      module OrderTac :
       sig
        
       end

      val eq_dec : Vertex.t -> Vertex.t -> bool

      val lt_dec : Vertex.t -> Vertex.t -> bool

      val eqb : Vertex.t -> Vertex.t -> bool
     end

    type t = Vertex.t * Vertex.t

    val compare : t -> t -> (Vertex.t * Vertex.t) OrderedType.coq_Compare

    val eq_dec : t -> t -> bool

    module OTFacts :
     sig
      module TO :
       sig
        type t = Vertex.t
       end

      module IsTO :
       sig
        
       end

      module OrderTac :
       sig
        
       end

      val eq_dec : Vertex.t -> Vertex.t -> bool

      val lt_dec : Vertex.t -> Vertex.t -> bool

      val eqb : Vertex.t -> Vertex.t -> bool
     end

    module OTPairFacts :
     sig
      module TO :
       sig
        type t = Vertex.t * Vertex.t
       end

      module IsTO :
       sig
        
       end

      module OrderTac :
       sig
        
       end

      val eq_dec : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

      val lt_dec : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

      val eqb : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool
     end

    val fst_end : t -> Vertex.t

    val snd_end : t -> Vertex.t

    val permute : t -> Vertex.t * Vertex.t
   end

  module EdgeMap :
   sig
    module E :
     sig
      type t = Vertex.t * Vertex.t

      val compare :
        (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) ->
        (Vertex.t * Vertex.t) OrderedType.coq_Compare

      val eq_dec : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool
     end

    module Raw :
     sig
      type key = Vertex.t * Vertex.t

      type 'elt tree =
      | Leaf
      | Node of 'elt tree * key * 'elt * 'elt tree * Int.Z_as_Int.t

      val tree_rect :
        'a2 -> ('a1 tree -> 'a2 -> key -> 'a1 -> 'a1 tree -> 'a2 ->
        Int.Z_as_Int.t -> 'a2) -> 'a1 tree -> 'a2

      val tree_rec :
        'a2 -> ('a1 tree -> 'a2 -> key -> 'a1 -> 'a1 tree -> 'a2 ->
        Int.Z_as_Int.t -> 'a2) -> 'a1 tree -> 'a2

      val height : 'a1 tree -> Int.Z_as_Int.t

      val cardinal : 'a1 tree -> int

      val empty : 'a1 tree

      val is_empty : 'a1 tree -> bool

      val mem : (Vertex.t * Vertex.t) -> 'a1 tree -> bool

      val find : (Vertex.t * Vertex.t) -> 'a1 tree -> 'a1 option

      val create : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val assert_false : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val bal : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val add : key -> 'a1 -> 'a1 tree -> 'a1 tree

      val remove_min :
        'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree * (key * 'a1)

      val merge : 'a1 tree -> 'a1 tree -> 'a1 tree

      val remove : (Vertex.t * Vertex.t) -> 'a1 tree -> 'a1 tree

      val join : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      type 'elt triple = { t_left : 'elt tree; t_opt : 'elt option;
                           t_right : 'elt tree }

      val t_left : 'a1 triple -> 'a1 tree

      val t_opt : 'a1 triple -> 'a1 option

      val t_right : 'a1 triple -> 'a1 tree

      val split : (Vertex.t * Vertex.t) -> 'a1 tree -> 'a1 triple

      val concat : 'a1 tree -> 'a1 tree -> 'a1 tree

      val elements_aux : (key * 'a1) list -> 'a1 tree -> (key * 'a1) list

      val elements : 'a1 tree -> (key * 'a1) list

      val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 tree -> 'a2 -> 'a2

      type 'elt enumeration =
      | End
      | More of key * 'elt * 'elt tree * 'elt enumeration

      val enumeration_rect :
        'a2 -> (key -> 'a1 -> 'a1 tree -> 'a1 enumeration -> 'a2 -> 'a2) ->
        'a1 enumeration -> 'a2

      val enumeration_rec :
        'a2 -> (key -> 'a1 -> 'a1 tree -> 'a1 enumeration -> 'a2 -> 'a2) ->
        'a1 enumeration -> 'a2

      val cons : 'a1 tree -> 'a1 enumeration -> 'a1 enumeration

      val equal_more :
        ('a1 -> 'a1 -> bool) -> (Vertex.t * Vertex.t) -> 'a1 -> ('a1
        enumeration -> bool) -> 'a1 enumeration -> bool

      val equal_cont :
        ('a1 -> 'a1 -> bool) -> 'a1 tree -> ('a1 enumeration -> bool) -> 'a1
        enumeration -> bool

      val equal_end : 'a1 enumeration -> bool

      val equal : ('a1 -> 'a1 -> bool) -> 'a1 tree -> 'a1 tree -> bool

      val map : ('a1 -> 'a2) -> 'a1 tree -> 'a2 tree

      val mapi : (key -> 'a1 -> 'a2) -> 'a1 tree -> 'a2 tree

      val map_option : (key -> 'a1 -> 'a2 option) -> 'a1 tree -> 'a2 tree

      val map2_opt :
        (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree) ->
        ('a2 tree -> 'a3 tree) -> 'a1 tree -> 'a2 tree -> 'a3 tree

      val map2 :
        ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 tree -> 'a2 tree ->
        'a3 tree

      module Proofs :
       sig
        module MX :
         sig
          module TO :
           sig
            type t = Vertex.t * Vertex.t
           end

          module IsTO :
           sig
            
           end

          module OrderTac :
           sig
            
           end

          val eq_dec : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

          val lt_dec : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

          val eqb : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool
         end

        module PX :
         sig
          module MO :
           sig
            module TO :
             sig
              type t = Vertex.t * Vertex.t
             end

            module IsTO :
             sig
              
             end

            module OrderTac :
             sig
              
             end

            val eq_dec :
              (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

            val lt_dec :
              (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

            val eqb : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool
           end
         end

        module L :
         sig
          module MX :
           sig
            module TO :
             sig
              type t = Vertex.t * Vertex.t
             end

            module IsTO :
             sig
              
             end

            module OrderTac :
             sig
              
             end

            val eq_dec :
              (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

            val lt_dec :
              (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

            val eqb : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool
           end

          module PX :
           sig
            module MO :
             sig
              module TO :
               sig
                type t = Vertex.t * Vertex.t
               end

              module IsTO :
               sig
                
               end

              module OrderTac :
               sig
                
               end

              val eq_dec :
                (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

              val lt_dec :
                (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

              val eqb : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool
             end
           end

          type key = Vertex.t * Vertex.t

          type 'elt t = ((Vertex.t * Vertex.t) * 'elt) list

          val empty : 'a1 t

          val is_empty : 'a1 t -> bool

          val mem : key -> 'a1 t -> bool

          type 'elt coq_R_mem =
          | R_mem_0 of 'elt t
          | R_mem_1 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list
          | R_mem_2 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list
          | R_mem_3 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list * bool * 'elt coq_R_mem

          val coq_R_mem_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> bool ->
            'a1 coq_R_mem -> 'a2 -> 'a2) -> 'a1 t -> bool -> 'a1 coq_R_mem ->
            'a2

          val coq_R_mem_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> bool ->
            'a1 coq_R_mem -> 'a2 -> 'a2) -> 'a1 t -> bool -> 'a1 coq_R_mem ->
            'a2

          val mem_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2 ->
            'a2) -> 'a1 t -> 'a2

          val mem_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2 ->
            'a2) -> 'a1 t -> 'a2

          val coq_R_mem_correct : key -> 'a1 t -> bool -> 'a1 coq_R_mem

          val find : key -> 'a1 t -> 'a1 option

          type 'elt coq_R_find =
          | R_find_0 of 'elt t
          | R_find_1 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list
          | R_find_2 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list
          | R_find_3 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list * 'elt option
             * 'elt coq_R_find

          val coq_R_find_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a1
            option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> 'a1 t -> 'a1 option ->
            'a1 coq_R_find -> 'a2

          val coq_R_find_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a1
            option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> 'a1 t -> 'a1 option ->
            'a1 coq_R_find -> 'a2

          val find_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2 ->
            'a2) -> 'a1 t -> 'a2

          val find_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2 ->
            'a2) -> 'a1 t -> 'a2

          val coq_R_find_correct :
            key -> 'a1 t -> 'a1 option -> 'a1 coq_R_find

          val add : key -> 'a1 -> 'a1 t -> 'a1 t

          type 'elt coq_R_add =
          | R_add_0 of 'elt t
          | R_add_1 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list
          | R_add_2 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list
          | R_add_3 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list * 'elt t * 'elt coq_R_add

          val coq_R_add_rect :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t ->
            (Vertex.t * Vertex.t) -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1)
            list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t)
            -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a1 t ->
            'a1 coq_R_add -> 'a2 -> 'a2) -> 'a1 t -> 'a1 t -> 'a1 coq_R_add
            -> 'a2

          val coq_R_add_rec :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t ->
            (Vertex.t * Vertex.t) -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1)
            list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t)
            -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a1 t ->
            'a1 coq_R_add -> 'a2 -> 'a2) -> 'a1 t -> 'a1 t -> 'a1 coq_R_add
            -> 'a2

          val add_rect :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t ->
            (Vertex.t * Vertex.t) -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1)
            list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t)
            -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2 ->
            'a2) -> 'a1 t -> 'a2

          val add_rec :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t ->
            (Vertex.t * Vertex.t) -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1)
            list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t)
            -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2 ->
            'a2) -> 'a1 t -> 'a2

          val coq_R_add_correct :
            key -> 'a1 -> 'a1 t -> 'a1 t -> 'a1 coq_R_add

          val remove : key -> 'a1 t -> 'a1 t

          type 'elt coq_R_remove =
          | R_remove_0 of 'elt t
          | R_remove_1 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list
          | R_remove_2 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list
          | R_remove_3 of 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list * 'elt t
             * 'elt coq_R_remove

          val coq_R_remove_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a1 t ->
            'a1 coq_R_remove -> 'a2 -> 'a2) -> 'a1 t -> 'a1 t -> 'a1
            coq_R_remove -> 'a2

          val coq_R_remove_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a1 t ->
            'a1 coq_R_remove -> 'a2 -> 'a2) -> 'a1 t -> 'a1 t -> 'a1
            coq_R_remove -> 'a2

          val remove_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2 ->
            'a2) -> 'a1 t -> 'a2

          val remove_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (Vertex.t * Vertex.t) ->
            'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2) -> ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2) ->
            ('a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ -> 'a2 ->
            'a2) -> 'a1 t -> 'a2

          val coq_R_remove_correct : key -> 'a1 t -> 'a1 t -> 'a1 coq_R_remove

          val elements : 'a1 t -> 'a1 t

          val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2

          type ('elt, 'a) coq_R_fold =
          | R_fold_0 of (key -> 'elt -> 'a -> 'a) * 'elt t * 'a
          | R_fold_1 of (key -> 'elt -> 'a -> 'a) * 'elt t * 'a
             * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list * 'a
             * ('elt, 'a) coq_R_fold

          val coq_R_fold_rect :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ ->
            (Vertex.t * Vertex.t) -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1)
            list -> __ -> __ -> ('a1, __) coq_R_fold -> 'a2 -> 'a2) -> (key
            -> 'a1 -> 'a3 -> 'a3) -> 'a1 t -> 'a3 -> 'a3 -> ('a1, 'a3)
            coq_R_fold -> 'a2

          val coq_R_fold_rec :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ ->
            (Vertex.t * Vertex.t) -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1)
            list -> __ -> __ -> ('a1, __) coq_R_fold -> 'a2 -> 'a2) -> (key
            -> 'a1 -> 'a3 -> 'a3) -> 'a1 t -> 'a3 -> 'a3 -> ('a1, 'a3)
            coq_R_fold -> 'a2

          val fold_rect :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ ->
            (Vertex.t * Vertex.t) -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1)
            list -> __ -> 'a2 -> 'a2) -> (key -> 'a1 -> 'a3 -> 'a3) -> 'a1 t
            -> 'a3 -> 'a2

          val fold_rec :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ ->
            (Vertex.t * Vertex.t) -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1)
            list -> __ -> 'a2 -> 'a2) -> (key -> 'a1 -> 'a3 -> 'a3) -> 'a1 t
            -> 'a3 -> 'a2

          val coq_R_fold_correct :
            (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2 -> ('a1, 'a2)
            coq_R_fold

          val equal : ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool

          type 'elt coq_R_equal =
          | R_equal_0 of 'elt t * 'elt t
          | R_equal_1 of 'elt t * 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list * (Vertex.t * Vertex.t)
             * 'elt * ((Vertex.t * Vertex.t) * 'elt) list * bool
             * 'elt coq_R_equal
          | R_equal_2 of 'elt t * 'elt t * (Vertex.t * Vertex.t) * 'elt
             * ((Vertex.t * Vertex.t) * 'elt) list * (Vertex.t * Vertex.t)
             * 'elt * ((Vertex.t * Vertex.t) * 'elt) list
             * (Vertex.t * Vertex.t) OrderedType.coq_Compare
          | R_equal_3 of 'elt t * 'elt t * 'elt t * 'elt t

          val coq_R_equal_rect :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> (Vertex.t * Vertex.t)
            -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            bool -> 'a1 coq_R_equal -> 'a2 -> 'a2) -> ('a1 t -> 'a1 t ->
            (Vertex.t * Vertex.t) -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1)
            list -> __ -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> (Vertex.t * Vertex.t)
            OrderedType.coq_Compare -> __ -> __ -> 'a2) -> ('a1 t -> 'a1 t ->
            'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) -> 'a1 t -> 'a1 t ->
            bool -> 'a1 coq_R_equal -> 'a2

          val coq_R_equal_rec :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> (Vertex.t * Vertex.t)
            -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            bool -> 'a1 coq_R_equal -> 'a2 -> 'a2) -> ('a1 t -> 'a1 t ->
            (Vertex.t * Vertex.t) -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1)
            list -> __ -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> (Vertex.t * Vertex.t)
            OrderedType.coq_Compare -> __ -> __ -> 'a2) -> ('a1 t -> 'a1 t ->
            'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) -> 'a1 t -> 'a1 t ->
            bool -> 'a1 coq_R_equal -> 'a2

          val equal_rect :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> (Vertex.t * Vertex.t)
            -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2 -> 'a2) -> ('a1 t -> 'a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> (Vertex.t * Vertex.t)
            -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ ->
            (Vertex.t * Vertex.t) OrderedType.coq_Compare -> __ -> __ -> 'a2)
            -> ('a1 t -> 'a1 t -> 'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) ->
            'a1 t -> 'a1 t -> 'a2

          val equal_rec :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> (Vertex.t * Vertex.t)
            -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ -> __ -> __ ->
            'a2 -> 'a2) -> ('a1 t -> 'a1 t -> (Vertex.t * Vertex.t) -> 'a1 ->
            ((Vertex.t * Vertex.t) * 'a1) list -> __ -> (Vertex.t * Vertex.t)
            -> 'a1 -> ((Vertex.t * Vertex.t) * 'a1) list -> __ ->
            (Vertex.t * Vertex.t) OrderedType.coq_Compare -> __ -> __ -> 'a2)
            -> ('a1 t -> 'a1 t -> 'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) ->
            'a1 t -> 'a1 t -> 'a2

          val coq_R_equal_correct :
            ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool -> 'a1 coq_R_equal

          val map : ('a1 -> 'a2) -> 'a1 t -> 'a2 t

          val mapi : (key -> 'a1 -> 'a2) -> 'a1 t -> 'a2 t

          val option_cons :
            key -> 'a1 option -> (key * 'a1) list -> (key * 'a1) list

          val map2_l :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a3 t

          val map2_r :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a2 t -> 'a3 t

          val map2 :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t -> 'a3
            t

          val combine : 'a1 t -> 'a2 t -> ('a1 option * 'a2 option) t

          val fold_right_pair :
            ('a1 -> 'a2 -> 'a3 -> 'a3) -> ('a1 * 'a2) list -> 'a3 -> 'a3

          val map2_alt :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t ->
            (key * 'a3) list

          val at_least_one :
            'a1 option -> 'a2 option -> ('a1 option * 'a2 option) option

          val at_least_one_then_f :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 option -> 'a2
            option -> 'a3 option
         end

        type 'elt coq_R_mem =
        | R_mem_0 of 'elt tree
        | R_mem_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * bool * 'elt coq_R_mem
        | R_mem_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_mem_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * bool * 'elt coq_R_mem

        val coq_R_mem_rect :
          (Vertex.t * Vertex.t) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> bool -> 'a1 coq_R_mem -> 'a2 ->
          'a2) -> 'a1 tree -> bool -> 'a1 coq_R_mem -> 'a2

        val coq_R_mem_rec :
          (Vertex.t * Vertex.t) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> bool -> 'a1 coq_R_mem -> 'a2 ->
          'a2) -> 'a1 tree -> bool -> 'a1 coq_R_mem -> 'a2

        type 'elt coq_R_find =
        | R_find_0 of 'elt tree
        | R_find_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt option * 'elt coq_R_find
        | R_find_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_find_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt option * 'elt coq_R_find

        val coq_R_find_rect :
          (Vertex.t * Vertex.t) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a1 option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 option -> 'a1 coq_R_find ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 option -> 'a1 coq_R_find -> 'a2

        val coq_R_find_rec :
          (Vertex.t * Vertex.t) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a1 option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 option -> 'a1 coq_R_find ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 option -> 'a1 coq_R_find -> 'a2

        type 'elt coq_R_bal =
        | R_bal_0 of 'elt tree * key * 'elt * 'elt tree
        | R_bal_1 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_2 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_3 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 
           'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_4 of 'elt tree * key * 'elt * 'elt tree
        | R_bal_5 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_6 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_7 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 
           'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_8 of 'elt tree * key * 'elt * 'elt tree

        val coq_R_bal_rect :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> __ ->
          'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2)
          -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __
          -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ ->
          __ -> __ -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> __ -> 'a2) -> ('a1 tree -> key
          -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2) -> ('a1 tree
          -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a2) -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_bal -> 'a2

        val coq_R_bal_rec :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> __ ->
          'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2)
          -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __
          -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ ->
          __ -> __ -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> __ -> 'a2) -> ('a1 tree -> key
          -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2) -> ('a1 tree
          -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a2) -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_bal -> 'a2

        type 'elt coq_R_add =
        | R_add_0 of 'elt tree
        | R_add_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_add
        | R_add_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_add_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_add

        val coq_R_add_rect :
          key -> 'a1 -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          tree -> 'a1 coq_R_add -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_add ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_add -> 'a2

        val coq_R_add_rec :
          key -> 'a1 -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          tree -> 'a1 coq_R_add -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_add ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_add -> 'a2

        type 'elt coq_R_remove_min =
        | R_remove_min_0 of 'elt tree * key * 'elt * 'elt tree
        | R_remove_min_1 of 'elt tree * key * 'elt * 'elt tree * 'elt tree
           * key * 'elt * 'elt tree * Int.Z_as_Int.t
           * ('elt tree * (key * 'elt)) * 'elt coq_R_remove_min * 'elt tree
           * (key * 'elt)

        val coq_R_remove_min_rect :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> 'a2) -> ('a1 tree ->
          key -> 'a1 -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> ('a1 tree * (key * 'a1)) -> 'a1
          coq_R_remove_min -> 'a2 -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> ('a1 tree * (key * 'a1)) ->
          'a1 coq_R_remove_min -> 'a2

        val coq_R_remove_min_rec :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> 'a2) -> ('a1 tree ->
          key -> 'a1 -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> ('a1 tree * (key * 'a1)) -> 'a1
          coq_R_remove_min -> 'a2 -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> ('a1 tree * (key * 'a1)) ->
          'a1 coq_R_remove_min -> 'a2

        type 'elt coq_R_merge =
        | R_merge_0 of 'elt tree * 'elt tree
        | R_merge_1 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_merge_2 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * (key * 'elt) * key * 'elt

        val coq_R_merge_rect :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> key -> 'a1
          -> __ -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1
          coq_R_merge -> 'a2

        val coq_R_merge_rec :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> key -> 'a1
          -> __ -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1
          coq_R_merge -> 'a2

        type 'elt coq_R_remove =
        | R_remove_0 of 'elt tree
        | R_remove_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_remove
        | R_remove_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_remove_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_remove

        val coq_R_remove_rect :
          (Vertex.t * Vertex.t) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a1 tree -> 'a1 coq_R_remove -> 'a2 -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_remove ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_remove -> 'a2

        val coq_R_remove_rec :
          (Vertex.t * Vertex.t) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a1 tree -> 'a1 coq_R_remove -> 'a2 -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_remove ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_remove -> 'a2

        type 'elt coq_R_concat =
        | R_concat_0 of 'elt tree * 'elt tree
        | R_concat_1 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_concat_2 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * (key * 'elt)

        val coq_R_concat_rect :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_concat -> 'a2

        val coq_R_concat_rec :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_concat -> 'a2

        type 'elt coq_R_split =
        | R_split_0 of 'elt tree
        | R_split_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt triple * 'elt coq_R_split * 'elt tree
           * 'elt option * 'elt tree
        | R_split_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_split_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt triple * 'elt coq_R_split * 'elt tree
           * 'elt option * 'elt tree

        val coq_R_split_rect :
          (Vertex.t * Vertex.t) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a1 triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1
          option -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1
          tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a1 triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree ->
          'a1 option -> 'a1 tree -> __ -> 'a2) -> 'a1 tree -> 'a1 triple ->
          'a1 coq_R_split -> 'a2

        val coq_R_split_rec :
          (Vertex.t * Vertex.t) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a1 triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1
          option -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1
          tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a1 triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree ->
          'a1 option -> 'a1 tree -> __ -> 'a2) -> 'a1 tree -> 'a1 triple ->
          'a1 coq_R_split -> 'a2

        type ('elt, 'x) coq_R_map_option =
        | R_map_option_0 of 'elt tree
        | R_map_option_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'x * 'x tree * ('elt, 'x) coq_R_map_option
           * 'x tree * ('elt, 'x) coq_R_map_option
        | R_map_option_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'x tree * ('elt, 'x) coq_R_map_option * 
           'x tree * ('elt, 'x) coq_R_map_option

        val coq_R_map_option_rect :
          (key -> 'a1 -> 'a2 option) -> ('a1 tree -> __ -> 'a3) -> ('a1 tree
          -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ ->
          'a2 -> __ -> 'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2
          tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a3) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2 tree -> ('a1,
          'a2) coq_R_map_option -> 'a3 -> 'a3) -> 'a1 tree -> 'a2 tree ->
          ('a1, 'a2) coq_R_map_option -> 'a3

        val coq_R_map_option_rec :
          (key -> 'a1 -> 'a2 option) -> ('a1 tree -> __ -> 'a3) -> ('a1 tree
          -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ ->
          'a2 -> __ -> 'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2
          tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a3) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2 tree -> ('a1,
          'a2) coq_R_map_option -> 'a3 -> 'a3) -> 'a1 tree -> 'a2 tree ->
          ('a1, 'a2) coq_R_map_option -> 'a3

        type ('elt, 'x0, 'x) coq_R_map2_opt =
        | R_map2_opt_0 of 'elt tree * 'x0 tree
        | R_map2_opt_1 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_map2_opt_2 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'x0 tree * key * 'x0 * 'x0 tree
           * Int.Z_as_Int.t * 'x0 tree * 'x0 option * 'x0 tree * 'x * 
           'x tree * ('elt, 'x0, 'x) coq_R_map2_opt * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt
        | R_map2_opt_3 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'x0 tree * key * 'x0 * 'x0 tree
           * Int.Z_as_Int.t * 'x0 tree * 'x0 option * 'x0 tree * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt

        val coq_R_map2_opt_rect :
          (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree)
          -> ('a2 tree -> 'a3 tree) -> ('a1 tree -> 'a2 tree -> __ -> 'a4) ->
          ('a1 tree -> 'a2 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree
          -> key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          'a2 option -> 'a2 tree -> __ -> 'a3 -> __ -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree -> 'a2
          option -> 'a2 tree -> __ -> __ -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3) coq_R_map2_opt
          -> 'a4 -> 'a4) -> 'a1 tree -> 'a2 tree -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4

        val coq_R_map2_opt_rec :
          (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree)
          -> ('a2 tree -> 'a3 tree) -> ('a1 tree -> 'a2 tree -> __ -> 'a4) ->
          ('a1 tree -> 'a2 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree
          -> key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          'a2 option -> 'a2 tree -> __ -> 'a3 -> __ -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree -> 'a2
          option -> 'a2 tree -> __ -> __ -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3) coq_R_map2_opt
          -> 'a4 -> 'a4) -> 'a1 tree -> 'a2 tree -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4

        val fold' : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 tree -> 'a2 -> 'a2

        val flatten_e : 'a1 enumeration -> (key * 'a1) list
       end
     end

    type 'elt bst =
      'elt Raw.tree
      (* singleton inductive, whose constructor was Bst *)

    val this : 'a1 bst -> 'a1 Raw.tree

    type 'elt t = 'elt bst

    type key = Vertex.t * Vertex.t

    val empty : 'a1 t

    val is_empty : 'a1 t -> bool

    val add : key -> 'a1 -> 'a1 t -> 'a1 t

    val remove : key -> 'a1 t -> 'a1 t

    val mem : key -> 'a1 t -> bool

    val find : key -> 'a1 t -> 'a1 option

    val map : ('a1 -> 'a2) -> 'a1 t -> 'a2 t

    val mapi : (key -> 'a1 -> 'a2) -> 'a1 t -> 'a2 t

    val map2 :
      ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t -> 'a3 t

    val elements : 'a1 t -> (key * 'a1) list

    val cardinal : 'a1 t -> int

    val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2

    val equal : ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool
   end

  module EMapFacts :
   sig
    module MapFacts :
     sig
      val eqb : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

      val coq_In_dec : 'a1 EdgeMap.t -> EdgeMap.key -> bool
     end

    val eqb : (Vertex.t * Vertex.t) -> (Vertex.t * Vertex.t) -> bool

    val coq_In_dec : 'a1 EdgeMap.t -> EdgeMap.key -> bool
   end

  type t

  val coq_V : t -> Lab.coq_VertexLabel option VertexMap.t

  val vertex_label : Vertex.t -> t -> Lab.coq_VertexLabel option option

  val coq_E : t -> Lab.coq_EdgeLabel option EdgeMap.t

  val edge_label : Edge.t -> t -> Lab.coq_EdgeLabel option option

  val successors : Vertex.t -> t -> Lab.coq_EdgeLabel option VertexMap.t

  val predecessors : Vertex.t -> t -> Lab.coq_EdgeLabel option VertexMap.t

  val link : t -> Vertex.t -> Vertex.t -> Lab.coq_EdgeLabel option option

  val fold_edges :
    (t -> Edge.t -> Lab.coq_EdgeLabel option -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

  val fold_vertices :
    (t -> Vertex.t -> Lab.coq_VertexLabel option -> 'a1 -> 'a1) -> t -> 'a1
    -> 'a1

  val fold_succ :
    (t -> Vertex.t -> Vertex.t -> Lab.coq_EdgeLabel option -> 'a1 -> 'a1) ->
    Vertex.t -> t -> 'a1 -> 'a1

  val fold_pred :
    (t -> Vertex.t -> Vertex.t -> Lab.coq_EdgeLabel option -> 'a1 -> 'a1) ->
    Vertex.t -> t -> 'a1 -> 'a1

  val fold_succ_ne :
    (t -> Vertex.t -> Vertex.t -> Lab.coq_EdgeLabel option -> 'a1 -> 'a1) ->
    Vertex.t -> t -> 'a1 -> 'a1

  val fold_pred_ne :
    (t -> Vertex.t -> Vertex.t -> Lab.coq_EdgeLabel option -> 'a1 -> 'a1) ->
    Vertex.t -> t -> 'a1 -> 'a1
 end) ->
 struct
  module Coq_eqlistAF = Coq_eqlistAFacts(G.Edge)

  module EA = Eadd(Vertex)(Lab)(G)

  module Coq_list_as_OT =
   functor (O:OrderedType.OrderedType) ->
   struct
    module OP = OrderedType.OrderedTypeFacts(O)

    type t = O.t list

    (** val eq_dec : O.t list -> O.t list -> bool **)

    let rec eq_dec l l' =
      match l with
      | [] ->
        (match l' with
         | [] -> true
         | _ :: _ -> false)
      | y :: l0 ->
        (match l' with
         | [] -> false
         | t0 :: l'0 ->
           let s = eq_dec l0 l'0 in if s then O.eq_dec y t0 else false)

    (** val compare : t -> t -> t OrderedType.coq_Compare **)

    let rec compare l y =
      match l with
      | [] ->
        (match y with
         | [] -> OrderedType.EQ
         | _ :: _ -> OrderedType.LT)
      | y0 :: l0 ->
        (match y with
         | [] -> OrderedType.GT
         | t0 :: y1 ->
           let c = O.compare y0 t0 in
           (match c with
            | OrderedType.LT -> OrderedType.LT
            | OrderedType.EQ -> compare l0 y1
            | OrderedType.GT -> OrderedType.GT))
   end

  module EdgeList = Coq_list_as_OT(G.Edge)

  module Couple_ = PairOrderedType(Nat_as_OT)(Vertex)

  module Couple = PairOrderedType(Couple_)(EdgeList)

  module CoupleSet = Make(Couple)

  module PS = Properties(CoupleSet)

  module PPS = Facts(CoupleSet)

  type status =
  | Open
  | Optimal

  (** val status_rect : 'a1 -> 'a1 -> status -> 'a1 **)

  let status_rect f f0 = function
  | Open -> f
  | Optimal -> f0

  (** val status_rec : 'a1 -> 'a1 -> status -> 'a1 **)

  let status_rec f f0 = function
  | Open -> f
  | Optimal -> f0

  type vertex_status =
  | Reached of status * int * G.Edge.t list
  | Unreached

  (** val vertex_status_rect :
      (status -> int -> G.Edge.t list -> 'a1) -> 'a1 -> vertex_status -> 'a1 **)

  let vertex_status_rect f f0 = function
  | Reached (x, x0, x1) -> f x x0 x1
  | Unreached -> f0

  (** val vertex_status_rec :
      (status -> int -> G.Edge.t list -> 'a1) -> 'a1 -> vertex_status -> 'a1 **)

  let vertex_status_rec f f0 = function
  | Reached (x, x0, x1) -> f x x0 x1
  | Unreached -> f0

  module P = Path(Vertex)(Lab)(G)

  type pathmap = { graph : G.t; root : Vertex.t;
                   map : vertex_status G.VertexMap.t; set : CoupleSet.t }

  (** val graph : pathmap -> G.t **)

  let graph p =
    p.graph

  (** val root : pathmap -> Vertex.t **)

  let root p =
    p.root

  (** val map : pathmap -> vertex_status G.VertexMap.t **)

  let map p =
    p.map

  (** val set : pathmap -> CoupleSet.t **)

  let set p =
    p.set

  (** val null_init : G.t -> vertex_status G.VertexMap.t **)

  let null_init g =
    G.VertexMap.map (fun _ -> Unreached) (G.coq_V g)

  (** val first_iter :
      Vertex.t -> G.t -> (vertex_status G.VertexMap.t * CoupleSet.t) ->
      vertex_status G.VertexMap.t * CoupleSet.t **)

  let first_iter v g init0 =
    G.fold_succ_ne (fun _ _ x o ms ->
      let (m, s) = ms in
      ((G.VertexMap.add x (Reached (Open, (P.eval o), ((v, x) :: []))) m),
      (CoupleSet.add (((P.eval o), x), ((v, x) :: [])) s))) v g init0

  (** val init_dijkstra :
      Vertex.t -> G.t -> vertex_status G.VertexMap.t * CoupleSet.t **)

  let init_dijkstra v g =
    let init0 = ((null_init g), CoupleSet.empty) in
    let (itermap, iterset) = first_iter v g init0 in
    ((G.VertexMap.add v (Reached (Optimal, 0, [])) itermap), iterset)

  (** val init : G.t -> Vertex.t -> pathmap **)

  let init g r =
    { graph = g; root = r; map = (fst (init_dijkstra r g)); set =
      (snd (init_dijkstra r g)) }

  (** val find_min : CoupleSet.t -> CoupleSet.elt option **)

  let find_min s =
    CoupleSet.min_elt s

  (** val update :
      Vertex.t -> int -> G.Edge.t list -> vertex_status G.VertexMap.t ->
      Nat_as_OT.t -> Vertex.t -> CoupleSet.t -> vertex_status
      G.VertexMap.t * CoupleSet.t **)

  let update v dist pre m w x s =
    match G.VertexMap.find x m with
    | Some v0 ->
      (match v0 with
       | Reached (s0, n, p) ->
         (match s0 with
          | Open ->
            let new_w = add dist w in
            if le_lt_dec n new_w
            then (m, s)
            else ((G.VertexMap.add x (Reached (Open, new_w, ((v, x) :: pre)))
                    m),
                   (CoupleSet.add ((new_w, x), ((v, x) :: pre))
                     (CoupleSet.remove ((n, x), p) s)))
          | Optimal -> (m, s))
       | Unreached ->
         let new_dist = add dist w in
         ((G.VertexMap.add x (Reached (Open, new_dist, ((v, x) :: pre))) m),
         (CoupleSet.add ((new_dist, x), ((v, x) :: pre)) s)))
    | None -> (m, s)

  (** val iteration :
      pathmap -> (Vertex.t option * vertex_status G.VertexMap.t) * CoupleSet.t **)

  let iteration pm =
    let d = map pm in
    let g = graph pm in
    let s = set pm in
    (match find_min s with
     | Some e ->
       let (p0, p) = e in
       let (dist, v) = p0 in
       let (newd, news) =
         G.fold_succ_ne (fun _ _ x w ms ->
           let (m, s0) = ms in update v dist p m (P.eval w) x s0) v g (d, s)
       in
       (((Some v), (G.VertexMap.add v (Reached (Optimal, dist, p)) newd)),
       (CoupleSet.remove ((dist, v), p) news))
     | None -> ((None, d), s))

  (** val update_list :
      Vertex.t -> int -> G.Edge.t list -> (vertex_status
      G.VertexMap.t * CoupleSet.t) -> (G.VertexMap.E.t * Lab.coq_EdgeLabel
      option) -> vertex_status G.VertexMap.t * CoupleSet.t **)

  let update_list min d l a p =
    let (m, s0) = a in update min d l m (P.eval (snd p)) (fst p) s0

  (** val iteration_pathmap : pathmap -> Vertex.t option * pathmap **)

  let iteration_pathmap pm =
    let g = graph pm in
    let r = root pm in
    ((fst (fst (iteration pm))), { graph = g; root = r; map =
    (snd (fst (iteration pm))); set = (snd (iteration pm)) })

  (** val nonoptimal_vertices_nb : vertex_status G.VertexMap.t -> int **)

  let nonoptimal_vertices_nb d =
    G.VertexMap.fold (fun _ s n ->
      match s with
      | Reached (s0, _, _) ->
        (match s0 with
         | Open -> (fun x -> x + 1) n
         | Optimal -> n)
      | Unreached -> (fun x -> x + 1) n) d 0

  (** val coq_Dijkstra_measure : pathmap -> int **)

  let coq_Dijkstra_measure p =
    nonoptimal_vertices_nb (map p)

  (** val coq_Dijkstra_aux_F : (pathmap -> pathmap) -> pathmap -> pathmap **)

  let coq_Dijkstra_aux_F dijkstra_aux pm =
    let (o, iter) = iteration_pathmap pm in
    (match o with
     | Some _ -> dijkstra_aux iter
     | None -> pm)

  (** val coq_Dijkstra_aux_terminate : pathmap -> pathmap **)

  let rec coq_Dijkstra_aux_terminate pm =
    let (o, iter) = iteration_pathmap pm in
    (match o with
     | Some _ -> coq_Dijkstra_aux_terminate iter
     | None -> pm)

  (** val coq_Dijkstra_aux : pathmap -> pathmap **)

  let coq_Dijkstra_aux x =
    coq_Dijkstra_aux_terminate x

  type coq_R_Dijkstra_aux =
  | R_Dijkstra_aux_0 of pathmap * Vertex.t option * pathmap
  | R_Dijkstra_aux_1 of pathmap * Vertex.t option * pathmap * Vertex.t
     * pathmap * coq_R_Dijkstra_aux

  (** val coq_R_Dijkstra_aux_rect :
      (pathmap -> Vertex.t option -> pathmap -> __ -> __ -> 'a1) -> (pathmap
      -> Vertex.t option -> pathmap -> __ -> Vertex.t -> __ -> pathmap ->
      coq_R_Dijkstra_aux -> 'a1 -> 'a1) -> pathmap -> pathmap ->
      coq_R_Dijkstra_aux -> 'a1 **)

  let rec coq_R_Dijkstra_aux_rect f f0 _ _ = function
  | R_Dijkstra_aux_0 (pm, o, iter) -> f pm o iter __ __
  | R_Dijkstra_aux_1 (pm, o, iter, v, _res, r0) ->
    f0 pm o iter __ v __ _res r0 (coq_R_Dijkstra_aux_rect f f0 iter _res r0)

  (** val coq_R_Dijkstra_aux_rec :
      (pathmap -> Vertex.t option -> pathmap -> __ -> __ -> 'a1) -> (pathmap
      -> Vertex.t option -> pathmap -> __ -> Vertex.t -> __ -> pathmap ->
      coq_R_Dijkstra_aux -> 'a1 -> 'a1) -> pathmap -> pathmap ->
      coq_R_Dijkstra_aux -> 'a1 **)

  let rec coq_R_Dijkstra_aux_rec f f0 _ _ = function
  | R_Dijkstra_aux_0 (pm, o, iter) -> f pm o iter __ __
  | R_Dijkstra_aux_1 (pm, o, iter, v, _res, r0) ->
    f0 pm o iter __ v __ _res r0 (coq_R_Dijkstra_aux_rec f f0 iter _res r0)

  (** val coq_Dijkstra_aux_rect :
      (pathmap -> Vertex.t option -> pathmap -> __ -> __ -> 'a1) -> (pathmap
      -> Vertex.t option -> pathmap -> __ -> Vertex.t -> __ -> 'a1 -> 'a1) ->
      pathmap -> 'a1 **)

  let rec coq_Dijkstra_aux_rect f f0 pm =
    let f1 = f0 pm in
    let f2 = f pm in
    let (o, p) = iteration_pathmap pm in
    let f3 = f2 o p __ in
    let f4 = f1 o p __ in
    (match o with
     | Some t0 ->
       let f5 = f4 t0 __ in
       let hrec = coq_Dijkstra_aux_rect f f0 p in f5 hrec
     | None -> f3 __)

  (** val coq_Dijkstra_aux_rec :
      (pathmap -> Vertex.t option -> pathmap -> __ -> __ -> 'a1) -> (pathmap
      -> Vertex.t option -> pathmap -> __ -> Vertex.t -> __ -> 'a1 -> 'a1) ->
      pathmap -> 'a1 **)

  let coq_Dijkstra_aux_rec =
    coq_Dijkstra_aux_rect

  (** val coq_R_Dijkstra_aux_correct :
      pathmap -> pathmap -> coq_R_Dijkstra_aux **)

  let coq_R_Dijkstra_aux_correct pm _res =
    coq_Dijkstra_aux_rect (fun y y0 y1 _ _ _ _ -> R_Dijkstra_aux_0 (y, y0,
      y1)) (fun y y0 y1 _ y3 _ y5 _ _ -> R_Dijkstra_aux_1 (y, y0, y1, y3,
      (coq_Dijkstra_aux y1), (y5 (coq_Dijkstra_aux y1) __))) pm _res __

  (** val coq_Dijkstra : G.t -> Vertex.t -> pathmap **)

  let coq_Dijkstra g v =
    coq_Dijkstra_aux (init g v)

  (** val shortest_path_length : G.t -> Vertex.t -> G.VertexMap.key -> int **)

  let shortest_path_length g v v' =
    let m = map (coq_Dijkstra g v) in
    let vs = G.VertexMap.find v' m in
    (match vs with
     | Some v0 ->
       (match v0 with
        | Reached (_, l, _) -> l
        | Unreached -> 0)
     | None -> 0)
 end
