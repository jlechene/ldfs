open BinInt
open BinNat
open BinNums
open BinPos
open Datatypes
open PeanoNat

module type UsualOrderedType =
 sig
  type t

  val compare : t -> t -> t OrderedType.coq_Compare

  val eq_dec : t -> t -> bool
 end

module UOT_to_OT =
 functor (U:UsualOrderedType) ->
 U

module Nat_as_OT =
 struct
  type t = int

  (** val compare : int -> int -> int OrderedType.coq_Compare **)

  let compare x y =
    match Nat.compare x y with
    | Eq -> OrderedType.EQ
    | Lt -> OrderedType.LT
    | Gt -> OrderedType.GT

  (** val eq_dec : int -> int -> bool **)

  let eq_dec =
    Nat.eq_dec
 end

module Z_as_OT =
 struct
  type t = coq_Z

  (** val compare : coq_Z -> coq_Z -> coq_Z OrderedType.coq_Compare **)

  let compare x y =
    match Z.compare x y with
    | Eq -> OrderedType.EQ
    | Lt -> OrderedType.LT
    | Gt -> OrderedType.GT

  (** val eq_dec : coq_Z -> coq_Z -> bool **)

  let eq_dec =
    Z.eq_dec
 end

module Positive_as_OT =
 struct
  type t = positive

  (** val compare :
      positive -> positive -> positive OrderedType.coq_Compare **)

  let compare x y =
    match Pos.compare x y with
    | Eq -> OrderedType.EQ
    | Lt -> OrderedType.LT
    | Gt -> OrderedType.GT

  (** val eq_dec : positive -> positive -> bool **)

  let eq_dec =
    Pos.eq_dec
 end

module N_as_OT =
 struct
  type t = coq_N

  (** val compare : coq_N -> coq_N -> coq_N OrderedType.coq_Compare **)

  let compare x y =
    match N.compare x y with
    | Eq -> OrderedType.EQ
    | Lt -> OrderedType.LT
    | Gt -> OrderedType.GT

  (** val eq_dec : coq_N -> coq_N -> bool **)

  let eq_dec =
    N.eq_dec
 end

module PairOrderedType =
 functor (O1:OrderedType.OrderedType) ->
 functor (O2:OrderedType.OrderedType) ->
 struct
  module MO1 = OrderedType.OrderedTypeFacts(O1)

  module MO2 = OrderedType.OrderedTypeFacts(O2)

  type t = O1.t * O2.t

  (** val compare : t -> t -> (O1.t * O2.t) OrderedType.coq_Compare **)

  let compare x y =
    let (x1, x2) = x in
    let (y1, y2) = y in
    let c = O1.compare x1 y1 in
    (match c with
     | OrderedType.LT -> OrderedType.LT
     | OrderedType.EQ ->
       let c0 = O2.compare x2 y2 in
       (match c0 with
        | OrderedType.LT -> OrderedType.LT
        | OrderedType.EQ -> OrderedType.EQ
        | OrderedType.GT -> OrderedType.GT)
     | OrderedType.GT -> OrderedType.GT)

  (** val eq_dec : t -> t -> bool **)

  let eq_dec x y =
    match compare x y with
    | OrderedType.EQ -> true
    | _ -> false
 end

module PositiveOrderedTypeBits =
 struct
  type t = positive

  (** val compare : t -> t -> positive OrderedType.coq_Compare **)

  let rec compare p y =
    match p with
    | Coq_xI p0 ->
      (match y with
       | Coq_xI y0 -> compare p0 y0
       | _ -> OrderedType.GT)
    | Coq_xO p0 ->
      (match y with
       | Coq_xO y0 -> compare p0 y0
       | _ -> OrderedType.LT)
    | Coq_xH ->
      (match y with
       | Coq_xI _ -> OrderedType.LT
       | Coq_xO _ -> OrderedType.GT
       | Coq_xH -> OrderedType.EQ)

  (** val eq_dec : positive -> positive -> bool **)

  let eq_dec x y =
    match Pos.compare x y with
    | Eq -> true
    | _ -> false
 end
