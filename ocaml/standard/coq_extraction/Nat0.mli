open Datatypes

type t = int

val zero : int

val one : int

val two : int

val succ : int -> int

val pred : int -> int

val add : int -> int -> int

val double : int -> int

val mul : int -> int -> int

val sub : int -> int -> int

val eqb : int -> int -> bool

val leb : int -> int -> bool

val ltb : int -> int -> bool

val compare : int -> int -> comparison

val max : int -> int -> int

val min : int -> int -> int

val even : int -> bool

val odd : int -> bool

val pow : int -> int -> int

val divmod : int -> int -> int -> int -> int * int

val div : int -> int -> int

val modulo : int -> int -> int

val gcd : int -> int -> int

val square : int -> int

val sqrt_iter : int -> int -> int -> int -> int

val sqrt : int -> int

val log2_iter : int -> int -> int -> int -> int

val log2 : int -> int

val iter : int -> ('a1 -> 'a1) -> 'a1 -> 'a1

val div2 : int -> int

val testbit : int -> int -> bool

val shiftl : int -> int -> int

val shiftr : int -> int -> int

val bitwise : (bool -> bool -> bool) -> int -> int -> int -> int

val coq_land : int -> int -> int

val coq_lor : int -> int -> int

val ldiff : int -> int -> int

val coq_lxor : int -> int -> int
