open FMapFacts
open FMapInterface

module MyMapFacts :
 functor (M:S) ->
 sig
  module MapFacts :
   sig
    val eqb : M.E.t -> M.E.t -> bool

    val coq_In_dec : 'a1 M.t -> M.key -> bool
   end

  val eqb : M.E.t -> M.E.t -> bool

  val coq_In_dec : 'a1 M.t -> M.key -> bool
 end
