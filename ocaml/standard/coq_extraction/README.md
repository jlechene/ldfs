# Coq formalization of Danicic's algorithm extracted into OCaml

The code in this repository is the result of extracting the Coq
formalization of Danicic's algorithm into OCaml. It can be automatically
produced from the Coq sources in directory [`/coq/`](/coq/), and it was.
It is made available in this directory for convenience.

## License

The files in this directory are extracted from Coq files from several sources:
- the Coq standard library (LGPL 2.1 only, see [`LICENSE.coq`](LICENSE.coq)),
- the [graph library](/coq/concrete_graphs/) distributed in this repository
  (GPL 3 or later, see the corresponding [`LICENSE`](/coq/concrete_graphs/LICENSE)) file)
- the Coq formalization of Danicic's algorithm itself
  (MIT, the [main license](/LICENSE) of this repository).
