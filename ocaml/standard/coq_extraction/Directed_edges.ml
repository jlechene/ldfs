open Datatypes
open OrderedTypeEx

module Directed_Edge =
 functor (Vertex:OrderedType.OrderedType) ->
 struct
  module VertexPair = PairOrderedType(Vertex)(Vertex)

  module MO1 = VertexPair.MO1

  module MO2 = VertexPair.MO2

  type t = Vertex.t * Vertex.t

  (** val compare :
      t -> t -> (Vertex.t * Vertex.t) OrderedType.coq_Compare **)

  let compare x y =
    let (x1, x2) = x in
    let (y1, y2) = y in
    let c = Vertex.compare x1 y1 in
    (match c with
     | OrderedType.LT -> OrderedType.LT
     | OrderedType.EQ ->
       let c0 = Vertex.compare x2 y2 in
       (match c0 with
        | OrderedType.LT -> OrderedType.LT
        | OrderedType.EQ -> OrderedType.EQ
        | OrderedType.GT -> OrderedType.GT)
     | OrderedType.GT -> OrderedType.GT)

  (** val eq_dec : t -> t -> bool **)

  let eq_dec x y =
    match compare x y with
    | OrderedType.EQ -> true
    | _ -> false

  module OTFacts = OrderedType.OrderedTypeFacts(Vertex)

  module OTPairFacts = OrderedType.OrderedTypeFacts(VertexPair)

  (** val fst_end : t -> Vertex.t **)

  let fst_end x =
    fst x

  (** val snd_end : t -> Vertex.t **)

  let snd_end x =
    snd x

  (** val permute : t -> Vertex.t * Vertex.t **)

  let permute = function
  | (x, y) -> (y, x)
 end
