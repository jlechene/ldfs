open Bool
open Datatypes

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

module Nat =
 struct
  type t = int

  (** val zero : int **)

  let zero =
    0

  (** val one : int **)

  let one =
    (fun x -> x + 1) 0

  (** val two : int **)

  let two =
    (fun x -> x + 1) ((fun x -> x + 1) 0)

  (** val succ : int -> int **)

  let succ x =
    (fun x -> x + 1) x

  (** val pred : int -> int **)

  let pred n =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      n)
      (fun u ->
      u)
      n

  (** val add : int -> int -> int **)

  let rec add n m =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      m)
      (fun p -> (fun x -> x + 1)
      (add p m))
      n

  (** val double : int -> int **)

  let double n =
    add n n

  (** val mul : int -> int -> int **)

  let rec mul n m =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      0)
      (fun p ->
      add m (mul p m))
      n

  (** val sub : int -> int -> int **)

  let rec sub n m =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      n)
      (fun k ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        n)
        (fun l ->
        sub k l)
        m)
      n

  (** val eqb : int -> int -> bool **)

  let rec eqb n m =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        true)
        (fun _ ->
        false)
        m)
      (fun n' ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        false)
        (fun m' ->
        eqb n' m')
        m)
      n

  (** val leb : int -> int -> bool **)

  let rec leb n m =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      true)
      (fun n' ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        false)
        (fun m' ->
        leb n' m')
        m)
      n

  (** val ltb : int -> int -> bool **)

  let ltb n m =
    leb ((fun x -> x + 1) n) m

  (** val compare : int -> int -> comparison **)

  let rec compare n m =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        Eq)
        (fun _ ->
        Lt)
        m)
      (fun n' ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        Gt)
        (fun m' ->
        compare n' m')
        m)
      n

  (** val max : int -> int -> int **)

  let rec max n m =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      m)
      (fun n' ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        n)
        (fun m' -> (fun x -> x + 1)
        (max n' m'))
        m)
      n

  (** val min : int -> int -> int **)

  let rec min n m =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      0)
      (fun n' ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        0)
        (fun m' -> (fun x -> x + 1)
        (min n' m'))
        m)
      n

  (** val even : int -> bool **)

  let rec even n =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      true)
      (fun n0 ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        false)
        (fun n' ->
        even n')
        n0)
      n

  (** val odd : int -> bool **)

  let odd n =
    negb (even n)

  (** val pow : int -> int -> int **)

  let rec pow n m =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ -> (fun x -> x + 1)
      0)
      (fun m0 ->
      mul n (pow n m0))
      m

  (** val divmod : int -> int -> int -> int -> int * int **)

  let rec divmod x y q u =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ -> (q,
      u))
      (fun x' ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        divmod x' y ((fun x -> x + 1) q) y)
        (fun u' ->
        divmod x' y q u')
        u)
      x

  (** val div : int -> int -> int **)

  let div x y =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      y)
      (fun y' ->
      fst (divmod x y' 0 y'))
      y

  (** val modulo : int -> int -> int **)

  let modulo x y =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      y)
      (fun y' ->
      sub y' (snd (divmod x y' 0 y')))
      y

  (** val gcd : int -> int -> int **)

  let rec gcd a b =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      b)
      (fun a' ->
      gcd (modulo b ((fun x -> x + 1) a')) ((fun x -> x + 1) a'))
      a

  (** val square : int -> int **)

  let square n =
    mul n n

  (** val sqrt_iter : int -> int -> int -> int -> int **)

  let rec sqrt_iter k p q r =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      p)
      (fun k' ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        sqrt_iter k' ((fun x -> x + 1) p) ((fun x -> x + 1) ((fun x -> x + 1)
          q)) ((fun x -> x + 1) ((fun x -> x + 1) q)))
        (fun r' ->
        sqrt_iter k' p q r')
        r)
      k

  (** val sqrt : int -> int **)

  let sqrt n =
    sqrt_iter n 0 0 0

  (** val log2_iter : int -> int -> int -> int -> int **)

  let rec log2_iter k p q r =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      p)
      (fun k' ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        log2_iter k' ((fun x -> x + 1) p) ((fun x -> x + 1) q) q)
        (fun r' ->
        log2_iter k' p ((fun x -> x + 1) q) r')
        r)
      k

  (** val log2 : int -> int **)

  let log2 n =
    log2_iter (pred n) 0 ((fun x -> x + 1) 0) 0

  (** val iter : int -> ('a1 -> 'a1) -> 'a1 -> 'a1 **)

  let rec iter n f x =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      x)
      (fun n0 ->
      f (iter n0 f x))
      n

  (** val div2 : int -> int **)

  let rec div2 n =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      0)
      (fun n0 ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        0)
        (fun n' -> (fun x -> x + 1)
        (div2 n'))
        n0)
      n

  (** val testbit : int -> int -> bool **)

  let rec testbit a n =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      odd a)
      (fun n0 ->
      testbit (div2 a) n0)
      n

  (** val shiftl : int -> int -> int **)

  let rec shiftl a n =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      a)
      (fun n0 ->
      double (shiftl a n0))
      n

  (** val shiftr : int -> int -> int **)

  let rec shiftr a n =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      a)
      (fun n0 ->
      div2 (shiftr a n0))
      n

  (** val bitwise : (bool -> bool -> bool) -> int -> int -> int -> int **)

  let rec bitwise op n a b =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      0)
      (fun n' ->
      add (if op (odd a) (odd b) then (fun x -> x + 1) 0 else 0)
        (mul ((fun x -> x + 1) ((fun x -> x + 1) 0))
          (bitwise op n' (div2 a) (div2 b))))
      n

  (** val coq_land : int -> int -> int **)

  let coq_land a b =
    bitwise (&&) a a b

  (** val coq_lor : int -> int -> int **)

  let coq_lor a b =
    bitwise (||) (max a b) a b

  (** val ldiff : int -> int -> int **)

  let ldiff a b =
    bitwise (fun b0 b' -> (&&) b0 (negb b')) a a b

  (** val coq_lxor : int -> int -> int **)

  let coq_lxor a b =
    bitwise xorb (max a b) a b

  (** val recursion : 'a1 -> (int -> 'a1 -> 'a1) -> int -> 'a1 **)

  let rec recursion x f n =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      x)
      (fun n0 ->
      f n0 (recursion x f n0))
      n

  (** val eq_dec : int -> int -> bool **)

  let rec eq_dec n m =
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        true)
        (fun _ ->
        false)
        m)
      (fun n0 ->
      (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
        (fun _ ->
        false)
        (fun m0 ->
        eq_dec n0 m0)
        m)
      n

  (** val leb_spec0 : int -> int -> reflect **)

  let leb_spec0 x y =
    iff_reflect (leb x y)

  (** val ltb_spec0 : int -> int -> reflect **)

  let ltb_spec0 x y =
    iff_reflect (ltb x y)

  module Private_OrderTac =
   struct
    module IsTotal =
     struct
      
     end

    module Tac =
     struct
      
     end
   end

  module Private_Tac =
   struct
    
   end

  module Private_Dec =
   struct
    (** val max_case_strong :
        int -> int -> (int -> int -> __ -> 'a1 -> 'a1) -> (__ -> 'a1) -> (__
        -> 'a1) -> 'a1 **)

    let max_case_strong n m compat hl hr =
      let c = coq_CompSpec2Type n m (compare n m) in
      (match c with
       | CompGtT -> compat n (max n m) __ (hl __)
       | _ -> compat m (max n m) __ (hr __))

    (** val max_case :
        int -> int -> (int -> int -> __ -> 'a1 -> 'a1) -> 'a1 -> 'a1 -> 'a1 **)

    let max_case n m x x0 x1 =
      max_case_strong n m x (fun _ -> x0) (fun _ -> x1)

    (** val max_dec : int -> int -> bool **)

    let max_dec n m =
      max_case n m (fun _ _ _ h0 -> h0) true false

    (** val min_case_strong :
        int -> int -> (int -> int -> __ -> 'a1 -> 'a1) -> (__ -> 'a1) -> (__
        -> 'a1) -> 'a1 **)

    let min_case_strong n m compat hl hr =
      let c = coq_CompSpec2Type n m (compare n m) in
      (match c with
       | CompGtT -> compat m (min n m) __ (hr __)
       | _ -> compat n (min n m) __ (hl __))

    (** val min_case :
        int -> int -> (int -> int -> __ -> 'a1 -> 'a1) -> 'a1 -> 'a1 -> 'a1 **)

    let min_case n m x x0 x1 =
      min_case_strong n m x (fun _ -> x0) (fun _ -> x1)

    (** val min_dec : int -> int -> bool **)

    let min_dec n m =
      min_case n m (fun _ _ _ h0 -> h0) true false
   end

  (** val max_case_strong :
      int -> int -> (__ -> 'a1) -> (__ -> 'a1) -> 'a1 **)

  let max_case_strong n m x x0 =
    Private_Dec.max_case_strong n m (fun _ _ _ x1 -> x1) x x0

  (** val max_case : int -> int -> 'a1 -> 'a1 -> 'a1 **)

  let max_case n m x x0 =
    max_case_strong n m (fun _ -> x) (fun _ -> x0)

  (** val max_dec : int -> int -> bool **)

  let max_dec =
    Private_Dec.max_dec

  (** val min_case_strong :
      int -> int -> (__ -> 'a1) -> (__ -> 'a1) -> 'a1 **)

  let min_case_strong n m x x0 =
    Private_Dec.min_case_strong n m (fun _ _ _ x1 -> x1) x x0

  (** val min_case : int -> int -> 'a1 -> 'a1 -> 'a1 **)

  let min_case n m x x0 =
    min_case_strong n m (fun _ -> x) (fun _ -> x0)

  (** val min_dec : int -> int -> bool **)

  let min_dec =
    Private_Dec.min_dec

  module Private_Parity =
   struct
    
   end

  module Private_NZPow =
   struct
    
   end

  module Private_NZSqrt =
   struct
    
   end

  (** val sqrt_up : int -> int **)

  let sqrt_up a =
    match compare 0 a with
    | Lt -> (fun x -> x + 1) (sqrt (pred a))
    | _ -> 0

  (** val log2_up : int -> int **)

  let log2_up a =
    match compare ((fun x -> x + 1) 0) a with
    | Lt -> (fun x -> x + 1) (log2 (pred a))
    | _ -> 0

  module Private_NZDiv =
   struct
    
   end

  (** val lcm : int -> int -> int **)

  let lcm a b =
    mul a (div b (gcd a b))

  (** val eqb_spec : int -> int -> reflect **)

  let eqb_spec x y =
    iff_reflect (eqb x y)

  (** val b2n : bool -> int **)

  let b2n = function
  | true -> (fun x -> x + 1) 0
  | false -> 0

  (** val setbit : int -> int -> int **)

  let setbit a n =
    coq_lor a (shiftl ((fun x -> x + 1) 0) n)

  (** val clearbit : int -> int -> int **)

  let clearbit a n =
    ldiff a (shiftl ((fun x -> x + 1) 0) n)

  (** val ones : int -> int **)

  let ones n =
    pred (shiftl ((fun x -> x + 1) 0) n)

  (** val lnot : int -> int -> int **)

  let lnot a n =
    coq_lxor a (ones n)
 end
