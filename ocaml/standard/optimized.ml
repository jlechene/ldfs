open MyGraph
open Graph.Pack.Digraph

let confirm g obs u u_o =
  let exception Found in
  match iter_succ (fun v -> match Hashtbl.find obs v with
                            | exception Not_found -> ()
                            | v_o when v_o <> u_o -> raise Found
                            | _ -> ()) g u
  with
  | exception Found -> true
  | () -> false

let propagate g w obs u v =
  let worklist_current = ref (S.singleton u) in
  let candidates = ref S.empty in
  while not (S.is_empty !worklist_current) do
     let u = S.choose !worklist_current in
     worklist_current := S.remove u !worklist_current;
     let pred = ref (S.of_list (pred g u)) in
     while not (S.is_empty !pred) do
        let u0 = S.choose !pred in
        pred := S.remove u0 !pred;
        if not (S.mem u0 !w) then
           match Hashtbl.find obs u0 with
           | exception Not_found -> Hashtbl.add obs u0 v; worklist_current := S.add u0 !worklist_current
           | w -> if w <> v then (Hashtbl.replace obs u0 v; worklist_current := S.add u0 !worklist_current;
                                  if out_degree g u0 > 1 then candidates := S.add u0 !candidates)
     done
  done;
  !candidates

let main ?(debug=false) g v' =
  let w = ref v' in
  let obs = Hashtbl.create 97 in
  S.iter (fun u -> Hashtbl.add obs u u) v';
  let worklist = ref v' in
  while not (S.is_empty !worklist) do
    let u = S.choose !worklist in
    worklist := S.remove u !worklist;
    let candidates = ref (propagate g w obs u u) in
    let new_nodes = ref S.empty in
    while not (S.is_empty !candidates) do
      let v = S.choose !candidates in
      candidates := S.remove v !candidates;
      if confirm g obs v u then new_nodes := S.add v !new_nodes
    done;
    w := S.union !w !new_nodes;
    worklist := S.union !worklist !new_nodes;
    S.iter (fun u -> Hashtbl.replace obs u u) !new_nodes;
  done;
  !w
