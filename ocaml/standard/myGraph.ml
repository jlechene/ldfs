open Graph.Pack.Digraph

module S = Set.Make (V)

let affiche fmt v =
  Format.fprintf fmt "N:%d" (V.label v)

let affiche_edge fmt e =
  Format.fprintf fmt "%a-%a" affiche (E.src e) affiche (E.dst e)

let affiche_occ fmt occ =
  Format.fprintf fmt "(%a, %d)" affiche (fst occ) (snd occ)

let affiche_list print fmt l =
  Format.fprintf fmt "[";
  List.iteri (fun i x -> if i <> List.length l - 1 then Format.fprintf fmt "%a, " print x else Format.fprintf fmt "%a" print x) l;
  Format.fprintf fmt "]"

let affiche_set print fmt s =
  Format.fprintf fmt "{";
  let n = S.cardinal s in
  let count = ref 0 in
  S.iter (fun x -> if !count <> n - 1 then Format.fprintf fmt "%a, " print x else Format.fprintf fmt "%a" print x; incr count) s;
  Format.fprintf fmt "}"

let affiche_hashtbl print_key print_elt fmt h =
  Format.fprintf fmt "〈";
  let n = Hashtbl.length h in
  let count = ref 0 in
  Hashtbl.iter (fun key elt-> if !count <> n - 1 then Format.fprintf fmt "(%a, %a), " print_key key print_elt elt else Format.fprintf fmt "(%a, %a)" print_key key print_elt elt; incr count) h;
  Format.fprintf fmt "〉"

let affiche_stack print fmt s =
    Format.fprintf fmt "[";
    let i = ref 0 in
    Stack.iter (fun x -> if !i <> Stack.length s - 1 then Format.fprintf fmt "%a, " print x else Format.fprintf fmt "%a" print x; incr i) s;
    Format.fprintf fmt "]"
