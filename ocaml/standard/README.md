# Benchmarks

This directory contains OCaml implementations of Danicic's algorithm and our
optimization, along with the Coq formalization of Danicic's algorithm extracted
into OCaml.

The implementation of graphs used is said to be standard (access to the successors
is cheap, access to the predecessors is expensive).

## Compilation

Tested with OCaml 4.05.0 and ocamlbuild.0.11.0.

The compilation relies on ocamlbuild. Just enter
```
ocamlbuild test_performance.native -pkg ocamlgraph -pkg unix
```

This produces an executable `test_performance.native` which itself calls the
implementations.

## Available algorithms

The following algorithms are available:
- `danicic`: Danicic's initial algorithm,
- `danicic_opti_step`: Danicic's algorithm optimized by considering all the critical edges at each step,
- `danicic_opti_all`: Danicic's algorithm optimized by considering all the critical edges at each step and by weakening the definition of critical edge,
- `optimized`: our optimized algorithm,
- `coq`: the Coq formalization of Danicic's algorithm extracted into OCaml.

## Usage

`test_performance.native` automates the tests of the algorithms on
random graphs. It takes as arguments the sizes of the graph to generate
and the algorithms to call on those graphs. On each graph, all the algorithms
are called and their results are automatically compared. The execution fails
if the results are not equal.

For instructions, use
```
./test_performance.native --help
```

Example:
```
./test_performance.native 10 1000 10 5 danicic coq
```
For each number between 10 and 1000 and multiple of 10,
5 different graphs are generated of that size and Danicic's algorithm
and the Coq extraction are called on them.

The results are presented in columns:
- number of nodes in the graph
- size of the closure
- size of $V' \cup \operatorname{WD}_G(V')$ if one of the algorithms called computes this set
  (here, only our optimized version computes this set)
- time used by the generation step
- time used by the first algorithm (in s)
- time used by the second algorithm (in s)
- ...
