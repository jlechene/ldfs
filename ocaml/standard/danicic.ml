open MyGraph
open Graph.Pack.Digraph

let reachable_from p vs e =
  S.fold (fun v res -> res || PathCheck.check_path p v e) vs false

let reachable_to p e vs =
  S.fold (fun v res -> res || PathCheck.check_path p e v) vs false

let theta ?(debug=false) g vs =
  if debug then assert (S.fold (fun x e -> e && mem_vertex g x) vs true);
  let h = copy g in
  let vs' = S.fold (fun u acc -> S.add (find_vertex h (V.label u)) acc) vs S.empty in
  assert (S.fold (fun u acc -> acc && mem_vertex h u) vs' true);
  (* let s = fold_vertex (fun acc u -> if S.mem u vs then S.union acc (fold_succ_e (fun acc' e -> S.add e acc') h S.empty u)) h S.empty; *)
  S.iter (fun u -> let s = succ_e h u in List.iter (fun e -> remove_edge_e h e) s) vs';
  let p = PathCheck.create h in
  fun u ->
  S.fold (fun x e -> if PathCheck.check_path p (find_vertex h (V.label u)) (find_vertex h (V.label x)) then S.add x e else e) vs S.empty

(* Danicic's original version *)
let weak_superset ?(debug=false) g vs =
  let p = PathCheck.create g in
  if debug then assert (S.fold (fun x e -> e && mem_vertex g x) vs true);
  let x = ref vs in
  let don = ref false in
  let exception Not_finished in
  while (not !don) do
    if debug then Format.printf "Size of the closure: %d@." (S.cardinal !x);
    don := true;
    try
    let obs = theta g !x in
    iter_edges (fun v1 v2 -> if (reachable_from p vs v1 && S.cardinal (obs v2) = 1 &&
    S.cardinal (obs v1) >=2) then
    (if debug then Format.printf "critical edge (%a%a, %a%a)@." affiche v1 (affiche_set affiche) (obs v1) affiche v2 (affiche_set affiche) (obs v2); x := S.add v1 !x; raise Not_finished)) g
    with Not_finished -> don := false
  done;
  !x

(* Danicic's algorithm, optimized by weakening the definition of critical edge *)
let weak_superset_opti_n ?(debug=false) g vs =
  let p = PathCheck.create g in
  if debug then assert (S.fold (fun x e -> e && mem_vertex g x) vs true);
  let x = ref vs in
  let don = ref false in
  let exception Not_finished in
  while (not !don) do
    if debug then Format.printf "Size of the closure: %d@." (S.cardinal !x);
    don := true;
    try
    let obs = theta g !x in
    iter_edges (fun v1 v2 -> if (reachable_from p vs v1 && let n = S.cardinal (obs v2) in n >= 1 &&
    S.cardinal (obs v1) > n) then
    (if debug then Format.printf "critical edge (%a%a, %a%a)@." affiche v1 (affiche_set affiche) (obs v1) affiche v2 (affiche_set affiche) (obs v2); x := S.add v1 !x; raise Not_finished)) g
    with Not_finished -> don := false
  done;
  !x

(* Danicic's algorithm, optimized by considering all the critical edges at each step *)
let weak_superset_opti_step ?(debug=false) g vs =
  let p = PathCheck.create g in
  if debug then assert (S.fold (fun x e -> e && mem_vertex g x) vs true);
  let x = ref vs in
  let don = ref false in
  while (not !don) do
    if debug then Format.printf "Size of the closure: %d@." (S.cardinal !x);
    don := true;
    let obs = theta g !x in
    iter_edges (fun v1 v2 -> if (reachable_from p vs v1 && S.cardinal (obs v2) = 1 &&
    S.cardinal (obs v1) >= 2) then
    (if debug then Format.printf "critical edge (%a%a, %a%a)@." affiche v1 (affiche_set affiche) (obs v1) affiche v2 (affiche_set affiche) (obs v2); x := S.add v1 !x; don := false)) g
  done;
  !x

(* Danicic's algorithm, optimized using both optimizations *)
let weak_superset_opti_all ?(debug=false) g vs =
  let p = PathCheck.create g in
  if debug then assert (S.fold (fun x e -> e && mem_vertex g x) vs true);
  let x = ref vs in
  let don = ref false in
  while (not !don) do
    if debug then Format.printf "Size of the closure: %d@." (S.cardinal !x);
    don := true;
    let obs = theta g !x in
    iter_edges (fun v1 v2 -> if (reachable_from p vs v1 && let n = S.cardinal (obs v2) in n >= 1 &&
    S.cardinal (obs v1) > n) then
    (if debug then Format.printf "critical edge (%a%a, %a%a)@." affiche v1 (affiche_set affiche) (obs v1) affiche v2 (affiche_set affiche) (obs v2); x := S.add v1 !x; don := false)) g
  done;
  !x
