# Benchmarks

This directory contains OCaml implementations of Danicic's algorithm and our
optimization, several variants of these, and the infrastructure to perform
benchmarks comparing their execution times.

## Contents

Two implementations of graphs were tested.
- The first one is a standard implementation, where every vertex can cheaply
  access its successors,
  but not its predecessors (in subdirectory [`standard/`](standard/)).
  The benchmarks presented in Léchenet et al., 2018
  (see the [publications](/README.md#publications))
  were done using this implementation.
- The second one is a bidirectional implementation, where every vertex can
  cheaply access both its successors and its predecessors
  (in subdirectory [`bidirectional/`](bidirectional/)).
  This implementation takes more memory space, but accesses
  to predecessors are claimed to be in constant time instead of being linearly
  dependent on the size of the graph.
  The benchmarks presented in Léchenet et al., 2023
  (see the [publications](/README.md#publications))
  were done using
  this implementation.

Initially, we used the standard implementation, but since
our optimized algorithm is based on backward traversal,
we later switched to the
bidirectional implementation.
We also performed more thorough experiments in the bidirectional case.
