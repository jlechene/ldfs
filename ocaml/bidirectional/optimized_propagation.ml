open MyGraph

(* OCaml version of optimized.
   To compute the new deciding nodes, a new traversal is performed,
   instead of accumulating a set of candidates during the propagation.
*)
let confirm g obs u u' =
  let exception Found in
  try
    iter_succ (fun v -> match Hashtbl.find obs v with
                        | exception Not_found -> ()
                        | v' -> if not (V.equal u' v') then raise Found) g u; false
  with
  | Found -> true

let propagate g ws obs u v =
  let l = ref (S.singleton u) in
  while not (S.is_empty !l) do
    let w = S.choose !l in
    l := S.remove w !l;
    iter_pred (fun w0 ->
      if not (S.mem w0 ws) then
      match Hashtbl.find obs w0 with
      | exception Not_found -> Hashtbl.add obs w0 v; l := S.add w0 !l
      | w0' -> if not (V.equal w0' v) then
                 (Hashtbl.replace obs w0 v;
                  l := S.add w0 !l)
              ) g w
  done

let propagate_c g ws obs u v =
  let l = ref (S.singleton u) in
  let c = ref S.empty in
  let seen = Hashtbl.create 97 in
  Hashtbl.add seen u true;
  while not (S.is_empty !l) do
    let w = S.choose !l in
    l := S.remove w !l;
    iter_pred (fun w0 ->
      if not (S.mem w0 ws) && not (Hashtbl.mem seen w0) then
      (match Hashtbl.find obs w0 with
      | exception Not_found -> assert false
      | w0' -> (assert (w0' = v);
               l := S.add w0 !l;
               Hashtbl.add seen w0 true;
               if confirm g obs w0 v then c := S.add w0 !c)
              )) g w
  done;
  !c

let main g vs =
  let ws = ref vs in
  let l = ref vs in
  let obs = Hashtbl.create 97 in
  S.iter (fun v -> Hashtbl.replace obs v v) vs;
  while not (S.is_empty !l) do
    let u = S.choose !l in
    l := S.remove u !l;
    let () = propagate g !ws obs u u in
    let c = propagate_c g !ws obs u u in
    assert (S.for_all (fun v -> confirm g obs v u) c);
    ws := S.union c !ws;
    l := S.union c !l;
    S.iter (fun v -> Hashtbl.replace obs v v) c
  done;
  (!ws, obs)

let wrap g vs =
  fst (main g vs)
