open MyGraph

let main g (crit: S.t) =
  let g_coq = fold_vertex (fun v g -> Cfg_test.DG.add_vertex (V.label v) g None) g Cfg_test.DG.empty_graph in
  let g_coq : Cfg_test.DG.t = fold_edges (fun u v g -> Cfg_test.DG.add_edge (V.label u, V.label v) g None) g g_coq in
  let crit_coq : int list = S.elements crit in
  S.of_list (Cfg_test.test g_coq crit_coq)
