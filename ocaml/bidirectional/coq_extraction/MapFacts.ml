open FMapFacts
open FMapInterface

module MyMapFacts =
 functor (M:S) ->
 struct
  module MapFacts = Facts(M)

  (** val eqb : M.E.t -> M.E.t -> bool **)

  let eqb x y =
    if M.E.eq_dec x y then true else false

  (** val coq_In_dec : 'a1 M.t -> M.key -> bool **)

  let coq_In_dec m x =
    MapFacts.coq_In_dec m x
 end
