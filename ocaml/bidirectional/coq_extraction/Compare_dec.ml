open Datatypes

(** val zerop : int -> bool **)

let zerop n =
  (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
    (fun _ ->
    true)
    (fun _ ->
    false)
    n

(** val lt_eq_lt_dec : int -> int -> bool option **)

let rec lt_eq_lt_dec n m =
  (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
    (fun _ ->
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ -> Some
      false)
      (fun _ -> Some
      true)
      m)
    (fun n0 ->
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      None)
      (fun m0 ->
      lt_eq_lt_dec n0 m0)
      m)
    n

(** val gt_eq_gt_dec : int -> int -> bool option **)

let gt_eq_gt_dec n m =
  lt_eq_lt_dec n m

(** val le_lt_dec : int -> int -> bool **)

let rec le_lt_dec n m =
  (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
    (fun _ ->
    true)
    (fun n0 ->
    (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
      (fun _ ->
      false)
      (fun m0 ->
      le_lt_dec n0 m0)
      m)
    n

(** val le_le_S_dec : int -> int -> bool **)

let le_le_S_dec n m =
  le_lt_dec n m

(** val le_ge_dec : int -> int -> bool **)

let le_ge_dec n m =
  le_lt_dec n m

(** val le_gt_dec : int -> int -> bool **)

let le_gt_dec n m =
  le_lt_dec n m

(** val le_lt_eq_dec : int -> int -> bool **)

let le_lt_eq_dec n m =
  let s = lt_eq_lt_dec n m in
  (match s with
   | Some s0 -> s0
   | None -> assert false (* absurd case *))

(** val le_dec : int -> int -> bool **)

let le_dec n m =
  le_gt_dec n m

(** val lt_dec : int -> int -> bool **)

let lt_dec n m =
  le_dec ((fun x -> x + 1) n) m

(** val gt_dec : int -> int -> bool **)

let gt_dec n m =
  lt_dec m n

(** val ge_dec : int -> int -> bool **)

let ge_dec n m =
  le_dec m n

(** val nat_compare_alt : int -> int -> comparison **)

let nat_compare_alt n m =
  match lt_eq_lt_dec n m with
  | Some s -> if s then Lt else Eq
  | None -> Gt
