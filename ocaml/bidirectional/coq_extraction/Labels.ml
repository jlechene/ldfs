module type Labels =
 sig
  type coq_VertexLabel

  type coq_EdgeLabel
 end

module type Labels_with_weight =
 sig
  type coq_VertexLabel

  type coq_EdgeLabel

  val weight : coq_EdgeLabel -> int
 end

module Nat_Labels =
 struct
  type coq_VertexLabel = int

  type coq_EdgeLabel = int
 end
