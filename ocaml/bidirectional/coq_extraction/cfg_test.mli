open Datatypes
open DigraphMapImp
open List0
open OrderedTypeEx
open Algo

type __ = Obj.t

module V :
 sig
  type t = int

  val compare : int -> int -> int OrderedType.coq_Compare

  val eq_dec : int -> int -> bool
 end

module Lab :
 sig
  type coq_VertexLabel = int

  type coq_EdgeLabel = int
 end

module C :
 sig
  type coq_Label = int option

  val combine : coq_Label -> coq_Label -> coq_Label
 end

module DG :
 sig
  module VertexMap :
   sig
    module E :
     sig
      type t = int

      val compare : int -> int -> int OrderedType.coq_Compare

      val eq_dec : int -> int -> bool
     end

    module Raw :
     sig
      type key = int

      type 'elt tree =
      | Leaf
      | Node of 'elt tree * key * 'elt * 'elt tree * Int.Z_as_Int.t

      val tree_rect :
        'a2 -> ('a1 tree -> 'a2 -> key -> 'a1 -> 'a1 tree -> 'a2 ->
        Int.Z_as_Int.t -> 'a2) -> 'a1 tree -> 'a2

      val tree_rec :
        'a2 -> ('a1 tree -> 'a2 -> key -> 'a1 -> 'a1 tree -> 'a2 ->
        Int.Z_as_Int.t -> 'a2) -> 'a1 tree -> 'a2

      val height : 'a1 tree -> Int.Z_as_Int.t

      val cardinal : 'a1 tree -> int

      val empty : 'a1 tree

      val is_empty : 'a1 tree -> bool

      val mem : int -> 'a1 tree -> bool

      val find : int -> 'a1 tree -> 'a1 option

      val create : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val assert_false : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val bal : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val add : key -> 'a1 -> 'a1 tree -> 'a1 tree

      val remove_min :
        'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree * (key * 'a1)

      val merge : 'a1 tree -> 'a1 tree -> 'a1 tree

      val remove : int -> 'a1 tree -> 'a1 tree

      val join : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      type 'elt triple = { t_left : 'elt tree; t_opt : 'elt option;
                           t_right : 'elt tree }

      val t_left : 'a1 triple -> 'a1 tree

      val t_opt : 'a1 triple -> 'a1 option

      val t_right : 'a1 triple -> 'a1 tree

      val split : int -> 'a1 tree -> 'a1 triple

      val concat : 'a1 tree -> 'a1 tree -> 'a1 tree

      val elements_aux : (key * 'a1) list -> 'a1 tree -> (key * 'a1) list

      val elements : 'a1 tree -> (key * 'a1) list

      val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 tree -> 'a2 -> 'a2

      type 'elt enumeration =
      | End
      | More of key * 'elt * 'elt tree * 'elt enumeration

      val enumeration_rect :
        'a2 -> (key -> 'a1 -> 'a1 tree -> 'a1 enumeration -> 'a2 -> 'a2) ->
        'a1 enumeration -> 'a2

      val enumeration_rec :
        'a2 -> (key -> 'a1 -> 'a1 tree -> 'a1 enumeration -> 'a2 -> 'a2) ->
        'a1 enumeration -> 'a2

      val cons : 'a1 tree -> 'a1 enumeration -> 'a1 enumeration

      val equal_more :
        ('a1 -> 'a1 -> bool) -> int -> 'a1 -> ('a1 enumeration -> bool) ->
        'a1 enumeration -> bool

      val equal_cont :
        ('a1 -> 'a1 -> bool) -> 'a1 tree -> ('a1 enumeration -> bool) -> 'a1
        enumeration -> bool

      val equal_end : 'a1 enumeration -> bool

      val equal : ('a1 -> 'a1 -> bool) -> 'a1 tree -> 'a1 tree -> bool

      val map : ('a1 -> 'a2) -> 'a1 tree -> 'a2 tree

      val mapi : (key -> 'a1 -> 'a2) -> 'a1 tree -> 'a2 tree

      val map_option : (key -> 'a1 -> 'a2 option) -> 'a1 tree -> 'a2 tree

      val map2_opt :
        (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree) ->
        ('a2 tree -> 'a3 tree) -> 'a1 tree -> 'a2 tree -> 'a3 tree

      val map2 :
        ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 tree -> 'a2 tree ->
        'a3 tree

      module Proofs :
       sig
        module MX :
         sig
          module TO :
           sig
            type t = int
           end

          module IsTO :
           sig
            
           end

          module OrderTac :
           sig
            
           end

          val eq_dec : int -> int -> bool

          val lt_dec : int -> int -> bool

          val eqb : int -> int -> bool
         end

        module PX :
         sig
          module MO :
           sig
            module TO :
             sig
              type t = int
             end

            module IsTO :
             sig
              
             end

            module OrderTac :
             sig
              
             end

            val eq_dec : int -> int -> bool

            val lt_dec : int -> int -> bool

            val eqb : int -> int -> bool
           end
         end

        module L :
         sig
          module MX :
           sig
            module TO :
             sig
              type t = int
             end

            module IsTO :
             sig
              
             end

            module OrderTac :
             sig
              
             end

            val eq_dec : int -> int -> bool

            val lt_dec : int -> int -> bool

            val eqb : int -> int -> bool
           end

          module PX :
           sig
            module MO :
             sig
              module TO :
               sig
                type t = int
               end

              module IsTO :
               sig
                
               end

              module OrderTac :
               sig
                
               end

              val eq_dec : int -> int -> bool

              val lt_dec : int -> int -> bool

              val eqb : int -> int -> bool
             end
           end

          type key = int

          type 'elt t = (int * 'elt) list

          val empty : 'a1 t

          val is_empty : 'a1 t -> bool

          val mem : key -> 'a1 t -> bool

          type 'elt coq_R_mem =
          | R_mem_0 of 'elt t
          | R_mem_1 of 'elt t * int * 'elt * (int * 'elt) list
          | R_mem_2 of 'elt t * int * 'elt * (int * 'elt) list
          | R_mem_3 of 'elt t * int * 'elt * (int * 'elt) list * bool
             * 'elt coq_R_mem

          val coq_R_mem_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> bool -> 'a1
            coq_R_mem -> 'a2 -> 'a2) -> 'a1 t -> bool -> 'a1 coq_R_mem -> 'a2

          val coq_R_mem_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> bool -> 'a1
            coq_R_mem -> 'a2 -> 'a2) -> 'a1 t -> bool -> 'a1 coq_R_mem -> 'a2

          val mem_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2)
            -> 'a1 t -> 'a2

          val mem_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2)
            -> 'a1 t -> 'a2

          val coq_R_mem_correct : key -> 'a1 t -> bool -> 'a1 coq_R_mem

          val find : key -> 'a1 t -> 'a1 option

          type 'elt coq_R_find =
          | R_find_0 of 'elt t
          | R_find_1 of 'elt t * int * 'elt * (int * 'elt) list
          | R_find_2 of 'elt t * int * 'elt * (int * 'elt) list
          | R_find_3 of 'elt t * int * 'elt * (int * 'elt) list * 'elt option
             * 'elt coq_R_find

          val coq_R_find_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a1 option ->
            'a1 coq_R_find -> 'a2 -> 'a2) -> 'a1 t -> 'a1 option -> 'a1
            coq_R_find -> 'a2

          val coq_R_find_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a1 option ->
            'a1 coq_R_find -> 'a2 -> 'a2) -> 'a1 t -> 'a1 option -> 'a1
            coq_R_find -> 'a2

          val find_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2)
            -> 'a1 t -> 'a2

          val find_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2)
            -> 'a1 t -> 'a2

          val coq_R_find_correct :
            key -> 'a1 t -> 'a1 option -> 'a1 coq_R_find

          val add : key -> 'a1 -> 'a1 t -> 'a1 t

          type 'elt coq_R_add =
          | R_add_0 of 'elt t
          | R_add_1 of 'elt t * int * 'elt * (int * 'elt) list
          | R_add_2 of 'elt t * int * 'elt * (int * 'elt) list
          | R_add_3 of 'elt t * int * 'elt * (int * 'elt) list * 'elt t
             * 'elt coq_R_add

          val coq_R_add_rect :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a1 t -> 'a1
            coq_R_add -> 'a2 -> 'a2) -> 'a1 t -> 'a1 t -> 'a1 coq_R_add -> 'a2

          val coq_R_add_rec :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a1 t -> 'a1
            coq_R_add -> 'a2 -> 'a2) -> 'a1 t -> 'a1 t -> 'a1 coq_R_add -> 'a2

          val add_rect :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2)
            -> 'a1 t -> 'a2

          val add_rec :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2)
            -> 'a1 t -> 'a2

          val coq_R_add_correct :
            key -> 'a1 -> 'a1 t -> 'a1 t -> 'a1 coq_R_add

          val remove : key -> 'a1 t -> 'a1 t

          type 'elt coq_R_remove =
          | R_remove_0 of 'elt t
          | R_remove_1 of 'elt t * int * 'elt * (int * 'elt) list
          | R_remove_2 of 'elt t * int * 'elt * (int * 'elt) list
          | R_remove_3 of 'elt t * int * 'elt * (int * 'elt) list * 'elt t
             * 'elt coq_R_remove

          val coq_R_remove_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a1 t -> 'a1
            coq_R_remove -> 'a2 -> 'a2) -> 'a1 t -> 'a1 t -> 'a1 coq_R_remove
            -> 'a2

          val coq_R_remove_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a1 t -> 'a1
            coq_R_remove -> 'a2 -> 'a2) -> 'a1 t -> 'a1 t -> 'a1 coq_R_remove
            -> 'a2

          val remove_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2)
            -> 'a1 t -> 'a2

          val remove_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            int -> 'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2)
            -> 'a1 t -> 'a2

          val coq_R_remove_correct : key -> 'a1 t -> 'a1 t -> 'a1 coq_R_remove

          val elements : 'a1 t -> 'a1 t

          val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2

          type ('elt, 'a) coq_R_fold =
          | R_fold_0 of (key -> 'elt -> 'a -> 'a) * 'elt t * 'a
          | R_fold_1 of (key -> 'elt -> 'a -> 'a) * 'elt t * 'a * int * 
             'elt * (int * 'elt) list * 'a * ('elt, 'a) coq_R_fold

          val coq_R_fold_rect :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> ('a1, __) coq_R_fold -> 'a2 ->
            'a2) -> (key -> 'a1 -> 'a3 -> 'a3) -> 'a1 t -> 'a3 -> 'a3 ->
            ('a1, 'a3) coq_R_fold -> 'a2

          val coq_R_fold_rec :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> int -> 'a1 ->
            (int * 'a1) list -> __ -> __ -> ('a1, __) coq_R_fold -> 'a2 ->
            'a2) -> (key -> 'a1 -> 'a3 -> 'a3) -> 'a1 t -> 'a3 -> 'a3 ->
            ('a1, 'a3) coq_R_fold -> 'a2

          val fold_rect :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> int -> 'a1 ->
            (int * 'a1) list -> __ -> 'a2 -> 'a2) -> (key -> 'a1 -> 'a3 ->
            'a3) -> 'a1 t -> 'a3 -> 'a2

          val fold_rec :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> int -> 'a1 ->
            (int * 'a1) list -> __ -> 'a2 -> 'a2) -> (key -> 'a1 -> 'a3 ->
            'a3) -> 'a1 t -> 'a3 -> 'a2

          val coq_R_fold_correct :
            (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2 -> ('a1, 'a2)
            coq_R_fold

          val equal : ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool

          type 'elt coq_R_equal =
          | R_equal_0 of 'elt t * 'elt t
          | R_equal_1 of 'elt t * 'elt t * int * 'elt * (int * 'elt) list
             * int * 'elt * (int * 'elt) list * bool * 'elt coq_R_equal
          | R_equal_2 of 'elt t * 'elt t * int * 'elt * (int * 'elt) list
             * int * 'elt * (int * 'elt) list * int OrderedType.coq_Compare
          | R_equal_3 of 'elt t * 'elt t * 'elt t * 'elt t

          val coq_R_equal_rect :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> int -> 'a1 -> (int * 'a1) list -> __ -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> bool -> 'a1
            coq_R_equal -> 'a2 -> 'a2) -> ('a1 t -> 'a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> int -> 'a1 -> (int * 'a1) list -> __ ->
            int OrderedType.coq_Compare -> __ -> __ -> 'a2) -> ('a1 t -> 'a1
            t -> 'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) -> 'a1 t -> 'a1 t
            -> bool -> 'a1 coq_R_equal -> 'a2

          val coq_R_equal_rec :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> int -> 'a1 -> (int * 'a1) list -> __ -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> bool -> 'a1
            coq_R_equal -> 'a2 -> 'a2) -> ('a1 t -> 'a1 t -> int -> 'a1 ->
            (int * 'a1) list -> __ -> int -> 'a1 -> (int * 'a1) list -> __ ->
            int OrderedType.coq_Compare -> __ -> __ -> 'a2) -> ('a1 t -> 'a1
            t -> 'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) -> 'a1 t -> 'a1 t
            -> bool -> 'a1 coq_R_equal -> 'a2

          val equal_rect :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> int -> 'a1 -> (int * 'a1) list -> __ -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2) -> ('a1
            t -> 'a1 t -> int -> 'a1 -> (int * 'a1) list -> __ -> int -> 'a1
            -> (int * 'a1) list -> __ -> int OrderedType.coq_Compare -> __ ->
            __ -> 'a2) -> ('a1 t -> 'a1 t -> 'a1 t -> __ -> 'a1 t -> __ -> __
            -> 'a2) -> 'a1 t -> 'a1 t -> 'a2

          val equal_rec :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> int -> 'a1 -> (int * 'a1) list -> __ -> int ->
            'a1 -> (int * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2) -> ('a1
            t -> 'a1 t -> int -> 'a1 -> (int * 'a1) list -> __ -> int -> 'a1
            -> (int * 'a1) list -> __ -> int OrderedType.coq_Compare -> __ ->
            __ -> 'a2) -> ('a1 t -> 'a1 t -> 'a1 t -> __ -> 'a1 t -> __ -> __
            -> 'a2) -> 'a1 t -> 'a1 t -> 'a2

          val coq_R_equal_correct :
            ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool -> 'a1 coq_R_equal

          val map : ('a1 -> 'a2) -> 'a1 t -> 'a2 t

          val mapi : (key -> 'a1 -> 'a2) -> 'a1 t -> 'a2 t

          val option_cons :
            key -> 'a1 option -> (key * 'a1) list -> (key * 'a1) list

          val map2_l :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a3 t

          val map2_r :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a2 t -> 'a3 t

          val map2 :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t -> 'a3
            t

          val combine : 'a1 t -> 'a2 t -> ('a1 option * 'a2 option) t

          val fold_right_pair :
            ('a1 -> 'a2 -> 'a3 -> 'a3) -> ('a1 * 'a2) list -> 'a3 -> 'a3

          val map2_alt :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t ->
            (key * 'a3) list

          val at_least_one :
            'a1 option -> 'a2 option -> ('a1 option * 'a2 option) option

          val at_least_one_then_f :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 option -> 'a2
            option -> 'a3 option
         end

        type 'elt coq_R_mem =
        | R_mem_0 of 'elt tree
        | R_mem_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * bool * 'elt coq_R_mem
        | R_mem_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_mem_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * bool * 'elt coq_R_mem

        val coq_R_mem_rect :
          int -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> bool -> 'a1
          coq_R_mem -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 ->
          'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1 tree
          -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __
          -> __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> 'a1 tree -> bool
          -> 'a1 coq_R_mem -> 'a2

        val coq_R_mem_rec :
          int -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> bool -> 'a1
          coq_R_mem -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 ->
          'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1 tree
          -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __
          -> __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> 'a1 tree -> bool
          -> 'a1 coq_R_mem -> 'a2

        type 'elt coq_R_find =
        | R_find_0 of 'elt tree
        | R_find_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt option * 'elt coq_R_find
        | R_find_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_find_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt option * 'elt coq_R_find

        val coq_R_find_rect :
          int -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 option
          -> 'a1 coq_R_find -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1
          tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a1 option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> 'a1
          tree -> 'a1 option -> 'a1 coq_R_find -> 'a2

        val coq_R_find_rec :
          int -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 option
          -> 'a1 coq_R_find -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1
          tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a1 option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> 'a1
          tree -> 'a1 option -> 'a1 coq_R_find -> 'a2

        type 'elt coq_R_bal =
        | R_bal_0 of 'elt tree * key * 'elt * 'elt tree
        | R_bal_1 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_2 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_3 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 
           'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_4 of 'elt tree * key * 'elt * 'elt tree
        | R_bal_5 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_6 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_7 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 
           'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_8 of 'elt tree * key * 'elt * 'elt tree

        val coq_R_bal_rect :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> __ ->
          'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2)
          -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __
          -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ ->
          __ -> __ -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> __ -> 'a2) -> ('a1 tree -> key
          -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2) -> ('a1 tree
          -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a2) -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_bal -> 'a2

        val coq_R_bal_rec :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> __ ->
          'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2)
          -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __
          -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ ->
          __ -> __ -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> __ -> 'a2) -> ('a1 tree -> key
          -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2) -> ('a1 tree
          -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a2) -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_bal -> 'a2

        type 'elt coq_R_add =
        | R_add_0 of 'elt tree
        | R_add_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_add
        | R_add_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_add_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_add

        val coq_R_add_rect :
          key -> 'a1 -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          tree -> 'a1 coq_R_add -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_add ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_add -> 'a2

        val coq_R_add_rec :
          key -> 'a1 -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          tree -> 'a1 coq_R_add -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_add ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_add -> 'a2

        type 'elt coq_R_remove_min =
        | R_remove_min_0 of 'elt tree * key * 'elt * 'elt tree
        | R_remove_min_1 of 'elt tree * key * 'elt * 'elt tree * 'elt tree
           * key * 'elt * 'elt tree * Int.Z_as_Int.t
           * ('elt tree * (key * 'elt)) * 'elt coq_R_remove_min * 'elt tree
           * (key * 'elt)

        val coq_R_remove_min_rect :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> 'a2) -> ('a1 tree ->
          key -> 'a1 -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> ('a1 tree * (key * 'a1)) -> 'a1
          coq_R_remove_min -> 'a2 -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> ('a1 tree * (key * 'a1)) ->
          'a1 coq_R_remove_min -> 'a2

        val coq_R_remove_min_rec :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> 'a2) -> ('a1 tree ->
          key -> 'a1 -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> ('a1 tree * (key * 'a1)) -> 'a1
          coq_R_remove_min -> 'a2 -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> ('a1 tree * (key * 'a1)) ->
          'a1 coq_R_remove_min -> 'a2

        type 'elt coq_R_merge =
        | R_merge_0 of 'elt tree * 'elt tree
        | R_merge_1 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_merge_2 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * (key * 'elt) * key * 'elt

        val coq_R_merge_rect :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> key -> 'a1
          -> __ -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1
          coq_R_merge -> 'a2

        val coq_R_merge_rec :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> key -> 'a1
          -> __ -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1
          coq_R_merge -> 'a2

        type 'elt coq_R_remove =
        | R_remove_0 of 'elt tree
        | R_remove_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_remove
        | R_remove_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_remove_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_remove

        val coq_R_remove_rect :
          int -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree ->
          'a1 coq_R_remove -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1
          tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a1 tree -> 'a1 coq_R_remove -> 'a2 -> 'a2) -> 'a1
          tree -> 'a1 tree -> 'a1 coq_R_remove -> 'a2

        val coq_R_remove_rec :
          int -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree ->
          'a1 coq_R_remove -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1
          tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a1 tree -> 'a1 coq_R_remove -> 'a2 -> 'a2) -> 'a1
          tree -> 'a1 tree -> 'a1 coq_R_remove -> 'a2

        type 'elt coq_R_concat =
        | R_concat_0 of 'elt tree * 'elt tree
        | R_concat_1 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_concat_2 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * (key * 'elt)

        val coq_R_concat_rect :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_concat -> 'a2

        val coq_R_concat_rec :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_concat -> 'a2

        type 'elt coq_R_split =
        | R_split_0 of 'elt tree
        | R_split_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt triple * 'elt coq_R_split * 'elt tree
           * 'elt option * 'elt tree
        | R_split_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_split_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt triple * 'elt coq_R_split * 'elt tree
           * 'elt option * 'elt tree

        val coq_R_split_rect :
          int -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 triple
          -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1 option -> 'a1 tree ->
          __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a1 triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1 option ->
          'a1 tree -> __ -> 'a2) -> 'a1 tree -> 'a1 triple -> 'a1 coq_R_split
          -> 'a2

        val coq_R_split_rec :
          int -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 triple
          -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1 option -> 'a1 tree ->
          __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a1 triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1 option ->
          'a1 tree -> __ -> 'a2) -> 'a1 tree -> 'a1 triple -> 'a1 coq_R_split
          -> 'a2

        type ('elt, 'x) coq_R_map_option =
        | R_map_option_0 of 'elt tree
        | R_map_option_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'x * 'x tree * ('elt, 'x) coq_R_map_option
           * 'x tree * ('elt, 'x) coq_R_map_option
        | R_map_option_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'x tree * ('elt, 'x) coq_R_map_option * 
           'x tree * ('elt, 'x) coq_R_map_option

        val coq_R_map_option_rect :
          (key -> 'a1 -> 'a2 option) -> ('a1 tree -> __ -> 'a3) -> ('a1 tree
          -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ ->
          'a2 -> __ -> 'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2
          tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a3) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2 tree -> ('a1,
          'a2) coq_R_map_option -> 'a3 -> 'a3) -> 'a1 tree -> 'a2 tree ->
          ('a1, 'a2) coq_R_map_option -> 'a3

        val coq_R_map_option_rec :
          (key -> 'a1 -> 'a2 option) -> ('a1 tree -> __ -> 'a3) -> ('a1 tree
          -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ ->
          'a2 -> __ -> 'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2
          tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a3) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2 tree -> ('a1,
          'a2) coq_R_map_option -> 'a3 -> 'a3) -> 'a1 tree -> 'a2 tree ->
          ('a1, 'a2) coq_R_map_option -> 'a3

        type ('elt, 'x0, 'x) coq_R_map2_opt =
        | R_map2_opt_0 of 'elt tree * 'x0 tree
        | R_map2_opt_1 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_map2_opt_2 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'x0 tree * key * 'x0 * 'x0 tree
           * Int.Z_as_Int.t * 'x0 tree * 'x0 option * 'x0 tree * 'x * 
           'x tree * ('elt, 'x0, 'x) coq_R_map2_opt * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt
        | R_map2_opt_3 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'x0 tree * key * 'x0 * 'x0 tree
           * Int.Z_as_Int.t * 'x0 tree * 'x0 option * 'x0 tree * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt

        val coq_R_map2_opt_rect :
          (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree)
          -> ('a2 tree -> 'a3 tree) -> ('a1 tree -> 'a2 tree -> __ -> 'a4) ->
          ('a1 tree -> 'a2 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree
          -> key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          'a2 option -> 'a2 tree -> __ -> 'a3 -> __ -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree -> 'a2
          option -> 'a2 tree -> __ -> __ -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3) coq_R_map2_opt
          -> 'a4 -> 'a4) -> 'a1 tree -> 'a2 tree -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4

        val coq_R_map2_opt_rec :
          (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree)
          -> ('a2 tree -> 'a3 tree) -> ('a1 tree -> 'a2 tree -> __ -> 'a4) ->
          ('a1 tree -> 'a2 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree
          -> key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          'a2 option -> 'a2 tree -> __ -> 'a3 -> __ -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree -> 'a2
          option -> 'a2 tree -> __ -> __ -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3) coq_R_map2_opt
          -> 'a4 -> 'a4) -> 'a1 tree -> 'a2 tree -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4

        val fold' : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 tree -> 'a2 -> 'a2

        val flatten_e : 'a1 enumeration -> (key * 'a1) list
       end
     end

    type 'elt bst =
      'elt Raw.tree
      (* singleton inductive, whose constructor was Bst *)

    val this : 'a1 bst -> 'a1 Raw.tree

    type 'elt t = 'elt bst

    type key = int

    val empty : 'a1 t

    val is_empty : 'a1 t -> bool

    val add : key -> 'a1 -> 'a1 t -> 'a1 t

    val remove : key -> 'a1 t -> 'a1 t

    val mem : key -> 'a1 t -> bool

    val find : key -> 'a1 t -> 'a1 option

    val map : ('a1 -> 'a2) -> 'a1 t -> 'a2 t

    val mapi : (key -> 'a1 -> 'a2) -> 'a1 t -> 'a2 t

    val map2 :
      ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t -> 'a3 t

    val elements : 'a1 t -> (key * 'a1) list

    val cardinal : 'a1 t -> int

    val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2

    val equal : ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool
   end

  module Edge :
   sig
    module VertexPair :
     sig
      module MO1 :
       sig
        module TO :
         sig
          type t = int
         end

        module IsTO :
         sig
          
         end

        module OrderTac :
         sig
          
         end

        val eq_dec : int -> int -> bool

        val lt_dec : int -> int -> bool

        val eqb : int -> int -> bool
       end

      module MO2 :
       sig
        module TO :
         sig
          type t = int
         end

        module IsTO :
         sig
          
         end

        module OrderTac :
         sig
          
         end

        val eq_dec : int -> int -> bool

        val lt_dec : int -> int -> bool

        val eqb : int -> int -> bool
       end

      type t = int * int

      val compare : t -> t -> (int * int) OrderedType.coq_Compare

      val eq_dec : t -> t -> bool
     end

    module MO1 :
     sig
      module TO :
       sig
        type t = int
       end

      module IsTO :
       sig
        
       end

      module OrderTac :
       sig
        
       end

      val eq_dec : int -> int -> bool

      val lt_dec : int -> int -> bool

      val eqb : int -> int -> bool
     end

    module MO2 :
     sig
      module TO :
       sig
        type t = int
       end

      module IsTO :
       sig
        
       end

      module OrderTac :
       sig
        
       end

      val eq_dec : int -> int -> bool

      val lt_dec : int -> int -> bool

      val eqb : int -> int -> bool
     end

    type t = int * int

    val compare : t -> t -> (int * int) OrderedType.coq_Compare

    val eq_dec : t -> t -> bool

    module OTFacts :
     sig
      module TO :
       sig
        type t = int
       end

      module IsTO :
       sig
        
       end

      module OrderTac :
       sig
        
       end

      val eq_dec : int -> int -> bool

      val lt_dec : int -> int -> bool

      val eqb : int -> int -> bool
     end

    module OTPairFacts :
     sig
      module TO :
       sig
        type t = int * int
       end

      module IsTO :
       sig
        
       end

      module OrderTac :
       sig
        
       end

      val eq_dec : (int * int) -> (int * int) -> bool

      val lt_dec : (int * int) -> (int * int) -> bool

      val eqb : (int * int) -> (int * int) -> bool
     end

    val fst_end : t -> int

    val snd_end : t -> int

    val permute : t -> int * int
   end

  module EdgeMap :
   sig
    module E :
     sig
      type t = int * int

      val compare :
        (int * int) -> (int * int) -> (int * int) OrderedType.coq_Compare

      val eq_dec : (int * int) -> (int * int) -> bool
     end

    module Raw :
     sig
      type key = int * int

      type 'elt tree =
      | Leaf
      | Node of 'elt tree * key * 'elt * 'elt tree * Int.Z_as_Int.t

      val tree_rect :
        'a2 -> ('a1 tree -> 'a2 -> key -> 'a1 -> 'a1 tree -> 'a2 ->
        Int.Z_as_Int.t -> 'a2) -> 'a1 tree -> 'a2

      val tree_rec :
        'a2 -> ('a1 tree -> 'a2 -> key -> 'a1 -> 'a1 tree -> 'a2 ->
        Int.Z_as_Int.t -> 'a2) -> 'a1 tree -> 'a2

      val height : 'a1 tree -> Int.Z_as_Int.t

      val cardinal : 'a1 tree -> int

      val empty : 'a1 tree

      val is_empty : 'a1 tree -> bool

      val mem : (int * int) -> 'a1 tree -> bool

      val find : (int * int) -> 'a1 tree -> 'a1 option

      val create : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val assert_false : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val bal : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      val add : key -> 'a1 -> 'a1 tree -> 'a1 tree

      val remove_min :
        'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree * (key * 'a1)

      val merge : 'a1 tree -> 'a1 tree -> 'a1 tree

      val remove : (int * int) -> 'a1 tree -> 'a1 tree

      val join : 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

      type 'elt triple = { t_left : 'elt tree; t_opt : 'elt option;
                           t_right : 'elt tree }

      val t_left : 'a1 triple -> 'a1 tree

      val t_opt : 'a1 triple -> 'a1 option

      val t_right : 'a1 triple -> 'a1 tree

      val split : (int * int) -> 'a1 tree -> 'a1 triple

      val concat : 'a1 tree -> 'a1 tree -> 'a1 tree

      val elements_aux : (key * 'a1) list -> 'a1 tree -> (key * 'a1) list

      val elements : 'a1 tree -> (key * 'a1) list

      val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 tree -> 'a2 -> 'a2

      type 'elt enumeration =
      | End
      | More of key * 'elt * 'elt tree * 'elt enumeration

      val enumeration_rect :
        'a2 -> (key -> 'a1 -> 'a1 tree -> 'a1 enumeration -> 'a2 -> 'a2) ->
        'a1 enumeration -> 'a2

      val enumeration_rec :
        'a2 -> (key -> 'a1 -> 'a1 tree -> 'a1 enumeration -> 'a2 -> 'a2) ->
        'a1 enumeration -> 'a2

      val cons : 'a1 tree -> 'a1 enumeration -> 'a1 enumeration

      val equal_more :
        ('a1 -> 'a1 -> bool) -> (int * int) -> 'a1 -> ('a1 enumeration ->
        bool) -> 'a1 enumeration -> bool

      val equal_cont :
        ('a1 -> 'a1 -> bool) -> 'a1 tree -> ('a1 enumeration -> bool) -> 'a1
        enumeration -> bool

      val equal_end : 'a1 enumeration -> bool

      val equal : ('a1 -> 'a1 -> bool) -> 'a1 tree -> 'a1 tree -> bool

      val map : ('a1 -> 'a2) -> 'a1 tree -> 'a2 tree

      val mapi : (key -> 'a1 -> 'a2) -> 'a1 tree -> 'a2 tree

      val map_option : (key -> 'a1 -> 'a2 option) -> 'a1 tree -> 'a2 tree

      val map2_opt :
        (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree) ->
        ('a2 tree -> 'a3 tree) -> 'a1 tree -> 'a2 tree -> 'a3 tree

      val map2 :
        ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 tree -> 'a2 tree ->
        'a3 tree

      module Proofs :
       sig
        module MX :
         sig
          module TO :
           sig
            type t = int * int
           end

          module IsTO :
           sig
            
           end

          module OrderTac :
           sig
            
           end

          val eq_dec : (int * int) -> (int * int) -> bool

          val lt_dec : (int * int) -> (int * int) -> bool

          val eqb : (int * int) -> (int * int) -> bool
         end

        module PX :
         sig
          module MO :
           sig
            module TO :
             sig
              type t = int * int
             end

            module IsTO :
             sig
              
             end

            module OrderTac :
             sig
              
             end

            val eq_dec : (int * int) -> (int * int) -> bool

            val lt_dec : (int * int) -> (int * int) -> bool

            val eqb : (int * int) -> (int * int) -> bool
           end
         end

        module L :
         sig
          module MX :
           sig
            module TO :
             sig
              type t = int * int
             end

            module IsTO :
             sig
              
             end

            module OrderTac :
             sig
              
             end

            val eq_dec : (int * int) -> (int * int) -> bool

            val lt_dec : (int * int) -> (int * int) -> bool

            val eqb : (int * int) -> (int * int) -> bool
           end

          module PX :
           sig
            module MO :
             sig
              module TO :
               sig
                type t = int * int
               end

              module IsTO :
               sig
                
               end

              module OrderTac :
               sig
                
               end

              val eq_dec : (int * int) -> (int * int) -> bool

              val lt_dec : (int * int) -> (int * int) -> bool

              val eqb : (int * int) -> (int * int) -> bool
             end
           end

          type key = int * int

          type 'elt t = ((int * int) * 'elt) list

          val empty : 'a1 t

          val is_empty : 'a1 t -> bool

          val mem : key -> 'a1 t -> bool

          type 'elt coq_R_mem =
          | R_mem_0 of 'elt t
          | R_mem_1 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list
          | R_mem_2 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list
          | R_mem_3 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list * bool * 'elt coq_R_mem

          val coq_R_mem_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) ->
            'a1 t -> bool -> 'a1 coq_R_mem -> 'a2

          val coq_R_mem_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) ->
            'a1 t -> bool -> 'a1 coq_R_mem -> 'a2

          val mem_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val mem_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val coq_R_mem_correct : key -> 'a1 t -> bool -> 'a1 coq_R_mem

          val find : key -> 'a1 t -> 'a1 option

          type 'elt coq_R_find =
          | R_find_0 of 'elt t
          | R_find_1 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list
          | R_find_2 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list
          | R_find_3 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list * 'elt option * 'elt coq_R_find

          val coq_R_find_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> 'a1 option -> 'a1 coq_R_find -> 'a2 ->
            'a2) -> 'a1 t -> 'a1 option -> 'a1 coq_R_find -> 'a2

          val coq_R_find_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> 'a1 option -> 'a1 coq_R_find -> 'a2 ->
            'a2) -> 'a1 t -> 'a1 option -> 'a1 coq_R_find -> 'a2

          val find_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val find_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val coq_R_find_correct :
            key -> 'a1 t -> 'a1 option -> 'a1 coq_R_find

          val add : key -> 'a1 -> 'a1 t -> 'a1 t

          type 'elt coq_R_add =
          | R_add_0 of 'elt t
          | R_add_1 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list
          | R_add_2 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list
          | R_add_3 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list * 'elt t * 'elt coq_R_add

          val coq_R_add_rect :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) ->
            'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1
            t -> (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __
            -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a1 t -> 'a1
            coq_R_add -> 'a2 -> 'a2) -> 'a1 t -> 'a1 t -> 'a1 coq_R_add -> 'a2

          val coq_R_add_rec :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) ->
            'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1
            t -> (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __
            -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a1 t -> 'a1
            coq_R_add -> 'a2 -> 'a2) -> 'a1 t -> 'a1 t -> 'a1 coq_R_add -> 'a2

          val add_rect :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) ->
            'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1
            t -> (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __
            -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a1
            t -> 'a2

          val add_rec :
            key -> 'a1 -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) ->
            'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1
            t -> (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __
            -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a1
            t -> 'a2

          val coq_R_add_correct :
            key -> 'a1 -> 'a1 t -> 'a1 t -> 'a1 coq_R_add

          val remove : key -> 'a1 t -> 'a1 t

          type 'elt coq_R_remove =
          | R_remove_0 of 'elt t
          | R_remove_1 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list
          | R_remove_2 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list
          | R_remove_3 of 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list * 'elt t * 'elt coq_R_remove

          val coq_R_remove_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> 'a1 t -> 'a1 coq_R_remove -> 'a2 ->
            'a2) -> 'a1 t -> 'a1 t -> 'a1 coq_R_remove -> 'a2

          val coq_R_remove_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> 'a1 t -> 'a1 coq_R_remove -> 'a2 ->
            'a2) -> 'a1 t -> 'a1 t -> 'a1 coq_R_remove -> 'a2

          val remove_rect :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val remove_rec :
            key -> ('a1 t -> __ -> 'a2) -> ('a1 t -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> __ -> __ -> 'a2) -> ('a1 t ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ -> __ -> __
            -> 'a2) -> ('a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1)
            list -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a1 t -> 'a2

          val coq_R_remove_correct : key -> 'a1 t -> 'a1 t -> 'a1 coq_R_remove

          val elements : 'a1 t -> 'a1 t

          val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2

          type ('elt, 'a) coq_R_fold =
          | R_fold_0 of (key -> 'elt -> 'a -> 'a) * 'elt t * 'a
          | R_fold_1 of (key -> 'elt -> 'a -> 'a) * 'elt t * 'a * (int * int)
             * 'elt * ((int * int) * 'elt) list * 'a * ('elt, 'a) coq_R_fold

          val coq_R_fold_rect :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> (int * int) ->
            'a1 -> ((int * int) * 'a1) list -> __ -> __ -> ('a1, __)
            coq_R_fold -> 'a2 -> 'a2) -> (key -> 'a1 -> 'a3 -> 'a3) -> 'a1 t
            -> 'a3 -> 'a3 -> ('a1, 'a3) coq_R_fold -> 'a2

          val coq_R_fold_rec :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> (int * int) ->
            'a1 -> ((int * int) * 'a1) list -> __ -> __ -> ('a1, __)
            coq_R_fold -> 'a2 -> 'a2) -> (key -> 'a1 -> 'a3 -> 'a3) -> 'a1 t
            -> 'a3 -> 'a3 -> ('a1, 'a3) coq_R_fold -> 'a2

          val fold_rect :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> (int * int) ->
            'a1 -> ((int * int) * 'a1) list -> __ -> 'a2 -> 'a2) -> (key ->
            'a1 -> 'a3 -> 'a3) -> 'a1 t -> 'a3 -> 'a2

          val fold_rec :
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> __ -> 'a2) ->
            (__ -> (key -> 'a1 -> __ -> __) -> 'a1 t -> __ -> (int * int) ->
            'a1 -> ((int * int) * 'a1) list -> __ -> 'a2 -> 'a2) -> (key ->
            'a1 -> 'a3 -> 'a3) -> 'a1 t -> 'a3 -> 'a2

          val coq_R_fold_correct :
            (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2 -> ('a1, 'a2)
            coq_R_fold

          val equal : ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool

          type 'elt coq_R_equal =
          | R_equal_0 of 'elt t * 'elt t
          | R_equal_1 of 'elt t * 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list * (int * int) * 'elt
             * ((int * int) * 'elt) list * bool * 'elt coq_R_equal
          | R_equal_2 of 'elt t * 'elt t * (int * int) * 'elt
             * ((int * int) * 'elt) list * (int * int) * 'elt
             * ((int * int) * 'elt) list
             * (int * int) OrderedType.coq_Compare
          | R_equal_3 of 'elt t * 'elt t * 'elt t * 'elt t

          val coq_R_equal_rect :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1) list
            -> __ -> (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ ->
            __ -> __ -> bool -> 'a1 coq_R_equal -> 'a2 -> 'a2) -> ('a1 t ->
            'a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ ->
            (int * int) OrderedType.coq_Compare -> __ -> __ -> 'a2) -> ('a1 t
            -> 'a1 t -> 'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) -> 'a1 t ->
            'a1 t -> bool -> 'a1 coq_R_equal -> 'a2

          val coq_R_equal_rec :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1) list
            -> __ -> (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ ->
            __ -> __ -> bool -> 'a1 coq_R_equal -> 'a2 -> 'a2) -> ('a1 t ->
            'a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ ->
            (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ ->
            (int * int) OrderedType.coq_Compare -> __ -> __ -> 'a2) -> ('a1 t
            -> 'a1 t -> 'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) -> 'a1 t ->
            'a1 t -> bool -> 'a1 coq_R_equal -> 'a2

          val equal_rect :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1) list
            -> __ -> (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ ->
            __ -> __ -> 'a2 -> 'a2) -> ('a1 t -> 'a1 t -> (int * int) -> 'a1
            -> ((int * int) * 'a1) list -> __ -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> (int * int)
            OrderedType.coq_Compare -> __ -> __ -> 'a2) -> ('a1 t -> 'a1 t ->
            'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) -> 'a1 t -> 'a1 t -> 'a2

          val equal_rec :
            ('a1 -> 'a1 -> bool) -> ('a1 t -> 'a1 t -> __ -> __ -> 'a2) ->
            ('a1 t -> 'a1 t -> (int * int) -> 'a1 -> ((int * int) * 'a1) list
            -> __ -> (int * int) -> 'a1 -> ((int * int) * 'a1) list -> __ ->
            __ -> __ -> 'a2 -> 'a2) -> ('a1 t -> 'a1 t -> (int * int) -> 'a1
            -> ((int * int) * 'a1) list -> __ -> (int * int) -> 'a1 ->
            ((int * int) * 'a1) list -> __ -> (int * int)
            OrderedType.coq_Compare -> __ -> __ -> 'a2) -> ('a1 t -> 'a1 t ->
            'a1 t -> __ -> 'a1 t -> __ -> __ -> 'a2) -> 'a1 t -> 'a1 t -> 'a2

          val coq_R_equal_correct :
            ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool -> 'a1 coq_R_equal

          val map : ('a1 -> 'a2) -> 'a1 t -> 'a2 t

          val mapi : (key -> 'a1 -> 'a2) -> 'a1 t -> 'a2 t

          val option_cons :
            key -> 'a1 option -> (key * 'a1) list -> (key * 'a1) list

          val map2_l :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a3 t

          val map2_r :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a2 t -> 'a3 t

          val map2 :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t -> 'a3
            t

          val combine : 'a1 t -> 'a2 t -> ('a1 option * 'a2 option) t

          val fold_right_pair :
            ('a1 -> 'a2 -> 'a3 -> 'a3) -> ('a1 * 'a2) list -> 'a3 -> 'a3

          val map2_alt :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t ->
            (key * 'a3) list

          val at_least_one :
            'a1 option -> 'a2 option -> ('a1 option * 'a2 option) option

          val at_least_one_then_f :
            ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 option -> 'a2
            option -> 'a3 option
         end

        type 'elt coq_R_mem =
        | R_mem_0 of 'elt tree
        | R_mem_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * bool * 'elt coq_R_mem
        | R_mem_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_mem_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * bool * 'elt coq_R_mem

        val coq_R_mem_rect :
          (int * int) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> bool
          -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1
          tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> 'a1 tree ->
          bool -> 'a1 coq_R_mem -> 'a2

        val coq_R_mem_rec :
          (int * int) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> bool
          -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1
          tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> bool -> 'a1 coq_R_mem -> 'a2 -> 'a2) -> 'a1 tree ->
          bool -> 'a1 coq_R_mem -> 'a2

        type 'elt coq_R_find =
        | R_find_0 of 'elt tree
        | R_find_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt option * 'elt coq_R_find
        | R_find_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_find_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt option * 'elt coq_R_find

        val coq_R_find_rect :
          (int * int) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 option -> 'a1 coq_R_find ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 option -> 'a1 coq_R_find -> 'a2

        val coq_R_find_rec :
          (int * int) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          option -> 'a1 coq_R_find -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 option -> 'a1 coq_R_find ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 option -> 'a1 coq_R_find -> 'a2

        type 'elt coq_R_bal =
        | R_bal_0 of 'elt tree * key * 'elt * 'elt tree
        | R_bal_1 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_2 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_3 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 
           'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_4 of 'elt tree * key * 'elt * 'elt tree
        | R_bal_5 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_6 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_7 of 'elt tree * key * 'elt * 'elt tree * 'elt tree * 
           key * 'elt * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 
           'elt * 'elt tree * Int.Z_as_Int.t
        | R_bal_8 of 'elt tree * key * 'elt * 'elt tree

        val coq_R_bal_rect :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> __ ->
          'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2)
          -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __
          -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ ->
          __ -> __ -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> __ -> 'a2) -> ('a1 tree -> key
          -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2) -> ('a1 tree
          -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a2) -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_bal -> 'a2

        val coq_R_bal_rec :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) ->
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree -> key
          -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> __ ->
          'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2)
          -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ ->
          __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __
          -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __
          -> __ -> __ -> 'a2) -> ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ ->
          __ -> __ -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> __ -> 'a2) -> ('a1 tree -> key
          -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a1 tree -> key ->
          'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2) -> ('a1 tree
          -> key -> 'a1 -> 'a1 tree -> __ -> __ -> __ -> __ -> 'a2) -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_bal -> 'a2

        type 'elt coq_R_add =
        | R_add_0 of 'elt tree
        | R_add_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_add
        | R_add_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_add_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_add

        val coq_R_add_rect :
          key -> 'a1 -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          tree -> 'a1 coq_R_add -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_add ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_add -> 'a2

        val coq_R_add_rec :
          key -> 'a1 -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          tree -> 'a1 coq_R_add -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_add ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_add -> 'a2

        type 'elt coq_R_remove_min =
        | R_remove_min_0 of 'elt tree * key * 'elt * 'elt tree
        | R_remove_min_1 of 'elt tree * key * 'elt * 'elt tree * 'elt tree
           * key * 'elt * 'elt tree * Int.Z_as_Int.t
           * ('elt tree * (key * 'elt)) * 'elt coq_R_remove_min * 'elt tree
           * (key * 'elt)

        val coq_R_remove_min_rect :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> 'a2) -> ('a1 tree ->
          key -> 'a1 -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> ('a1 tree * (key * 'a1)) -> 'a1
          coq_R_remove_min -> 'a2 -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> ('a1 tree * (key * 'a1)) ->
          'a1 coq_R_remove_min -> 'a2

        val coq_R_remove_min_rec :
          ('a1 tree -> key -> 'a1 -> 'a1 tree -> __ -> 'a2) -> ('a1 tree ->
          key -> 'a1 -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> ('a1 tree * (key * 'a1)) -> 'a1
          coq_R_remove_min -> 'a2 -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> ('a1 tree * (key * 'a1)) ->
          'a1 coq_R_remove_min -> 'a2

        type 'elt coq_R_merge =
        | R_merge_0 of 'elt tree * 'elt tree
        | R_merge_1 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_merge_2 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * (key * 'elt) * key * 'elt

        val coq_R_merge_rect :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> key -> 'a1
          -> __ -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1
          coq_R_merge -> 'a2

        val coq_R_merge_rec :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> key -> 'a1
          -> __ -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1
          coq_R_merge -> 'a2

        type 'elt coq_R_remove =
        | R_remove_0 of 'elt tree
        | R_remove_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_remove
        | R_remove_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_remove_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * 'elt coq_R_remove

        val coq_R_remove_rect :
          (int * int) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          tree -> 'a1 coq_R_remove -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_remove ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_remove -> 'a2

        val coq_R_remove_rec :
          (int * int) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          tree -> 'a1 coq_R_remove -> 'a2 -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2)
          -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1 tree -> 'a1 coq_R_remove ->
          'a2 -> 'a2) -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_remove -> 'a2

        type 'elt coq_R_concat =
        | R_concat_0 of 'elt tree * 'elt tree
        | R_concat_1 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_concat_2 of 'elt tree * 'elt tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt tree * (key * 'elt)

        val coq_R_concat_rect :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_concat -> 'a2

        val coq_R_concat_rec :
          ('a1 tree -> 'a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2) -> ('a1 tree -> 'a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree
          -> Int.Z_as_Int.t -> __ -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> 'a1 tree -> (key * 'a1) -> __ -> 'a2) ->
          'a1 tree -> 'a1 tree -> 'a1 tree -> 'a1 coq_R_concat -> 'a2

        type 'elt coq_R_split =
        | R_split_0 of 'elt tree
        | R_split_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt triple * 'elt coq_R_split * 'elt tree
           * 'elt option * 'elt tree
        | R_split_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t
        | R_split_3 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'elt triple * 'elt coq_R_split * 'elt tree
           * 'elt option * 'elt tree

        val coq_R_split_rect :
          (int * int) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1 option -> 'a1
          tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1
          tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a1 triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1
          option -> 'a1 tree -> __ -> 'a2) -> 'a1 tree -> 'a1 triple -> 'a1
          coq_R_split -> 'a2

        val coq_R_split_rec :
          (int * int) -> ('a1 tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree ->
          key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a1
          triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1 option -> 'a1
          tree -> __ -> 'a2) -> ('a1 tree -> 'a1 tree -> key -> 'a1 -> 'a1
          tree -> Int.Z_as_Int.t -> __ -> __ -> __ -> 'a2) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          __ -> 'a1 triple -> 'a1 coq_R_split -> 'a2 -> 'a1 tree -> 'a1
          option -> 'a1 tree -> __ -> 'a2) -> 'a1 tree -> 'a1 triple -> 'a1
          coq_R_split -> 'a2

        type ('elt, 'x) coq_R_map_option =
        | R_map_option_0 of 'elt tree
        | R_map_option_1 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'x * 'x tree * ('elt, 'x) coq_R_map_option
           * 'x tree * ('elt, 'x) coq_R_map_option
        | R_map_option_2 of 'elt tree * 'elt tree * key * 'elt * 'elt tree
           * Int.Z_as_Int.t * 'x tree * ('elt, 'x) coq_R_map_option * 
           'x tree * ('elt, 'x) coq_R_map_option

        val coq_R_map_option_rect :
          (key -> 'a1 -> 'a2 option) -> ('a1 tree -> __ -> 'a3) -> ('a1 tree
          -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ ->
          'a2 -> __ -> 'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2
          tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a3) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2 tree -> ('a1,
          'a2) coq_R_map_option -> 'a3 -> 'a3) -> 'a1 tree -> 'a2 tree ->
          ('a1, 'a2) coq_R_map_option -> 'a3

        val coq_R_map_option_rec :
          (key -> 'a1 -> 'a2 option) -> ('a1 tree -> __ -> 'a3) -> ('a1 tree
          -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ ->
          'a2 -> __ -> 'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2
          tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a3) -> ('a1 tree ->
          'a1 tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> __ ->
          'a2 tree -> ('a1, 'a2) coq_R_map_option -> 'a3 -> 'a2 tree -> ('a1,
          'a2) coq_R_map_option -> 'a3 -> 'a3) -> 'a1 tree -> 'a2 tree ->
          ('a1, 'a2) coq_R_map_option -> 'a3

        type ('elt, 'x0, 'x) coq_R_map2_opt =
        | R_map2_opt_0 of 'elt tree * 'x0 tree
        | R_map2_opt_1 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t
        | R_map2_opt_2 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'x0 tree * key * 'x0 * 'x0 tree
           * Int.Z_as_Int.t * 'x0 tree * 'x0 option * 'x0 tree * 'x * 
           'x tree * ('elt, 'x0, 'x) coq_R_map2_opt * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt
        | R_map2_opt_3 of 'elt tree * 'x0 tree * 'elt tree * key * 'elt
           * 'elt tree * Int.Z_as_Int.t * 'x0 tree * key * 'x0 * 'x0 tree
           * Int.Z_as_Int.t * 'x0 tree * 'x0 option * 'x0 tree * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt * 'x tree
           * ('elt, 'x0, 'x) coq_R_map2_opt

        val coq_R_map2_opt_rect :
          (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree)
          -> ('a2 tree -> 'a3 tree) -> ('a1 tree -> 'a2 tree -> __ -> 'a4) ->
          ('a1 tree -> 'a2 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree
          -> key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          'a2 option -> 'a2 tree -> __ -> 'a3 -> __ -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree -> 'a2
          option -> 'a2 tree -> __ -> __ -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3) coq_R_map2_opt
          -> 'a4 -> 'a4) -> 'a1 tree -> 'a2 tree -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4

        val coq_R_map2_opt_rec :
          (key -> 'a1 -> 'a2 option -> 'a3 option) -> ('a1 tree -> 'a3 tree)
          -> ('a2 tree -> 'a3 tree) -> ('a1 tree -> 'a2 tree -> __ -> 'a4) ->
          ('a1 tree -> 'a2 tree -> 'a1 tree -> key -> 'a1 -> 'a1 tree ->
          Int.Z_as_Int.t -> __ -> __ -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1
          tree -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree
          -> key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          'a2 option -> 'a2 tree -> __ -> 'a3 -> __ -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a4) -> ('a1 tree -> 'a2 tree -> 'a1 tree
          -> key -> 'a1 -> 'a1 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree ->
          key -> 'a2 -> 'a2 tree -> Int.Z_as_Int.t -> __ -> 'a2 tree -> 'a2
          option -> 'a2 tree -> __ -> __ -> 'a3 tree -> ('a1, 'a2, 'a3)
          coq_R_map2_opt -> 'a4 -> 'a3 tree -> ('a1, 'a2, 'a3) coq_R_map2_opt
          -> 'a4 -> 'a4) -> 'a1 tree -> 'a2 tree -> 'a3 tree -> ('a1, 'a2,
          'a3) coq_R_map2_opt -> 'a4

        val fold' : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 tree -> 'a2 -> 'a2

        val flatten_e : 'a1 enumeration -> (key * 'a1) list
       end
     end

    type 'elt bst =
      'elt Raw.tree
      (* singleton inductive, whose constructor was Bst *)

    val this : 'a1 bst -> 'a1 Raw.tree

    type 'elt t = 'elt bst

    type key = int * int

    val empty : 'a1 t

    val is_empty : 'a1 t -> bool

    val add : key -> 'a1 -> 'a1 t -> 'a1 t

    val remove : key -> 'a1 t -> 'a1 t

    val mem : key -> 'a1 t -> bool

    val find : key -> 'a1 t -> 'a1 option

    val map : ('a1 -> 'a2) -> 'a1 t -> 'a2 t

    val mapi : (key -> 'a1 -> 'a2) -> 'a1 t -> 'a2 t

    val map2 :
      ('a1 option -> 'a2 option -> 'a3 option) -> 'a1 t -> 'a2 t -> 'a3 t

    val elements : 'a1 t -> (key * 'a1) list

    val cardinal : 'a1 t -> int

    val fold : (key -> 'a1 -> 'a2 -> 'a2) -> 'a1 t -> 'a2 -> 'a2

    val equal : ('a1 -> 'a1 -> bool) -> 'a1 t -> 'a1 t -> bool
   end

  module EMapFacts :
   sig
    module MapFacts :
     sig
      val eqb : (int * int) -> (int * int) -> bool

      val coq_In_dec : 'a1 EdgeMap.t -> EdgeMap.key -> bool
     end

    val eqb : (int * int) -> (int * int) -> bool

    val coq_In_dec : 'a1 EdgeMap.t -> EdgeMap.key -> bool
   end

  module VMapFacts :
   sig
    module MapFacts :
     sig
      val eqb : int -> int -> bool

      val coq_In_dec : 'a1 VertexMap.t -> VertexMap.key -> bool
     end

    val eqb : int -> int -> bool

    val coq_In_dec : 'a1 VertexMap.t -> VertexMap.key -> bool
   end

  type elttype =
    Lab.coq_VertexLabel option * (Lab.coq_EdgeLabel option
    VertexMap.t * Lab.coq_EdgeLabel option VertexMap.t)

  type maptype = elttype VertexMap.t

  val adj_map : int -> maptype -> elttype

  val succ : int -> maptype -> Lab.coq_EdgeLabel option VertexMap.t

  val pred : int -> maptype -> Lab.coq_EdgeLabel option VertexMap.t

  type tt =
    maptype
    (* singleton inductive, whose constructor was Make_Graph *)

  val map : tt -> maptype

  val successors : int -> tt -> Lab.coq_EdgeLabel option VertexMap.t

  val predecessors : int -> tt -> Lab.coq_EdgeLabel option VertexMap.t

  type t = tt

  val coq_V : tt -> Lab.coq_VertexLabel option VertexMap.t

  val vertex_label : VertexMap.key -> tt -> Lab.coq_VertexLabel option option

  val add_neighborhood :
    int -> elttype -> Lab.coq_EdgeLabel option EdgeMap.t -> Lab.coq_EdgeLabel
    option EdgeMap.t

  val coq_E : t -> Lab.coq_EdgeLabel option EdgeMap.t

  val edge_label : Edge.t -> tt -> Lab.coq_EdgeLabel option option

  val link : tt -> int -> VertexMap.key -> Lab.coq_EdgeLabel option option

  val fold_vertices :
    (tt -> VertexMap.key -> Lab.coq_VertexLabel option -> 'a1 -> 'a1) -> tt
    -> 'a1 -> 'a1

  val fold_edges :
    (t -> EdgeMap.key -> Lab.coq_EdgeLabel option -> 'a1 -> 'a1) -> t -> 'a1
    -> 'a1

  val fold_succ :
    (tt -> int -> VertexMap.key -> Lab.coq_EdgeLabel option -> 'a1 -> 'a1) ->
    int -> tt -> 'a1 -> 'a1

  val fold_pred :
    (tt -> int -> VertexMap.key -> Lab.coq_EdgeLabel option -> 'a1 -> 'a1) ->
    int -> tt -> 'a1 -> 'a1

  val fold_succ_ne :
    (tt -> VertexMap.key -> VertexMap.key -> Lab.coq_EdgeLabel option -> 'a1
    -> 'a1) -> VertexMap.key -> tt -> 'a1 -> 'a1

  val fold_pred_ne :
    (tt -> VertexMap.key -> VertexMap.key -> Lab.coq_EdgeLabel option -> 'a1
    -> 'a1) -> VertexMap.key -> tt -> 'a1 -> 'a1

  val empty_map : maptype

  val empty_graph : tt

  val add_vertex_map :
    maptype -> VertexMap.key -> Lab.coq_VertexLabel option -> maptype

  val add_vertex : VertexMap.key -> tt -> Lab.coq_VertexLabel option -> tt

  val remove_neighbor :
    VertexMap.key -> elttype -> Lab.coq_VertexLabel
    option * (Lab.coq_EdgeLabel option VertexMap.t * Lab.coq_EdgeLabel option
    VertexMap.t)

  val remove_map : maptype -> VertexMap.key -> maptype

  val remove_vertex : VertexMap.key -> tt -> tt

  val add_edge_map :
    Edge.t -> maptype -> Lab.coq_EdgeLabel option -> (Lab.coq_VertexLabel
    option * (Lab.coq_EdgeLabel option VertexMap.t * Lab.coq_EdgeLabel option
    VertexMap.t)) VertexMap.t

  val coq_In_ext_dec : Edge.t -> tt -> bool

  val add_edge : Edge.t -> tt -> Lab.coq_EdgeLabel option -> tt

  val add_edge_w : Edge.t -> tt -> Lab.coq_EdgeLabel option -> tt

  val remove_edge_map :
    Edge.t -> maptype -> (Lab.coq_VertexLabel option * (Lab.coq_EdgeLabel
    option VertexMap.t * Lab.coq_EdgeLabel option VertexMap.t)) VertexMap.t

  val remove_edge : Edge.t -> tt -> tt
 end

module Test :
 sig
  module V :
   sig
    type t = int

    val compare : int -> int -> int OrderedType.coq_Compare

    val eq_dec : int -> int -> bool
   end

  module GP :
   sig
    val coq_In_graph_vertex_dec : int -> DG.t -> bool

    val coq_In_graph_edge_dec : DG.Edge.t -> DG.t -> bool
   end

  module CGP :
   sig
    module GraphProps :
     sig
      val coq_In_graph_vertex_dec : int -> DG.t -> bool

      val coq_In_graph_edge_dec : DG.Edge.t -> DG.t -> bool
     end

    val coq_In_ext_dec : DG.t -> DG.Edge.t -> bool
   end

  module G :
   sig
    type coq_Graph = DG.t
   end

  type coq_Graph = DG.t

  module Lab_with_weight :
   sig
    type coq_VertexLabel = Lab.coq_VertexLabel

    type coq_EdgeLabel = Lab.coq_EdgeLabel

    val weight : coq_EdgeLabel -> int
   end

  module Graph :
   sig
    module Coq_lemma53_preresults :
     sig
      
     end
   end

  module Coq_lemma53_preresults :
   sig
    
   end

  module Dijkstra :
   sig
    module Coq_eqlistAF :
     sig
      
     end

    module EA :
     sig
      
     end

    module Coq_list_as_OT :
     functor (O:OrderedType.OrderedType) ->
     sig
      module OP :
       sig
        module TO :
         sig
          type t = O.t
         end

        module IsTO :
         sig
          
         end

        module OrderTac :
         sig
          
         end

        val eq_dec : O.t -> O.t -> bool

        val lt_dec : O.t -> O.t -> bool

        val eqb : O.t -> O.t -> bool
       end

      type t = O.t list

      val eq_dec : O.t list -> O.t list -> bool

      val compare : t -> t -> t OrderedType.coq_Compare
     end

    module EdgeList :
     sig
      module OP :
       sig
        module TO :
         sig
          type t = int * int
         end

        module IsTO :
         sig
          
         end

        module OrderTac :
         sig
          
         end

        val eq_dec : (int * int) -> (int * int) -> bool

        val lt_dec : (int * int) -> (int * int) -> bool

        val eqb : (int * int) -> (int * int) -> bool
       end

      type t = (int * int) list

      val eq_dec : (int * int) list -> (int * int) list -> bool

      val compare : t -> t -> t OrderedType.coq_Compare
     end

    module Couple_ :
     sig
      module MO1 :
       sig
        module TO :
         sig
          type t = int
         end

        module IsTO :
         sig
          
         end

        module OrderTac :
         sig
          
         end

        val eq_dec : int -> int -> bool

        val lt_dec : int -> int -> bool

        val eqb : int -> int -> bool
       end

      module MO2 :
       sig
        module TO :
         sig
          type t = int
         end

        module IsTO :
         sig
          
         end

        module OrderTac :
         sig
          
         end

        val eq_dec : int -> int -> bool

        val lt_dec : int -> int -> bool

        val eqb : int -> int -> bool
       end

      type t = int * int

      val compare : t -> t -> (int * int) OrderedType.coq_Compare

      val eq_dec : t -> t -> bool
     end

    module Couple :
     sig
      module MO1 :
       sig
        module TO :
         sig
          type t = int * int
         end

        module IsTO :
         sig
          
         end

        module OrderTac :
         sig
          
         end

        val eq_dec : (int * int) -> (int * int) -> bool

        val lt_dec : (int * int) -> (int * int) -> bool

        val eqb : (int * int) -> (int * int) -> bool
       end

      module MO2 :
       sig
        module TO :
         sig
          type t = (int * int) list
         end

        module IsTO :
         sig
          
         end

        module OrderTac :
         sig
          
         end

        val eq_dec : (int * int) list -> (int * int) list -> bool

        val lt_dec : (int * int) list -> (int * int) list -> bool

        val eqb : (int * int) list -> (int * int) list -> bool
       end

      type t = (int * int) * (int * int) list

      val compare :
        t -> t -> ((int * int) * (int * int) list) OrderedType.coq_Compare

      val eq_dec : t -> t -> bool
     end

    module CoupleSet :
     sig
      module X' :
       sig
        type t = (int * int) * (int * int) list

        val eq_dec :
          ((int * int) * (int * int) list) -> ((int * int) * (int * int)
          list) -> bool

        val compare :
          ((int * int) * (int * int) list) -> ((int * int) * (int * int)
          list) -> comparison
       end

      module MSet :
       sig
        module Raw :
         sig
          type elt = (int * int) * (int * int) list

          type tree =
          | Leaf
          | Node of Int.Z_as_Int.t * tree * ((int * int) * (int * int) list)
             * tree

          val empty : tree

          val is_empty : tree -> bool

          val mem : ((int * int) * (int * int) list) -> tree -> bool

          val min_elt : tree -> elt option

          val max_elt : tree -> elt option

          val choose : tree -> elt option

          val fold : (elt -> 'a1 -> 'a1) -> tree -> 'a1 -> 'a1

          val elements_aux :
            ((int * int) * (int * int) list) list -> tree ->
            ((int * int) * (int * int) list) list

          val elements : tree -> ((int * int) * (int * int) list) list

          val rev_elements_aux :
            ((int * int) * (int * int) list) list -> tree ->
            ((int * int) * (int * int) list) list

          val rev_elements : tree -> ((int * int) * (int * int) list) list

          val cardinal : tree -> int

          val maxdepth : tree -> int

          val mindepth : tree -> int

          val for_all : (elt -> bool) -> tree -> bool

          val exists_ : (elt -> bool) -> tree -> bool

          type enumeration =
          | End
          | More of elt * tree * enumeration

          val cons : tree -> enumeration -> enumeration

          val compare_more :
            ((int * int) * (int * int) list) -> (enumeration -> comparison)
            -> enumeration -> comparison

          val compare_cont :
            tree -> (enumeration -> comparison) -> enumeration -> comparison

          val compare_end : enumeration -> comparison

          val compare : tree -> tree -> comparison

          val equal : tree -> tree -> bool

          val subsetl :
            (tree -> bool) -> ((int * int) * (int * int) list) -> tree -> bool

          val subsetr :
            (tree -> bool) -> ((int * int) * (int * int) list) -> tree -> bool

          val subset : tree -> tree -> bool

          type t = tree

          val height : t -> Int.Z_as_Int.t

          val singleton : ((int * int) * (int * int) list) -> tree

          val create : t -> ((int * int) * (int * int) list) -> t -> tree

          val assert_false :
            t -> ((int * int) * (int * int) list) -> t -> tree

          val bal : t -> ((int * int) * (int * int) list) -> t -> tree

          val add : ((int * int) * (int * int) list) -> tree -> tree

          val join : tree -> elt -> t -> t

          val remove_min : tree -> elt -> t -> t * elt

          val merge : tree -> tree -> tree

          val remove : ((int * int) * (int * int) list) -> tree -> tree

          val concat : tree -> tree -> tree

          type triple = { t_left : t; t_in : bool; t_right : t }

          val t_left : triple -> t

          val t_in : triple -> bool

          val t_right : triple -> t

          val split : ((int * int) * (int * int) list) -> tree -> triple

          val inter : tree -> tree -> tree

          val diff : tree -> tree -> tree

          val union : tree -> tree -> tree

          val filter : (elt -> bool) -> tree -> tree

          val partition : (elt -> bool) -> t -> t * t

          val ltb_tree : ((int * int) * (int * int) list) -> tree -> bool

          val gtb_tree : ((int * int) * (int * int) list) -> tree -> bool

          val isok : tree -> bool

          module MX :
           sig
            module OrderTac :
             sig
              module OTF :
               sig
                type t = (int * int) * (int * int) list

                val compare :
                  ((int * int) * (int * int) list) ->
                  ((int * int) * (int * int) list) -> comparison

                val eq_dec :
                  ((int * int) * (int * int) list) ->
                  ((int * int) * (int * int) list) -> bool
               end

              module TO :
               sig
                type t = (int * int) * (int * int) list

                val compare :
                  ((int * int) * (int * int) list) ->
                  ((int * int) * (int * int) list) -> comparison

                val eq_dec :
                  ((int * int) * (int * int) list) ->
                  ((int * int) * (int * int) list) -> bool
               end
             end

            val eq_dec :
              ((int * int) * (int * int) list) -> ((int * int) * (int * int)
              list) -> bool

            val lt_dec :
              ((int * int) * (int * int) list) -> ((int * int) * (int * int)
              list) -> bool

            val eqb :
              ((int * int) * (int * int) list) -> ((int * int) * (int * int)
              list) -> bool
           end

          type coq_R_min_elt =
          | R_min_elt_0 of tree
          | R_min_elt_1 of tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree
          | R_min_elt_2 of tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree * Int.Z_as_Int.t
             * tree * ((int * int) * (int * int) list) * tree * elt option
             * coq_R_min_elt

          type coq_R_max_elt =
          | R_max_elt_0 of tree
          | R_max_elt_1 of tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree
          | R_max_elt_2 of tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree * Int.Z_as_Int.t
             * tree * ((int * int) * (int * int) list) * tree * elt option
             * coq_R_max_elt

          module L :
           sig
            module MO :
             sig
              module OrderTac :
               sig
                module OTF :
                 sig
                  type t = (int * int) * (int * int) list

                  val compare :
                    ((int * int) * (int * int) list) ->
                    ((int * int) * (int * int) list) -> comparison

                  val eq_dec :
                    ((int * int) * (int * int) list) ->
                    ((int * int) * (int * int) list) -> bool
                 end

                module TO :
                 sig
                  type t = (int * int) * (int * int) list

                  val compare :
                    ((int * int) * (int * int) list) ->
                    ((int * int) * (int * int) list) -> comparison

                  val eq_dec :
                    ((int * int) * (int * int) list) ->
                    ((int * int) * (int * int) list) -> bool
                 end
               end

              val eq_dec :
                ((int * int) * (int * int) list) ->
                ((int * int) * (int * int) list) -> bool

              val lt_dec :
                ((int * int) * (int * int) list) ->
                ((int * int) * (int * int) list) -> bool

              val eqb :
                ((int * int) * (int * int) list) ->
                ((int * int) * (int * int) list) -> bool
             end
           end

          val flatten_e : enumeration -> elt list

          type coq_R_bal =
          | R_bal_0 of t * ((int * int) * (int * int) list) * t
          | R_bal_1 of t * ((int * int) * (int * int) list) * t
             * Int.Z_as_Int.t * tree * ((int * int) * (int * int) list)
             * tree
          | R_bal_2 of t * ((int * int) * (int * int) list) * t
             * Int.Z_as_Int.t * tree * ((int * int) * (int * int) list)
             * tree
          | R_bal_3 of t * ((int * int) * (int * int) list) * t
             * Int.Z_as_Int.t * tree * ((int * int) * (int * int) list)
             * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree
          | R_bal_4 of t * ((int * int) * (int * int) list) * t
          | R_bal_5 of t * ((int * int) * (int * int) list) * t
             * Int.Z_as_Int.t * tree * ((int * int) * (int * int) list)
             * tree
          | R_bal_6 of t * ((int * int) * (int * int) list) * t
             * Int.Z_as_Int.t * tree * ((int * int) * (int * int) list)
             * tree
          | R_bal_7 of t * ((int * int) * (int * int) list) * t
             * Int.Z_as_Int.t * tree * ((int * int) * (int * int) list)
             * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree
          | R_bal_8 of t * ((int * int) * (int * int) list) * t

          type coq_R_remove_min =
          | R_remove_min_0 of tree * elt * t
          | R_remove_min_1 of tree * elt * t * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree * (t * elt)
             * coq_R_remove_min * t * elt

          type coq_R_merge =
          | R_merge_0 of tree * tree
          | R_merge_1 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree
          | R_merge_2 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree * Int.Z_as_Int.t
             * tree * ((int * int) * (int * int) list) * tree * t * elt

          type coq_R_concat =
          | R_concat_0 of tree * tree
          | R_concat_1 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree
          | R_concat_2 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree * Int.Z_as_Int.t
             * tree * ((int * int) * (int * int) list) * tree * t * elt

          type coq_R_inter =
          | R_inter_0 of tree * tree
          | R_inter_1 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree
          | R_inter_2 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree * Int.Z_as_Int.t
             * tree * ((int * int) * (int * int) list) * tree * t * bool * 
             t * tree * coq_R_inter * tree * coq_R_inter
          | R_inter_3 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree * Int.Z_as_Int.t
             * tree * ((int * int) * (int * int) list) * tree * t * bool * 
             t * tree * coq_R_inter * tree * coq_R_inter

          type coq_R_diff =
          | R_diff_0 of tree * tree
          | R_diff_1 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree
          | R_diff_2 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree * Int.Z_as_Int.t
             * tree * ((int * int) * (int * int) list) * tree * t * bool * 
             t * tree * coq_R_diff * tree * coq_R_diff
          | R_diff_3 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree * Int.Z_as_Int.t
             * tree * ((int * int) * (int * int) list) * tree * t * bool * 
             t * tree * coq_R_diff * tree * coq_R_diff

          type coq_R_union =
          | R_union_0 of tree * tree
          | R_union_1 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree
          | R_union_2 of tree * tree * Int.Z_as_Int.t * tree
             * ((int * int) * (int * int) list) * tree * Int.Z_as_Int.t
             * tree * ((int * int) * (int * int) list) * tree * t * bool * 
             t * tree * coq_R_union * tree * coq_R_union
         end

        module E :
         sig
          type t = (int * int) * (int * int) list

          val compare :
            ((int * int) * (int * int) list) -> ((int * int) * (int * int)
            list) -> comparison

          val eq_dec :
            ((int * int) * (int * int) list) -> ((int * int) * (int * int)
            list) -> bool
         end

        type elt = (int * int) * (int * int) list

        type t_ =
          Raw.t
          (* singleton inductive, whose constructor was Mkt *)

        val this : t_ -> Raw.t

        type t = t_

        val mem : elt -> t -> bool

        val add : elt -> t -> t

        val remove : elt -> t -> t

        val singleton : elt -> t

        val union : t -> t -> t

        val inter : t -> t -> t

        val diff : t -> t -> t

        val equal : t -> t -> bool

        val subset : t -> t -> bool

        val empty : t

        val is_empty : t -> bool

        val elements : t -> elt list

        val choose : t -> elt option

        val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

        val cardinal : t -> int

        val filter : (elt -> bool) -> t -> t

        val for_all : (elt -> bool) -> t -> bool

        val exists_ : (elt -> bool) -> t -> bool

        val partition : (elt -> bool) -> t -> t * t

        val eq_dec : t -> t -> bool

        val compare : t -> t -> comparison

        val min_elt : t -> elt option

        val max_elt : t -> elt option
       end

      type elt = (int * int) * (int * int) list

      type t = MSet.t

      val empty : t

      val is_empty : t -> bool

      val mem : elt -> t -> bool

      val add : elt -> t -> t

      val singleton : elt -> t

      val remove : elt -> t -> t

      val union : t -> t -> t

      val inter : t -> t -> t

      val diff : t -> t -> t

      val eq_dec : t -> t -> bool

      val equal : t -> t -> bool

      val subset : t -> t -> bool

      val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

      val for_all : (elt -> bool) -> t -> bool

      val exists_ : (elt -> bool) -> t -> bool

      val filter : (elt -> bool) -> t -> t

      val partition : (elt -> bool) -> t -> t * t

      val cardinal : t -> int

      val elements : t -> elt list

      val choose : t -> elt option

      module MF :
       sig
        val eqb :
          ((int * int) * (int * int) list) -> ((int * int) * (int * int)
          list) -> bool
       end

      val min_elt : t -> elt option

      val max_elt : t -> elt option

      val compare : t -> t -> t OrderedType.coq_Compare

      module E :
       sig
        type t = (int * int) * (int * int) list

        val compare :
          ((int * int) * (int * int) list) -> ((int * int) * (int * int)
          list) -> ((int * int) * (int * int) list) OrderedType.coq_Compare

        val eq_dec :
          ((int * int) * (int * int) list) -> ((int * int) * (int * int)
          list) -> bool
       end
     end

    module PS :
     sig
      module Dec :
       sig
        module F :
         sig
          val eqb :
            ((int * int) * (int * int) list) -> ((int * int) * (int * int)
            list) -> bool
         end

        module FSetLogicalFacts :
         sig
          
         end

        module FSetDecideAuxiliary :
         sig
          
         end

        module FSetDecideTestCases :
         sig
          
         end
       end

      module FM :
       sig
        val eqb :
          ((int * int) * (int * int) list) -> ((int * int) * (int * int)
          list) -> bool
       end

      val coq_In_dec : CoupleSet.elt -> CoupleSet.t -> bool

      val of_list : CoupleSet.elt list -> CoupleSet.t

      val to_list : CoupleSet.t -> CoupleSet.elt list

      val fold_rec :
        (CoupleSet.elt -> 'a1 -> 'a1) -> 'a1 -> CoupleSet.t -> (CoupleSet.t
        -> __ -> 'a2) -> (CoupleSet.elt -> 'a1 -> CoupleSet.t -> CoupleSet.t
        -> __ -> __ -> __ -> 'a2 -> 'a2) -> 'a2

      val fold_rec_bis :
        (CoupleSet.elt -> 'a1 -> 'a1) -> 'a1 -> CoupleSet.t -> (CoupleSet.t
        -> CoupleSet.t -> 'a1 -> __ -> 'a2 -> 'a2) -> 'a2 -> (CoupleSet.elt
        -> 'a1 -> CoupleSet.t -> __ -> __ -> 'a2 -> 'a2) -> 'a2

      val fold_rec_nodep :
        (CoupleSet.elt -> 'a1 -> 'a1) -> 'a1 -> CoupleSet.t -> 'a2 ->
        (CoupleSet.elt -> 'a1 -> __ -> 'a2 -> 'a2) -> 'a2

      val fold_rec_weak :
        (CoupleSet.elt -> 'a1 -> 'a1) -> 'a1 -> (CoupleSet.t -> CoupleSet.t
        -> 'a1 -> __ -> 'a2 -> 'a2) -> 'a2 -> (CoupleSet.elt -> 'a1 ->
        CoupleSet.t -> __ -> 'a2 -> 'a2) -> CoupleSet.t -> 'a2

      val fold_rel :
        (CoupleSet.elt -> 'a1 -> 'a1) -> (CoupleSet.elt -> 'a2 -> 'a2) -> 'a1
        -> 'a2 -> CoupleSet.t -> 'a3 -> (CoupleSet.elt -> 'a1 -> 'a2 -> __ ->
        'a3 -> 'a3) -> 'a3

      val set_induction :
        (CoupleSet.t -> __ -> 'a1) -> (CoupleSet.t -> CoupleSet.t -> 'a1 ->
        CoupleSet.elt -> __ -> __ -> 'a1) -> CoupleSet.t -> 'a1

      val set_induction_bis :
        (CoupleSet.t -> CoupleSet.t -> __ -> 'a1 -> 'a1) -> 'a1 ->
        (CoupleSet.elt -> CoupleSet.t -> __ -> 'a1 -> 'a1) -> CoupleSet.t ->
        'a1

      val cardinal_inv_2 : CoupleSet.t -> int -> CoupleSet.elt

      val cardinal_inv_2b : CoupleSet.t -> CoupleSet.elt
     end

    module PPS :
     sig
      val eqb :
        ((int * int) * (int * int) list) -> ((int * int) * (int * int) list)
        -> bool
     end

    type status =
    | Open
    | Optimal

    val status_rect : 'a1 -> 'a1 -> status -> 'a1

    val status_rec : 'a1 -> 'a1 -> status -> 'a1

    type vertex_status =
    | Reached of status * int * DG.Edge.t list
    | Unreached

    val vertex_status_rect :
      (status -> int -> DG.Edge.t list -> 'a1) -> 'a1 -> vertex_status -> 'a1

    val vertex_status_rec :
      (status -> int -> DG.Edge.t list -> 'a1) -> 'a1 -> vertex_status -> 'a1

    module P :
     sig
      module P :
       sig
        val coq_In_graph_vertex_dec : int -> DG.t -> bool

        val coq_In_graph_edge_dec : DG.Edge.t -> DG.t -> bool
       end

      val eval : Lab_with_weight.coq_EdgeLabel option -> int
     end

    type pathmap = { graph : DG.t; root : int;
                     map : vertex_status DG.VertexMap.t; set : CoupleSet.t }

    val graph : pathmap -> DG.t

    val root : pathmap -> int

    val map : pathmap -> vertex_status DG.VertexMap.t

    val set : pathmap -> CoupleSet.t

    val null_init : DG.t -> vertex_status DG.VertexMap.t

    val first_iter :
      int -> DG.t -> (vertex_status DG.VertexMap.t * CoupleSet.t) ->
      vertex_status DG.VertexMap.t * CoupleSet.t

    val init_dijkstra :
      int -> DG.t -> vertex_status DG.VertexMap.t * CoupleSet.t

    val init : DG.t -> int -> pathmap

    val find_min : CoupleSet.t -> CoupleSet.elt option

    val update :
      int -> int -> DG.Edge.t list -> vertex_status DG.VertexMap.t ->
      Nat_as_OT.t -> int -> CoupleSet.t -> vertex_status
      DG.VertexMap.t * CoupleSet.t

    val iteration :
      pathmap -> (int option * vertex_status DG.VertexMap.t) * CoupleSet.t

    val update_list :
      int -> int -> DG.Edge.t list -> (vertex_status
      DG.VertexMap.t * CoupleSet.t) -> (int * Lab_with_weight.coq_EdgeLabel
      option) -> vertex_status DG.VertexMap.t * CoupleSet.t

    val iteration_pathmap : pathmap -> int option * pathmap

    val nonoptimal_vertices_nb : vertex_status DG.VertexMap.t -> int

    val coq_Dijkstra_measure : pathmap -> int

    val coq_Dijkstra_aux_F : (pathmap -> pathmap) -> pathmap -> pathmap

    val coq_Dijkstra_aux_terminate : pathmap -> pathmap

    val coq_Dijkstra_aux : pathmap -> pathmap

    type coq_R_Dijkstra_aux =
    | R_Dijkstra_aux_0 of pathmap * int option * pathmap
    | R_Dijkstra_aux_1 of pathmap * int option * pathmap * int * pathmap
       * coq_R_Dijkstra_aux

    val coq_R_Dijkstra_aux_rect :
      (pathmap -> int option -> pathmap -> __ -> __ -> 'a1) -> (pathmap ->
      int option -> pathmap -> __ -> int -> __ -> pathmap ->
      coq_R_Dijkstra_aux -> 'a1 -> 'a1) -> pathmap -> pathmap ->
      coq_R_Dijkstra_aux -> 'a1

    val coq_R_Dijkstra_aux_rec :
      (pathmap -> int option -> pathmap -> __ -> __ -> 'a1) -> (pathmap ->
      int option -> pathmap -> __ -> int -> __ -> pathmap ->
      coq_R_Dijkstra_aux -> 'a1 -> 'a1) -> pathmap -> pathmap ->
      coq_R_Dijkstra_aux -> 'a1

    val coq_Dijkstra_aux_rect :
      (pathmap -> int option -> pathmap -> __ -> __ -> 'a1) -> (pathmap ->
      int option -> pathmap -> __ -> int -> __ -> 'a1 -> 'a1) -> pathmap ->
      'a1

    val coq_Dijkstra_aux_rec :
      (pathmap -> int option -> pathmap -> __ -> __ -> 'a1) -> (pathmap ->
      int option -> pathmap -> __ -> int -> __ -> 'a1 -> 'a1) -> pathmap ->
      'a1

    val coq_R_Dijkstra_aux_correct : pathmap -> pathmap -> coq_R_Dijkstra_aux

    val coq_Dijkstra : DG.t -> int -> pathmap

    val shortest_path_length : DG.t -> int -> DG.VertexMap.key -> int
   end

  val reachable : DG.t -> int -> DG.VertexMap.key -> bool

  module VertexSet :
   sig
    module X' :
     sig
      type t = int

      val eq_dec : int -> int -> bool

      val compare : int -> int -> comparison
     end

    module MSet :
     sig
      module Raw :
       sig
        type elt = int

        type tree =
        | Leaf
        | Node of Int.Z_as_Int.t * tree * int * tree

        val empty : tree

        val is_empty : tree -> bool

        val mem : int -> tree -> bool

        val min_elt : tree -> elt option

        val max_elt : tree -> elt option

        val choose : tree -> elt option

        val fold : (elt -> 'a1 -> 'a1) -> tree -> 'a1 -> 'a1

        val elements_aux : int list -> tree -> int list

        val elements : tree -> int list

        val rev_elements_aux : int list -> tree -> int list

        val rev_elements : tree -> int list

        val cardinal : tree -> int

        val maxdepth : tree -> int

        val mindepth : tree -> int

        val for_all : (elt -> bool) -> tree -> bool

        val exists_ : (elt -> bool) -> tree -> bool

        type enumeration =
        | End
        | More of elt * tree * enumeration

        val cons : tree -> enumeration -> enumeration

        val compare_more :
          int -> (enumeration -> comparison) -> enumeration -> comparison

        val compare_cont :
          tree -> (enumeration -> comparison) -> enumeration -> comparison

        val compare_end : enumeration -> comparison

        val compare : tree -> tree -> comparison

        val equal : tree -> tree -> bool

        val subsetl : (tree -> bool) -> int -> tree -> bool

        val subsetr : (tree -> bool) -> int -> tree -> bool

        val subset : tree -> tree -> bool

        type t = tree

        val height : t -> Int.Z_as_Int.t

        val singleton : int -> tree

        val create : t -> int -> t -> tree

        val assert_false : t -> int -> t -> tree

        val bal : t -> int -> t -> tree

        val add : int -> tree -> tree

        val join : tree -> elt -> t -> t

        val remove_min : tree -> elt -> t -> t * elt

        val merge : tree -> tree -> tree

        val remove : int -> tree -> tree

        val concat : tree -> tree -> tree

        type triple = { t_left : t; t_in : bool; t_right : t }

        val t_left : triple -> t

        val t_in : triple -> bool

        val t_right : triple -> t

        val split : int -> tree -> triple

        val inter : tree -> tree -> tree

        val diff : tree -> tree -> tree

        val union : tree -> tree -> tree

        val filter : (elt -> bool) -> tree -> tree

        val partition : (elt -> bool) -> t -> t * t

        val ltb_tree : int -> tree -> bool

        val gtb_tree : int -> tree -> bool

        val isok : tree -> bool

        module MX :
         sig
          module OrderTac :
           sig
            module OTF :
             sig
              type t = int

              val compare : int -> int -> comparison

              val eq_dec : int -> int -> bool
             end

            module TO :
             sig
              type t = int

              val compare : int -> int -> comparison

              val eq_dec : int -> int -> bool
             end
           end

          val eq_dec : int -> int -> bool

          val lt_dec : int -> int -> bool

          val eqb : int -> int -> bool
         end

        type coq_R_min_elt =
        | R_min_elt_0 of tree
        | R_min_elt_1 of tree * Int.Z_as_Int.t * tree * int * tree
        | R_min_elt_2 of tree * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree * elt option * coq_R_min_elt

        type coq_R_max_elt =
        | R_max_elt_0 of tree
        | R_max_elt_1 of tree * Int.Z_as_Int.t * tree * int * tree
        | R_max_elt_2 of tree * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree * elt option * coq_R_max_elt

        module L :
         sig
          module MO :
           sig
            module OrderTac :
             sig
              module OTF :
               sig
                type t = int

                val compare : int -> int -> comparison

                val eq_dec : int -> int -> bool
               end

              module TO :
               sig
                type t = int

                val compare : int -> int -> comparison

                val eq_dec : int -> int -> bool
               end
             end

            val eq_dec : int -> int -> bool

            val lt_dec : int -> int -> bool

            val eqb : int -> int -> bool
           end
         end

        val flatten_e : enumeration -> elt list

        type coq_R_bal =
        | R_bal_0 of t * int * t
        | R_bal_1 of t * int * t * Int.Z_as_Int.t * tree * int * tree
        | R_bal_2 of t * int * t * Int.Z_as_Int.t * tree * int * tree
        | R_bal_3 of t * int * t * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree
        | R_bal_4 of t * int * t
        | R_bal_5 of t * int * t * Int.Z_as_Int.t * tree * int * tree
        | R_bal_6 of t * int * t * Int.Z_as_Int.t * tree * int * tree
        | R_bal_7 of t * int * t * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree
        | R_bal_8 of t * int * t

        type coq_R_remove_min =
        | R_remove_min_0 of tree * elt * t
        | R_remove_min_1 of tree * elt * t * Int.Z_as_Int.t * tree * 
           int * tree * (t * elt) * coq_R_remove_min * t * elt

        type coq_R_merge =
        | R_merge_0 of tree * tree
        | R_merge_1 of tree * tree * Int.Z_as_Int.t * tree * int * tree
        | R_merge_2 of tree * tree * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree * t * elt

        type coq_R_concat =
        | R_concat_0 of tree * tree
        | R_concat_1 of tree * tree * Int.Z_as_Int.t * tree * int * tree
        | R_concat_2 of tree * tree * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree * t * elt

        type coq_R_inter =
        | R_inter_0 of tree * tree
        | R_inter_1 of tree * tree * Int.Z_as_Int.t * tree * int * tree
        | R_inter_2 of tree * tree * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree * t * bool * t * tree
           * coq_R_inter * tree * coq_R_inter
        | R_inter_3 of tree * tree * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree * t * bool * t * tree
           * coq_R_inter * tree * coq_R_inter

        type coq_R_diff =
        | R_diff_0 of tree * tree
        | R_diff_1 of tree * tree * Int.Z_as_Int.t * tree * int * tree
        | R_diff_2 of tree * tree * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree * t * bool * t * tree
           * coq_R_diff * tree * coq_R_diff
        | R_diff_3 of tree * tree * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree * t * bool * t * tree
           * coq_R_diff * tree * coq_R_diff

        type coq_R_union =
        | R_union_0 of tree * tree
        | R_union_1 of tree * tree * Int.Z_as_Int.t * tree * int * tree
        | R_union_2 of tree * tree * Int.Z_as_Int.t * tree * int * tree
           * Int.Z_as_Int.t * tree * int * tree * t * bool * t * tree
           * coq_R_union * tree * coq_R_union
       end

      module E :
       sig
        type t = int

        val compare : int -> int -> comparison

        val eq_dec : int -> int -> bool
       end

      type elt = int

      type t_ =
        Raw.t
        (* singleton inductive, whose constructor was Mkt *)

      val this : t_ -> Raw.t

      type t = t_

      val mem : elt -> t -> bool

      val add : elt -> t -> t

      val remove : elt -> t -> t

      val singleton : elt -> t

      val union : t -> t -> t

      val inter : t -> t -> t

      val diff : t -> t -> t

      val equal : t -> t -> bool

      val subset : t -> t -> bool

      val empty : t

      val is_empty : t -> bool

      val elements : t -> elt list

      val choose : t -> elt option

      val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

      val cardinal : t -> int

      val filter : (elt -> bool) -> t -> t

      val for_all : (elt -> bool) -> t -> bool

      val exists_ : (elt -> bool) -> t -> bool

      val partition : (elt -> bool) -> t -> t * t

      val eq_dec : t -> t -> bool

      val compare : t -> t -> comparison

      val min_elt : t -> elt option

      val max_elt : t -> elt option
     end

    type elt = int

    type t = MSet.t

    val empty : t

    val is_empty : t -> bool

    val mem : elt -> t -> bool

    val add : elt -> t -> t

    val singleton : elt -> t

    val remove : elt -> t -> t

    val union : t -> t -> t

    val inter : t -> t -> t

    val diff : t -> t -> t

    val eq_dec : t -> t -> bool

    val equal : t -> t -> bool

    val subset : t -> t -> bool

    val fold : (elt -> 'a1 -> 'a1) -> t -> 'a1 -> 'a1

    val for_all : (elt -> bool) -> t -> bool

    val exists_ : (elt -> bool) -> t -> bool

    val filter : (elt -> bool) -> t -> t

    val partition : (elt -> bool) -> t -> t * t

    val cardinal : t -> int

    val elements : t -> elt list

    val choose : t -> elt option

    module MF :
     sig
      val eqb : int -> int -> bool
     end

    val min_elt : t -> elt option

    val max_elt : t -> elt option

    val compare : t -> t -> t OrderedType.coq_Compare

    module E :
     sig
      type t = int

      val compare : int -> int -> int OrderedType.coq_Compare

      val eq_dec : int -> int -> bool
     end
   end

  val reachable_from : DG.t -> VertexSet.t -> DG.VertexMap.key -> bool

  val filter_edges : (DG.Edge.t -> bool) -> DG.t -> DG.t

  val copy : DG.t -> DG.t

  val map : (DG.Edge.t -> DG.Edge.t) -> DG.t -> DG.t

  val copy_without_vertices : DG.t -> VertexSet.t -> DG.t

  val theta_set : DG.t -> VertexSet.t -> int -> VertexSet.t

  val theta : DG.t -> VertexSet.t -> int -> int

  val is_critical_candidate : DG.t -> VertexSet.t -> (int * int) -> bool

  val is_critical_edge : DG.t -> VertexSet.t -> (int * int) -> bool

  val algo61 : DG.t -> VertexSet.t -> bool * VertexSet.t

  val real_algo61_F :
    (DG.t -> VertexSet.t -> VertexSet.t) -> DG.t -> VertexSet.t -> VertexSet.t

  val real_algo61_terminate : DG.t -> VertexSet.t -> VertexSet.t

  val real_algo61 : DG.t -> VertexSet.t -> VertexSet.t

  type coq_R_real_algo61 =
  | R_real_algo61_0 of VertexSet.t * VertexSet.t
  | R_real_algo61_1 of VertexSet.t * VertexSet.t * VertexSet.t
     * coq_R_real_algo61

  val coq_R_real_algo61_rect :
    DG.t -> (VertexSet.t -> VertexSet.t -> __ -> 'a1) -> (VertexSet.t ->
    VertexSet.t -> __ -> VertexSet.t -> coq_R_real_algo61 -> 'a1 -> 'a1) ->
    VertexSet.t -> VertexSet.t -> coq_R_real_algo61 -> 'a1

  val coq_R_real_algo61_rec :
    DG.t -> (VertexSet.t -> VertexSet.t -> __ -> 'a1) -> (VertexSet.t ->
    VertexSet.t -> __ -> VertexSet.t -> coq_R_real_algo61 -> 'a1 -> 'a1) ->
    VertexSet.t -> VertexSet.t -> coq_R_real_algo61 -> 'a1

  val real_algo61_rect :
    DG.t -> (VertexSet.t -> VertexSet.t -> __ -> 'a1) -> (VertexSet.t ->
    VertexSet.t -> __ -> 'a1 -> 'a1) -> VertexSet.t -> 'a1

  val real_algo61_rec :
    DG.t -> (VertexSet.t -> VertexSet.t -> __ -> 'a1) -> (VertexSet.t ->
    VertexSet.t -> __ -> 'a1 -> 'a1) -> VertexSet.t -> 'a1

  val coq_R_real_algo61_correct :
    DG.t -> VertexSet.t -> VertexSet.t -> coq_R_real_algo61
 end

val g : DG.tt

val seq : int -> int list

val g_example : DG.tt

val g_while_rand_1 : DG.tt

val test : DG.t -> Test.VertexSet.elt list -> Test.VertexSet.elt list
