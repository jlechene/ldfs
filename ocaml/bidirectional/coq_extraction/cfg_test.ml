open Datatypes
open DigraphMapImp
open List0
open OrderedTypeEx
open Algo

type __ = Obj.t

module V = Nat_as_OT

module Lab =
 struct
  type coq_VertexLabel = int

  type coq_EdgeLabel = int
 end

module C =
 struct
  type coq_Label = int option

  (** val combine : coq_Label -> coq_Label -> coq_Label **)

  let combine x _ =
    x
 end

module DG = Directed_GraphMap(V)(Lab)

module Test = Coq_preCFG(V)(Lab)(C)(DG)

(** val g : DG.tt **)

let g =
  let g0 = DG.add_vertex 0 DG.empty_graph None in
  let g1 = DG.add_vertex ((fun x -> x + 1) 0) g0 None in
  let g2 = DG.add_vertex ((fun x -> x + 1) ((fun x -> x + 1) 0)) g1 None in
  let g3 =
    DG.add_vertex ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0)))
      g2 None
  in
  let g4 = DG.add_edge (0, ((fun x -> x + 1) 0)) g3 None in
  let g5 = DG.add_edge (0, ((fun x -> x + 1) ((fun x -> x + 1) 0))) g4 None
  in
  let g6 =
    DG.add_edge (((fun x -> x + 1) 0), ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) 0)))) g5 None
  in
  let g7 =
    DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) 0)), ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) 0)))) g6 None
  in
  DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0))), 0)
    g7 None

(** val seq : int -> int list **)

let rec seq n =
  (fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))
    (fun _ ->
    [])
    (fun n' ->
    app (seq n') (n' :: []))
    n

(** val g_example : DG.tt **)

let g_example =
  let g0 = DG.add_vertex 0 DG.empty_graph None in
  let g1 = DG.add_vertex ((fun x -> x + 1) 0) g0 None in
  let g2 = DG.add_vertex ((fun x -> x + 1) ((fun x -> x + 1) 0)) g1 None in
  let g3 =
    DG.add_vertex ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0)))
      g2 None
  in
  let g4 =
    DG.add_vertex ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) 0)))) g3 None
  in
  let g5 =
    DG.add_vertex ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) 0))))) g4 None
  in
  let g6 =
    DG.add_vertex ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0)))))) g5 None
  in
  let g7 = DG.add_edge (0, ((fun x -> x + 1) 0)) g6 None in
  let g8 = DG.add_edge (0, ((fun x -> x + 1) ((fun x -> x + 1) 0))) g7 None
  in
  let g9 =
    DG.add_edge (((fun x -> x + 1) 0), ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) 0)))) g8 None
  in
  let g10 =
    DG.add_edge (((fun x -> x + 1) 0), ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) 0))))) g9 None
  in
  let g11 =
    DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) 0)), ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) 0)))) g10 None
  in
  let g12 =
    DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) 0)), ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0))))) g11 None
  in
  let g13 =
    DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) 0)))), ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) 0)))) g12 None
  in
  let g14 =
    DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) 0)))), ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0)))))) g13 None
  in
  let g15 =
    DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) 0))))), ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) 0))))))) g14 None
  in
  let g16 =
    DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0)))))), 0) g15
      None
  in
  let g17 =
    DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
      ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0)))))),
      ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
      0))))) g16 None
  in
  DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
    ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0)))))),
    ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1)
    ((fun x -> x + 1) 0)))))) g17 None

(** val g_while_rand_1 : DG.tt **)

let g_while_rand_1 =
  let g0 =
    DG.add_vertex ((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0)))
      DG.empty_graph None
  in
  let g1 = DG.add_edge (((fun x -> x + 1) 0), 0) g0 None in
  let g2 =
    DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) 0)), ((fun x -> x + 1)
      0)) g1 None
  in
  let g3 =
    DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0))),
      ((fun x -> x + 1) ((fun x -> x + 1) 0))) g2 None
  in
  DG.add_edge (((fun x -> x + 1) ((fun x -> x + 1) ((fun x -> x + 1) 0))),
    ((fun x -> x + 1) 0)) g3 None

(** val test : DG.t -> Test.VertexSet.elt list -> Test.VertexSet.elt list **)

let test g0 l =
  let s =
    fold_left (fun acc x -> Test.VertexSet.add x acc) l Test.VertexSet.empty
  in
  Test.VertexSet.elements (Test.real_algo61 g0 s)
