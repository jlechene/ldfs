open MyGraph

let filter g crit w =
  let f = is_reachable_from g crit in
  S.filter f w

type aggregate =
| Sum (* all components are summed *)
| Split (* all components are showned separately *)

type algo_res = { closure : S.t; size_before_filter : int option; time_exec : float; time_filter : float option }
type line = { gen_time : float; closure_size : int; semi_closure_size : int option; results : (float * float option) list }

(* name, reachable to be done ?, function *)
let l = [
  "coq", (false, Coq.main);

  "danicic_very_naive_pathcheck", (false, Danicic_very_naive.danicic_pathcheck); (* a naive danicic implementation *)
  "danicic_very_naive_pathcheck_opti_n", (false, Danicic_very_naive.danicic_pathcheck_opti_n);
  "danicic_very_naive_pathcheck_opti_mult", (false, Danicic_very_naive.danicic_pathcheck_opti_mult);
  "danicic_very_naive_pathcheck_opti_mult_n", (false, Danicic_very_naive.danicic_pathcheck_opti_mult_n);

  "danicic", (false, Danicic.danicic); (* a less naive danicic implem *)
  "danicic_opti_n", (false, Danicic.danicic_opti_n);
  "danicic_opti_mult", (false, Danicic.danicic_opti_mult);
  "danicic_opti_mult_n", (false, Danicic.danicic_opti_mult_n);
  "danicic_opti_mult_n_no_reach", (true, Danicic.danicic_opti_mult_n_no_reach); (* reach at the end instead of during the algo *)

  "optimized_why3", (true, Optimized_why3.wrap); (* copy of whyml code adapted to OCaml *)
  "optimized", (true, Optimized.wrap); (* more idiomatic OCaml code *)
  "optimized_propagation", (true, Optimized_propagation.wrap); (* second traversal to find new deciding nodes *)
]

let test (reachable, f) g crit =
   let start_time = Unix.gettimeofday () in
   let result = f g crit in
   let end_time = Unix.gettimeofday () in
   let time_exec = end_time -. start_time in
   let size_before_filter = if reachable then Some (S.cardinal result) else None in
   let time_filter, result_final =
     if reachable then
       let start_time = Unix.gettimeofday () in
       let result_final = filter g crit result in
       let end_time = Unix.gettimeofday () in
       let time_filter = end_time -. start_time in
       Some time_filter, result_final
     else
       None, result
   in
   { closure = result_final; size_before_filter; time_exec; time_filter }

let main nb algos =
    let start_time = Unix.gettimeofday () in
    (* let nb_e = int_of_float ((float)nb *. 1.5) in *)
    let nb_e = nb * 2 in
    let g = Rand.graph ~loops:true ~v:nb ~e:nb_e () in
    let vertices = S.elements (fold_vertex (fun u acc -> S.add u acc) g S.empty) in
    let crit = [List.nth vertices 0; List.nth vertices 2; List.nth vertices 4] in
    let crit = S.of_list crit in
    let end_time = Unix.gettimeofday () in
    let time_gen = end_time -. start_time in
    let vts = List.map (fun f -> test f g crit) algos in
    let rec aux res check vts =
      match vts with
      | [] -> res
      | algo_res :: vts' ->
        assert (S.equal check algo_res.closure);
        let semi_closure_size = match res.semi_closure_size, algo_res.size_before_filter with
                                | None, x -> x
                                | x, None -> x
                                | Some x, Some y -> assert (x = y); Some x
        in
        aux {res with semi_closure_size; results = res.results @ [algo_res.time_exec, algo_res.time_filter] } check vts'
    in
    let first = List.nth vts 0 in
    aux { gen_time = time_gen; closure_size = S.cardinal first.closure; semi_closure_size = None; results = [] } first.closure vts

let pp_line ~aggregate fmt line =
  let {gen_time; closure_size; semi_closure_size; results} = line in
  Format.fprintf fmt "%i," closure_size;
  (match semi_closure_size with
  | None -> Format.fprintf fmt "N/A,"
  | Some n -> Format.fprintf fmt "%i," n);
  Format.fprintf fmt "%.2e," gen_time;
  let size = List.length results in
  List.iteri (fun i (t_exec, t_filter) ->
                  begin match aggregate with
                  | Sum -> let t2 = match t_filter with | None -> 0. | Some t -> t in
                           Format.fprintf fmt "%.2e" (t_exec +. t2)
                  | Split -> Format.fprintf fmt "(%.2e|" t_exec;
                             begin match t_filter with
                             | None -> Format.fprintf fmt "N/A)"
                             | Some t -> Format.fprintf fmt "%.2e)" t
                             end
                  end;
                  if i < size - 1 then Format.fprintf fmt ",") results

let print ?(aggregate=Sum) nb repetitions algos =
  for i = 0 to repetitions - 1 do
     Format.printf "%d," nb;
     let ts = main nb algos in
     Format.printf "%a@." (pp_line ~aggregate) ts
  done

let () =
  if Array.exists ((=) "--list") Sys.argv
  then
  begin
    List.iter (fun (s, _) -> Format.printf "%s@." s) l;
    exit 0
  end;
  if Array.exists ((=) "--help") Sys.argv
  then
  begin
    Format.printf "Tests of different algorithms. Available options are:@;\
                   %-20s %s@;\
                   %-20s %s@;\
                   %-20s %s@;\
                   %-20s %s@."
                   "--help" "display this help"
                   "--list" "lists the available algorithm"
                   "--sum" "sums the times of the intern operations (default)"
                   "--split" "shows each time separately";
    exit 0
  end;
  if Array.length Sys.argv <= 5 then failwith "not enough arguments";
  let start = int_of_string (Sys.argv.(1)) in
  let final = int_of_string (Sys.argv.(2)) in
  let step = int_of_string (Sys.argv.(3)) in
  let repetitions = int_of_string (Sys.argv.(4)) in
  let other_args = Array.sub Sys.argv 5 (Array.length Sys.argv - 5) in
  let aggregate =
    if Array.exists ((=) "--split") Sys.argv then Split
    else Sum
  in
  let num_algos = Array.fold_left (fun acc opt -> if opt.[0] = '-' && opt.[1] = '-' then acc else 1+acc) 0 other_args in
  let i = ref 0 in
  let algo_names = Array.init num_algos (fun _ -> while let s = other_args.(!i) in s.[0] = '-' && s.[1] = '-' do incr i done;
                                                  let s = other_args.(!i) in incr i; s)
  in
  if Array.length algo_names = 0 then begin failwith "At least one algo must be specified" end;
  let algos = match Array.map (fun x -> List.assoc x l) algo_names with
              | exception Not_found -> failwith "unknown algo (to print a list, use --list)"
              | algos -> algos
  in
  Random.self_init ();
  Format.printf "nb_nodes,size,size_before_filtering,generation,";
  Array.iteri (fun i name -> if not(name.[0] = '-' && name.[1] = '-') then Format.printf "%s" name;
                             if i < Array.length algo_names - 1 then Format.printf ",") algo_names;
  Format.printf "@.";

  let algos = Array.to_list algos in
  let i = ref start in
  while !i <= final do
    print ~aggregate !i repetitions algos;
    i := !i + step
  done
