# Benchmarks

This directory contains OCaml implementations of Danicic's algorithm and our
optimization, along with the Coq formalization of Danicic's algorithm extracted
into OCaml.

The implementation of graphs used is said to be bidirectional (access to the successors
and access to the predecessors are both cheap).

## Compilation

Tested with OCaml 4.06.1 and ocamlbuild.0.12.0.
The version of OCamlgraph used is the master version, patched with a [pull request](https://github.com/backtracking/ocamlgraph/pull/69) that repairs the module `Fixpoint`.
To get the right version of OCamlgraph, you can type:
```
git clone https://github.com/backtracking/ocamlgraph/
cd ocamlgraph
git fetch origin pull/69/head:patched_version
git checkout patched_version
```
and then follow the build instructions of OCamlgraph.

The compilation relies on ocamlbuild. Just enter
```
ocamlbuild test_performance.native -pkg ocamlgraph -pkg unix
```

This produces an executable `test_performance.native` which itself calls the
implementations.

## Available algorithms

The following algorithms are available:
- `coq`: the Coq formalization of Danicic's algorithm extracted into OCaml,

- `danicic_very_naive_pathcheck`: a naive implementation of Danicic's algorithm,
- `danicic_very_naive_pathcheck_opti_n`: the naive implementation, improved by weakening the definition of critical edge,
- `danicic_very_naive_pathcheck_opti_mult`: the naive implementation, improved by considering all the critical edges at each step,
- `danicic_very_naive_pathcheck_opti_mult_n`: the naive implementation, improved by both optimizations,

- `danicic`: a smart implementation of Danicic's initial algorithm,
- `danicic_opti_n`: the smart implementation, improved by weakening the definition of critical edge,
- `danicic_opti_mult`: the smart implementation, improved by considering all the critical edges at each step,
- `danicic_opti_mult_n`: the smart implementation, improved by both optimizations,
- `danicic_opti_mult_n_no_reach`: the smart implementation with both optimizations, where reachability is checked only at the end,

- `optimized_why3`: the optimized algorithm manually extracted from the Why3 formalization,
- `optimized`: the optimized algorithm directly written in OCaml,
- `optimized_propagation`: the optimized algorithm, where a second propagation is performed instead of using a set of candidate nodes.

## Usage

`test_performance.native` automates the tests of the algorithms on
random graphs. It takes as arguments the sizes of the graph to generate
and the algorithms to call on those graphs. On each graph, all the algorithms
are called and their results are automatically compared. The execution fails
if the results are not equal.

For instructions, use
```
./test_performance.native --help
```

Example:
```
./test_performance.native 10 1000 10 5 danicic coq
```
For each number between 10 and 1000 and multiple of 10,
5 different graphs are generated of that size and Danicic's algorithm
and the Coq extraction are called on them.

The results are presented in columns:
- number of nodes in the graph
- size of the closure
- size of $V' \cup \operatorname{WD}_G(V')$ if one of the algorithms called computes this set
- time used by the generation step
- time used by the first algorithm (in s)
- time used by the second algorithm (in s)
- ...
