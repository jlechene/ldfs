open MyGraph

let theta_naive_pathcheck g vs u =
  let h = copy g in
  S.iter (fun u -> let s = succ_e h u in List.iter (fun e -> remove_edge_e h e) s) vs;
  let p = PathCheck.create h in
  S.fold (fun x e -> if PathCheck.check_path p u x then S.add x e else e) vs S.empty

let danicic_pathcheck g vs =
  let reachable_from = is_reachable_from g vs in
  let x = ref vs in
  let don = ref false in
  let exception Found in
  while not !don do
    don := true;
    try
      iter_edges (fun u v -> if reachable_from u && S.cardinal (theta_naive_pathcheck g !x v) = 1 && 2 <= S.cardinal (theta_naive_pathcheck g !x u) then (x := S.add u !x; raise Found)) g;
    with Found -> don := false
  done;
  !x

(* The definition of critical edge is weakened *)
let danicic_pathcheck_opti_n g vs =
  let reachable_from = is_reachable_from g vs in
  let x = ref vs in
  let don = ref false in
  let exception Found in
  while not !don do
    don := true;
    try
      iter_edges (fun u v -> if reachable_from u && let n = S.cardinal (theta_naive_pathcheck g !x v) in
                             n >= 1 && n < S.cardinal (theta_naive_pathcheck g !x u) then (x := S.add u !x; raise Found)) g;
    with Found -> don := false
  done;
  !x

(* Several nodes can be added in each iteration *)
let danicic_pathcheck_opti_mult g vs =
  let reachable_from = is_reachable_from g vs in
  let x = ref vs in
  let don = ref false in
  while not !don do
    don := true;
    let x0 = !x in
    iter_edges (fun u v -> if reachable_from u && S.cardinal (theta_naive_pathcheck g x0 v) = 1 && 2 <= S.cardinal (theta_naive_pathcheck g x0 u) then (x := S.add u !x; don := false)) g;
  done;
  !x

(* Danicic's implementation with both optimizations *)
let danicic_pathcheck_opti_mult_n g vs =
  let reachable_from = is_reachable_from g vs in
  let x = ref vs in
  let don = ref false in
  while not !don do
    don := true;
    let x0 = !x in
    iter_edges (fun u v -> if reachable_from u && let n = S.cardinal (theta_naive_pathcheck g x0 v) in
                           n >= 1 && n < S.cardinal (theta_naive_pathcheck g x0 u) then (x := S.add u !x; don := false)) g;
  done;
  !x
