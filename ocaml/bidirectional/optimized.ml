open MyGraph

(* OCaml version of optimized *)
let confirm g obs u u' =
  let exception Found in
  try
    iter_succ (fun v -> match Hashtbl.find obs v with
                        | exception Not_found -> ()
                        | v' -> if not (V.equal u' v') then raise Found) g u; false
  with
  | Found -> true

let propagate g ws obs u v =
  let l = ref (S.singleton u) in
  let c = ref S.empty in
  while not (S.is_empty !l) do
    let w = S.choose !l in
    l := S.remove w !l;
    iter_pred (fun w0 ->
      if not (S.mem w0 ws) then
      match Hashtbl.find obs w0 with
      | exception Not_found -> Hashtbl.add obs w0 v; l := S.add w0 !l
      | w0' -> if not (V.equal w0' v) then
                 (Hashtbl.replace obs w0 v;
                  l := S.add w0 !l;
                  if out_degree g w0 > 1 then c := S.add w0 !c)
              ) g w
  done;
  !c

let main g vs =
  let ws = ref vs in
  let l = ref vs in
  let obs = Hashtbl.create 97 in
  S.iter (fun v -> Hashtbl.replace obs v v) vs;
  while not (S.is_empty !l) do
    let u = S.choose !l in
    l := S.remove u !l;
    let c = propagate g !ws obs u u in
    let delta = S.filter (fun v -> confirm g obs v u) c in
    ws := S.union delta !ws;
    l := S.union delta !l;
    S.iter (fun v -> Hashtbl.replace obs v v) delta
  done;
  (!ws, obs)

let wrap g vs =
  fst (main g vs)
