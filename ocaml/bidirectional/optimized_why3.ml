open MyGraph

(* Adapted from the Why3 formalization. *)

module H = Hashtbl

module B : sig
  type t
  val empty : unit -> t
  val is_empty : t -> bool
  val mem : vertex -> t -> bool
  val add : vertex -> t -> unit
  val impset_of_list : vertex list -> t
  val choose_and_remove : t -> vertex
  val contents : t -> S.t
end = struct
  type t = S.t ref
  let empty () = ref S.empty
  let is_empty x = S.is_empty !x
  let mem u x = S.mem u !x
  let add u x = x := S.add u !x
  let impset_of_list l = ref (S.of_list l)
  let choose_and_remove x = let u = S.choose !x in x := S.remove u !x; u
  let contents x = !x
end

let eq_vertex = V.equal

let confirm g obs u u_obs =
    let result = ref false in
    let succ0 = B.impset_of_list (succ g u) in
    while not (B.is_empty succ0) do
      let v = B.choose_and_remove succ0 in
      if H.mem obs v && not (eq_vertex u_obs (H.find obs v)) then result := true;
    done;
    !result

  let propagate (g : t) (w : B.t) obs (u :vertex) (v : vertex) =
    let worklist = B.empty () in
    B.add u worklist;
    let candidates = B.empty () in
    while not (B.is_empty worklist) do
       let n = B.choose_and_remove worklist in
       let pred_todo = B.impset_of_list (pred g n) in
       while not (B.is_empty pred_todo) do
          let u0 = B.choose_and_remove pred_todo in
          if not (B.mem u0 w) then
          begin
             if H.mem obs u0 then
             begin
               if not (eq_vertex (H.find obs u0) v) then
               begin
                 H.replace obs u0 v;
                 B.add u0 worklist;
                 if out_degree g u0 > 1 then B.add u0 candidates
               end
             end
             else
             begin
               H.add obs u0 v;
               B.add u0 worklist;
             end
          end
       done;
    done;
    candidates

let main g v' =
  let w = B.empty () in
  let obs = H.create 97 in
  let worklist = B.empty () in

  let tmp = B.impset_of_list v' in
  while not (B.is_empty tmp) do
    let u = B.choose_and_remove tmp in
    B.add u w;
    H.add obs u u;
    B.add u worklist
  done;

  while not (B.is_empty worklist) do
    let u = B.choose_and_remove worklist in
    let candidates = propagate g w obs u u in

    let new_nodes = B.empty () in
    while not (B.is_empty candidates) do
      let v = B.choose_and_remove candidates in
      if confirm g obs v u then B.add v new_nodes
    done;

    while not (B.is_empty new_nodes) do
      let v = B.choose_and_remove new_nodes in
      B.add v w;
      H.replace obs v v;
      B.add v worklist;
    done
  done;
  (B.contents w, obs)

let wrap g v' =
  fst (main g (S.elements v'))
