open MyGraph

let theta_naive g vs =
  let h = copy g in
  S.iter (fun u -> let s = succ_e h u in List.iter (fun e -> remove_edge_e h e) s) vs;
  let obs = naive_observable_in h vs in
  fun u -> obs u

let danicic g vs =
  let reachable_from = is_reachable_from g vs in
  let x = ref vs in
  let don = ref false in
  let exception Found in
  while (not !don) do
    don := true;
    try
      let obs = theta_naive g !x in
      iter_edges (fun u v -> if reachable_from u
                             && S.cardinal (obs v) = 1
                             && S.cardinal (obs u) >= 2
                             then (x := S.add u !x; raise Found)) g
    with Found -> (don := false)
  done;
  !x

(* The definition of critical edge is weakened *)
let danicic_opti_n g vs =
  let reachable_from = is_reachable_from g vs in
  let x = ref vs in
  let don = ref false in
  let exception Found in
  while (not !don) do
    don := true;
    try
      let obs = theta_naive g !x in
      iter_edges (fun u v -> if reachable_from u &&
                             let n = S.cardinal (obs v) in
                             n >= 1
                             && S.cardinal (obs u) > n
                             then (x := S.add u !x; raise Found)) g
    with Found -> (don := false)
  done;
  !x

(* Several nodes can be added in each iteration *)
let danicic_opti_mult g vs =
  let reachable_from = is_reachable_from g vs in
  let x = ref vs in
  let don = ref false in
  while (not !don) do
    don := true;
    let obs = theta_naive g !x in
    iter_edges (fun u v -> if reachable_from u
                           && S.cardinal (obs v) = 1
                           && S.cardinal (obs u) >= 2
                           then (x := S.add u !x; don := false)) g
  done;
  !x

(* Danicic's implementation with both optimizations *)
let danicic_opti_mult_n g vs =
  let reachable_from = is_reachable_from g vs in
  let x = ref vs in
  let don = ref false in
  while (not !don) do
    don := true;
    let obs = theta_naive g !x in
    iter_edges (fun u v -> if reachable_from u &&
                           let n = S.cardinal (obs v) in
                           n >= 1
                           && S.cardinal (obs u) > n
                           then (x := S.add u !x; don := false)) g
  done;
  !x

(* Danicic's implementation with both optimizations,
   and reachability checked only at the end *)
let danicic_opti_mult_n_no_reach g vs =
  let x = ref vs in
  let don = ref false in
  while (not !don) do
    don := true;
    let obs = theta_naive g !x in
    iter_edges (fun u v -> if
                           let n = S.cardinal (obs v) in
                           n >= 1
                           && S.cardinal (obs u) > n
                           then (x := S.add u !x; don := false)) g
  done;
  !x
