module C = struct
  type t = int
  let compare = Pervasives.compare
  let hash = Hashtbl.hash
  let equal = (=)
end

module S = Set.Make (C)

module G = Graph.Imperative.Digraph.ConcreteBidirectional (C)
include G

let affiche fmt v =
  Format.fprintf fmt "N:%d" v

let affiche_edge fmt e =
  Format.fprintf fmt "%a-%a" affiche (E.src e) affiche (E.dst e)

let affiche_occ fmt occ =
  Format.fprintf fmt "(%a, %d)" affiche (fst occ) (snd occ)

let affiche_list print fmt l =
  Format.fprintf fmt "[";
  List.iteri (fun i x -> if i <> List.length l - 1 then Format.fprintf fmt "%a, " print x else Format.fprintf fmt "%a" print x) l;
  Format.fprintf fmt "]"

let affiche_set print fmt s =
  Format.fprintf fmt "{";
  let n = S.cardinal s in
  let count = ref 0 in
  S.iter (fun x -> if !count <> n - 1 then Format.fprintf fmt "%a, " print x else Format.fprintf fmt "%a" print x; incr count) s;
  Format.fprintf fmt "}"

let affiche_hashtbl print_key print_elt fmt h =
  Format.fprintf fmt "〈";
  let n = Hashtbl.length h in
  let count = ref 0 in
  Hashtbl.iter (fun key elt-> if !count <> n - 1 then Format.fprintf fmt "(%a, %a), " print_key key print_elt elt else Format.fprintf fmt "(%a, %a)" print_key key print_elt elt; incr count) h;
  Format.fprintf fmt "〉"

let affiche_stack print fmt s =
    Format.fprintf fmt "[";
    let i = ref 0 in
    Stack.iter (fun x -> if !i <> Stack.length s - 1 then Format.fprintf fmt "%a, " print x else Format.fprintf fmt "%a" print x; incr i) s;
    Format.fprintf fmt "]"

module Rand = Graph.Rand.I(G)

let is_reachable_from g vs =
  let l = ref vs in
  let reach = Hashtbl.create 97 in
  S.iter (fun u -> Hashtbl.add reach u true) vs;
  while not (S.is_empty !l) do
    let x = S.choose !l in
    l := S.remove x !l;
    iter_succ (fun u -> if not (Hashtbl.mem reach u) then
                          (Hashtbl.add reach u true; l := S.add u !l)) g x
  done;
  fun u -> Hashtbl.mem reach u

module PathCheck = Graph.Path.Check (G)

let naive_observable_in g vs =
  let module R = Graph.Fixpoint.Make(G)
          (struct
            type vertex = V.t
            type edge = E.t
            type g = t
            type data = S.t
            let direction = Graph.Fixpoint.Backward
            let equal = S.equal
            let join = S.union
            let analyze _ d = d
          end)
  in
  R.analyze (fun u -> if S.mem u vs then S.singleton u else S.empty) g
