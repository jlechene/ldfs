# Formalization and proof of the optimized algorithm, lDFS, in Why3 0.90+git

## Versions of Why3 and provers

- Why3 0.90+git
  ```
  git clone https://gitlab.inria.fr/why3/why3.git
  git checkout 1991be5c7
  ```
- Alt-Ergo 1.30
  ```
  opam install alt-ergo.1.30
  ```
- CVC4 1.5:
  http://cvc4.cs.stanford.edu/downloads/builds/x86_64-linux-opt/cvc4-1.5-x86_64-linux-opt
- Coq 8.6.1
  ```
  opam install coq.8.6.1 coqide.8.6.1
  ```
- Eprover 2.0:
  http://wwwlehre.dhbw-stuttgart.de/~sschulz/WORK/E_DOWNLOAD/V_2.0/E.tgz

## Commands

- `why3 ide formalization.mlw`: opens Why3 IDE to interact with the proofs.
- `why3 replay formalization.mlw`: checks that all the proofs are correct.
- `why3 session html formalization.mlw`: creates the file
  `formalization/why3session.html` presenting the list of goals
  and, for each, the prover used (the file has already been created,
  this command recreates it).
