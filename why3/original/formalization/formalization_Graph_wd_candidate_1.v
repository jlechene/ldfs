(* This file is generated by Why3's Coq driver *)
(* Beware! Only edit allowed sections below    *)
Require Import BuiltIn.
Require BuiltIn.
Require HighOrd.
Require int.Int.
Require list.List.
Require list.Length.
Require list.Mem.
Require option.Option.
Require list.Append.

Axiom set : forall (a:Type), Type.
Parameter set_WhyType : forall (a:Type) {a_WT:WhyType a}, WhyType (set a).
Existing Instance set_WhyType.

Parameter mem: forall {a:Type} {a_WT:WhyType a}, a -> (set a) -> Prop.

Parameter infix_eqeq: forall {a:Type} {a_WT:WhyType a}, (set a) -> (set a) ->
  Prop.

Axiom infix_eqeq_spec : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), (infix_eqeq s1 s2) <-> forall (x:a), (mem x s1) <-> (mem x
  s2).

Axiom extensionality : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), (infix_eqeq s1 s2) -> (s1 = s2).

Parameter subset: forall {a:Type} {a_WT:WhyType a}, (set a) -> (set a) ->
  Prop.

Axiom subset_spec : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), (subset s1 s2) <-> forall (x:a), (mem x s1) -> (mem x s2).

Axiom subset_refl : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  (subset s s).

Axiom subset_trans : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)) (s3:(set a)), (subset s1 s2) -> ((subset s2 s3) -> (subset s1
  s3)).

Parameter is_empty: forall {a:Type} {a_WT:WhyType a}, (set a) -> Prop.

Axiom is_empty_spec : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  (is_empty s) <-> forall (x:a), ~ (mem x s).

Parameter empty: forall {a:Type} {a_WT:WhyType a}, (set a).

Axiom empty_def : forall {a:Type} {a_WT:WhyType a}, (is_empty (empty : (set
  a))).

Parameter add: forall {a:Type} {a_WT:WhyType a}, a -> (set a) -> (set a).

Axiom add_spec : forall {a:Type} {a_WT:WhyType a}, forall (x:a) (s:(set a)),
  forall (y:a), (mem y (add x s)) <-> ((y = x) \/ (mem y s)).

Parameter remove: forall {a:Type} {a_WT:WhyType a}, a -> (set a) -> (set a).

Axiom remove_spec : forall {a:Type} {a_WT:WhyType a}, forall (x:a) (s:(set
  a)), forall (y:a), (mem y (remove x s)) <-> ((~ (y = x)) /\ (mem y s)).

Axiom add_remove : forall {a:Type} {a_WT:WhyType a}, forall (x:a) (s:(set
  a)), (mem x s) -> ((add x (remove x s)) = s).

Axiom remove_add : forall {a:Type} {a_WT:WhyType a}, forall (x:a) (s:(set
  a)), ((remove x (add x s)) = (remove x s)).

Axiom subset_remove : forall {a:Type} {a_WT:WhyType a}, forall (x:a) (s:(set
  a)), (subset (remove x s) s).

Parameter union: forall {a:Type} {a_WT:WhyType a}, (set a) -> (set a) -> (set
  a).

Axiom union_spec : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), forall (x:a), (mem x (union s1 s2)) <-> ((mem x s1) \/ (mem x
  s2)).

Parameter inter: forall {a:Type} {a_WT:WhyType a}, (set a) -> (set a) -> (set
  a).

Axiom inter_spec : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), forall (x:a), (mem x (inter s1 s2)) <-> ((mem x s1) /\ (mem x
  s2)).

Parameter diff: forall {a:Type} {a_WT:WhyType a}, (set a) -> (set a) -> (set
  a).

Axiom diff_spec : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), forall (x:a), (mem x (diff s1 s2)) <-> ((mem x s1) /\ ~ (mem
  x s2)).

Axiom subset_diff : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), (subset (diff s1 s2) s1).

Parameter choose: forall {a:Type} {a_WT:WhyType a}, (set a) -> a.

Axiom choose_spec : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  (~ (is_empty s)) -> (mem (choose s) s).

Parameter cardinal: forall {a:Type} {a_WT:WhyType a}, (set a) -> Z.

Axiom cardinal_nonneg : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  (0%Z <= (cardinal s))%Z.

Axiom cardinal_empty : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  ((cardinal s) = 0%Z) <-> (is_empty s).

Axiom cardinal_add : forall {a:Type} {a_WT:WhyType a}, forall (x:a),
  forall (s:(set a)), (~ (mem x s)) -> ((cardinal (add x
  s)) = (1%Z + (cardinal s))%Z).

Axiom cardinal_remove : forall {a:Type} {a_WT:WhyType a}, forall (x:a),
  forall (s:(set a)), (mem x s) -> ((cardinal s) = (1%Z + (cardinal (remove x
  s)))%Z).

Axiom cardinal_subset : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), (subset s1 s2) -> ((cardinal s1) <= (cardinal s2))%Z.

Axiom subset_eq : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), (subset s1 s2) -> (((cardinal s1) = (cardinal s2)) ->
  (infix_eqeq s1 s2)).

Axiom cardinal1 : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  ((cardinal s) = 1%Z) -> forall (x:a), (mem x s) -> (x = (choose s)).

Axiom graph : Type.
Parameter graph_WhyType : WhyType graph.
Existing Instance graph_WhyType.

Axiom vertex : Type.
Parameter vertex_WhyType : WhyType vertex.
Existing Instance vertex_WhyType.

Parameter support: graph -> (set vertex).

Parameter succ: graph -> vertex -> (set vertex).

Axiom succ_support : forall (g:graph) (u:vertex), (mem u (support g)) ->
  (subset (succ g u) (support g)).

Parameter pred: graph -> vertex -> (set vertex).

Axiom pred_succ : forall (g:graph) (u:vertex) (v:vertex), (mem v (succ g
  u)) <-> (mem u (pred g v)).

Axiom pred_support : forall (g:graph) (u:vertex), (mem u (support g)) ->
  (subset (pred g u) (support g)).

(* Why3 assumption *)
Inductive is_path: graph -> vertex -> vertex -> (list vertex) -> Prop :=
  | is_path_empty : forall (g:graph) (u:vertex), (mem u (support g)) ->
      (is_path g u u (Init.Datatypes.cons u Init.Datatypes.nil))
  | is_path_succ : forall (g:graph) (u:vertex) (v1:vertex) (v2:vertex)
      (l:(list vertex)), (mem v1 (succ g u)) -> ((is_path g v1 v2 l) ->
      (is_path g u v2 (Init.Datatypes.cons u l))).

Axiom is_path_last : forall (g:graph) (u:vertex) (v:vertex)
  (l:(list vertex)), (is_path g u v l) -> (list.Mem.mem v l).

(* Why3 assumption *)
Inductive path: graph -> vertex -> vertex -> Prop :=
  | path_empty : forall (g:graph) (u:vertex), (mem u (support g)) -> (path g
      u u)
  | path_succ : forall (g:graph) (u:vertex) (v1:vertex) (v2:vertex), (mem v1
      (succ g u)) -> ((path g v1 v2) -> (path g u v2)).

Axiom path_is_path : forall (g:graph) (u:vertex) (v:vertex), (path g u v) ->
  exists l:(list vertex), (is_path g u v l).

Axiom is_path_path : forall (g:graph) (u:vertex) (v:vertex)
  (l:(list vertex)), (is_path g u v l) -> (path g u v).

Axiom path_support_1 : forall (g:graph) (v1:vertex) (v2:vertex), (path g v1
  v2) -> (mem v1 (support g)).

Axiom path_support_2 : forall (g:graph) (v1:vertex) (v2:vertex), (path g v1
  v2) -> (mem v2 (support g)).

Parameter can_reach: (graph -> ((set vertex) -> (set vertex))).

Axiom can_reach_spec : forall (g:graph) (u:vertex) (v':(set vertex)), (mem u
  ((can_reach g) v')) <-> exists v:vertex, (mem v v') /\ (path g u v).

(* Why3 assumption *)
Inductive is_v_disjoint: graph -> (set vertex) -> vertex -> vertex ->
  (list vertex) -> Prop :=
  | is_v_disjoint_nil : forall (g:graph) (v':(set vertex)) (u:vertex), (mem u
      (support g)) -> (is_v_disjoint g v' u u
      (Init.Datatypes.cons u Init.Datatypes.nil))
  | is_v_disjoint_cons : forall (g:graph) (v':(set vertex)) (u:vertex)
      (v1:vertex) (v2:vertex) (l:(list vertex)), (mem v1 (succ g u)) ->
      ((~ (mem u v')) -> ((is_v_disjoint g v' v1 v2 l) -> (is_v_disjoint g v'
      u v2 (Init.Datatypes.cons u l)))).

Axiom is_v_disjoint_is_path : forall (g:graph) (v':(set vertex)) (u:vertex)
  (v:vertex) (l:(list vertex)), (is_v_disjoint g v' u v l) -> (is_path g u v
  l).

Axiom is_v_disjoint_split_path_l : forall (g:graph) (v':(set vertex))
  (u:vertex) (v:vertex) (l1:(list vertex)) (v_l:vertex) (l2:(list vertex)),
  (is_v_disjoint g v' u v
  (Init.Datatypes.app l1 (Init.Datatypes.cons v_l l2))) -> (is_v_disjoint g
  v' u v_l
  (Init.Datatypes.app l1 (Init.Datatypes.cons v_l Init.Datatypes.nil))).

Axiom is_v_disjoint_split_path_r : forall (g:graph) (v':(set vertex))
  (u:vertex) (v:vertex) (l1:(list vertex)) (v_l:vertex) (l2:(list vertex)),
  (is_v_disjoint g v' u v
  (Init.Datatypes.app l1 (Init.Datatypes.cons v_l l2))) -> (is_v_disjoint g
  v' v_l v (Init.Datatypes.cons v_l l2)).

(* Why3 assumption *)
Inductive v_disjoint: graph -> (set vertex) -> vertex -> vertex -> Prop :=
  | v_disjoint_nil : forall (g:graph) (v':(set vertex)) (u:vertex), (mem u
      (support g)) -> (v_disjoint g v' u u)
  | v_disjoint_cons : forall (g:graph) (v':(set vertex)) (u:vertex)
      (v1:vertex) (v2:vertex), (mem v1 (succ g u)) -> ((~ (mem u v')) ->
      ((v_disjoint g v' v1 v2) -> (v_disjoint g v' u v2))).

Axiom v_disjoint_is_v_disjoint : forall (g:graph) (v':(set vertex))
  (u:vertex) (v:vertex), (v_disjoint g v' u v) -> exists l:(list vertex),
  (is_v_disjoint g v' u v l).

Axiom is_v_disjoint_v_disjoint : forall (g:graph) (v':(set vertex))
  (u:vertex) (v:vertex) (l:(list vertex)), (is_v_disjoint g v' u v l) ->
  (v_disjoint g v' u v).

Axiom v_disjoint_subset : forall (g:graph) (v1':(set vertex)) (v2':(set
  vertex)) (u:vertex) (v:vertex), (subset v1' v2') -> ((v_disjoint g v2' u
  v) -> (v_disjoint g v1' u v)).

Axiom v_disjoint_trans : forall (g:graph) (v':(set vertex)) (u:vertex)
  (v1:vertex) (v2:vertex), (v_disjoint g v' u v1) -> ((v_disjoint g v' v1
  v2) -> (v_disjoint g v' u v2)).

(* Why3 assumption *)
Definition is_v_path (g:graph) (v':(set vertex)) (u:vertex) (v:vertex)
  (l:(list vertex)): Prop := (is_v_disjoint g v' u v l) /\ (mem v v').

Axiom is_path_exists_is_v_path : forall (g:graph) (v':(set vertex))
  (u1:vertex) (u2:vertex) (l:(list vertex)), (is_path g u1 u2 l) -> ((mem u2
  v') -> exists l1:(list vertex), exists v:vertex, exists l2:(list vertex),
  (is_v_path g v' u1 v l1) /\ (l = (Init.Datatypes.app l1 l2))).

(* Why3 assumption *)
Definition v_path (g:graph) (v':(set vertex)) (u:vertex) (v:vertex): Prop :=
  (v_disjoint g v' u v) /\ (mem v v').

Axiom v_path_trans : forall (g:graph) (v':(set vertex)) (u:vertex)
  (v1:vertex) (v2:vertex), (v_disjoint g v' u v1) -> ((v_path g v' v1 v2) ->
  (v_path g v' u v2)).

Axiom path_exists_v_path : forall (g:graph) (v':(set vertex)) (u1:vertex)
  (u2:vertex), (path g u1 u2) -> ((mem u2 v') -> exists v:vertex, (v_path g
  v' u1 v)).

Parameter obs_g: (graph -> (vertex -> ((set vertex) -> (set vertex)))).

Axiom obs_g_spec : forall (g:graph) (u:vertex) (v':(set vertex)) (v:vertex),
  (mem v (((obs_g g) u) v')) <-> (v_path g v' u v).

Axiom obs_g_closure : forall (g:graph) (u:vertex) (v':(set vertex)) (w:(set
  vertex)), (mem u w) -> ((forall (v1:vertex) (v2:vertex), (mem v2 w) ->
  ((~ (mem v1 v')) -> ((mem v1 (pred g v2)) -> (mem v1 w)))) ->
  forall (z:vertex), (mem u (((obs_g g) z) v')) -> (mem z w)).

Parameter can_reachB: (graph -> (vertex -> ((set vertex) -> (set vertex)))).

Axiom can_reachB_spec : forall (g:graph) (v:vertex) (vb:(set vertex))
  (u:vertex), (mem u (((can_reachB g) v) vb)) <-> (mem v (((obs_g g) u) vb)).

Parameter wd: (graph -> ((set vertex) -> (set vertex))).

Axiom wd_spec : forall (g:graph) (v':(set vertex)) (u:vertex), (mem u ((wd g)
  v')) <-> exists u1:vertex, exists l1:(list vertex), exists u2:vertex,
  exists l2:(list vertex), (~ (u1 = u2)) /\ ((is_v_path g v' u u1 l1) /\
  ((is_v_path g v' u u2 l2) /\ forall (z:vertex), (list.Mem.mem z l1) ->
  ((list.Mem.mem z l2) -> (z = u)))).

Axiom wd_disjoint : forall (g:graph) (v':(set vertex)) (v:vertex), (mem v
  ((wd g) v')) -> ~ (mem v v').

Axiom wd_property_2 : forall (g:graph) (v1':(set vertex)) (v2':(set vertex)),
  (subset v1' v2') -> (subset ((wd g) v1') (union v2' ((wd g) v2'))).

Axiom wd_property_3 : forall (g:graph) (v':(set vertex)), (is_empty ((wd g)
  (union v' ((wd g) v')))).

(* Why3 goal *)
Theorem wd_candidate : forall (g:graph) (v':(set vertex)) (w:(set vertex)),
  (subset v' w) -> ((subset w (union v' ((wd g) v'))) -> (infix_eqeq (union w
  ((wd g) w)) (union v' ((wd g) v')))).
intros g v' w h1 h2.
pose proof (wd_property_2 g _ _ h1).
pose proof (wd_property_2 g _ _ h2).
pose proof (wd_property_3 g v').
rewrite is_empty_spec in H1.
rewrite subset_spec in h1, h2, H, H0.
apply infix_eqeq_spec. split; intros.
- apply union_spec in H2.
  destruct H2.
  + auto.
  + apply H0 in H2. apply union_spec in H2. destruct H2; [assumption|].
    apply H1 in H2. contradiction.
- apply union_spec in H2.
  destruct H2.
  + apply union_spec. left; auto.
  + auto.
Qed.
