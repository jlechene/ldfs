# Formalization and proof of the optimized algorithm, lDFS, in Why3

This is a formalization in Why3 of lDFS, an algorithm computing weak
control-closure optimizing Danicic's algorithm (Danicic et al., 2011).

The algorithm itself is proved correct. All of the needed graph-theoretic
properties are proved, except one (`wd_property_3`) that is assumed but is
proved in the Coq formalization (see [`coq/`](/coq/)).

## Contents

The original development was done with a development version of Why3 preparing
version 1.0. It is made available in subdirectory [`original/`](original/).
The exact commit used was lost, but commit `1991be5c7` is compatible with this
original development.

Since it can be cumbersome to install Why3 at this exact version, the
development was ported to version 1.0 of Why3. It is made available in
subdirectory [`1.0/`](1.0/). Due to the changes between
both versions, some automatic proofs did not work anymore. They were replaced
with other proofs, either automatic proofs using other SMTs or manual proofs in
Coq.
