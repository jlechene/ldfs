# Formalization and proof of the optimized algorithm, lDFS, in Why3 1.0.0

## Versions of Why3 and provers

- Why3 1.0.0
  ```
  opam install why3.1.0.0
  ```
- Alt-Ergo 1.30
  ```
  opam install alt-ergo.1.30
  ```
- CVC4 1.5:
  http://cvc4.cs.stanford.edu/downloads/builds/x86_64-linux-opt/cvc4-1.5-x86_64-linux-opt
- Coq 8.6.1
  ```
  opam install coq.8.6.1 coqide.8.6.1
  ```
- Eprover 2.0:
  http://wwwlehre.dhbw-stuttgart.de/~sschulz/WORK/E_DOWNLOAD/V_2.0/E.tgz

## Commands

- `why3 ide formalization.mlw`: opens Why3 IDE to interact with the proofs.
- `why3 replay formalization.mlw`: checks that all the proofs are correct.
- `why3 session html formalization.mlw`: creates the file
  `formalization/why3session.html` presenting the list of goals
  and, for each, the prover used (the file has already been created,
  this command recreates it).
