Module Type Labels.

Parameter VertexLabel : Type.

Parameter EdgeLabel : Type.

End Labels.

Module Type Labels_with_weight <: Labels.
  Include Labels.
  Parameter weight : EdgeLabel -> nat. (* from ocamlgraph *)
End Labels_with_weight.

Module Nat_Labels <: Labels.

Definition VertexLabel := nat.

Definition EdgeLabel := nat.

End Nat_Labels.