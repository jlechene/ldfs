Require Import FMaps.

Module MyMapFacts (M : S).

  (* standard library facts *)
  Module MapFacts := Facts M.

  Include MapFacts.

  Section MapsTo_In.

    Context {A : Type}.

    Lemma MapsTo_In : forall x (e : A) m,
      M.MapsTo x e m ->
      M.In x m.
    Proof. 
      intros. rewrite MapFacts.in_find_iff.
      rewrite MapFacts.find_mapsto_iff in H. rewrite H; discriminate.
    Qed.

    Lemma In_MapsTo : forall x m,
      M.In x m ->
      exists e : A, M.MapsTo x e m.
    Proof.
      intros. rewrite MapFacts.in_find_iff in H.
      destruct (M.find x m) as [e|] eqn:Heqfind.
      exists e. rewrite MapFacts.find_mapsto_iff. assumption.
      contradiction H; reflexivity.
    Qed.

    Lemma Mapsto_In : forall v (m : M.t A),
      M.In v m <-> exists x, M.MapsTo v x m.
    Proof.
      split.
      - apply In_MapsTo.
      - intros. destruct H as [x H].
        (* apply In_MapsTo. (* pourquoi ça marche ?*) *)
        apply MapsTo_In in H. assumption.
    Qed.

  End MapsTo_In.

  Section Submap.

    Context {A : Type}.

    Definition SubMap m1 m2 := forall k (v : A), 
      M.MapsTo k v m1 -> M.MapsTo k v m2.

  End Submap.

  Section general_associative_fold.

  Context {A B : Type}.
  Variable eqA : A -> A -> Prop.
  Variables eqB : B -> B -> Prop.
  Hypothesis eqA_refl : forall a, eqA a a.
  Hypothesis eqA_trans : forall x y z, eqA x y -> eqA y z -> eqA x z.

    Lemma fold_left_compat : forall f l e e',
      eqA e e' ->
      (forall (e1 e2: A) (a : B), eqA e1 e2 -> eqA (f e1 a) (f e2 a)) ->
      eqA (fold_left f l e) (fold_left f l e').
    Proof.
      intros f l. induction l; intros.
      - (* l = nil *)
        assumption.
      - (* l = a :: l *)
        apply IHl.
        apply H0; assumption.
        assumption.
    Qed.

    Lemma fold_left_assoc : forall f,
      (forall (y z : B) (s : A), eqA (f (f s y) z)  (f (f s z) y)) ->
      (forall (e1 e2 : A) (a : B), eqA e1 e2 -> eqA (f e1 a) (f e2 a)) ->
      forall l x a, eqA (fold_left f (a :: l) x)  (f (fold_left f l x) a).
    Proof.
      intros. revert x a. induction l; intros; simpl.
      - (* l = nil *)
        apply eqA_refl.
      - (* l = a :: l *)
        eapply eqA_trans.
        + apply fold_left_compat; [| assumption].
          apply H.
        + apply IHl.
    Qed.

    Lemma fold_left_assoc_map_find_nodup : forall l f x h,
      NoDupA eqB (h :: l) ->
      (forall (y z : B) s, ~ eqB y z -> eqA (f (f s y) z) (f (f s z) y)) ->
      (forall (e1 e2 : A) a, eqA e1 e2 -> eqA (f e1 a) (f e2 a)) ->
      eqA (fold_left f (h :: l) x) (f (fold_left f l x) h).
    Proof.
      intros l. induction l; intros; simpl.
      - (* l = nil *)
        apply eqA_refl.
      - (* l = a :: l *)
        eapply eqA_trans.
        + apply fold_left_compat; [| assumption].
          apply H0.
          inversion H; subst. intros contra. apply H4.
          apply InA_cons_hd; assumption.
        + simpl in IHl. apply IHl; try assumption.
          inversion H; subst. inversion H5; subst.
          constructor; [| assumption].
          intros contra. apply H4. apply InA_cons_tl; assumption.
    Qed.

  End general_associative_fold.

  Section associative_fold_maps.

    Context {A B : Type}.
    Variable eqA : A -> A -> Prop.
    Hypothesis eqA_refl : forall a, eqA a a.
    Hypothesis eqA_trans : forall x y z, eqA x y -> eqA y z -> eqA x z.

    (******** generalization of M.Equal with any equality for elements ******)

    Inductive eq_map_option : option A -> option A -> Prop :=
    | None_eq : eq_map_option None None
    | Some_eq : forall m m', eqA m m' -> eq_map_option (Some m) (Some m').

    (*
      Definition EqualMap m1 m2 := forall x e1 e2,
        eqA e1 e2 <-> (M.MapsTo x e1 m1 <-> M.MapsTo x e2 m2).
    *)

    Definition EqualMap m1 m2 := forall x, eq_map_option (M.find x m1) (M.find x m2).

    Lemma EqualMap_refl : forall m, EqualMap m m.
    Proof.
      unfold EqualMap. intros m x.
      destruct (M.find x m).
      - constructor. apply eqA_refl.
      - constructor.
    Qed.

    Lemma EqualMap_trans : forall m1 m2 m3,
    EqualMap m1 m2 ->
    EqualMap m2 m3 ->
    EqualMap m1 m3.
    Proof.
      intros m1 m2 m3 H H0. unfold EqualMap in *.
      intros x.
      specialize (H x). specialize (H0 x).
      destruct (M.find x m1).
      - (* find x m2 = Some *)
        inversion H; subst.
        rewrite <- H2 in H0.
        destruct (M.find x m3).
        + (* find x m3 = Some *)
          constructor. inversion H0; subst.
          eapply eqA_trans; eassumption.
        + (* find x m3 = None *)
          inversion H0.
      - (* find x m2 = None *)
        destruct (M.find x m3).
        + (* find x m3 = Some *)
          inversion H0; subst. inversion H. rewrite <- H1 in H2. discriminate.
        + (* find x m3 = None *)
          constructor.
    Qed.

    Lemma add_add_Eq : forall x y (fx fy : A) m,
      ~ M.E.eq x y ->
      M.Equal (M.add x fx (M.add y fy m))
                   (M.add y fy (M.add x fx m)).
    Proof.
      unfold M.Equal; intros.
      destruct (M.E.eq_dec y0 x).
      - (* y0 = x *)
        rewrite MapFacts.add_eq_o by (apply M.E.eq_sym; assumption).
        rewrite MapFacts.add_neq_o. rewrite MapFacts.add_eq_o. reflexivity.
        apply M.E.eq_sym; assumption. intros contra. apply H.
        apply M.E.eq_sym; eapply M.E.eq_trans; eassumption.
      - (* y0 <> x *)
        rewrite MapFacts.add_neq_o
          by (intros contra; apply n; apply M.E.eq_sym; assumption).
        destruct (M.E.eq_dec y0 y).
        + (* y0 = y *)
          rewrite 2!MapFacts.add_eq_o by (apply M.E.eq_sym; assumption).
          reflexivity.
        + rewrite !MapFacts.add_neq_o
            by (intros contra; apply M.E.eq_sym in contra; contradiction).
          reflexivity.
    Qed.

    (****************** associativity lemma with extended equality ************)
    Lemma fold_left_assoc_map : forall l (f : M.t A -> B -> M.t A) x h,
      (forall (y z : B) s, EqualMap (f (f s y) z) (f (f s z) y)) ->
      (forall e1 e2 a, EqualMap e1 e2 -> EqualMap (f e1 a) (f e2 a)) ->
      EqualMap (fold_left f (h :: l) x) (f (fold_left f l x) h).
    Proof.
      intros. apply fold_left_assoc; try assumption.
      exact EqualMap_refl.
      exact EqualMap_trans.
    Qed.

    (****************** property preservation on fold lemma *****************)

    Lemma fold_invariant : forall (f : A -> B -> A) l a (P:A->Prop),
      P a ->
      (forall x t, In x l -> P t -> P (f t x)) ->
      P (fold_left f l a).
    Proof.
      intros f l.
      induction l; intros.
      - (* l = nil *)
        simpl. assumption.
      - (* l = a :: l *)
        simpl. apply IHl.
        + apply H0. 
          left. reflexivity.
          assumption.
        + intros. apply H0.
          right. assumption.
          assumption.
    Qed.

    Lemma fold_invariant2 : forall (f : A -> B -> A) l a (P P':A->Prop),
      (P' a -> P a) ->
      (forall x t, In x l -> (P' t -> P t) -> P (f t x)) ->
      P' (fold_left f l a) -> P (fold_left f l a).
    Proof.
      intros. revert H1.
      apply fold_invariant with (P:= fun a => P' a -> P a).
      assumption. intros. apply H0; assumption.
    Qed.

    Lemma fold_invariant_cons : forall (f : A -> B -> A) x l a (P:A->Prop),
      P (f a x) ->
      (forall x t, In x l -> P t -> P (f t x)) ->
      P (fold_left f (x :: l) a).
    Proof.
      intros. simpl. apply fold_invariant; assumption.
    Qed.

  End associative_fold_maps.

End MyMapFacts.
