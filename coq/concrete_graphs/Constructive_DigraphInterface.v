Require Import FMaps.
Require Import Directed_edges.
Require Import FMapAVL.
Require Import MapFacts.
Require Import DigraphInterface.
Require Import Labels.

Module Type Constructive_Directed_Graph (Vertex : OrderedType) (Lab : Labels)
  <: Directed_Graph Vertex Lab.
(********************* Abstract modules definitions ********************************)

  Include Directed_Graph Vertex Lab.

  (* Types of labels *)
  Import Lab.

  (************************** graph construction functions ***********************)
  Parameter empty_graph : t.
  Parameter empty_graph_empty : forall v, ~ In_graph_vertex v empty_graph.
  (* Function for adding a vertex *)
  Parameter add_vertex : Vertex.t -> t -> option VertexLabel -> t.
  (* Function for adding an edge *)
  Parameter add_edge : Edge.t -> t -> option EdgeLabel -> t.
  Parameter add_edge_w : Edge.t -> t -> option EdgeLabel -> t.
  (* Function for removing a vertex *)
  Parameter remove_vertex : Vertex.t -> t -> t.
  (* Function for removing an edge *)
  Parameter remove_edge : Edge.t -> t -> t.

  (****************************** remove_vertex *********************************)

  (* A vertex x is in (remove_vertex r g) iff it is in g
     and it is different from r *)
  Parameter In_remove_vertex : forall x r g w,
    In_graph_labeled_vertex x (remove_vertex r g) w <-> 
    (In_graph_labeled_vertex x g w /\ ~Vertex.eq x r).

  Import Edge.

  (* An edge e is in (remove_vertex r g) iff it is in g
     and is not incident to r *)
  Parameter In_remove_edge : forall e r g w,
    In_graph_labeled_edge e (remove_vertex r g) w <-> 
    (In_graph_labeled_edge e g w /\ ~ incident e r).

  (***************************** add_vertex ***********************************)

  Parameter In_add_vertex : forall v g x w w',
  In_graph_labeled_vertex x (add_vertex v g w) w' <-> 
  (~In_graph_vertex v g /\ Vertex.eq x v /\ w = w') \/ In_graph_labeled_vertex x g w'.

  Parameter In_add_vertex_edge : forall v g e w w',
  In_graph_labeled_edge e (add_vertex v g w) w' <-> In_graph_labeled_edge e g w'.

  Definition In_ext e g := In_graph_vertex (fst_end e) g /\ In_graph_vertex (snd_end e) g.

  (****************************** add_edge ************************************)

  (* vertices *)
  Parameter In_graph_add_edge_vertex : forall v g e w w',
  (In_graph_labeled_vertex v (add_edge e g w) w' <-> In_graph_labeled_vertex v g w').

  (* edges *)
  Parameter In_graph_edge_add_edge : forall e e' g w w',
  In_graph_labeled_edge e' g w->
  ~Edge.eq e e' ->
  In_graph_labeled_edge e' (add_edge e g w') w.

  Parameter In_graph_edge_add_edge_2 : forall e g w,
  In_ext e g ->
  In_graph_labeled_edge e (add_edge e g w) w.

  Parameter In_graph_add_edge_edge : forall g e e' w w',
  In_ext e g ->
  In_graph_labeled_edge e' (add_edge e g w) w' ->
  ~Edge.eq e e' ->
  In_graph_labeled_edge e' g w'.

  (* fail *)
  Parameter Add_edge_fail_vertex : forall e g w v w',
  ~In_ext e g -> 
  (In_graph_labeled_vertex v g w' <-> 
  In_graph_labeled_vertex v (add_edge e g w) w').

  Parameter Add_edge_fail_edge : forall e g w e' w',
  ~In_ext e g -> 
  (In_graph_labeled_edge e' g w' <-> 
  In_graph_labeled_edge e' (add_edge e g w) w').

  (****************************** add_edge_weak *******************************)

  Parameter In_graph_add_edge_weak_vertex : forall v g e w w',
  In_graph_labeled_vertex v (add_edge_w e g w) w' <-> 
  In_graph_labeled_vertex v g w' \/ (incident e v /\ w' = None /\ ~In_graph_vertex v g).

  Parameter In_graph_add_edge_weak_edge : forall g e e' w w',
  In_graph_labeled_edge e' (add_edge_w e g w) w' <-> 
  (eq e e' /\ w = w') \/ (~eq e e' /\ In_graph_labeled_edge e' g w').

  (****************************** remove_edge *********************************)

  Parameter In_graph_remove_edge_vertex : forall v e g w,
  (In_graph_labeled_vertex v (remove_edge e g) w <-> 
  In_graph_labeled_vertex v g w).

  Parameter In_graph_remove_edge_edge : forall g e e' l',
    ~ Edge.eq e e' ->
    In_graph_labeled_edge e' (remove_edge e g) l' <->
    In_graph_labeled_edge e' g l'.

  Parameter In_graph_remove_edge_edge_2 : forall g e,
    ~ In_graph_edge e (remove_edge e g).

End Constructive_Directed_Graph.
