Require Import OrderedTypeEx.
Require Import OrderedType.

(* Module of edges in a simple graph : there is never more
   than one edge between two vertices *)

Module Directed_Edge (Vertex : OrderedType) <: OrderedType.

  (* Definition of pair of vertices, to represent edges *)
  Module VertexPair := PairOrderedType Vertex Vertex.
  Include VertexPair.

  (* Useful modules imports *)
  Module Import OTFacts := OrderedTypeFacts Vertex.
  Module OTPairFacts := OrderedTypeFacts VertexPair.

  (* Accessors to the edges, their endpoints and their label *)
  Definition fst_end : t -> Vertex.t := fun x => fst x.
  Definition snd_end : t -> Vertex.t := fun x => snd x.

  Add Parametric Morphism : fst_end
    with signature (eq ==> Vertex.eq)
    as fst_end_eq.
  Proof.
    intros e1 e2 (e_eq & _).
    assumption.
  Qed.

  Add Parametric Morphism : snd_end
    with signature (eq ==> Vertex.eq)
    as snd_end_eq.
  Proof.
    intros e1 e2 (_ & e_eq).
    assumption.
  Qed.

  (* Expansion of an edge *)
  Lemma edge_eq : forall e, e = (fst_end e, snd_end e).
  Proof.
    intros e; destruct e; reflexivity.
  Qed.

  (* rewriting rules *)
  Lemma change_fst : forall x y, fst_end (x,y) = x.
  Proof.
    intros; reflexivity.
  Qed.

  Lemma change_snd : forall x y, snd_end (x,y) = y.
  Proof.
    intros; reflexivity.
  Qed.

  (* rewriting tactic *)
  Ltac reduce_edge :=
    repeat (try rewrite change_fst in *;try rewrite change_snd in *).

  (* An edge is incident to a vertex iff this vertex is an endpoint of the edge *)
  Definition incident e x := Vertex.eq x (fst_end e) \/ Vertex.eq x (snd_end e).

  (* An edge is incident to a vertex, or is not *)
  Lemma incident_dec : forall e x, incident e x \/ ~ incident e x.
  Proof.
    intros e x.
    destruct (eq_dec x (fst_end e)).
    - (* x = fst e *)
      left; left; assumption.
    - destruct (eq_dec x (snd_end e)).
      + (* x = fst e *)
        left; right; assumption.
      + (* e is not incident to x *)
        right. intros contra. destruct contra as [contra|contra]; contradiction.
  Qed.

  Definition permute (e : t) :=
    match e with
    | (x,y) => (y,x)
    end.

End Directed_Edge.