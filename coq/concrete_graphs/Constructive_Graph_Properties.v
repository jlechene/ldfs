Require Import Constructive_DigraphInterface.
Require Import OrderedType.
Require Import Labels.
Require Import Graph_Properties2.

Module ConstructiveGraphProps (Vertex : OrderedType) (Lab : Labels)
                                (G : Constructive_Directed_Graph Vertex Lab).

  Module Import GraphProps := GraphProps Vertex Lab G.

  Import G.

  Lemma In_ext_dec : forall g e,
    {In_ext e g}+{~In_ext e g}.
  Proof.
    intros. unfold In_ext.
    destruct (In_graph_vertex_dec (Edge.fst_end e) g).
    - destruct (In_graph_vertex_dec (Edge.snd_end e) g).
      + left; split; assumption.
      + right; intros contra; destruct contra; contradiction.
    - right; intros contra; destruct contra; contradiction.
  Qed.

  Add Parametric Morphism : In_ext
    with signature Edge.eq ==> eq ==> iff
    as In_ext_eqG.
  Proof.
    intros. unfold In_ext. rewrite H0. destruct H.
    unfold Edge.fst_end, Edge.snd_end. rewrite H, H1. reflexivity.
  Qed.

  Lemma In_graph_edge_In_ext : forall g e, In_graph_edge e g -> In_ext e g.
  Proof.
    intros; apply In_graph_edge_in_ext; assumption.
  Qed.

  Lemma In_ext_edge_add_edge : forall g e e' l,
    In_ext e g -> In_ext e (add_edge e' g l).
  Proof.
    intros.
    destruct H as (H1 & H2).
    apply In_graph_vertex_strong in H1.
    apply In_graph_vertex_strong in H2.
    destruct H1 as (l1 & H1).
    destruct H2 as (l2 & H2).
    rewrite <- In_graph_add_edge_vertex in H1, H2.
    apply In_graph_labeled_vertex_weak in H1.
    apply In_graph_labeled_vertex_weak in H2.
    split; eassumption.
  Qed.

  Lemma In_ext_add_edge_edge : forall g e e' l,
    In_ext e (add_edge e' g l) -> In_ext e g.
  Proof.
    intros. destruct H as (H1 & H2).
    apply In_graph_vertex_strong in H1.
    apply In_graph_vertex_strong in H2.
    destruct H1 as (l1 & H1).
    destruct H2 as (l2 & H2).
    rewrite In_graph_add_edge_vertex in H1, H2.
    apply In_graph_labeled_vertex_weak in H1.
    apply In_graph_labeled_vertex_weak in H2.
    split; assumption.
  Qed.

(* 
Lemma test : forall g e e' w w',
  In_ext e g ->
  In_graph_labeled_edge e' (add_edge e g w) w' ->
  Edge.eq e e' /\ w = w' \/ ~Edge.eq e e' /\
  In_graph_labeled_edge e' g w'.
  Proof.
    intros. destruct (Edge.eq_dec e e').
    - left. split; [assumption|].
      pose proof In_graph_edge_add_edge_2 e g w H.
      rewrite <- e0 in H0.
      eapply In_graph_labeled_edge_eq; eassumption.
    - right. split; [assumption|]. eapply In_graph_add_edge_edge; eassumption. assumption. *)

  (* Lemma In_graph_add_edge_edge_2 : forall g e e' w w',
    In_ext e g ->
    In_graph_labeled_edge e' (add_edge e g w) w' ->
    ~Edge.eq e e' ->
    In_graph_labeled_edge e' g w'.
  Proof.
    intros. eapply In_graph_add_edge_edge; try eassumption.
  Qed. *)

  Lemma In_graph_add_edge_edge_simpl : forall g e e' w w',
    In_graph_labeled_edge e' (add_edge e g w) w' ->
    ~ Edge.eq e e' -> In_graph_labeled_edge e' g w'.
  Proof.
    intros. destruct (In_ext_dec g e).
    - apply In_graph_add_edge_edge in H; assumption.
    - (* apply Add_edge_fail_edge in H adds an hypothsesis to the 2nd goal,
         really strange.
      *)
      apply Add_edge_fail_edge in H; assumption.
  Qed.

  Add Parametric Morphism : add_edge
    with signature Edge.eq ==> eq ==> Logic.eq ==> eq
    as add_edge_eqG.
  Proof.
    intros e1 e2 Heq g h HeqG l. apply eq_In_graph. split; intros.
    - rewrite 2!In_graph_add_edge_vertex. rewrite HeqG. reflexivity.
    - split; intros.
      + destruct (In_ext_dec g e1).
        * { destruct (Edge.eq_dec e1 e).
          - rewrite <- e0 in H. pose proof (In_graph_edge_add_edge_2 e1 g l i).
            pose proof (In_graph_labeled_edge_eq _ _ _ _ H H0). subst l0.
            rewrite <- e0, Heq. apply In_graph_edge_add_edge_2.
            rewrite <- Heq, <- HeqG. assumption.
          - apply In_graph_add_edge_edge_simpl in H; try assumption.
            apply In_graph_edge_add_edge.
            + rewrite <- HeqG. assumption.
            + rewrite <- Heq. assumption. }
        * rewrite <- Add_edge_fail_edge in H by assumption.
          { apply Add_edge_fail_edge.
          - rewrite <- Heq, <- HeqG. assumption.
          - rewrite <- HeqG. assumption. }
      + destruct (In_ext_dec h e2).
        * { destruct (Edge.eq_dec e2 e).
          - rewrite <- e0 in H. pose proof (In_graph_edge_add_edge_2 e2 h l i).
            pose proof (In_graph_labeled_edge_eq _ _ _ _ H H0). subst l0.
            rewrite <- e0, <- Heq. apply In_graph_edge_add_edge_2.
            rewrite Heq, HeqG. assumption.
          - apply In_graph_add_edge_edge in H; try assumption.
            apply In_graph_edge_add_edge.
            + rewrite HeqG. assumption.
            + rewrite Heq. assumption. }
        * rewrite <- Add_edge_fail_edge in H by assumption.
          { apply Add_edge_fail_edge.
          - rewrite Heq, HeqG. assumption.
          - rewrite HeqG. assumption. }
  Qed.

  Lemma add_edge_comm : forall g e1 l1 e2 l2
    (e_neq : ~ Edge.eq e1 e2),
    add_edge e1 (add_edge e2 g l2) l1 =G
    add_edge e2 (add_edge e1 g l1) l2.
  Proof.
    intros. apply eq_In_graph. split; intros.
    - rewrite 4!In_graph_add_edge_vertex. reflexivity.
    - split; intros.
      + destruct (In_ext_dec g e).
        * { destruct (Edge.eq_dec e1 e).
          - apply In_graph_edge_add_edge.
            + assert (l = l1) as Ha.
              { eapply In_graph_labeled_edge_eq.
                apply H.
                rewrite e0. apply In_graph_edge_add_edge_2.
                apply In_ext_edge_add_edge. assumption.
              }
              subst l1. rewrite e0. apply In_graph_edge_add_edge_2.
              assumption.
            + intros contra. rewrite <- e0 in contra. symmetry in contra.
              contradiction.
          - destruct (Edge.eq_dec e2 e).
            + apply In_graph_add_edge_edge_simpl in H; [|assumption].
              assert (l=l2) as Ha.
              { eapply In_graph_labeled_edge_eq.
                + apply H.
                + rewrite e0. apply In_graph_edge_add_edge_2. assumption.
              }
              subst l2. rewrite e0. apply In_graph_edge_add_edge_2.
              apply In_ext_edge_add_edge. assumption.
            + apply In_graph_add_edge_edge_simpl in H.
              * { apply In_graph_add_edge_edge_simpl in H.
                - apply In_graph_edge_add_edge; [|assumption].
                  apply In_graph_edge_add_edge; assumption.
                - assumption. }
              * assumption. }
        * apply In_graph_labeled_edge_weak in H.
          apply In_graph_edge_In_ext in H.
          do 2 apply In_ext_add_edge_edge in H. contradiction.
      + destruct (In_ext_dec g e).
        * { destruct (Edge.eq_dec e2 e).
          - apply In_graph_edge_add_edge.
            + assert (l = l2) as Ha.
              { eapply In_graph_labeled_edge_eq.
                apply H.
                rewrite e0. apply In_graph_edge_add_edge_2.
                apply In_ext_edge_add_edge. assumption.
              }
              subst l2. rewrite e0. apply In_graph_edge_add_edge_2.
              assumption.
            + intros contra. rewrite <- e0 in contra.
              contradiction.
          - destruct (Edge.eq_dec e1 e).
            + apply In_graph_add_edge_edge_simpl in H; [|assumption].
              assert (l=l1) as Ha.
              { eapply In_graph_labeled_edge_eq.
                + apply H.
                + rewrite e0. apply In_graph_edge_add_edge_2. assumption.
              }
              subst l1. rewrite e0. apply In_graph_edge_add_edge_2.
              apply In_ext_edge_add_edge. assumption.
            + apply In_graph_add_edge_edge_simpl in H.
              * { apply In_graph_add_edge_edge_simpl in H.
                - apply In_graph_edge_add_edge; [|assumption].
                  apply In_graph_edge_add_edge; assumption.
                - assumption. }
              * assumption. }
        * apply In_graph_labeled_edge_weak in H.
          apply In_graph_edge_In_ext in H.
          do 2 apply In_ext_add_edge_edge in H. contradiction.
  Qed.

  Lemma In_graph_remove_edge_edge_succ : forall e g,
  ~VertexMap.In (Edge.snd_end e)
                (successors (Edge.fst_end e) (remove_edge e g)).
  Proof.
    intros. intros contra. apply VMapFacts.In_MapsTo in contra.
    destruct contra as (l & contra).
    apply mapsto_edge_equiv_succ in contra.
    apply In_graph_labeled_edge_weak in contra.
    rewrite <- Edge.edge_eq in contra.
    contradiction (In_graph_remove_edge_edge_2 g e).
  Qed.

  Lemma In_graph_remove_edge_edge_pred : forall e g,
  ~VertexMap.In (Edge.fst_end e)
                (predecessors (Edge.snd_end e) (remove_edge e g)).
  Proof.
    intros. intros contra. apply VMapFacts.In_MapsTo in contra.
    destruct contra as (l & contra).
    apply mapsto_edge_equiv_pred in contra.
    apply In_graph_labeled_edge_weak in contra.
    rewrite <- Edge.edge_eq in contra.
    contradiction (In_graph_remove_edge_edge_2 g e).
  Qed.

  Add Parametric Morphism : remove_edge
    with signature Edge.eq ==> eq ==> eq
    as remove_edge_eqG.
  Proof.
    intros e1 e2 Heq g h HeqG.
    rewrite eq_In_graph. split; intros.
    - rewrite 2!In_graph_remove_edge_vertex. rewrite HeqG. reflexivity.
    - split; intros.
      + destruct (Edge.eq_dec e e1).
        * rewrite e0 in H. apply In_graph_labeled_edge_weak in H.
          contradiction (In_graph_remove_edge_edge_2 g e1).
        * { rewrite In_graph_remove_edge_edge in H.
          - apply In_graph_remove_edge_edge.
            + rewrite <- Heq. intros contra. symmetry in contra. contradiction.
            + rewrite <- HeqG. assumption.
          - intros contra. symmetry in contra. contradiction. }
      + destruct (Edge.eq_dec e e2).
        * rewrite e0 in H. apply In_graph_labeled_edge_weak in H.
          contradiction (In_graph_remove_edge_edge_2 h e2).
        * { rewrite In_graph_remove_edge_edge in H.
          - apply In_graph_remove_edge_edge.
            + rewrite Heq. intros contra. symmetry in contra. contradiction.
            + rewrite HeqG. assumption.
          - intros contra. symmetry in contra. contradiction. }
  Qed.

  Lemma remove_edge_comm : forall e1 e2 g,
    remove_edge e1 (remove_edge e2 g) =G
    remove_edge e2 (remove_edge e1 g).
  Proof.
    intros. rewrite eq_In_graph. split; intros.
    - rewrite 4!In_graph_remove_edge_vertex. reflexivity.
    - destruct (Edge.eq_dec e1 e).
      + rewrite <- e0. split; intros.
        * apply In_graph_labeled_edge_weak in H.
          apply In_graph_remove_edge_edge_2 in H. contradiction.
        * { destruct (Edge.eq_dec e2 e1).
          - rewrite e3 in H. apply In_graph_labeled_edge_weak in H.
            apply In_graph_remove_edge_edge_2 in H. contradiction.
          - rewrite In_graph_remove_edge_edge in H by assumption.
            apply In_graph_labeled_edge_weak in H.
            apply In_graph_remove_edge_edge_2 in H. contradiction. }
      + destruct (Edge.eq_dec e2 e).
        * rewrite <- e0. { split; intros.
          - rewrite In_graph_remove_edge_edge in H.
            + apply In_graph_labeled_edge_weak in H.
              apply In_graph_remove_edge_edge_2 in H. contradiction.
            + rewrite e0. assumption.
          - apply In_graph_labeled_edge_weak in H.
            apply In_graph_remove_edge_edge_2 in H. contradiction. }
        * rewrite 4!In_graph_remove_edge_edge by assumption. reflexivity.
  Qed.

  Add Parametric Morphism : add_vertex
    with signature (Vertex.eq ==> eq ==> Logic.eq ==> eq) as add_vertex_eqG.
  Proof.
    intros. apply eq_In_graph. split; intros.
    - rewrite 2!In_add_vertex. rewrite <- H0, <- H. reflexivity.
    - rewrite 2!In_add_vertex_edge. rewrite <- H0. reflexivity.
  Qed.

  Lemma add_vertex_comm : forall g u1 u2 l1 l2
    (u_neq : ~ Vertex.eq u1 u2),
    add_vertex u1 (add_vertex u2 g l2) l1 =G
    add_vertex u2 (add_vertex u1 g l1) l2.
  Proof.
    intros. apply eq_In_graph. split; intros.
    - rewrite 4!In_add_vertex. split; intros.
      + destruct H as [H|[H|H]].
        * destruct H as (H1 & H2 & H3). right; left.
          { split.
          - intros contra. apply H1. apply In_graph_vertex_strong in contra.
            destruct contra as (l' & contra).
            apply In_graph_labeled_vertex_weak with (val:=l').
            apply In_add_vertex. right; assumption.
          - split; assumption. }
        * destruct H as (H1 & H2 & H3).
          left. { split.
          - intros contra. apply H1. apply In_graph_vertex_strong in contra.
            destruct contra as (l' & contra).
            apply In_graph_labeled_vertex_weak with (val:=l').
            apply In_add_vertex in contra. destruct contra; [|assumption].
            destruct H as (_ & H & _). symmetry in H; contradiction.
          - split; assumption. }
        * right; right; assumption.
      + destruct H as [H|[H|H]].
        * destruct H as (H1 & H2 & H3). right; left.
          { split.
          - intros contra. apply H1. apply In_graph_vertex_strong in contra.
            destruct contra as (l' & contra).
            apply In_graph_labeled_vertex_weak with (val:=l').
            apply In_add_vertex. right; assumption.
          - split; assumption. }
        * destruct H as (H1 & H2 & H3).
          left. { split.
          - intros contra. apply H1. apply In_graph_vertex_strong in contra.
            destruct contra as (l' & contra).
            apply In_graph_labeled_vertex_weak with (val:=l').
            apply In_add_vertex in contra. destruct contra; [|assumption].
            destruct H as (_ & H & _). contradiction.
          - split; assumption. }
        * right; right; assumption.
    - rewrite 4!In_add_vertex_edge. reflexivity.
  Qed.

  Lemma add_vertex_same : forall g u l1 l2,
    add_vertex u (add_vertex u g l2) l1 =G
    add_vertex u g l2.
  Proof.
    intros. apply eq_In_graph. split; intros.
    - rewrite 2!In_add_vertex. split; intros.
      + destruct H as [H|[H|H]].
        * destruct H as (H1 & H2 & H3). contradiction H1.
          { destruct (In_graph_vertex_dec u g).
          - apply In_graph_vertex_strong in i. destruct i as (l' & i).
            apply In_graph_labeled_vertex_weak with (val:=l').
            rewrite In_add_vertex. right; assumption.
          - apply In_graph_labeled_vertex_weak with (val:=l2).
            rewrite In_add_vertex. left. split; [assumption|].
            split; reflexivity. }
        * left; assumption.
        * right; assumption.
      + destruct H as [H|H].
        * right; left; assumption.
        * right; right; assumption.
    - rewrite 2!In_add_vertex_edge. reflexivity.
  Qed.

End ConstructiveGraphProps.