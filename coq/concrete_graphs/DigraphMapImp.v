Require Import FMaps.
Require Import DigraphInterface.
Require Import Constructive_DigraphInterface.
Require Import FMapAVL.
Require Import Directed_edges.
Require Import MapFacts.
Require Import FSets.
Require Import Labels.

(* Module of interference graphs *)
Module Directed_GraphMap (Vertex : OrderedType) (Lab : Labels)
       <: Directed_Graph Vertex Lab
       <: Constructive_Directed_Graph Vertex Lab.

  (* Types of labels *)
  Import Lab.

  (******************************* Modules ***********************************)
  Module VertexMap := FMapAVL.Make Vertex.
  (* Modules of edges *)
  Module Import Edge := Directed_Edge Vertex.
  (* Module of map of edges *)
  Module EdgeMap := FMapAVL.Make Edge.
  Module Import EMapFacts := MyMapFacts EdgeMap.
  Module Export VMapFacts := MyMapFacts VertexMap.

  (****************************** Defs ***************************************)

  (* a vertex maps to its label, its successors and its predecessors *)
  Definition elttype : Type :=
    option VertexLabel *
      (VertexMap.t (option EdgeLabel) *
        (VertexMap.t (option EdgeLabel))).
  Definition maptype :=
    VertexMap.t elttype.

  (* Definition of adjacency sets *)
  Definition adj_map (x : Vertex.t) (m : maptype) :=
    match (VertexMap.find x m) with
    | None => let empty_m := VertexMap.empty (option EdgeLabel) in 
              (None, (empty_m, empty_m))
    | Some wmaps => wmaps
    end.

  Definition succ x m := fst (snd (adj_map x m)).
  Definition pred x m := snd (snd (adj_map x m)).

  Definition is_succ y x m := VertexMap.In y (succ x m).
  Definition is_pred y x m := VertexMap.In y (pred x m).

  Definition is_labeled_succ y x m w := VertexMap.MapsTo y w (succ x m).
  Definition is_labeled_pred y x m w := VertexMap.MapsTo y w (pred x m).

  (* The graph data structure *)
  Record tt : Type := Make_Graph {
    map : maptype;
    (* successors and predecessors are consistent *)
    pred_succ : forall x y w,
      is_labeled_succ y x map w <-> is_labeled_pred x y map w
  }.

  Definition successors v g := succ v (map g).
  Definition predecessors v g := pred v (map g).

  Lemma unw_pred_succ :
    forall g x y, is_succ y x (map g) <-> is_pred x y (map g).
  Proof.
    intros.
    unfold is_succ, is_pred.
    rewrite 2!Mapsto_In.
    split; intros;
      destruct H as [x0 H]; exists x0; apply pred_succ; assumption.
  Qed.

  (* Definition of the type, as required by the module *)
  Definition t := tt.

  (******************************** properties **********************************)

  Definition V g := VertexMap.map (fun wsp => fst wsp) (map g).

  Definition vertex_label v g :=
    match VertexMap.find v (map g) with
    | None => None
    | Some wsp => Some (fst wsp)
    end.

  (* add the outgoing edges of x to the map s *)
  Definition add_neighborhood x
    (wsp : elttype)
    (s : EdgeMap.t (option EdgeLabel)) :=
    let '(_,(su, _)) := wsp in
    (VertexMap.fold (fun y v s' => EdgeMap.add (x,y) v s') su s).

  (* fold on all the map to list all the edges *)
  Definition E (g : t) :=
    VertexMap.fold (fun x wsp s => add_neighborhood x wsp s)
                   (map g)
                   (EdgeMap.empty (option EdgeLabel)).

  Definition edge_label e g :=
    VertexMap.find (snd_end e) (successors (fst_end e) g).

  Definition link g v1 v2 := VertexMap.find v2 (successors v1 g).

  Lemma vertex_label_spec : forall v g,
    vertex_label v g = VertexMap.find v (V g).
  Proof.
    intros. unfold vertex_label, V.
    rewrite map_o; fold elttype.
    destruct (VertexMap.find v (map g)); reflexivity.
  Qed.

  Definition In_graph_vertex v g := VertexMap.In v (map g).

  Definition In_graph_labeled_vertex v g w := 
    exists m, VertexMap.find v (map g) = Some (w,m).

  Definition In_graph_edge (e : Edge.t) g :=
    is_succ (snd_end e) (fst_end e) (map g).

  Definition In_graph_labeled_edge e g w :=
    is_labeled_succ (snd_end e) (fst_end e) (map g) w.

  Definition In_ext e g :=
    In_graph_vertex (fst_end e) g /\ In_graph_vertex (snd_end e) g.

  (***************************** Specifications **************************************)

  (* Belonging specification *)
  Lemma In_graph_vertex_spec : forall v g,
    In_graph_vertex v g <-> VertexMap.In v (V g).
  Proof.
    intros.
    unfold In_graph_vertex, V; fold elttype.
    rewrite map_in_iff. reflexivity.
  Qed.

  Lemma In_graph_labeled_vertex_spec : forall v g l,
    In_graph_labeled_vertex v g l <-> VertexMap.MapsTo v l (V g).
  Proof.
    intros.
    unfold In_graph_labeled_vertex, V; fold elttype.
    rewrite map_mapsto_iff.
    split; intros.
    - destruct H as [m H]. exists (l, m). split; [reflexivity |].
      apply VertexMap.find_2. assumption.
    - destruct H as [a [H H0]]. exists (snd a). rewrite H.
      rewrite <- surjective_pairing. apply VertexMap.find_1. assumption.
  Qed.

  (* Graph belonging is a morphism *)
  Add Parametric Morphism : In_graph_vertex
    with signature (Vertex.eq ==> Logic.eq ==> iff) as In_graph_vertex_m.
  Proof.
    intros; unfold In_graph_vertex.
    rewrite H. reflexivity.
  Qed.

  Add Parametric Morphism : In_graph_labeled_vertex
    with signature (Vertex.eq ==> Logic.eq ==> Logic.eq ==> iff)
    as In_graph_labeled_vertex_m.
  Proof.
    intros; unfold In_graph_labeled_vertex.
    setoid_rewrite H. reflexivity.
  Qed.

  (* ces tactiques doivent être dans le module type, peut-être à enlever *)
  Ltac eq_key_simpl :=
    unfold VertexMap.eq_key, VertexMap.Raw.Proofs.PX.eqk in *; simpl in *; auto.

  Ltac eq_key_elt_simpl :=
    unfold VertexMap.eq_key_elt, VertexMap.Raw.Proofs.PX.eqke in *; simpl in *; auto.

  Ltac eq_key_solve := eq_key_simpl; eq_key_elt_simpl.

  Lemma add_neighborhood_diff : forall v1 v2 v3 wsp v m,
    ~Vertex.eq v1 v3 ->
    (EdgeMap.MapsTo (v1,v2) v (add_neighborhood v3 wsp m) <->
     EdgeMap.MapsTo (v1,v2) v m).
  Proof.
    intros. unfold add_neighborhood.
    destruct wsp as [w [su pu]]. simpl.
    rewrite VertexMap.fold_1.
    apply fold_invariant.
    - reflexivity.
    - intros. rewrite EMapFacts.add_mapsto_iff; simpl.
      split; intros.
      + destruct H2 as [H2|H2].
        * contradiction H. symmetry. apply H2.
        * apply H1. apply H2.
      + right. split.
        * intros contra. destruct contra as [contra _].
          symmetry in contra. contradiction.
        * apply H1; assumption.
  Qed.

  Lemma add_neighborhood_eq : forall x wsp s a b v,
    Vertex.eq x a ->
    (EdgeMap.MapsTo (a,b) v (add_neighborhood x wsp s) <->
    ((forall val, ~VertexMap.MapsTo b val (fst (snd wsp))) /\ EdgeMap.MapsTo (a,b) v s) 
     \/ VertexMap.MapsTo b v (fst (snd wsp))).
  Proof.
    intros. unfold add_neighborhood.
    destruct wsp as [w [su pu]]. simpl.
    rewrite VertexMap.fold_1.
    apply fold_invariant.
  Admitted.

  Lemma add_neighborhood_comm : forall y z s,
    ~ VertexMap.eq_key y z ->
    EdgeMap.Equal
      (add_neighborhood (fst z) (snd z) (add_neighborhood (fst y) (snd y) s))
      (add_neighborhood (fst y) (snd y) (add_neighborhood (fst z) (snd z) s)).
  Proof.
  Admitted.

  Lemma add_neighborhood_compat : forall a e1 e2,
    EdgeMap.Equal e1 e2 ->
    EdgeMap.Equal (add_neighborhood (fst a) (snd a) e1)
      (add_neighborhood (fst a) (snd a) e2).
  Proof.
  Admitted.

  Lemma In_graph_labeled_edge_spec : forall e g l,
    In_graph_labeled_edge e g l <-> EdgeMap.MapsTo e l (E g).
  Proof.
  Admitted.

  Lemma edge_label_spec : forall e g,
    edge_label e g = EdgeMap.find e (E g).
  Proof.
  Admitted.

  (* Belonging specification *)
  Lemma In_graph_edge_spec : forall e g,
    In_graph_edge e g <-> EdgeMap.In e (E g).
  Proof.
    intros. rewrite EMapFacts.Mapsto_In. unfold In_graph_edge, is_succ.
    split; intros.
    generalize (In_MapsTo _ _ H). intro. destruct H0.
    exists x. rewrite <-In_graph_labeled_edge_spec. 
    unfold In_graph_labeled_edge, is_labeled_succ. auto.
    destruct H. rewrite Mapsto_In. exists x.
    rewrite <-In_graph_labeled_edge_spec in H.
    unfold In_graph_labeled_edge, is_labeled_succ in H. auto.
  Qed.

  Opaque Edge.t.

  (* Graph belonging is a morphism *)
  Add Morphism In_graph_edge : In_graph_edge_m.
  Proof.
    intros. do 2 rewrite In_graph_edge_spec.
    do 2 rewrite EMapFacts.in_find_iff.
    rewrite (EMapFacts.find_o _ H). reflexivity.
  Qed.

  Add Morphism In_graph_labeled_edge : In_graph_labeled_edge_m.
  Proof.
    intros. do 2 rewrite In_graph_labeled_edge_spec.
    rewrite EMapFacts.MapsTo_iff. reflexivity. auto.
  Qed.

  (* Equivalence of edges and relationship *)
  Lemma link_edge_equiv : forall v1 v2 g l,
    link g v1 v2 = Some l <-> In_graph_labeled_edge (v1,v2) g l.
  Proof.
    unfold link, successors, In_graph_labeled_edge, is_labeled_succ, 
               succ, adj_map, In_graph_labeled_edge. intros. simpl.
    split; intros.
    apply VertexMap.find_2. auto.
    apply VertexMap.find_1. auto.
  Qed.

  Lemma mapsto_edge_equiv_succ : forall v1 v2 g l,
    VertexMap.MapsTo v2 l (successors v1 g) <-> In_graph_labeled_edge (v1,v2) g l.
  Proof.
    intros. rewrite <-link_edge_equiv. unfold link.
    split; intros.
    apply VertexMap.find_1. auto.
    apply VertexMap.find_2. auto.
  Qed.

  Lemma mapsto_edge_equiv_pred : forall v1 v2 g l,
    VertexMap.MapsTo v1 l (predecessors v2 g) <-> In_graph_labeled_edge (v1,v2) g l.
  Proof.
    intros. rewrite <-link_edge_equiv. unfold link.
    unfold predecessors. fold (is_labeled_pred v1 v2 (map g) l).
    rewrite <-pred_succ. fold (In_graph_labeled_edge (v1,v2) g l).
    assert (is_labeled_succ v2 v1 (map g) l <-> In_graph_labeled_edge (v1,v2) g l).
    unfold In_graph_labeled_edge. simpl. reflexivity.
    rewrite H. clear H.
    split; intros.
    apply VertexMap.find_1. auto.
    apply VertexMap.find_2. auto.
  Qed.

  (******************************** iterators on graphs ********************************)

  Definition fold_vertices (A : Type) f g (a : A) := VertexMap.fold (f g) (V g) a.

  Definition fold_edges (A : Type) f g (a : A) := EdgeMap.fold (f g) (E g) a.

  Definition fold_succ (A : Type) f v g (a : A) := 
  VertexMap.fold (f g v) (successors v g) a.

  Definition fold_pred (A : Type) f v g (a : A) := 
  VertexMap.fold (f g v) (predecessors v g) a.

  Definition fold_succ_ne (A : Type) f v g (a : A) := 
  VertexMap.fold (f g v) (VertexMap.remove v (successors v g)) a.

  Definition fold_pred_ne (A : Type) f v g (a : A) := 
  VertexMap.fold (f g v) (VertexMap.remove v (predecessors v g)) a.

  Implicit Arguments fold_edges [A].
  Implicit Arguments fold_vertices [A].
  Implicit Arguments fold_succ [A].
  Implicit Arguments fold_pred [A].
  Implicit Arguments fold_succ_ne [A].
  Implicit Arguments fold_pred_ne [A].

  Lemma fold_vertices_spec : forall (A : Type) f g (a : A),
  fold_vertices f g a = VertexMap.fold (f g) (V g) a.

  Proof.
  auto.
  Qed.

  Lemma fold_edges_spec : forall (A : Type) f g (a : A),
  fold_edges f g a = EdgeMap.fold (f g) (E g) a.

  Proof.
  auto.
  Qed.

  (* folding on succ of a given vertex *)
  Lemma fold_succ_spec : forall (A : Type) f v g (a : A),
  fold_succ f v g a = VertexMap.fold (f g v) (successors v g) a.

  Proof.
  auto.
  Qed.

  Lemma fold_pred_spec : forall (A : Type) f v g (a : A),
  fold_pred f v g a = VertexMap.fold (f g v) (predecessors v g) a.

  Proof.
  auto.
  Qed.

  (* folding on succ of a given vertex *)
  Lemma fold_succ_ne_spec : forall (A : Type) f v g (a : A),
  fold_succ_ne f v g a = VertexMap.fold (f g v) (VertexMap.remove v (successors v g)) a.

  Proof.
  auto.
  Qed.

  Lemma fold_pred_ne_spec : forall (A : Type) f v g (a : A),
  fold_pred_ne f v g a = VertexMap.fold (f g v) (VertexMap.remove v (predecessors v g)) a.

  Proof.
  auto.
  Qed.

  Definition empty_map  : maptype := 
  VertexMap.empty (option VertexLabel* (VertexMap.t (option EdgeLabel) * VertexMap.t (option EdgeLabel))).

  Lemma pred_succ_empty : forall x y w, 
  is_labeled_succ y x empty_map w <-> is_labeled_pred x y empty_map w.

  Proof.
  intros. unfold is_labeled_succ, is_labeled_pred. do 2 rewrite MapFacts.empty_mapsto_iff. reflexivity.
  Qed.

  Definition empty_graph := Make_Graph empty_map (pred_succ_empty).

  Lemma empty_graph_empty : forall v, ~In_graph_vertex v empty_graph.

  Proof.
  unfold In_graph_vertex. simpl. 
  unfold empty_map. intros. rewrite MapFacts.empty_in_iff. intuition.
  Qed.

  (*********************************** Props **********************************)

  (* Extremities of edges are in the graph *)
  Lemma In_graph_edge_in_ext : forall e g,
  In_graph_edge e g -> In_ext e g.

  Proof.
  unfold In_graph_edge, In_ext, In_graph_vertex.
  split.
  (* 1/2 *)
  unfold is_succ, succ, adj_map in H. rewrite MapFacts.in_find_iff in *.
  case_eq (VertexMap.find (fst_end e) (map g)); intros; rewrite H0 in H.
  congruence.
  simpl in H. rewrite MapFacts.empty_o in H. congruence.
  (* 2 / 2 *)
  rewrite unw_pred_succ in H.
  unfold is_pred, pred, adj_map in H. rewrite MapFacts.in_find_iff in *.
  case_eq (VertexMap.find (snd_end e) (map g)); intros.
  congruence.
  rewrite H0 in H.
  simpl in H. rewrite MapFacts.empty_o in H. congruence.
  Qed.

  (*************************** Add_vertex *************************************)

  (* The function for adding a vertex in the graph *)

  Definition add_vertex_map (m : maptype) x w :=
  match VertexMap.find x m with
  | None => let empty_m := VertexMap.empty (option EdgeLabel) in
                                             VertexMap.add x (w,(empty_m,empty_m)) m
  | Some _ => m
  end.

  Lemma pred_succ_add_vertex : forall g v w x y w',
    is_labeled_succ y x (add_vertex_map (map g) v w) w' <-> is_labeled_pred x y (add_vertex_map (map g) v w) w'.
  Proof.
  Admitted.

  Definition add_vertex v g w :=
    Make_Graph (add_vertex_map (map g) v w) (pred_succ_add_vertex g v w).

  Lemma In_add_vertex : forall v g x w w',
    In_graph_labeled_vertex x (add_vertex v g w) w' <-> 
      (~In_graph_vertex v g /\ Vertex.eq x v /\ w = w') \/
      In_graph_labeled_vertex x g w'.
  Proof.
  unfold In_graph_labeled_vertex, add_vertex, add_vertex_map. intros. simpl.
  split; intros.
  destruct H.
  case_eq (VertexMap.find v (map g)); intros; rewrite H0 in H.
  (* right *)
  right.
  exists x0. assumption.
  destruct (Vertex.eq_dec x v).
  (* left *)
  left.
  rewrite MapFacts.add_eq_o in H.
  split.
  unfold In_graph_vertex. rewrite MapFacts.in_find_iff. rewrite H0. congruence.
  split;[auto| inversion H; auto].
  auto.
  (* right *)
  right.
  rewrite MapFacts.add_neq_o in H.
  exists x0. auto.
  auto.

  (* <- *)
  do 2 destruct H. destruct H0.
  unfold In_graph_vertex in H. rewrite MapFacts.in_find_iff in H.
  case_eq (VertexMap.find v (map g)); intros; rewrite H2 in H; try congruence.
  elim H. congruence.
  rewrite MapFacts.add_eq_o.
  exists ((VertexMap.empty (option EdgeLabel), VertexMap.empty (option EdgeLabel))). 
  rewrite H1. reflexivity.
  auto.
  case_eq (VertexMap.find v (map g)); intros.
  exists x0. assumption.
  rewrite MapFacts.add_neq_o.
  exists x0. assumption.
  intro. rewrite (MapFacts.find_o _ H1) in H0. congruence.
  Qed.

  Lemma In_add_vertex_edge : forall v g e w w',
    In_graph_labeled_edge e (add_vertex v g w) w' <-> In_graph_labeled_edge e g w'.
  Proof.
    unfold In_graph_labeled_edge, add_vertex, add_vertex_map. intros. simpl.
    split; intros.
    case_eq (VertexMap.find v (map g)); intros; rewrite H0 in H.
    assumption.
    unfold is_labeled_succ, succ, adj_map in *.
    destruct (Vertex.eq_dec (fst_end e) v).
    rewrite MapFacts.add_eq_o in H. simpl in H. 
    rewrite MapFacts.empty_mapsto_iff in H. inversion H.
    auto.
    rewrite MapFacts.add_neq_o in H.
    assumption.
    auto.
    case_eq (VertexMap.find v (map g)); intros.
    assumption.
    unfold is_labeled_succ, succ, adj_map in *.
    destruct (Vertex.eq_dec (fst_end e) v).
    rewrite (MapFacts.find_o _ (Vertex.eq_sym e0)) in H0.
    rewrite H0 in H. simpl in H. rewrite MapFacts.empty_mapsto_iff in H. inversion H.
    rewrite MapFacts.add_neq_o. simpl. assumption.
    auto.
  Qed.

  (*************************** Remove_vertex *********************************)

  Definition remove_neighbor x (wsp : elttype) :=
    let (w,sp) := wsp in
    let (s,p) := sp in 
    (w, (VertexMap.remove x s, VertexMap.remove x p)).

  Definition remove_map (m : maptype) x : maptype :=
    VertexMap.map (remove_neighbor x) (VertexMap.remove x m).

  Lemma remove_neighbor_spec : forall v wsp w s p,
    remove_neighbor v wsp = (w,(s,p)) ->
    ~VertexMap.In v s /\ ~VertexMap.In v p.
  Proof.
    intros.
    unfold remove_neighbor. destruct wsp. destruct p0. simpl in *.
    inversion H. subst.
    split; rewrite MapFacts.remove_in_iff; intro; destruct H0; elim H0; auto.
  Qed.

  Lemma remove_map_mapsto : forall x v g,
    VertexMap.In x (map g) ->
    ~Vertex.eq x v ->
    VertexMap.MapsTo x (remove_neighbor v (adj_map x (map g))) (remove_map (map g) v).
  Proof.
    intros.
    apply VertexMap.map_1. apply VertexMap.remove_2. auto.
    unfold adj_map. case_eq (VertexMap.find x (map g)); intros.
    apply VertexMap.find_2. auto.
    rewrite MapFacts.in_find_iff in H. congruence.
  Qed.

  Lemma Equal_remove_empty : forall v,
    VertexMap.Equal (VertexMap.remove v (VertexMap.empty (option EdgeLabel)))
                                (VertexMap.empty (option EdgeLabel)).
  Proof.
    intros. unfold VertexMap.Equal. intros.
    reflexivity.
  Qed.

  Lemma remove_map_succ : forall x v g,
    ~Vertex.eq x v ->
    VertexMap.Equal (succ x (remove_map (map g) v))
                                (VertexMap.remove v (succ x (map g))).
  Proof.
  Admitted.

  Lemma remove_map_pred : forall x v g,
  ~Vertex.eq x v ->
  VertexMap.Equal (pred x (remove_map (map g) v))
                              (VertexMap.remove v (pred x (map g))).

  Proof.
  Admitted.

  Lemma remove_map_succ_eq : forall x v g,
  Vertex.eq x v ->
  succ x (remove_map (map g) v) = VertexMap.empty (option EdgeLabel).

  Proof.
  intros.
  unfold succ, adj_map.
  case_eq (VertexMap.find x (remove_map (map g) v)); intros.
  unfold remove_map in H0. generalize (VertexMap.find_2 H0). clear H0. intro.
  rewrite MapFacts.map_mapsto_iff in H0. destruct H0. destruct H0.
  rewrite MapFacts.remove_mapsto_iff in H1. destruct H1.
  elim H1. auto.
  simpl. reflexivity.
  Qed.

  Lemma remove_map_pred_eq : forall x v g,
  Vertex.eq x v ->
  pred x (remove_map (map g) v) = VertexMap.empty (option EdgeLabel).

  Proof.
  intros.
  unfold pred, adj_map.
  case_eq (VertexMap.find x (remove_map (map g) v)); intros.
  unfold remove_map in H0. generalize (VertexMap.find_2 H0). clear H0. intro.
  rewrite MapFacts.map_mapsto_iff in H0. destruct H0. destruct H0.
  rewrite MapFacts.remove_mapsto_iff in H1. destruct H1.
  elim H1. auto.
  simpl. reflexivity.
  Qed.

  Lemma pred_succ_remove_vertex : forall g v x y w, 
  is_labeled_succ y x (remove_map (map g) v) w <-> 
  is_labeled_pred x y (remove_map (map g) v) w.

  Proof.
  Admitted.

  Definition remove_vertex v g :=
  Make_Graph (remove_map (map g) v)
                       (pred_succ_remove_vertex g v).

  (* A vertex x is in (remove_vertex r g) iff it is in g
     and it is different from r*)
  Lemma In_remove_vertex : forall x r g w,
    In_graph_labeled_vertex x (remove_vertex r g) w <-> 
    (In_graph_labeled_vertex x g w /\ ~Vertex.eq x r).
  Proof.
  Admitted.

  (* An edge e is in (remove_vertex r g) iff it is in g
     and is not incident to r *)
  Lemma In_remove_edge : forall e r g w,
    In_graph_labeled_edge e (remove_vertex r g) w <-> 
    (In_graph_labeled_edge e g w /\ ~incident e r).
  Proof.
    unfold In_graph_labeled_edge, is_labeled_succ. intros.
    split; intros.
    destruct (Vertex.eq_dec (fst_end e) r).
    simpl in H. rewrite remove_map_succ_eq in H.
    rewrite MapFacts.empty_mapsto_iff in H. inversion H. auto.
    simpl in H. rewrite remove_map_succ in H.
    rewrite MapFacts.remove_mapsto_iff in H. destruct H.
    split.
    auto.
    unfold incident. intro. destruct H1; [elim n|elim H]; auto.
    auto.
    (* <- *)
    destruct H.
    simpl. rewrite remove_map_succ. rewrite MapFacts.remove_mapsto_iff. split.
    intro. elim H0. right. auto.
    auto.
    intro. elim H0. left. auto.
  Qed.

  (********************************* add_edge ************************************)

  Definition add_edge_map e (map : maptype) w :=
  let v1 := fst_end e in
  let v2 := snd_end e in
  match VertexMap.find v1 map with
  | None => map
  | Some wsp1 => match Vertex.eq_dec v1 v2 with
                  | left _ => let (w1, sp1) := wsp1 in let (s1,p1) := sp1 in
                                   let succ1 := VertexMap.add v2 w s1 in
                                   let pred1 := VertexMap.add v1 w p1 in
                                   VertexMap.add v1 (w1, (succ1,pred1)) map
                  | right _ => match VertexMap.find v2 map with
                               | None => map
                               | Some wsp2 => let (w1, sp1) := wsp1 in let (s1,p1) := sp1 in
                                                           let (w2, sp2) := wsp2 in let (s2,p2) := sp2 in
                                                           let succ1 := VertexMap.add v2 w s1 in
                                                           let pred2 := VertexMap.add v1 w p2 in
                                  VertexMap.add v2 (w2,(s2,pred2)) 
                                              (VertexMap.add v1 (w1,(succ1,p1)) map)
                              end
                 end
  end.

  Lemma add_edge_succ : forall e g w x,
  In_ext e g ->
  ~Vertex.eq x (fst_end e) ->
  VertexMap.Equal (succ x (add_edge_map e (map g) w)) (succ x (map g)).

  Proof.
  Admitted.

  Lemma add_edge_succ_fst : forall g e w x,
  In_ext e g ->
  Vertex.eq x (fst_end e) ->
  VertexMap.Equal (succ x (add_edge_map e (map g) w))
                              (VertexMap.add (snd_end e) w (succ x (map g))).

  Proof.
  Admitted.

  Lemma add_edge_succ_notin : forall e g w x,
  ~In_ext e g ->
  VertexMap.Equal (succ x (add_edge_map e (map g) w)) (succ x (map g)).

  Proof.
  Admitted.

  Lemma add_edge_pred : forall e g w x,
  In_ext e g ->
  ~Vertex.eq x (snd_end e) ->
  VertexMap.Equal (pred x (add_edge_map e (map g) w)) (pred x (map g)).

  Proof.
  Admitted.

  Lemma add_edge_pred_snd : forall g e w x,
  In_ext e g ->
  Vertex.eq x (snd_end e) ->
  VertexMap.Equal (pred x (add_edge_map e (map g) w))
                              (VertexMap.add (fst_end e) w (pred x (map g))).

  Proof.
  Admitted.

  Lemma add_edge_pred_notin : forall e g w x,
  ~In_ext e g ->
  VertexMap.Equal (pred x (add_edge_map e (map g) w)) (pred x (map g)).

  Proof.
  Admitted.

  Lemma In_ext_dec : forall e g, {In_ext e g}+{~In_ext e g}.

  Proof.
  unfold In_ext, In_graph_vertex. intros.
  destruct (MapFacts.In_dec (map g) (fst_end e)); 
  destruct (MapFacts.In_dec (map g) (snd_end e)).
  left; split; auto.
  right. intro. destruct H. elim n. auto.
  right. intro. destruct H. elim n. auto.
  right. intro. destruct H. elim n. auto.
  Qed.

  (* successors and predecessors are consistant *)
  Lemma pred_succ_add_edge : forall g e w' x y w, 
  is_labeled_succ y x (add_edge_map e (map g) w') w <-> 
  is_labeled_pred x y (add_edge_map e (map g) w') w.

  Proof.
  unfold is_labeled_succ, is_labeled_pred; intros.
  destruct (In_ext_dec e g).
  destruct (Vertex.eq_dec x (fst_end e)).
  rewrite add_edge_succ_fst.
  rewrite MapFacts.add_mapsto_iff.
  destruct (Vertex.eq_dec y (snd_end e)).
  rewrite add_edge_pred_snd.
  rewrite MapFacts.add_mapsto_iff.
  split; intros; destruct H; destruct H; subst.
  left. split; auto.
  right. split. auto.
  fold (is_labeled_succ y x (map g) w) in H0. rewrite pred_succ in H0.
  unfold is_labeled_pred in H0. auto.
  left. split; auto.
  elim H. auto.
  auto.
  auto.
  rewrite add_edge_pred.
  split; intros.
  destruct H; destruct H; subst. 
  elim n. auto.
  fold (is_labeled_succ y x (map g) w) in H0. rewrite pred_succ in H0.
  unfold is_labeled_pred in H0. auto.
  right. split. auto.
  fold (is_labeled_succ y x (map g) w). rewrite pred_succ.
  unfold is_labeled_pred. auto.
  auto.
  auto.
  auto.
  auto.

  rewrite add_edge_succ.
  destruct (Vertex.eq_dec y (snd_end e)).
  rewrite add_edge_pred_snd.
  split; intros.
  rewrite MapFacts.add_mapsto_iff. 
  right. split. auto.
  fold (is_labeled_succ y x (map g) w) in H. rewrite pred_succ in H.
  unfold is_labeled_pred in H. auto.
  rewrite MapFacts.add_mapsto_iff in H.
  destruct H; destruct H; subst.
  elim n. auto.
  fold (is_labeled_succ y x (map g) w). rewrite pred_succ.
  unfold is_labeled_pred. auto.
  auto.
  auto.

  rewrite add_edge_pred.
  fold (is_labeled_succ y x (map g) w). fold (is_labeled_pred x y (map g) w).
  apply pred_succ.
  auto.
  auto.
  auto.
  auto.

  rewrite add_edge_succ_notin. rewrite add_edge_pred_notin.
  fold (is_labeled_succ y x (map g) w). fold (is_labeled_pred x y (map g) w).
  apply pred_succ.
  auto.
  auto.
  Qed.

  Definition add_edge e g w :=
  Make_Graph (add_edge_map e (map g) w)
                       (pred_succ_add_edge g e w).

  Lemma In_graph_add_edge_vertex : forall v g e w w',
  In_graph_labeled_vertex v (add_edge e g w) w' <-> 
  In_graph_labeled_vertex v g w'.

  Proof.
  Admitted.

  Lemma In_graph_edge_add_edge : forall e e' g w w',
  In_graph_labeled_edge e' g w->
  ~Edge.eq e e' ->
  In_graph_labeled_edge e' (add_edge e g w') w.

  Proof.
  unfold In_graph_labeled_edge, is_labeled_succ. intros.
  simpl. destruct (In_ext_dec e g).
  destruct (Vertex.eq_dec (fst_end e') (fst_end e)).
  rewrite add_edge_succ_fst. rewrite MapFacts.add_mapsto_iff.
  destruct (Vertex.eq_dec (snd_end e) (snd_end e')).
  elim H0. split; auto.
  right. split; auto.
  auto.
  auto.
  rewrite add_edge_succ; auto.
  rewrite add_edge_succ_notin; auto.
  Qed.

  Lemma In_graph_edge_add_edge_2 : forall e g w,
  In_ext e g ->
  In_graph_labeled_edge e (add_edge e g w) w.

  Proof.
  intros. unfold In_graph_labeled_edge, is_labeled_succ.
  simpl. rewrite add_edge_succ_fst. rewrite MapFacts.add_mapsto_iff.
  left. split; auto.
  auto.
  auto.
  Qed.
(*
  Lemma test : forall (g e e' : t) (w w' : option EdgeLabel),
 In_ext e g ->
 In_graph_labeled_edge e' (add_edge e g w) w' ->
 eq e e' /\ w = w' \/ ~ eq e e' /\ In_graph_labeled_edge e' g w'.
*)
(* Error: Signature components for label In_graph_add_edge_edge do not match: expected type
"forall (g e e' : t) (w w' : option EdgeLabel),
 In_ext e g ->
 In_graph_labeled_edge e' (add_edge e g w) w' ->
 eq e e' /\ w = w' \/ ~ eq e e' /\ In_graph_labeled_edge e' g w'" but found type
"forall (g : tt) (e e' : t) (w w' : option EdgeLabel),
 In_ext e g ->
 In_graph_labeled_edge e' (add_edge e g w) w' -> ~ eq e e' -> In_graph_labeled_edge e' g w'". *)
 (*
  Lemma In_graph_add_edge_edge : forall g e e' w w',
  In_ext e g ->
  In_graph_labeled_edge e' (add_edge e g w) w' ->
  Edge.eq e e' /\ w = w' \/ ~Edge.eq e e' /\
  In_graph_labeled_edge e' g w'.
  Proof.
    unfold In_graph_labeled_edge, is_labeled_succ in *. intros.
    destruct (Vertex.eq_dec (fst_end e') (fst_end e)).
    simpl in H0. rewrite add_edge_succ_fst in H0.
    rewrite MapFacts.add_mapsto_iff in H0. 
    destruct H0; destruct H0; subst.
    left. split; auto. split. symmetry; assumption. assumption.
    right. split; auto. intros contra. destruct contra. contradiction.
    auto.
    auto.
    right. split. intros contra. destruct contra. symmetry in H1; contradiction.
    setoid_rewrite add_edge_succ in H0; assumption.
  Qed.
*)
  Lemma In_graph_add_edge_edge : forall g e e' w w',
  In_ext e g ->
  In_graph_labeled_edge e' (add_edge e g w) w' ->
  ~Edge.eq e e' ->
  In_graph_labeled_edge e' g w'.
  Proof.
  Proof.
  unfold In_graph_labeled_edge, is_labeled_succ in *. intros.
  destruct (Vertex.eq_dec (fst_end e') (fst_end e)).
  simpl in H0. rewrite add_edge_succ_fst in H0.
  rewrite MapFacts.add_mapsto_iff in H0. 
  destruct H0; destruct H0; subst.
  elim H1. split; auto.
  auto.
  auto.
  auto.
  simpl in H0. rewrite add_edge_succ in H0.
  auto.
  auto.
  auto.
  Qed.

  Lemma Add_edge_fail_vertex : forall e g w v w',
  ~In_ext e g -> 
  (In_graph_labeled_vertex v g w' <-> 
  In_graph_labeled_vertex v (add_edge e g w) w').

  Proof.
  Admitted.

  Lemma Add_edge_fail_edge : forall e g w e' w',
  ~In_ext e g -> 
  (In_graph_labeled_edge e' g w' <-> 
  In_graph_labeled_edge e' (add_edge e g w) w').

  Proof.
  Admitted.

  Definition add_edge_w e g w :=
  match VertexMap.find (fst_end e) (map g) with
  | Some _ => (match VertexMap.find (snd_end e) (map g) with
                   | Some _ => add_edge e g w
                   | None => add_edge e (add_vertex (snd_end e) g None) w
                   end)
  | None => (match VertexMap.find (snd_end e) (map g) with
                   | Some _ => add_edge e (add_vertex (fst_end e) g None) w
                   | None => add_edge e (add_vertex (fst_end e) (add_vertex (snd_end e) g None) None) w
                   end)
  end.

  Lemma In_graph_add_edge_weak_vertex : forall v g e w w',
  In_graph_labeled_vertex v (add_edge_w e g w) w' <-> 
  In_graph_labeled_vertex v g w' \/ (incident e v /\ w' = None /\ ~In_graph_vertex v g).

  Proof.
  unfold add_edge_w; intros.
  case_eq (VertexMap.find (fst_end e) (map g));
  case_eq (VertexMap.find (snd_end e) (map g)); intros.
  rewrite In_graph_add_edge_vertex.
  split; intros.
  left. auto.
  destruct H1. auto.
  destruct H1. destruct H2.
  elim H3. destruct H1.
  unfold In_graph_vertex. rewrite MapFacts.in_find_iff. rewrite (MapFacts.find_o _ H1). congruence.
  unfold In_graph_vertex. rewrite MapFacts.in_find_iff. rewrite (MapFacts.find_o _ H1). congruence.
  rewrite In_graph_add_edge_vertex. rewrite In_add_vertex.
  split; intros.
  destruct H1. destruct H1.
  destruct H2. right. split. right. auto.
  split. auto.
  unfold In_graph_vertex. rewrite MapFacts.in_find_iff. rewrite (MapFacts.find_o _ H2).
  rewrite H. congruence.
  left. auto.
  destruct H1.
  right. auto.
  destruct H1. destruct H2.
  left.
  unfold In_graph_vertex in *. rewrite MapFacts.in_find_iff in H3. 
  destruct H1; rewrite (MapFacts.find_o _ H1) in H3.
  rewrite H0 in H3. elim H3. congruence.
  split.
  unfold In_graph_vertex. rewrite MapFacts.in_find_iff. auto.
  split; auto.

  rewrite In_graph_add_edge_vertex. rewrite In_add_vertex.
  split; intros.
  destruct H1. destruct H1.
  destruct H2. right. split. left. auto.
  split. auto.
  unfold In_graph_vertex in *. rewrite MapFacts.in_find_iff. rewrite (MapFacts.find_o _ H2).
  rewrite H0. congruence.
  left. auto.
  destruct H1.
  right. auto.
  destruct H1. destruct H2.
  left.
  unfold In_graph_vertex in *. rewrite MapFacts.in_find_iff in H3. 
  destruct H1; rewrite (MapFacts.find_o _ H1) in H3.
  split.
  unfold In_graph_vertex. rewrite MapFacts.in_find_iff. auto.
  split; auto.
  rewrite H in H3. elim H3. congruence.

  rewrite In_graph_add_edge_vertex. rewrite In_add_vertex. rewrite In_add_vertex.
  split; intros.
  destruct H1; destruct H1. destruct H2.
  right. split.
  left. auto.
  split. auto.
  unfold In_graph_vertex in *. rewrite MapFacts.in_find_iff. rewrite (MapFacts.find_o _ H2). rewrite H0. auto.
  destruct H1. destruct H2.
  right.
  split. right. auto.
  split. auto.
  unfold In_graph_vertex in *. rewrite MapFacts.in_find_iff. rewrite (MapFacts.find_o _ H2). rewrite H. auto.
  left. auto.

  destruct H1. right. right. auto.
  destruct H1. destruct H2. destruct H1.
  destruct (Vertex.eq_dec (fst_end e) (snd_end e)).
  right. left. split.
  unfold In_graph_vertex in *. rewrite MapFacts.in_find_iff in *.
  rewrite (MapFacts.find_o _ H1) in H3. rewrite H. congruence.
  split. rewrite H1. auto.
  auto.
  left. split.
  unfold In_graph_vertex. rewrite MapFacts.in_find_iff. simpl.
  unfold add_vertex_map. rewrite H. rewrite MapFacts.add_neq_o. auto.
  auto. auto.

  right. left.
  split. unfold In_graph_vertex in *. rewrite MapFacts.in_find_iff in *.
  rewrite (MapFacts.find_o _ H1) in H3. rewrite H. congruence.
  split; auto.
  Qed.

  Lemma In_graph_labeled_edge_weak : forall e g val,
  In_graph_labeled_edge e g val ->
  In_graph_edge e g.

  Proof.
  Admitted.

  Lemma In_graph_add_edge_weak_edge_2 : forall e g w,
  In_graph_labeled_edge e (add_edge_w e g w) w.

  Proof.
  unfold add_edge_w; intros.
  case_eq (VertexMap.find (fst_end e) (map g));
  case_eq (VertexMap.find (snd_end e) (map g)); intros.
  apply In_graph_edge_add_edge_2.
  unfold In_ext, In_graph_vertex. do 2 rewrite MapFacts.in_find_iff.
  split; congruence.
  apply In_graph_edge_add_edge_2.
  unfold In_ext, In_graph_vertex. do 2 rewrite MapFacts.in_find_iff.
  split; try congruence.
  simpl. unfold add_vertex_map. rewrite H.
  destruct (Vertex.eq_dec (fst_end e) (snd_end e)).
  rewrite MapFacts.add_eq_o. congruence. auto.
  rewrite MapFacts.add_neq_o. fold elttype. congruence.
  auto.
  simpl. unfold add_vertex_map. rewrite H.
  rewrite MapFacts.add_eq_o. congruence.
  auto.
  apply In_graph_edge_add_edge_2.
  unfold In_ext, In_graph_vertex. do 2 rewrite MapFacts.in_find_iff.
  split; try congruence.
  simpl. unfold add_vertex_map. rewrite H0.
  rewrite MapFacts.add_eq_o. congruence.
  auto.
  simpl. unfold add_vertex_map. rewrite H0.
  destruct (Vertex.eq_dec (snd_end e) (fst_end e)).
  rewrite MapFacts.add_eq_o. congruence.
  auto.
  rewrite MapFacts.add_neq_o. fold elttype. congruence.
  auto.
  apply In_graph_edge_add_edge_2.
  unfold In_ext, In_graph_vertex. do 2 rewrite MapFacts.in_find_iff.
  split; try congruence.
  simpl. unfold add_vertex_map. rewrite H.
  destruct (Vertex.eq_dec (fst_end e) (snd_end e)).
  rewrite MapFacts.add_eq_o. rewrite MapFacts.add_eq_o. congruence.
  auto.
  auto.
  rewrite MapFacts.add_neq_o. fold elttype. rewrite H0.
  rewrite MapFacts.add_eq_o. congruence.
  auto.
  auto.
  simpl. unfold add_vertex_map. rewrite H.
  destruct (Vertex.eq_dec (snd_end e) (fst_end e)).
  rewrite MapFacts.add_eq_o. rewrite MapFacts.add_eq_o. congruence.
  auto.
  auto.
  rewrite MapFacts.add_neq_o. fold elttype. rewrite H0. rewrite MapFacts.add_neq_o.
  rewrite MapFacts.add_eq_o. congruence. 
  auto.
  auto.
  auto.
  Qed.

  Lemma In_graph_add_edge_weak_edge : forall g e e' w w',
  In_graph_labeled_edge e' (add_edge_w e g w) w' <-> 
  (eq e e' /\ w = w') \/ (~eq e e' /\ In_graph_labeled_edge e' g w').

  Proof.
  split; intros.
  destruct (Edge.eq_dec e e').
  left. split. auto.
  unfold add_edge_w in H.
  case_eq (VertexMap.find (fst_end e) (map g)); intros; rewrite H0 in H.
  case_eq (VertexMap.find (snd_end e) (map g)); intros; rewrite H1 in H.
  assert (In_graph_labeled_edge e (add_edge e g w) w).
  apply In_graph_edge_add_edge_2.
  split; unfold In_graph_vertex; rewrite MapFacts.in_find_iff; congruence.
  rewrite In_graph_labeled_edge_spec in H. rewrite <-e0 in H.
  rewrite In_graph_labeled_edge_spec in H2.
  apply (EMapFacts.MapsTo_fun H2 H).
  assert (In_graph_labeled_edge e (add_edge e (add_vertex (snd_end e) g None) w) w).
  apply In_graph_edge_add_edge_2.
  split. unfold In_graph_vertex. unfold add_vertex, add_vertex_map. simpl.
  rewrite H1. rewrite MapFacts.in_find_iff. rewrite MapFacts.add_neq_o. fold elttype. congruence.
  intro. rewrite (find_o _ H2) in H1. congruence.
  unfold In_graph_vertex, add_vertex, add_vertex_map. simpl.
  rewrite H1. rewrite MapFacts.in_find_iff. rewrite MapFacts.add_eq_o. congruence.
  auto.
  rewrite In_graph_labeled_edge_spec in H. rewrite In_graph_labeled_edge_spec in H2.
  rewrite <-e0 in H. apply (EMapFacts.MapsTo_fun H2 H).
  case_eq (VertexMap.find (snd_end e) (map g)); intros; rewrite H1 in H.
  assert (In_graph_labeled_edge e (add_edge e (add_vertex (fst_end e) g None) w) w).
  apply In_graph_edge_add_edge_2.
  split. unfold In_graph_vertex, add_vertex, add_vertex_map. simpl.
  rewrite H0. rewrite MapFacts.in_find_iff. rewrite MapFacts.add_eq_o. congruence.
  auto.
  unfold In_graph_vertex. unfold add_vertex, add_vertex_map. simpl.
  rewrite H0. rewrite MapFacts.in_find_iff. rewrite MapFacts.add_neq_o. fold elttype. congruence.
  intro. rewrite (find_o _ H2) in H0. congruence.
  rewrite In_graph_labeled_edge_spec in H. rewrite In_graph_labeled_edge_spec in H2.
  rewrite <-e0 in H. apply (EMapFacts.MapsTo_fun H2 H).
  assert (In_graph_labeled_edge e (add_edge e (add_vertex (fst_end e) (add_vertex (snd_end e) g None) None) w) w).
  apply In_graph_edge_add_edge_2.
  split; unfold In_graph_vertex, add_vertex, add_vertex_map; simpl; rewrite MapFacts.in_find_iff.
  rewrite H1.
  destruct (Vertex.eq_dec (fst_end e) (snd_end e)).
  rewrite MapFacts.add_eq_o;auto. rewrite MapFacts.add_eq_o; auto. congruence.
  rewrite MapFacts.add_neq_o; auto. fold elttype. rewrite H0. rewrite MapFacts.add_eq_o; auto. congruence.
  rewrite H1.
  destruct (Vertex.eq_dec (fst_end e) (snd_end e)).
  rewrite MapFacts.add_eq_o;auto. rewrite MapFacts.add_eq_o; auto. congruence.
  rewrite MapFacts.add_neq_o; auto. fold elttype. rewrite H0. rewrite MapFacts.add_neq_o; auto. 
  rewrite MapFacts.add_eq_o; auto. congruence.
  rewrite In_graph_labeled_edge_spec in H. rewrite In_graph_labeled_edge_spec in H2.
  rewrite <-e0 in H. apply (EMapFacts.MapsTo_fun H2 H).
  right. split. auto.
  unfold add_edge_w in H.
  case_eq (VertexMap.find (fst_end e) (map g));
  case_eq (VertexMap.find (snd_end e) (map g)); intros; rewrite H0 in *; rewrite H1 in *.
apply In_graph_add_edge_edge with (e := e) (w := w).
  unfold In_ext, In_graph_vertex. do 2 rewrite MapFacts.in_find_iff.
  split; congruence.
  auto.
  auto.
  assert (In_graph_labeled_edge e' (add_vertex (snd_end e) g None) w').
apply In_graph_add_edge_edge with (e := e) (w := w).
  unfold In_ext, In_graph_vertex. do 2 rewrite MapFacts.in_find_iff.
  split. 
  simpl. unfold add_vertex_map. rewrite H0. rewrite MapFacts.add_neq_o. fold elttype. congruence.
  intro. rewrite (MapFacts.find_o _ H2) in H0. congruence.
  simpl. unfold add_vertex_map. rewrite H0. rewrite MapFacts.add_eq_o. congruence.
  auto.
  auto.
  auto.
  rewrite In_add_vertex_edge in H2. auto.

  assert (In_graph_labeled_edge e' (add_vertex (fst_end e) g None) w').
apply In_graph_add_edge_edge with (e := e) (w := w).
  unfold In_ext, In_graph_vertex. do 2 rewrite MapFacts.in_find_iff.
  split.
  simpl. unfold add_vertex_map. rewrite H1. rewrite MapFacts.add_eq_o. congruence.
  auto.
  simpl. unfold add_vertex_map. rewrite H1. rewrite MapFacts.add_neq_o. fold elttype. congruence.
  intro. rewrite (MapFacts.find_o _ H2) in H1. congruence.
  auto.
  auto.
  rewrite In_add_vertex_edge in H2. auto.

  assert (In_graph_labeled_edge e' (add_vertex (fst_end e) (add_vertex (snd_end e) g None) None) w').
apply In_graph_add_edge_edge with (e := e) (w := w).
  unfold In_ext, In_graph_vertex. do 2 rewrite MapFacts.in_find_iff.
  split.
  simpl. unfold add_vertex_map. rewrite H0. 
  destruct (Vertex.eq_dec (fst_end e) (snd_end e)).
  rewrite MapFacts.add_eq_o. rewrite MapFacts.add_eq_o. congruence.
  auto.
  auto.
  rewrite MapFacts.add_neq_o. fold elttype. rewrite H1. rewrite MapFacts.add_eq_o. congruence.
  auto. 
  auto.
  simpl. unfold add_vertex_map. rewrite H0.
  destruct (Vertex.eq_dec (fst_end e) (snd_end e)).
  rewrite MapFacts.add_eq_o. rewrite MapFacts.add_eq_o. congruence.
  auto.
  auto.
  rewrite MapFacts.add_neq_o. fold elttype. rewrite H1. rewrite MapFacts.add_neq_o.
  rewrite MapFacts.add_eq_o. congruence.
  auto.
  auto.
  auto.
  auto.
  auto.
  rewrite In_add_vertex_edge in H2. rewrite In_add_vertex_edge in H2. auto.

  destruct H. destruct H.
  rewrite In_graph_labeled_edge_spec. rewrite <-H. rewrite H0.
  rewrite <-In_graph_labeled_edge_spec. apply In_graph_add_edge_weak_edge_2.
  destruct H.
  unfold add_edge_w.
  case_eq (VertexMap.find (fst_end e) (map g)); intros.
  case_eq (VertexMap.find (snd_end e) (map g)); intros.
  apply In_graph_edge_add_edge. auto. auto.
  apply In_graph_edge_add_edge.
  rewrite In_add_vertex_edge. auto. auto.
  case_eq (VertexMap.find (snd_end e) (map g)); intros.
  apply In_graph_edge_add_edge. rewrite In_add_vertex_edge. auto. auto.
  apply In_graph_edge_add_edge. rewrite  In_add_vertex_edge. rewrite In_add_vertex_edge. auto. auto.
  Qed.

  (******************************* remove_edge ***********************************)

  Definition remove_edge_map e (map : maptype) :=
  let v1 := fst_end e in
  let v2 := snd_end e in
  match VertexMap.find v1 map with
  | None => map
  | Some wsp1 => match Vertex.eq_dec v1 v2 with
                  | left _ => let (w1, sp1) := wsp1 in let (s1,p1) := sp1 in
                                   let succ1 := VertexMap.remove v2 s1 in
                                   let pred1 := VertexMap.remove v1 p1 in
                                   VertexMap.add v1 (w1, (succ1,pred1)) map
                  | right _ => match VertexMap.find v2 map with
                               | None => map
                               | Some wsp2 => let (w1, sp1) := wsp1 in let (s1,p1) := sp1 in
                                                           let (w2, sp2) := wsp2 in let (s2,p2) := sp2 in
                                                           let succ1 := VertexMap.remove v2 s1 in
                                                           let pred2 := VertexMap.remove v1 p2 in
                                  VertexMap.add v2 (w2,(s2,pred2)) 
                                              (VertexMap.add v1 (w1,(succ1,p1)) map)
                              end
                 end
  end.

  Lemma succ_remove_edge : forall g e x,
  ~Vertex.eq x (fst_end e) ->
  succ x (remove_edge_map e (map g)) = succ x (map g).

  Proof.
  Admitted.

  Lemma succ_remove_edge_fst : forall g e x,
  In_ext e g ->
  Vertex.eq x (fst_end e) ->
  succ x (remove_edge_map e (map g)) = VertexMap.remove (snd_end e) (succ x (map g)).

  Proof.
  Admitted.

  Lemma pred_remove_edge : forall g e x,
  ~Vertex.eq x (snd_end e) ->
  pred x (remove_edge_map e (map g)) = pred x (map g).

  Proof.
  Admitted.

  Lemma pred_remove_edge_snd : forall g e x,
  In_ext e g ->
  Vertex.eq x (snd_end e) ->
  pred x (remove_edge_map e (map g)) = VertexMap.remove (fst_end e) (pred x (map g)).

  Proof.
  Admitted.

  Lemma succ_remove_edge_notin : forall g e x,
  ~In_ext e g ->
  succ x (remove_edge_map e (map g)) = succ x (map g).

  Proof.
  Admitted.

  Lemma pred_remove_edge_notin : forall g e x,
  ~In_ext e g ->
  pred x (remove_edge_map e (map g)) = pred x (map g).

  Proof.
  Admitted.

  Lemma pred_succ_remove_edge : forall g e x y w, 
  is_labeled_succ y x (remove_edge_map e (map g)) w <-> 
  is_labeled_pred x y (remove_edge_map e (map g)) w.

  Proof.
  intros. destruct (In_ext_dec e g).
  unfold is_labeled_succ, is_labeled_pred, is_succ. intros.
  destruct (Vertex.eq_dec x (fst_end e)).
  rewrite succ_remove_edge_fst.
  destruct (Vertex.eq_dec y (snd_end e)).
  rewrite pred_remove_edge_snd.
  rewrite MapFacts.remove_mapsto_iff. rewrite MapFacts.remove_mapsto_iff.
  split; intros.
  destruct H. elim H. auto.
  destruct H. elim H. auto.
  auto.
  auto.
  rewrite pred_remove_edge.
  rewrite MapFacts.remove_mapsto_iff.
  split; intros.
  destruct H.
  fold (is_labeled_succ y x (map g) w) in H0. fold (is_labeled_pred x y (map g) w).
  apply pred_succ. auto.
  split. auto.
  fold (is_labeled_succ y x (map g) w). fold (is_labeled_pred x y (map g) w) in H.
  apply pred_succ. auto.
  auto.
  auto.
  auto.
  rewrite succ_remove_edge.
  destruct (Vertex.eq_dec y (snd_end e)).
  rewrite pred_remove_edge_snd.
  rewrite MapFacts.remove_mapsto_iff.
  split; intros.
  split. auto.
  fold (is_labeled_succ y x (map g) w) in H. fold (is_labeled_pred x y (map g) w).
  apply pred_succ. auto.
  destruct H.
  fold (is_labeled_succ y x (map g) w). fold (is_labeled_pred x y (map g) w) in H0.
  apply pred_succ. auto. auto.
  auto.
  rewrite pred_remove_edge.
  fold (is_labeled_succ y x (map g) w). fold (is_labeled_pred x y (map g) w).
  apply pred_succ. 
  auto.
  auto.

  unfold is_labeled_succ, is_labeled_pred.
  rewrite succ_remove_edge_notin. rewrite pred_remove_edge_notin.
  fold (is_labeled_succ y x (map g) w). fold (is_labeled_pred x y (map g) w).
  apply pred_succ.
  auto.
  auto.
  Qed.

  Definition remove_edge e g :=
  Make_Graph (remove_edge_map e (map g))
                       (pred_succ_remove_edge g e).

  Lemma In_graph_remove_edge_vertex : forall v e g w,
  (In_graph_labeled_vertex v (remove_edge e g) w <-> 
  In_graph_labeled_vertex v g w).

  Proof.
  Admitted.

  Lemma In_graph_remove_edge_edge_succ : forall e g,
  ~VertexMap.In (snd_end e) (successors (fst_end e) (remove_edge e g)).

  Proof.
  unfold successors. intros. simpl.
  destruct (In_ext_dec e g).
  rewrite succ_remove_edge_fst.
  apply VertexMap.remove_1. auto.
  auto.
  auto.
  rewrite succ_remove_edge_notin.
  intro. elim n.
  split.
  unfold In_graph_vertex, succ, adj_map in *. rewrite MapFacts.in_find_iff.
  case_eq (VertexMap.find (fst_end e) (map g)); intros; rewrite H0 in H.
  congruence.
  simpl in H. rewrite MapFacts.empty_in_iff in H. inversion H.
  fold (is_succ (snd_end e) (fst_end e) (map g)) in H. rewrite unw_pred_succ in H.
  unfold In_graph_vertex, is_pred, pred, adj_map in *. rewrite MapFacts.in_find_iff.
  case_eq (VertexMap.find (snd_end e) (map g)); intros; rewrite H0 in H.
  congruence.
  simpl in H. rewrite MapFacts.empty_in_iff in H. inversion H.
  auto.
  Qed.

  Lemma In_graph_remove_edge_edge_pred : forall e g,
  ~VertexMap.In (fst_end e) (predecessors (snd_end e) (remove_edge e g)).

  Proof.
  unfold predecessors. intros. simpl.
  destruct (In_ext_dec e g).
  rewrite pred_remove_edge_snd.
  apply VertexMap.remove_1. auto.
  auto.
  auto.
  rewrite pred_remove_edge_notin.
  intro. elim n.
  split.
  fold (is_pred (fst_end e) (snd_end e) (map g)) in H. rewrite <-unw_pred_succ in H.
  unfold In_graph_vertex, is_succ, succ, adj_map in *. rewrite MapFacts.in_find_iff.
  case_eq (VertexMap.find (fst_end e) (map g)); intros; rewrite H0 in H.
  congruence.
  simpl in H. rewrite MapFacts.empty_in_iff in H. inversion H.
  unfold In_graph_vertex, pred, adj_map in *. rewrite MapFacts.in_find_iff.
  case_eq (VertexMap.find (snd_end e) (map g)); intros; rewrite H0 in H.
  congruence.
  simpl in H. rewrite MapFacts.empty_in_iff in H. inversion H.
  auto.
  Qed.

  Lemma In_graph_remove_edge_edge : forall g e e' l',
    ~ Edge.eq e e' ->
    In_graph_labeled_edge e' (remove_edge e g) l' <->
    In_graph_labeled_edge e' g l'.
  Admitted.

  Lemma In_graph_remove_edge_edge_2 : forall g e,
    ~ In_graph_edge e (remove_edge e g).
  Admitted.

End Directed_GraphMap.
