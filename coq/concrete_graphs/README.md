# Graph library

This is a generic Coq library to deal with finite directed and undirected graphs.
It comes not only with specifications and proofs, but also, most importantly,
with a functional implementation which, once extracted into OCaml,
can be executed.
In a few words, it is a prototype in Coq of what
[OCamlgraph](https://github.com/backtracking/ocamlgraph)
is in OCaml.

See the associated [publication](#publication) (in French) for more details.

This code was initially written by Benoît Robillard (Cédric, ENSIIE, Cnam) for
Coq 8.3pl2. Jean-Christophe Léchenet (LSL, CEA List, CentraleSupélec) ported it
to Coq 8.6.1.
The port was done in a quick and dirty way. Some unused parts were dropped,
and some proofs were admitted. The initial version can be found
[online](http://www.ensiie.fr/~robillard/Graph_Library/).

## License

This library is distributed under the terms of the GNU General Public License,
either version 3 of the License, or (at your option) any later version
(see [`LICENSE`](LICENSE)).

## Publication

- [Graphes et couplages en Coq](https://hal.science/JFLA2015/hal-01099140v1) <br>
  C. Dubois, S. Elloumi, B. Robillard, and C. Vincent.
  In Journées Francophones des Langages Applicatifs (JFLA 2015), in French.
