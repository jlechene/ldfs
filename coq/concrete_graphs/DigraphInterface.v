Require Import FMaps.
Require Import Directed_edges.
Require Import FMapAVL.
Require Import MapFacts.
Require Import Labels.

(* Module of graphs *)
Module Type Directed_Graph (Vertex : OrderedType) (Lab : Labels).

  (* Types of labels *)
  Import Lab.

  (********************* Abstract modules definitions ********************************)
  (* Module of maps of vertices (vertices map to values) *)
  (* remplacer par des Declare Module ? *)
  Module VertexMap := FMapAVL.Make Vertex.
  Module Export VMapFacts := MyMapFacts VertexMap.
  (* Modules of edges, derived from vertices *)
  Module Import Edge := Directed_Edge Vertex.
  (* Module of map of edges *)
  Module EdgeMap := FMapAVL.Make Edge.
  Module Import EMapFacts := MyMapFacts EdgeMap.

  (****************************** The data structure *********************************)
  (* The graph datatype *)
  Parameter t : Type.
  (* The set of vertices *)
  Parameter V : t -> VertexMap.t (option VertexLabel).
  Parameter vertex_label : Vertex.t -> t -> option (option VertexLabel).
  (* The set of edges *)
  Parameter E : t -> EdgeMap.t (option EdgeLabel).
  Parameter edge_label : Edge.t -> t -> option (option EdgeLabel).
  (* The set of succ *)
  Parameter successors : Vertex.t -> t -> VertexMap.t (option EdgeLabel).
  (* The set of pred *)
  Parameter predecessors : Vertex.t -> t -> VertexMap.t (option EdgeLabel).
  (* Edges as a relation *)
  Parameter link : t -> Vertex.t -> Vertex.t -> option (option EdgeLabel).

  (***************************** label properties ***********************************)
  Parameter vertex_label_spec : forall v g,
    vertex_label v g = VertexMap.find v (V g).

  Parameter edge_label_spec : forall e g,
    edge_label e g = EdgeMap.find e (E g).

  (****************************** Graph predicates **********************************)
  (* A vertex is in the graph *)
  Parameter In_graph_vertex : Vertex.t -> t -> Prop.
  Parameter In_graph_labeled_vertex : Vertex.t -> t -> option VertexLabel -> Prop.
  (* An edge is in the graph*)
  Parameter In_graph_edge : Edge.t -> t -> Prop.
  Parameter In_graph_labeled_edge : Edge.t -> t -> option EdgeLabel -> Prop.

  (***************************** Specifications **************************************)
  (* Belonging specification *)
  Parameter In_graph_vertex_spec : forall v g,
    In_graph_vertex v g <-> VertexMap.In v (V g).

  Parameter In_graph_labeled_vertex_spec : forall v g l,
    In_graph_labeled_vertex v g l <-> VertexMap.MapsTo v l (V g).

  (* Graph belonging is a morphism *)
  (* replace by Declare Parametric Morphism (or equivalent) ?
     or in a functor rather than in the sig ? *)
  Add Parametric Morphism : In_graph_vertex
    with signature (Vertex.eq ==> Logic.eq ==> iff) as In_graph_vertex_m.
  Proof.
    split; intros. rewrite In_graph_vertex_spec in *.
    rewrite H in H0. assumption.
    rewrite In_graph_vertex_spec in *.
    rewrite H. assumption.
  Qed.

  Add Parametric Morphism : In_graph_labeled_vertex
    with signature (Vertex.eq ==> Logic.eq ==> Logic.eq ==> iff) as In_graph_labeled_vertex_m.
  Proof.
    intros. split; intros. rewrite In_graph_labeled_vertex_spec in *.
    rewrite H in H0. assumption.
    rewrite In_graph_labeled_vertex_spec in *.
    rewrite H. assumption.
  Qed.

  (* Belonging specification *)
  Parameter In_graph_edge_spec : forall e g,
    In_graph_edge e g <-> EdgeMap.In e (E g).

  Opaque Edge.t.

  Parameter In_graph_labeled_edge_spec : forall e g l,
    In_graph_labeled_edge e g l <-> EdgeMap.MapsTo e l (E g).

  (* Graph belonging is a morphism *)
  Add Morphism In_graph_edge : In_graph_edge_m.
  Add Morphism In_graph_labeled_edge : In_graph_labeled_edge_m.

  (* Equivalence of edges and relationship *)
  Parameter link_edge_equiv : forall v1 v2 g l,
    link g v1 v2 = Some l <-> In_graph_labeled_edge (v1,v2) g l.

  Parameter mapsto_edge_equiv_succ : forall v1 v2 g l,
    VertexMap.MapsTo v2 l (successors v1 g) <-> In_graph_labeled_edge (v1,v2) g l.

  Parameter mapsto_edge_equiv_pred : forall v1 v2 g l,
    VertexMap.MapsTo v1 l (predecessors v2 g) <-> In_graph_labeled_edge (v1,v2) g l.

  (********************************* graphs properties *******************************)

  (* Extremities of edges are in the graph *)
  Parameter In_graph_edge_in_ext : forall e g,
    In_graph_edge e g ->
    In_graph_vertex (fst_end e) g /\ In_graph_vertex (snd_end e) g.

  (******************************** iterators on graphs ********************************)
  Parameter fold_edges : forall {A : Type},
    (t -> Edge.t -> (option EdgeLabel) -> A -> A) -> t -> A -> A.
  Parameter fold_vertices : forall {A : Type},
    (t -> Vertex.t -> (option VertexLabel) -> A -> A) -> t -> A -> A.
  Parameter fold_succ : forall {A : Type},
    (t -> Vertex.t -> Vertex.t -> (option EdgeLabel) -> A -> A) -> Vertex.t -> t -> A -> A.
  Parameter fold_pred : forall {A : Type},
    (t -> Vertex.t -> Vertex.t -> (option EdgeLabel) -> A -> A) -> Vertex.t -> t -> A -> A.
  Parameter fold_succ_ne : forall {A : Type},
    (t -> Vertex.t -> Vertex.t -> (option EdgeLabel) -> A -> A) -> Vertex.t -> t -> A -> A.
  Parameter fold_pred_ne : forall {A : Type},
    (t -> Vertex.t -> Vertex.t -> (option EdgeLabel) -> A -> A) -> Vertex.t -> t -> A -> A.

  Parameter fold_vertices_spec : forall {A : Type} f g (a : A),
    fold_vertices f g a = VertexMap.fold (f g) (V g) a.

  Parameter fold_edges_spec : forall {A : Type} f g (a : A),
    fold_edges f g a = EdgeMap.fold (f g) (E g) a.

  (* folding on succ of a given vertex *)
  Parameter fold_succ_spec : forall {A : Type} f v g (a : A),
    fold_succ f v g a = VertexMap.fold (f g v) (successors v g) a.

  Parameter fold_pred_spec : forall {A : Type} f v g (a : A),
    fold_pred f v g a = VertexMap.fold (f g v) (predecessors v g) a.

  (* folding on succ of a given vertex *)
  Parameter fold_succ_ne_spec : forall {A : Type} f v g (a : A),
    fold_succ_ne f v g a = VertexMap.fold (f g v) (VertexMap.remove v (successors v g)) a.

  Parameter fold_pred_ne_spec : forall {A : Type} f v g (a : A),
    fold_pred_ne f v g a = VertexMap.fold (f g v) (VertexMap.remove v (predecessors v g)) a.

End Directed_Graph.