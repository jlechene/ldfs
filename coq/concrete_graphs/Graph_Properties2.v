Require Import DigraphInterface.
Require Import OrderedType.
Require Import Labels.
Require Import Graph_Properties.

Module GraphProps (Vertex : OrderedType) (Lab : Labels)
                                (G : Directed_Graph Vertex Lab).

  Include Graph_Properties.GraphProps Vertex Lab G.

  Import G.

  Add Parametric Relation : (t) eq
    reflexivity proved by eq_refl
    symmetry proved by eq_sym
    transitivity proved by eq_trans
  as eq_equiv.

  Add Parametric Morphism : V
    with signature eq ==> VertexMap.Equal as V_eqG.
  Proof.
    intros. apply H.
  Qed.

  Add Parametric Morphism : E
    with signature eq ==> EdgeMap.Equal as E_eqG.
  Proof.
    intros. apply H.
  Qed.

  Add Parametric Morphism : In_graph_vertex
    with signature Vertex.eq ==> eq ==> iff as In_graph_vertex_eqG.
  Proof.
    intros u v Heq g h Heqg. rewrite 2!In_graph_vertex_spec.
    rewrite Heqg. rewrite Heq. reflexivity.
  Qed.

  Add Parametric Morphism : In_graph_labeled_vertex
    with signature Vertex.eq ==> eq ==> Logic.eq ==> iff
    as In_graph_labeled_vertex_eqG.
  Proof.
    intros. rewrite 2!In_graph_labeled_vertex_spec.
    rewrite H, H0. reflexivity.
  Qed.

  Add Parametric Morphism : In_graph_edge
    with signature Edge.eq ==> eq ==> iff
    as In_graph_edge_eqG.
  Proof.
    intros. rewrite 2!In_graph_edge_spec.
    rewrite H, H0. reflexivity.
  Qed.

  Add Parametric Morphism : In_graph_labeled_edge
    with signature Edge.eq ==> eq ==> Logic.eq ==> iff
    as In_graph_labeled_edge_eqG.
  Proof.
    intros. rewrite 2!In_graph_labeled_edge_spec.
    rewrite H, H0. reflexivity.
  Qed.

  Lemma eq_In_graph : forall g h,
    g =G h <->
      (forall u l, In_graph_labeled_vertex u g l <->
                   In_graph_labeled_vertex u h l) /\
      (forall e l, In_graph_labeled_edge e g l <->
                   In_graph_labeled_edge e h l).
  Proof.
    intros.
    setoid_rewrite In_graph_labeled_vertex_spec.
    rewrite <- MapFacts.Equal_mapsto_iff.
    setoid_rewrite In_graph_labeled_edge_spec.
    rewrite <- EMapFacts.Equal_mapsto_iff. reflexivity.
  Qed.

End GraphProps.