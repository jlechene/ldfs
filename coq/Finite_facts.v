(** Some results about the finite sets defined in [MyFinite]. *)

Require Import MyFinite.

Require Import Decidable.

Require List. Import List.ListNotations.
Open Scope list_scope.

Require Import Setoid.

Lemma Ensemble_of_list_nil : forall {A},
  Ensemble_of_list (@nil A) == Empty_set.
Proof.
  split.
  - intros a H. inversion H.
  - apply Included_Empty.
Qed.

Lemma Non_disjoint_union : forall {A} (X : Ensemble A) (x : A),
  X x -> Add X x == X.
Proof.
  intros. apply Same_set_iff. intros x'. split; intros.
  - apply Add_inv in H0. destruct H0.
    + assumption.
    + subst x'. assumption.
  - apply Add_intro1. assumption.
Qed.

Lemma Subtract_not_In : forall {A} (X:Ensemble A) x,
  ~ Subtract X x x.
Proof.
  intros. intros contra.
  apply Subtract_inv in contra. destruct contra as (_ & contra).
  apply contra. reflexivity.
Qed.

Lemma Add_Subtract_Same_set : forall {A}
  (eq_dec : forall (x x':A), {x=x'}+{x<>x'})
  (X:Ensemble A) x, X x ->
  Add (Subtract X x) x == X.
Proof.
  split; intros.
  - intros x' ?. apply Add_inv in H0. destruct H0.
    + apply Subtract_inv in H0. apply H0.
    + subst x'. assumption.
  - intros x' ?. destruct (eq_dec x x').
    + subst x'. apply Add_intro2.
    + apply Add_intro1. apply Subtract_intro; assumption.
Qed.

Lemma Subtract_Add_Same_set : forall {A}
  (X:Ensemble A) x, ~ X x ->
  Subtract (Add X x) x == X.
Proof.
  split; intros.
  - intros x' ?. apply Subtract_inv in H0. destruct H0.
    apply Add_inv in H0. destruct H0.
    + assumption.
    + contradiction.
  - intros x' ?. apply Subtract_intro.
    + apply Add_intro1. assumption.
    + intros contra. subst x'. contradiction.
Qed.

Lemma Subtract_not_changed : forall {A}
  (X:Ensemble A) x, ~ X x ->
  Subtract X x == X.
Proof.
  intros. split; intros x' ?.
  - apply Subtract_inv in H0; apply H0.
  - apply Subtract_intro; [assumption|]. intros contra. subst x'; contradiction.
Qed.

Lemma Subtract_Add_comm : forall {A}
  (X:Ensemble A) x x', x <> x' ->
  Subtract (Add X x') x == Add (Subtract X x) x'.
Proof.
  split; intros x'' ?.
  - apply Subtract_inv in H0. destruct H0. apply Add_inv in H0. destruct H0.
    + apply Add_intro1. apply Subtract_intro; assumption.
    + subst x''. apply Add_intro2.
  - apply Add_inv in H0. destruct H0.
    + apply Subtract_inv in H0. destruct H0.
      apply Subtract_intro; [|assumption]. apply Add_intro1; assumption.
    + subst x''. apply Subtract_intro; [|assumption]. apply Add_intro2.
Qed.

Lemma Union_Empty_set_l : forall {A} (X:Ensemble A),
  Union Empty_set X == X.
Proof.
  split; intros.
  - intros x ?. apply Union_inv in H. destruct H.
    + apply Noone_in_empty in H. contradiction.
    + assumption.
  - intros x ?. apply Union_intror. assumption.
Qed.

Lemma Union_Empty_set_r : forall {A} (X:Ensemble A),
  Union X Empty_set == X.
Proof.
  split; intros.
  - intros x ?. apply Union_inv in H. destruct H.
    + assumption.
    + apply Noone_in_empty in H. contradiction.
  - intros x ?. apply Union_introl. assumption.
Qed.

Lemma cardinal_finite : forall {A} (X:Ensemble A) n,
  cardinal X n -> Finite X.
Proof.
  intros. induction H; econstructor; eassumption.
Qed.

Lemma finite_cardinal : forall {A} (X:Ensemble A),
  Finite X -> exists n, cardinal X n.
Proof.
  intros. induction H.
  - exists 0. constructor; assumption.
  - destruct IHFinite as (n & IHFinite).
    exists (S n). econstructor; eassumption.
Qed.

Lemma Singleton_cardinal_1 : forall {A} (a:A),
  cardinal (Singleton a) 1.
Proof.
  intros. apply card_add with (X:=Empty_set) (x:=a).
  - apply card_empty; reflexivity.
  - apply Noone_in_empty.
  - split; intros a' ?.
    + apply Singleton_inv in H. subst a'. apply Add_intro2.
    + apply Add_inv in H. destruct H.
      * apply Noone_in_empty in H. contradiction.
      * apply Singleton_intro. assumption.
Qed.

Lemma Singleton_finite : forall {A} (a:A),
  Finite (Singleton a).
Proof.
  intros. eapply cardinal_finite. apply Singleton_cardinal_1.
Qed.

Lemma Singleton_Add : forall {A} (a:A),
  Add Empty_set a == Singleton a.
Proof.
  split; intros a' ?.
  - apply Add_inv in H. destruct H.
    + apply Noone_in_empty in H. contradiction.
    + apply Singleton_intro; assumption.
  - apply Singleton_inv in H. subst a'. apply Add_intro2.
Qed.

Lemma cardinal_1_Singleton : forall {A} (X:Ensemble A),
  cardinal X 1 -> exists a, X == Singleton a.
Proof.
  intros. inversion H. exists x. rewrite H4.
  inversion H1. rewrite H5.
  apply Singleton_Add.
Qed.

Lemma Intersection_Empty_l : forall {A} (X:Ensemble A),
  Intersection Empty_set X == Empty_set.
Proof.
  split; intros.
  - intros x ?. apply Intersection_inv in H. apply H.
  - apply Included_Empty.
Qed.

Lemma Intersection_Empty_r : forall {A} (X:Ensemble A),
  Intersection X Empty_set == Empty_set.
Proof.
  split; intros.
  - intros x ?. apply Intersection_inv in H. apply H.
  - apply Included_Empty.
Qed.

Definition Has_at_most_one {A} (P:A->Prop) :=
  cardinal P 0 \/
  cardinal P 1.

Lemma uniqueness_cardinal : forall {A} (P:A->Prop),
  uniqueness P /\ Finite P <-> Has_at_most_one P.
Proof.
  split; intros.
  - unfold Has_at_most_one. destruct H.
    inversion H0; subst.
    + left. constructor. assumption.
    + right. inversion H1; subst.
      * { eapply card_add.
        - apply card_empty. eassumption.
        - eassumption.
        - assumption. }
      * assert (P x) as Ha1. { rewrite H3. apply Add_intro2. }
        assert (P x0) as Ha2.
        { rewrite H3. apply Add_intro1. rewrite H6. apply Add_intro2. }
        contradiction H2.
        rewrite H6.
        rewrite (H x x0 Ha1 Ha2). apply Add_intro2.
  - destruct H.
    + inversion H; subst.
      split.
      * unfold uniqueness; intros. rewrite H0 in H1. apply Noone_in_empty in H1.
        contradiction.
      * constructor; assumption.
    + inversion H; subst. inversion H1; subst.
      split.
      * unfold uniqueness; intros. rewrite H0 in H4. rewrite H4 in H3, H5.
        apply Add_inv in H3.
        destruct H3 as [H3|H3]; [apply Noone_in_empty in H3; contradiction|].
        apply Add_inv in H5.
        destruct H5 as [H5|H5]; [apply Noone_in_empty in H5; contradiction|].
        subst x0 y. reflexivity.
      * { eapply Union_is_finite.
        - apply Empty_is_finite. eassumption.
        - eassumption.
        - assumption. }
Qed.

Lemma Finite_In_dec : forall {A:Type}
  (eq_dec : forall (u v : A), {u = v} + {u <> v}) (X:Ensemble A)
  (X_Fin : Finite X), forall x, X x \/ ~ X x.
Proof.
  intros. induction X_Fin as [X|X X_Fin ? x' ? Y ?].
  - right. rewrite H. apply Noone_in_empty.
  - destruct (eq_dec x' x).
    + subst x'. left. rewrite H0. apply Add_intro2.
    + destruct IHX_Fin as [IHX_Fin|IHX_Fin].
      * left. rewrite H0. apply Add_intro1. assumption.
      * right. intros contra. rewrite H0 in contra. apply Add_inv in contra.
        destruct contra; contradiction.
Qed.

Lemma Finite_downward_closed : forall {A:Type}
  (eq_dec : forall (u v : A), {u=v}+{u<>v}) (X:Ensemble A)
  (X_Fin : Finite X), forall (Y:Ensemble A)
  (Y_In_dec : forall u, Y u \/ ~ Y u),
  Included Y X -> Finite Y.
Proof.
  intros A eq_dec X HFin.
  induction HFin as [X|X X_Fin IHX_Fin x ? X' ?]; intros.
  - constructor. split.
    + rewrite H in H0. assumption.
    + apply Included_Empty.
  - destruct (Y_In_dec x).
    + rewrite <- (Add_Subtract_Same_set eq_dec Y x H2).
      eapply Union_is_finite; [| |reflexivity].
      * { apply IHX_Fin.
        -  intros x'. destruct (eq_dec x x').
          + right. subst x'. apply Subtract_not_In.
          + destruct (Y_In_dec x').
            * left. apply Subtract_intro; assumption.
            * right. intros contra. apply Subtract_inv in contra.
              destruct contra; contradiction.
        - intros x' ?. apply Subtract_inv in H3. destruct H3.
          apply H1 in H3. rewrite H0 in H3. apply Add_inv in H3.
          destruct H3; [assumption|contradiction]. }
      * apply Subtract_not_In.
    + apply (IHX_Fin Y Y_In_dec). intros x' ?.
      assert (x <> x'). { intros contra. subst x'. contradiction. }
      apply H1 in H3. rewrite H0 in H3. apply Add_inv in H3.
      destruct H3; [assumption|contradiction].
Qed.

Lemma Union_preserves_Finite : forall {A : Type}
  (eq_dec : forall (u v : A), {u=v}+{u<>v})
  (X Y : Ensemble A)
  (X_Fin : Finite X) (Y_Fin : Finite Y), Finite (Union X Y).
Proof.
  intros. induction X_Fin as [X|X X_Fin IHX_Fin x ? X' ?].
  - rewrite H. rewrite Union_Empty_set_l. assumption.
  - destruct (Finite_In_dec eq_dec Y Y_Fin x).
    + setoid_replace (Union X' Y) with (Union X Y); [assumption|].
      rewrite H0. split.
      * intros x' ?. apply Union_inv in H2. { destruct H2.
        - apply Add_inv in H2. destruct H2.
          + apply Union_introl; assumption.
          + subst x'. apply Union_intror; assumption.
        - apply Union_intror; assumption. }
      * intros x' ?. apply Union_inv in H2. { destruct H2.
        - apply Union_introl. apply Add_intro1; assumption.
        - apply Union_intror. assumption. }
    + apply (Union_is_finite (Union X Y) IHX_Fin x).
      * intros contra. apply Union_inv in contra.
        destruct contra; contradiction.
      * rewrite H0. { split; intros x' ?.
        - apply Union_inv in H2. destruct H2.
          + apply Add_inv in H2. destruct H2.
            * apply Add_intro1. apply Union_introl. assumption.
            * subst x'. apply Add_intro2.
          + apply Add_intro1. apply Union_intror; assumption.
        - apply Add_inv in H2. destruct H2.
          + apply Union_inv in H2. destruct H2.
            * apply Union_introl. apply Add_intro1; assumption.
            * apply Union_intror; assumption.
          + subst x'. apply Union_introl. apply Add_intro2. }
Qed.

Lemma Finite_Union_preserves_Finite : forall {A B : Type}
  (eq_dec : forall (b1 b2 : B), {b1=b2}+{b1<>b2})
  (X : Ensemble A) (Y : A -> Ensemble B)
  (X_Fin : Finite X)
  (Y_Fin : forall a, Finite (Y a)),
  Finite (fun b => exists a, X a /\ Y a b).
Proof.
  intros. induction X_Fin as [X|X X_Fin IHX_Fin a ? X' ?].
  - apply Empty_is_finite. split.
    + intros b ?. destruct H0 as (a & H0 & _). apply H in H0.
      apply Noone_in_empty in H0. contradiction.
    + apply Included_Empty.
  - setoid_replace (fun b : B => exists a : A, X' a /\ Y a b)
      with (Union (Y a) (fun b : B => exists a : A, X a /\ Y a b)).
    + apply Union_preserves_Finite; try assumption. apply Y_Fin.
    + split; intros b ?.
      * destruct H1 as (a' & H11 & H12). rewrite H0 in H11.
        { apply Add_inv in H11. destruct H11.
        - apply Union_intror. exists a'. split; assumption.
        - subst a'. apply Union_introl; assumption. }
      * apply Union_inv in H1. { destruct H1.
        - exists a. split.
          + rewrite H0. apply Add_intro2.
          + assumption.
        - destruct H1 as (a' & H11 & H12).
          exists a'. split; [|assumption].
          rewrite H0. apply Add_intro1; assumption. }
Qed.

Lemma Finite_Subtract : forall {A} (eq_dec : forall (u v : A), {u=v}+{u<>v})
  (X:Ensemble A) (X_Fin : Finite X) x, Finite (Subtract X x).
Proof.
  intros. apply (Finite_downward_closed eq_dec X X_Fin).
  - intros x'. destruct (eq_dec x x').
    + right. subst x'. apply Subtract_not_In.
    + destruct (Finite_In_dec eq_dec X X_Fin x').
      * left. apply Subtract_intro; assumption.
      * right. intros contra. apply Subtract_inv in contra.
        destruct contra; contradiction.
  - intros x' ?. apply Subtract_inv in H. apply H.
Qed.

Lemma finite_image : forall {A B:Type}
  (eq_dec : forall (b1 b2 : B), {b1=b2}+{b1<>b2})
  (X:Ensemble A) (f:A -> B)
  (X_Fin : Finite X), Finite (Im X f).
Proof.
  intros. induction X_Fin as [X|X X_Fin IHX_Fin a ? X' ?].
  - apply Empty_is_finite. split.
    + intros b ?. apply Im_inv in H0. destruct H0 as (a & H0 & _).
      rewrite H in H0. apply Noone_in_empty in H0. contradiction.
    + apply Included_Empty.
  - setoid_replace (Im X' f) with (Add (Im X f) (f a)).
    + apply (Union_preserves_Finite eq_dec).
      * apply IHX_Fin.
      * apply Singleton_finite.
    + rewrite H0. split.
      * intros b ?. apply Im_inv in H1. destruct H1 as (a' & H11 & H12).
        apply Add_inv in H11. { destruct H11.
        - apply Add_intro1. apply Im_intro with (x:=a').
          + assumption.
          + symmetry; assumption.
        - subst a' b. apply Add_intro2. }
      * intros b ?. apply Add_inv in H1. { destruct H1.
        - apply Im_inv in H1. destruct H1 as (a' & H11 & H12).
          apply Im_intro with (x:=a').
          + apply Add_intro1. assumption.
          + symmetry; assumption.
        - apply Im_intro with (x:=a).
          + apply Add_intro2.
          + symmetry; assumption. }
Qed.

Lemma Finite_dec_Intersection_Finite : forall {A:Type} (X1 X2 : Ensemble A)
  (X1_Fin : Finite X1) (X2_dec : forall x, X2 x \/ ~ X2 x),
  Finite (Intersection X1 X2).
Proof.
  intros.
  induction X1_Fin as [X|X X_Fin IHX_Fin x ? X' ?]; intros.
  - rewrite H. rewrite Intersection_Empty_l. apply Empty_is_finite; reflexivity.
  - rewrite H0. destruct (X2_dec x).
    + apply (Union_is_finite (Intersection X X2) IHX_Fin x).
      * intros contra. apply Intersection_inv in contra.
        destruct contra; contradiction.
      * { split; intros x' ?.
        - apply Intersection_inv in H2. destruct H2. apply Add_inv in H2.
          destruct H2.
          + apply Add_intro1. apply Intersection_intro; assumption.
          + subst x'. apply Add_intro2.
        - apply Add_inv in H2. destruct H2.
          + apply Intersection_inv in H2. destruct H2.
            apply Intersection_intro; [|assumption].
            apply Add_intro1; assumption.
          + subst x'. apply Intersection_intro; [|assumption].
            apply Add_intro2. }
    + setoid_replace (Intersection (Add X x) X2)
        with (Intersection X X2); [assumption|].
      split; intros x' ?.
      * apply Intersection_inv in H2. destruct H2.
        apply Add_inv in H2. { destruct H2.
        - apply Intersection_intro; assumption.
        - subst x'. contradiction. }
      * apply Intersection_inv in H2. destruct H2.
        apply Intersection_intro; [|assumption]. apply Add_intro1. assumption.
Qed.

Lemma Finite_exists_dec_aux : forall {A:Type} (X:Ensemble A) (P : A -> Prop)
  (X_Fin : Finite X) (P_dec : forall (x:A), P x \/ ~ P x),
  (exists x, X x /\ P x) \/ ~ (exists x, X x /\ P x).
Proof.
  intros.
  pose proof (Finite_dec_Intersection_Finite X P X_Fin P_dec).
  inversion H; subst.
  - right. intros (x & contra1 & contra2). apply (Noone_in_empty x).
    apply H0. apply Intersection_intro; assumption.
  - left. exists x.
    assert (Intersection X P x) as Ha by (apply H2; apply Add_intro2).
    apply Intersection_inv in Ha. assumption.
Qed.

Lemma Finite_exists_dec_aux2_old : forall {A:Type}
  (P : A -> Prop)
  (P_dec : forall (x:A), P x \/ ~ P x)
  (X:Ensemble A) (HFin : Finite X),
  (exists x, X x /\ P x) \/ ~ (exists x, X x /\ P x).
Proof.
  intros.
  induction HFin as [X|X X_Fin IHX_Fin x ? X' ?]; intros.
  - right. intros (x & contra & _).
    apply H in contra.
    apply Noone_in_empty in contra. assumption.
  - destruct (P_dec x).
    + left. exists x. split.
      * apply H0. apply Add_intro2.
      * assumption.
    + destruct IHX_Fin.
      * destruct H2 as (x' & H11 & H12).
        left. exists x'. { split.
        - apply H0. apply Add_intro1. assumption.
        - assumption. }
      * right. intros (x' & contra1 & contra2).
        apply H2. exists x'. split; [|assumption].
        apply H0 in contra1.
        apply Add_inv in contra1. { destruct contra1.
        - assumption.
        - subst x'. contradiction. }
Abort.

Lemma Finite_exists_dec : forall {A:Type} (X:Ensemble A) (P : A -> Prop)
  (X_Fin : Finite X) (P_dec : forall (x:A), P x \/ ~ P x)
  (HIncl : forall x, P x -> In X x),
  (exists x, P x) \/ ~ (exists x, P x).
Proof.
  intros. destruct (Finite_exists_dec_aux X P X_Fin P_dec).
  - destruct H as (x & _ & H2).
    left. exists x. assumption.
  - right. intros contra. apply H. destruct contra as (x & contra).
    exists x. split; [|assumption]. apply HIncl; assumption.
Qed.

Definition list_n {A:Type} (X:Ensemble A) n (l:list A) :=
  List.Forall X l /\ List.length l <= n.

Lemma list_Sn_n : forall {A:Type} (X:Ensemble A) n,
  list_n X (S n) == Union (list_n X n)
      (fun l => exists x, X x /\ Im (list_n X n) (fun l' => x :: l') l).
Proof.
  intros. split.
  - intros l ?.
    destruct H as (H & H0).
    inversion H0; subst.
    + apply Union_intror. destruct l as [|x l]; [discriminate|].
      exists x. split.
      * inversion H; assumption.
      * apply Im_intro with (x:=l); [|reflexivity].
        { split.
        - inversion H; assumption.
        - simpl in H2. inversion H2. reflexivity. }
    + left. split; assumption.
  - intros l ?.
    apply Union_inv in H. destruct H.
    + split; [|apply le_S]; apply H.
    + destruct H as (x & H1 & H2). apply Im_inv in H2.
      destruct H2 as (l' & H2 & H3). subst l.
      split.
      * constructor; [assumption|]. apply H2.
      * simpl. apply le_n_S. apply H2.
Qed.

Lemma list_n_Finite : forall {A:Type} (eq_dec : forall (u v:A), {u=v}+{u<>v})
  (X:Ensemble A) (X_Fin : Finite X) n, Finite (list_n X n).
Proof.
  intros. induction n.
  - eapply Union_is_finite.
    + apply Empty_is_finite. reflexivity.
    + apply Noone_in_empty.
    + split.
      * intros l ?. destruct H as (H & H0). inversion H0.
        apply List.length_zero_iff_nil in H2. subst l. apply Add_intro2.
      * intros l ?. apply Add_inv in H. { destruct H.
        - apply Included_Empty. apply Noone_in_empty in H. contradiction.
        - subst l. repeat constructor. }
  - rewrite list_Sn_n.
    apply (Union_preserves_Finite (List.list_eq_dec eq_dec)).
    + assumption.
    + apply (Finite_Union_preserves_Finite (List.list_eq_dec eq_dec)).
      * assumption.
      * intros. apply (finite_image (List.list_eq_dec eq_dec)). assumption.
Qed.

(* taken from not_all_not_ex *)
Lemma Finite_not_all_not_ex : forall {A:Type}
  (P : A -> Prop)
  (P_dec : forall (x:A), P x \/ ~ P x)
  (X:Ensemble A) (HFin : Finite X)
  (HIncl : forall x, P x -> In X x),
  ~ (forall x, ~ P x) -> exists x, P x.
Proof.
  intros. apply not_not.
  eapply Finite_exists_dec; eassumption.
  intros contra. apply H. intros x H0. apply contra. exists x. assumption.
Qed.

(* taken from not_all_ex_not *)
Lemma Finite_not_all_ex_not : forall {A:Type}
  (eq_dec : forall (u v : A), {u = v} + {u <> v})
  (P : A -> Prop)
  (P_dec : forall (x:A), P x \/ ~ P x)
  (X:Ensemble A) (HFin : Finite X)
  (HElse : forall x, ~ X x -> P x),
  ~ (forall x, P x) -> exists x, ~ P x.
Proof.
  intros. eapply Finite_not_all_not_ex; try eassumption.
  - intros x. apply dec_not. apply P_dec.
  - intros. destruct (Finite_In_dec eq_dec X HFin x).
    + assumption.
    + apply HElse in H1. contradiction.
  - intros contra. apply H. intros x. apply not_not.
    + apply P_dec.
    + apply contra.
Qed.

Lemma Finite_forall_dec_aux : forall {A:Type}
  (P : A -> Prop)
  (P_dec : forall (x:A), P x \/ ~ P x)
  (X:Ensemble A) (X_Fin : Finite X),
  (forall x, X x -> P x) \/ ~ (forall x, X x -> P x).
Proof.
  intros. induction X_Fin as [X|X X_Fin ? x ? X' ?].
  - left. intros. apply H in H0. apply Noone_in_empty in H0. contradiction.
  - destruct (P_dec x).
    + destruct IHX_Fin.
      * left. intros. apply H0 in H3. apply Add_inv in H3. { destruct H3.
        - apply H2; assumption.
        - subst x0; assumption. }
      * right. intros contra. apply H2. intros. apply contra.
        apply H0. apply Add_intro1. assumption.
    + right. intros contra. apply H1. apply contra. apply H0. apply Add_intro2.
Qed.

Lemma Finite_forall_dec : forall {A:Type}
  (eq_dec : forall (x y : A), {x = y} + {x <> y})
  (P : A -> Prop)
  (P_dec : forall (x:A), P x \/ ~ P x)
  (X:Ensemble A) (HFin : Finite X)
  (HElse : forall x, ~ X x -> P x),
  (forall x, P x) \/ ~ (forall x, P x).
Proof.
  intros. destruct (Finite_forall_dec_aux P P_dec X HFin).
  - left. intros. destruct (Finite_In_dec eq_dec X HFin x).
    + apply H; assumption.
    + apply HElse; assumption.
  - right.
    intros contra. apply H. intros. apply contra.
Qed.

Lemma cardinal_Sn_Subtract : forall {A:Type} (X:Ensemble A) n,
  cardinal X (S n) -> forall x, X x ->
  cardinal (Subtract X x) n.
Proof.
  intros A X n. revert X. induction n; intros.
  - inversion H; subst. inversion H2; subst. rewrite H1 in H5.
    rewrite H5 in H0. apply Add_inv in H0.
    destruct H0; [apply Noone_in_empty in H0; contradiction|subst x0].
    rewrite H5. rewrite Subtract_Add_Same_set by (apply Noone_in_empty).
    apply card_empty. reflexivity.
  - inversion H; subst. rewrite H5 in H0. apply Add_inv in H0.
    destruct H0.
    + rewrite H5. eapply card_add.
      * apply (IHn _ H2 x H0).
      * intros contra. apply H3. apply Subtract_inv in contra. apply contra.
      * apply Subtract_Add_comm. intros contra. subst x0. contradiction.
    + subst x0. rewrite H5. rewrite Subtract_Add_Same_set by assumption.
      assumption.
Qed.

Lemma Finite_cardinal_dec : forall {A:Type}
  (eq_dec : forall (u v : A), {u=v} + {u<>v})
  (X:Ensemble A) (P : A -> Prop)
  (X_Fin : Finite X) (P_dec : forall (x:A), P x \/ ~ P x)
  (HIncl : forall x, P x -> In X x) n,
  cardinal P n \/ ~ cardinal P n.
Proof.
  intros. revert dependent P. revert dependent X. induction n; intros.
  - destruct (Finite_exists_dec X P X_Fin P_dec HIncl).
    + destruct H as (x & H). right. intros contra.
      inversion contra; subst. apply H0 in H. apply Noone_in_empty in H.
      contradiction.
    + left. constructor. split; [|apply Included_Empty].
      intros x ?. contradiction H. exists x. assumption.
  - destruct (Finite_exists_dec X P X_Fin P_dec HIncl).
    + destruct H as (x & H).
      destruct IHn with (X:=Subtract X x) (P:=Subtract P x).
      * apply (Finite_Subtract eq_dec). assumption.
      * intros x'. { destruct (eq_dec x x').
        - subst x'. right. apply Subtract_not_In.
        - destruct (P_dec x').
          + left. apply Subtract_intro; assumption.
          + right. intros contra. apply Subtract_inv in contra.
            destruct contra; contradiction. }
      * intros x' H0. apply Subtract_inv in H0. destruct H0.
        apply Subtract_intro; [|assumption]. apply HIncl; assumption.
      * left. { econstructor.
        - apply H0.
        - apply Subtract_not_In.
        - rewrite (Add_Subtract_Same_set eq_dec) by assumption. reflexivity. }
      * right. intros contra. apply H0. apply cardinal_Sn_Subtract; assumption.
    + right. intros contra. apply H. inversion contra; subst. exists x.
      rewrite H4. apply Add_intro2.
Qed.

Lemma Setminus_Empty_l : forall {A:Type} (X:Ensemble A),
  Setminus Empty_set X == Empty_set.
Proof.
  intros. apply Same_set_iff. intros. unfold Setminus. split; intros.
  - apply H.
  - apply Noone_in_empty in H. contradiction.
Qed.

Lemma Setminus_Empty_r : forall {A:Type} (X:Ensemble A),
  Setminus X Empty_set == X.
Proof.
  intros. rewrite Same_set_iff. intros.
  unfold Setminus. split; intros.
  - apply H.
  - split; [apply H|]. apply Noone_in_empty.
Qed.

Lemma Setminus_Add_In_l : forall {A:Type} (X1 X2 : Ensemble A) x,
  X2 x -> Setminus (Add X1 x) X2 == Setminus X1 X2.
Proof.
  intros. apply Same_set_iff. intros y. split; intros.
  - destruct H0 as (H0 & H1). apply Add_inv in H0.
    split; [|assumption]. destruct H0; [assumption|]. subst y; contradiction.
  - split; [apply Add_intro1|]; apply H0.
Qed.

Lemma Setminus_Add_not_In_l : forall {A:Type} (X1 X2 : Ensemble A) x,
  ~ X2 x -> Setminus (Add X1 x) X2 == Add (Setminus X1 X2) x.
Proof.
  intros. apply Same_set_iff. intros y. split; intros.
  - destruct H0 as (H0 & H1). apply Add_inv in H0.
    destruct H0.
    + apply Add_intro1. split; assumption.
    + subst y. apply Add_intro2.
  - apply Add_inv in H0. destruct H0.
    + split; [apply Add_intro1|]; apply H0.
    + subst y. split.
      * apply Add_intro2.
      * assumption.
Qed.

Lemma Setminus_Add_not_In_r : forall {A:Type} (X1 X2 : Ensemble A) x,
  ~ X1 x -> Setminus X1 (Add X2 x) == Setminus X1 X2.
Proof.
  intros. apply Same_set_iff. intros y. split; intros.
  - destruct H0 as (H0 & H1). split; [assumption|]. intros contra. apply H1.
    apply Add_intro1; assumption.
  - destruct H0 as (H0 & H1). split; [assumption|]. intros contra. apply H1.
    apply Add_inv in contra. destruct contra; [|subst y]; contradiction.
Qed.

Lemma Setminus_is_Empty : forall {A:Type} (X1 X2 : Ensemble A)
  (X2_In_dec : forall x, X2 x \/ ~ X2 x),
  Setminus X1 X2 == Empty_set <-> Included X1 X2.
Proof.
  intros. split; intros.
  - intros x x_In. destruct (X2_In_dec x).
    + assumption.
    + assert (Setminus X1 X2 x) as Ha by (split; assumption).
      apply H in Ha. apply Noone_in_empty in Ha. contradiction.
  - apply Same_set_iff. split; intros.
    + destruct H0 as (H01 & H02). apply H in H01. contradiction.
    + apply Noone_in_empty in H0. contradiction.
Qed.

Lemma Setminus_is_Empty_dec : forall {A:Type} (X1 X2 : Ensemble A)
  (X1_Fin : Finite X1)
  (X2_In_dec : forall x, X2 x \/ ~ X2 x),
  Setminus X1 X2 == Empty_set \/ exists x, Setminus X1 X2 x.
Proof.
  intros. induction X1_Fin.
  - left. rewrite H. rewrite Setminus_Empty_l. reflexivity.
  - destruct (X2_In_dec x).
    + destruct IHX1_Fin.
      * left. rewrite H0. rewrite Setminus_Add_In_l by assumption. assumption.
      * destruct H2 as (x' & H2). right. exists x'. split; [|apply H2].
        rewrite H0. apply Add_intro1; apply H2.
    + right. exists x. (* bizarre *)
      fold (In (Setminus Y X2) x).
      rewrite H0.
      rewrite Setminus_Add_not_In_l by assumption. apply Add_intro2.
Qed.

Lemma Setminus_is_Add : forall {A:Type} (X1 X2 : Ensemble A)
  (X1_Fin : Finite X1)
  (X2_In_dec : forall x, X2 x \/ ~ X2 x) X x,
  Setminus X1 X2 == Add X x ->
    Included X1 (Add X2 x) \/ exists X' x', Setminus X1 (Add X2 x) == Add X' x'.
Proof.
  intros A X1 X2 X1_Fin. revert X2.
  induction X1_Fin as [X1|X1 X1_Fin ? x ? Y ?]; intros.
  - rewrite H in H0. rewrite Setminus_Empty_l in H0.
    pose proof (Add_intro2 _ X x). apply H0 in H1. apply Noone_in_empty in H1.
    contradiction.
  - rewrite H0 in H1. destruct (X2_In_dec x).
    + rewrite Setminus_Add_In_l in H1 by assumption.
      apply IHX1_Fin in H1; try assumption.
      destruct H1.
      * left. intros y y_In. rewrite H0 in y_In. apply Add_inv in y_In.
        { destruct y_In.
        - apply H1; assumption.
        - subst y. apply Add_intro1. assumption. }
      * destruct H1 as (X' & x' & H1).
        right. exists X', x'.
        rewrite H0. { rewrite Setminus_Add_In_l.
        - assumption.
        - apply Add_intro1. assumption. }
    + rewrite Setminus_Add_not_In_l in H1 by assumption.
      assert (Setminus X1 X2 == Subtract (Add X x0) x) as Ha.
      { rewrite <- H1. rewrite Subtract_Add_Same_set.
        - reflexivity.
        - intros contra. destruct contra as (contra & _). contradiction. }
      pose proof (Add_intro2 _ X x0) as Hadd.
      apply H1 in Hadd. apply Add_inv in Hadd. destruct Hadd.
      * right. eexists; eexists. rewrite H0. rewrite Setminus_Add_not_In_l.
        reflexivity. intros contra. apply Add_inv in contra.
        destruct contra. contradiction. subst x0. destruct H3 as (H3 & _).
        contradiction.
      * subst x0. { destruct (Setminus_is_Empty_dec X1 X2 X1_Fin X2_In_dec).
        - left. rewrite H0. intros x0 x0_In. apply Add_inv in x0_In.
          destruct x0_In.
          + apply Add_intro1. apply Setminus_is_Empty in H3; try assumption.
            apply H3; assumption.
          + subst x0. apply Add_intro2.
        - destruct H3 as (x0 & H3).
          right. exists (Subtract (Add X x) x), x0.
          rewrite H0. rewrite <- Ha.
          rewrite Setminus_Add_In_l by (apply Add_intro2).
          rewrite Setminus_Add_not_In_r by assumption.
          rewrite Non_disjoint_union by assumption. reflexivity. }
Qed.

Lemma Setminus_finite : forall {A:Type} (X1 X2 : Ensemble A)
  (X1_Fin : Finite X1)
  (X2_In_dec : forall x, X2 x \/ ~ X2 x), Finite (Setminus X1 X2).
Proof.
  intros. induction X1_Fin as [X1|X1 X1_Fin ? x ? Y ?].
  - rewrite H. rewrite Setminus_Empty_l. apply Empty_is_finite; reflexivity.
  - rewrite H0. destruct (X2_In_dec x).
    + rewrite Setminus_Add_In_l by assumption. assumption.
    + rewrite Setminus_Add_not_In_l by assumption.
      apply (Union_is_finite _ IHX1_Fin x).
      * intros contra. destruct contra as (contra & _). contradiction.
      * reflexivity.
Qed.

Lemma cardinal_enum : forall {A:Type} (X:Ensemble A) n,
  cardinal X n <->
  exists l, X == Ensemble_of_list l /\ List.NoDup l /\ List.length l = n.
Proof.
  split; intros.
  - induction H.
    + exists []. split.
      * rewrite Same_set_iff. intros. rewrite H. { split; intros.
        - apply Noone_in_empty in H0. contradiction.
        - contradiction. }
      * split; constructor.
    + destruct IHcardinal as (l & IHcardinal1 & IHcardinal2 & IHcardinal3).
      exists (x::l). split.
      * rewrite Same_set_iff. intros. rewrite H1. { split; intros.
        - apply Add_inv in H2. destruct H2.
          + right. rewrite IHcardinal1 in H2. assumption.
          + left; assumption.
        - destruct H2.
          + subst x0. apply Add_intro2.
          +apply Add_intro1. apply IHcardinal1. assumption. }
      * { split.
        - constructor.
          + rewrite IHcardinal1 in H0. assumption.
          + assumption.
        - simpl. apply f_equal. assumption. }
  - destruct H as (l & H1 & H2 & H3).
    revert X n H1 H3.
    induction H2; intros.
    + simpl in H3. subst n. constructor. rewrite H1. apply Ensemble_of_list_nil.
    + simpl in H3. destruct n; [discriminate|].
      assert (Subtract X x == Ensemble_of_list l) as Ha.
      { rewrite Same_set_iff. intros a. split; intros.
        - apply Subtract_inv in H0. destruct H0 as (H01 & H02).
          apply H1 in H01. destruct H01 as [H01|H01].
          + contradiction.
          + assumption.
        - apply Subtract_intro.
          + apply H1. right; assumption.
          + intros contra. subst a. contradiction.
      }
      apply IHNoDup with (n:=n) in Ha.
      * { econstructor.
        - eassumption.
        - apply Subtract_not_In.
        - split; intros a ?.
          + rewrite H1 in H0. destruct H0.
            * subst a. apply Add_intro2.
            * apply Add_intro1. { apply Subtract_intro.
              - apply H1. right; assumption.
              - intros contra. subst a. contradiction. }
          + apply Add_inv in H0. destruct H0.
            * apply Subtract_inv in H0. apply H0.
            * subst a. apply H1. left; reflexivity. }
      * inversion H3. reflexivity.
Qed.

Lemma NoDup_equivlist_same_length : forall {A}
  (l1 l2 : list A)
  (l1_NoDup : List.NoDup l1) (l2_NoDup : List.NoDup l2)
  (incl_l1_l2 : List.incl l1 l2) (incl_l2_l1 : List.incl l2 l1),
  List.length l1 = List.length l2.
Proof.
  intros.
  pose proof (List.NoDup_incl_length l1_NoDup incl_l1_l2).
  pose proof (List.NoDup_incl_length l2_NoDup incl_l2_l1).
  apply le_antisym; assumption.
Qed.

Lemma cardinal_unicity : forall {A:Type} (X:Ensemble A) n1 n2,
  cardinal X n1 -> cardinal X n2 -> n1 = n2.
Proof.
  intros.
  apply cardinal_enum in H; try assumption.
  apply cardinal_enum in H0; try assumption.
  destruct H as (l1 & H1 & H2 & H3).
  destruct H0 as (l2 & H01 & H02 & H03).
  rewrite <- H3, <- H03. apply NoDup_equivlist_same_length; try assumption.
  - intros a ?. apply H01. apply H1. assumption.
  - intros a ?. apply H1. apply H01. assumption.
Qed.

Lemma Ensemble_of_list_finite : forall {A}
  (eq_dec : forall (u v : A), {u=v}+{u<>v}) (l:list A),
  Finite (Ensemble_of_list l).
Proof.
  intros. induction l.
  - apply Empty_is_finite.
    split.
    + intros a ?. contradiction.
    + apply Included_Empty.
  - setoid_replace (Ensemble_of_list (a::l))
      with (Union (Singleton a) (Ensemble_of_list l)).
    + apply (Union_preserves_Finite eq_dec).
      * apply Singleton_finite.
      * assumption.
    + split; intros a' ?.
      * destruct H; [|apply Union_intror; assumption].
        apply Union_introl. apply Singleton_intro. assumption.
      * apply Union_inv in H. destruct H; [|right; assumption].
        apply Singleton_inv in H. left. assumption.
Qed.

Lemma Ensemble_of_list_cardinal : forall {A} (l:list A)
  (l_NoDup : List.NoDup l),
  cardinal (Ensemble_of_list l) (List.length l).
Proof.
  intros. induction l.
  - apply card_empty.
    split.
    + intros a ?. contradiction.
    + apply Included_Empty.
  - simpl. econstructor.
    + apply IHl. inversion l_NoDup; assumption.
    + inversion l_NoDup; eassumption.
    + split; intros a' ?.
      * { destruct H.
        - subst a'. apply Add_intro2.
        - apply Add_intro1; assumption. }
      * apply Add_inv in H. { destruct H.
        - right; assumption.
        - left; assumption. }
Qed.

Lemma Included_cardinal : forall {A} (X1 X2 : Ensemble A)
  (X1_X2_Incl : Included X1 X2),
  forall n1 n2 (X1_card : cardinal X1 n1) (X2_card : cardinal X2 n2),
  n1 <= n2.
Proof.
  intros. apply cardinal_enum in X1_card. apply cardinal_enum in X2_card.
  destruct X1_card as (l1 & X1_card1 & X1_card2 & X1_card3).
  destruct X2_card as (l2 & X2_card1 & X2_card2 & X2_card3).
  rewrite <- X1_card3, <- X2_card3. apply List.NoDup_incl_length.
  - assumption.
  - intros a ?. apply X2_card1. apply X1_X2_Incl. apply X1_card1. assumption.
Qed.

Lemma cardinal_Included : forall {A} (X1 X2 : Ensemble A)
  (X1_X2_Incl : Included X1 X2),
  forall n1 n2 (X1_card : cardinal X1 n1) (X2_card : cardinal X2 n2),
  n2 <= n1 -> X1 == X2.
Proof.
  intros. apply cardinal_enum in X1_card. apply cardinal_enum in X2_card.
  destruct X1_card as (l1 & X1_card1 & X1_card2 & X1_card3).
  destruct X2_card as (l2 & X2_card1 & X2_card2 & X2_card3).
  split; [assumption|]. intros a ?.
  apply X1_card1. eapply List.NoDup_length_incl.
  - eassumption.
  - rewrite X2_card3, X1_card3. assumption.
  - intros a' ?. apply X2_card1. apply X1_X2_Incl. apply X1_card1. assumption.
  - apply X2_card1. assumption.
Qed.

Lemma Strict_Included_cardinal : forall {A} (X1 X2 : Ensemble A)
  (X1_X2_Incl : Strict_Included X1 X2),
  forall n1 n2 (X1_card : cardinal X1 n1) (X2_card : cardinal X2 n2),
  n1 < n2.
Proof.
  intros.
  assert (n1 <= n2) as Ha.
  { eapply Included_cardinal; try eassumption. apply X1_X2_Incl. }
  destruct (le_lt_or_eq n1 n2 Ha); [assumption|].
  subst n2.
  assert (X1 == X2) as Ha2.
  { eapply cardinal_Included; try eassumption. apply X1_X2_Incl. }
  destruct X1_X2_Incl as (_ & (x & x_not_In & x_In)).
  apply Ha2 in x_In. contradiction.
Qed.

Lemma Included_cardinal_lt : forall {A} (X1 X2 : Ensemble A)
  (X1_X2_Incl : Included X1 X2),
  forall n1 n2 (X1_card : cardinal X1 n1) (X2_card : cardinal X2 n2),
  n1 < n2 -> Strict_Included X1 X2.
Proof.
  intros A X1 X2 X1_X2_Incl n1 n2 X1_card. revert X2 X1_X2_Incl n2.
  induction X1_card; intros.
  - destruct n2; [inversion H0|]. inversion X2_card; subst.
    split; [assumption|]. exists x. split.
    + rewrite H. apply Noone_in_empty.
    + rewrite H5. apply Add_intro2.
  - split; [assumption|].
    assert (Included (Subtract Y x) (Subtract X2 x)) as Ha.
    { intros u u_In. apply Subtract_inv in u_In. apply Subtract_intro.
      - apply X1_X2_Incl; apply u_In.
      - apply u_In.
    }
    rewrite H0 in Ha. rewrite Subtract_Add_Same_set in Ha by assumption.
    destruct n2; [inversion H1|].
    apply IHX1_card with (n2:=n2) in Ha.
    + destruct Ha as (_ & (u & Ha1 & Ha2)).
      exists u. split.
      * intros contra. apply H0 in contra. apply Add_inv in contra.
        destruct contra as [contra|contra]; [contradiction|].
        subst u. apply Subtract_not_In in Ha2. contradiction.
      * apply Subtract_inv in Ha2. apply Ha2.
    + apply cardinal_Sn_Subtract.
      * assumption.
      * apply X1_X2_Incl. rewrite H0. apply Add_intro2.
    + apply le_S_n in H1. assumption.
Qed.
