(** Library implementing potentially infinite lists using [Coinductive]. *)

Require Import List.
Require Import Sorted.
Require Export Setoid.

CoInductive infList (A:Type) : Type :=
  | nil : infList A
  | cons : A -> infList A -> infList A.

Arguments nil {_}.
Arguments cons {_} _ _.

Module Import infListNotations.

  Notation "[ ]" := nil (format "[ ]") : inf_scope.
  Notation "x :: l" := (cons x l) : inf_scope.
  Notation "[ x ]" := (cons x nil) : inf_scope.
  Notation "[ x ; y ; .. ; z ]" := (cons x (cons y .. (cons z nil) ..)) : inf_scope.

  Bind Scope inf_scope with infList.
  Delimit Scope inf_scope with inf.

End infListNotations.

Local Open Scope inf_scope.

CoFixpoint app {A} (l1 l2 :infList A) :=
  match l1 with
  | [] => l2
  | x::l => x::l ++ l2
  end
  where "l1 ++ l2" := (app l1 l2) : inf_scope.

CoInductive inf_eq {A} : infList A -> infList A -> Prop :=
| inf_eq_nil : inf_eq [] []
| inf_eq_cons : forall a l1 l2, inf_eq l1 l2 -> inf_eq (a::l1) (a::l2).

Lemma inf_eq_refl : forall {A} (l:infList A),
  inf_eq l l.
Proof.
  cofix; intros. destruct l; constructor.
  apply inf_eq_refl.
Qed.

Lemma inf_eq_sym : forall {A} (l1 l2:infList A),
  inf_eq l1 l2 -> inf_eq l2 l1.
Proof.
  cofix; intros. destruct H.
  - constructor.
  - constructor. apply inf_eq_sym; assumption.
Qed.

Lemma inf_eq_trans : forall {A} (l1 l2 l3:infList A),
  inf_eq l1 l2 -> inf_eq l2 l3 -> inf_eq l1 l3.
Proof.
  cofix; intros. destruct H.
  - assumption.
  - inversion H0. constructor. eapply inf_eq_trans; eassumption.
Qed.

Add Parametric Relation {A} : (infList A) inf_eq
  reflexivity proved by inf_eq_refl
  symmetry proved by inf_eq_sym
  transitivity proved by inf_eq_trans
as inf_eq_equiv.

Local Notation "x == y" := (inf_eq x y) (at level 70) : inf_scope.

Section inf_eqA.

Context {A} (eqA : A -> A -> Prop) {eqA_equiv : Equivalence eqA}.

CoInductive inf_eqA :
  infList A -> infList A -> Prop :=
| eqlistA_nil : inf_eqA [] []
| eqlistA_cons : forall (x x' : A) (l l' : infList A),
    eqA x x' -> inf_eqA l l' -> inf_eqA (x :: l) (x' :: l').

Lemma inf_eqA_refl : forall (l:infList A), inf_eqA l l.
Proof.
  cofix; intros. destruct l.
  - constructor.
  - constructor.
    + reflexivity.
    + apply inf_eqA_refl.
Qed.

Lemma inf_eqA_sym : forall l1 l2, inf_eqA l1 l2 -> inf_eqA l2 l1.
Proof.
   cofix; intros. destruct H.
   - constructor.
   - constructor; [symmetry; assumption|]. apply inf_eqA_sym; assumption.
Qed.

Lemma inf_eqA_trans : forall l1 l2 l3, inf_eqA l1 l2 -> inf_eqA l2 l3 ->
  inf_eqA l1 l3.
Proof.
  cofix; intros. destruct H.
  - assumption.
  - inversion H0; subst. constructor.
    + rewrite H. assumption.
    + eapply inf_eqA_trans; eassumption.
Qed.

Global Add Parametric Relation : (infList A) inf_eqA
  reflexivity proved by inf_eqA_refl
  symmetry proved by inf_eqA_sym
  transitivity proved by inf_eqA_trans
as inf_eqA_equiv.

End inf_eqA.

Add Parametric Morphism {A} : (@cons A)
  with signature eq ==> inf_eq ==> inf_eq as cons_inf_eq.
Proof.
  exact inf_eq_cons.
Qed.

Definition frob {A} (s : infList A) : infList A :=
match s with
| [] => []
| h :: t => h :: t
end.

Theorem frob_eq : forall {A} (l:infList A), l = frob l.
Proof.
  intros; destruct l; reflexivity.
Qed.

Lemma app_nil_l : forall {A} (l:infList A),
  [] ++ l = l.
Proof.
  intros. destruct l; rewrite (frob_eq (_++_)); reflexivity.
Qed.

Lemma app_nil_r : forall {A} (l:infList A),
  l ++ [] == l.
Proof.
  cofix; intros.
  destruct l. rewrite (frob_eq (_++_)). reflexivity.
  rewrite (frob_eq (_++_)). simpl. apply inf_eq_cons. apply app_nil_r.
Qed.

Lemma app_comm_cons : forall {A} (x y :infList A) (a:A),
  a :: x ++ y = (a :: x) ++ y.
Proof.
  intros. rewrite frob_eq. reflexivity.
Qed.

Lemma app_assoc : forall {A : Type} (l m n : infList A),
  l ++ m ++ n == (l ++ m) ++ n.
Proof.
  cofix; intros.
  destruct l. destruct m. destruct n. rewrite frob_eq. simpl.
  rewrite (frob_eq (_ ++ _)). reflexivity.
  rewrite frob_eq. simpl.
  rewrite (frob_eq (_ ++ _)). reflexivity.
  rewrite frob_eq. simpl.
  rewrite (frob_eq (_ ++ _)). reflexivity.
  rewrite frob_eq. simpl.
  rewrite (frob_eq (_ ++ _)). simpl. constructor. apply app_assoc.
Qed.

Add Parametric Morphism {A} : (@app A)
  with signature inf_eq ==> inf_eq ==> inf_eq as app_inf_eq.
Proof.
  cofix; intros. destruct x.
  inversion H. rewrite 2!app_nil_l. assumption.
  inversion H. rewrite <- 2!app_comm_cons. apply inf_eq_cons.
  apply app_inf_eq_Proper; assumption.
Qed.

CoInductive Forall {A : Type} (P : A -> Prop) : infList A -> Prop :=
| Forall_nil : Forall P []
| Forall_cons : forall x l, P x -> Forall P l -> Forall P (x :: l).

Add Parametric Morphism {A} : Forall
  with signature
    (Morphisms.pointwise_relation A iff) ==> inf_eq ==> iff
  as Forall_inf_eq.
Proof.
  intros P1 P2 P_equiv x1 x2 x_inf_eq.
  split; revert x1 x2 x_inf_eq; cofix; intros.
  - destruct x1. inversion x_inf_eq. constructor.
    inversion x_inf_eq. inversion H. constructor.
    + apply P_equiv. assumption.
    + eapply Forall_inf_eq_Proper; eassumption.
  - destruct x2. inversion x_inf_eq. constructor.
    inversion x_inf_eq. inversion H. constructor.
    + apply P_equiv. assumption.
    + eapply Forall_inf_eq_Proper; eassumption.
Qed.

Inductive In {A} (x:A) : infList A -> Prop :=
| In_here : forall l, In x (x::l)
| In_after : forall a l, In x l -> In x (a::l).

Add Parametric Morphism {A} : In
  with signature eq ==> inf_eq (A:=A) ==> iff as In_inf_eq.
Proof.
  intros a l1 l2 H. split; intros.
  - revert dependent l2. induction H0 as [l1 | b l1]; intros.
    inversion H. apply In_here.
    inversion H. apply In_after. eapply IHIn; eassumption.
  - revert dependent l1. induction H0 as [l2 | b l2]; intros.
    inversion H. apply In_here.
    inversion H. apply In_after. eapply IHIn; eassumption.
Qed.

Lemma in_app : forall {A : Type} (l l' : infList A) (a : A),
  In a (l ++ l') -> In a l \/ In a l'.
Proof.
  intros.
  - remember (l++l') as L. revert l l' HeqL.
    induction H as [L|b L]; intros.
    + destruct l. destruct l'.
      rewrite frob_eq in HeqL. discriminate.
      rewrite frob_eq in HeqL. inversion HeqL. right.
      apply In_here.
      rewrite frob_eq in HeqL. inversion HeqL. left. apply In_here.
    + destruct l. destruct l'. rewrite frob_eq in HeqL. discriminate.
      rewrite frob_eq in HeqL. inversion HeqL.
      destruct (IHIn [] l').
      rewrite H2. rewrite frob_eq. destruct l'; reflexivity.
      inversion H0. right. apply In_after; assumption.
      rewrite frob_eq in HeqL. inversion HeqL. apply IHIn in H2.
      destruct H2. left. apply In_after; assumption. right; assumption.
Qed.

Lemma in_app_l : forall {A : Type} (l l' : infList A) (a : A),
  In a l -> In a (l ++ l').
Proof.
  intros.
  induction H. rewrite frob_eq. apply In_here.
  rewrite frob_eq. apply In_after. assumption.
Qed.

Lemma Forall_forall : forall {A} (P : A -> Prop) (l : infList A),
  Forall P l <-> (forall x : A, In x l -> P x).
Proof.
  split; intros.
  - induction H0.
    + inversion H; assumption.
    + apply IHIn. inversion H; assumption.
  - revert dependent l. cofix. intros.
    destruct l. constructor. constructor. apply H. apply In_here.
    apply Forall_forall. intros. apply H. apply In_after. assumption.
Qed.

Inductive HdRel {A : Type} (R : A -> A -> Prop) (a : A) : infList A -> Prop :=
| HdRel_nil : HdRel R a []
| HdRel_cons : forall b l, R a b -> HdRel R a (b :: l).

Add Parametric Morphism {A} : HdRel
  with signature
    ((eq ==> eq ==> iff) ==>
      eq ==> inf_eq (A:=A) ==> iff)
  as HdRel_inf_eq.
Proof.
  intros P1 P2 P_equiv a l1 l2 l_inf_eq. split; intros.
  - inversion l_inf_eq; subst.
    + constructor.
    + inversion H. constructor. eapply P_equiv; try reflexivity. assumption.
  - inversion l_inf_eq; subst.
    + constructor.
    + inversion H. constructor. eapply P_equiv; try reflexivity. assumption.
Qed.

CoInductive Sorted {A : Type} (R : A -> A -> Prop) : infList A -> Prop :=
| Sorted_nil : Sorted R []
| Sorted_cons : forall a l, Sorted R l -> HdRel R a l -> Sorted R (a :: l).

Add Parametric Morphism {A} : Sorted
  with signature
    (eq ==> eq ==> iff) ==>
      inf_eq (A:=A) ==> iff
  as Sorted_inf_eq.
Proof.
  intros P1 P2 P_equiv l1 l2 l_inf_eq.
  split; revert l1 l2 l_inf_eq; cofix; intros.
  - inversion l_inf_eq; subst.
    + constructor.
    + inversion H. constructor.
      * eapply Sorted_inf_eq_Proper; eassumption.
      * rewrite <- P_equiv, <- H0. assumption.
  - inversion l_inf_eq; subst.
    + constructor.
    + inversion H. constructor.
      * eapply Sorted_inf_eq_Proper; eassumption.
      * rewrite P_equiv, H0. assumption.
Qed.

Section inf_eqA_properties.

Context {A} (eqA : A -> A -> Prop) {eqA_equiv : Equivalence eqA}.

Global Add Parametric Morphism : Forall
  with signature (eqA ==> iff) ==> inf_eqA eqA ==> iff
  as Forall_inf_eqA.
Proof.
  intros P1 P2 P_equiv l1 l2 l_inf_eqA.
  split; revert l1 l2 l_inf_eqA; cofix; intros.
  - inversion l_inf_eqA; subst.
    + constructor.
    + inversion H. constructor.
      * eapply P_equiv; eassumption.
      * eapply Forall_inf_eqA_Proper; eassumption.
  - inversion l_inf_eqA; subst.
    + constructor.
    + inversion H. constructor.
      * eapply P_equiv; eassumption.
      * eapply Forall_inf_eqA_Proper; eassumption.
Qed.

Global Add Parametric Morphism : HdRel
  with signature (eqA ==> eqA ==> iff) ==> eqA ==> inf_eqA eqA ==> iff
  as HdRel_inf_eqA.
Proof.
  intros P1 P2 P_equiv x1 x2 x_eqA l1 l2 l_inf_eq.
  unfold Morphisms.respectful in P_equiv.
  inversion l_inf_eq.
  - split; intros; constructor.
  - split; intros; inversion H3; subst; constructor;
      eapply P_equiv; eassumption.
Qed.

Global Add Parametric Morphism : Sorted
  with signature (eqA ==> eqA ==> iff) ==> inf_eqA eqA ==> iff
  as Sorted_inf_eqA.
Proof.
  intros P1 P2 P_equiv l1 l2 l_inf_eqA.
  split; revert l1 l2 l_inf_eqA; cofix; intros.
  - inversion l_inf_eqA; subst.
    + constructor.
    + inversion H; subst. constructor.
      * eapply Sorted_inf_eqA_Proper; eassumption.
      * rewrite <- HdRel_inf_eqA; eassumption.
  - inversion l_inf_eqA; subst.
    + constructor.
    + inversion H; subst. constructor.
      * eapply Sorted_inf_eqA_Proper; eassumption.
      * rewrite HdRel_inf_eqA; eassumption.
Qed.

End inf_eqA_properties.

CoFixpoint map {A B} (f:A->B) (l:infList A) :=
  match l with
  | [] => []
  | a::l => f a :: map f l
  end.

Add Parametric Morphism {A B} : (@map A B)
  with signature eq ==> inf_eq ==> inf_eq as map_inf_eq.
Proof.
  cofix; intros f l1 l2 H.
  destruct H.
  reflexivity.
  rewrite frob_eq. simpl. rewrite (frob_eq (map _ _)). simpl.
  apply inf_eq_cons. apply map_inf_eq_Proper. assumption.
Qed.

Lemma map_nil : forall {A B} (f:A->B),
  map f [] = [].
Proof.
  intros.
  rewrite (frob_eq (map _ _)). reflexivity.
Qed.

Lemma map_cons : forall {A B} (f:A->B) (a:A) (l:infList A),
  map f (a::l) = f a :: map f l.
Proof.
  intros. rewrite (frob_eq (map _ _)). reflexivity.
Qed.

Lemma map_app : forall {A B} (f:A->B) (l1 l2:infList A),
  map f (l1++l2) == map f l1 ++ map f l2.
Proof.
  cofix; intros.
  destruct l1. rewrite frob_eq. rewrite (frob_eq (map _ _)).
  destruct l2; reflexivity.
  rewrite frob_eq. rewrite (frob_eq (map _ _)). simpl.
  apply inf_eq_cons. apply map_app.
Qed.

Import ListNotations.

Fixpoint of_list {A} (l:list A) :=
  match l with
  | []%list => []
  | (a::l)%list => a :: of_list l
  end.

Coercion of_list : list >-> infList.

Lemma of_list_cons : forall {A} (l:list A) a,
  of_list (a::l) = (a :: of_list l)%inf.
Proof.
  intros. reflexivity.
Qed.

Lemma of_list_app : forall {A} (l1 l2:list A),
  of_list (l1++l2) = of_list l1 ++ of_list l2.
Proof.
  intros A l1. induction l1; intros.
  - simpl. rewrite app_nil_l. reflexivity.
  - simpl. rewrite <- app_comm_cons. rewrite IHl1. reflexivity.
Qed.

(* Could we write a more general lemma ? some morphism ? *)
Lemma map_finite : forall {A B} (f:A->B) (l:list A),
  map f l = List.map f l.
Proof.
  intros. induction l.
  simpl. rewrite (frob_eq (map _ _)). reflexivity.
  rewrite (frob_eq (map _ _)). simpl. rewrite IHl. reflexivity.
Qed.

Lemma In_finite : forall {A} (l:list A) a,
  In a l <-> List.In a l.
Proof.
  split; intros.
  - induction l. inversion H.
    simpl. inversion H.
    + left. reflexivity.
    + right. apply IHl; assumption.
  - induction l. inversion H.
    destruct H.
    + rewrite H. apply In_here.
    + simpl. apply In_after. apply IHl; assumption.
Qed.

Lemma Forall_finite : forall {A} (P : A -> Prop) (l:list A),
  Forall P l <-> List.Forall P l.
Proof.
  split; intros.
  - induction l.
    + constructor.
    + simpl in H. inversion H; subst. constructor.
      * assumption.
      * apply IHl. assumption.
  - induction H.
    + constructor.
    + simpl. constructor; assumption.
Qed.

Lemma HdRel_finite : forall {A} (P:A->A->Prop) a (l:list A),
  HdRel P a l <-> Sorted.HdRel P a l.
Proof.
  split; intros.
  - destruct l.
    + constructor.
    + simpl in H. inversion H; subst. constructor; assumption.
  - inversion H; subst.
    + constructor.
    + constructor; assumption.
Qed.

Lemma Sorted_finite : forall {A} (P:A->A->Prop) (l:list A),
  Sorted P l <-> Sorted.Sorted P l.
Proof.
  split; intros.
  - induction l.
    + constructor.
    + inversion H; subst. constructor.
      * apply IHl; assumption.
      * apply HdRel_finite. assumption.
  - induction H.
    + constructor.
    + simpl. constructor.
      * assumption.
      * apply HdRel_finite. assumption.
Qed.

Lemma in_app_finite_r : forall {A} (l : list A) (l': infList A) (a:A),
  In a l' -> In a (l++l').
Proof.
  intros. induction l.
  + simpl. rewrite app_nil_l. assumption.
  + simpl. rewrite <- app_comm_cons. apply In_after. assumption.
Qed.

Hint Rewrite <- @map_finite @app_comm_cons @app_assoc : inf. 
Hint Rewrite @app_nil_l @app_nil_r @map_nil @map_cons @map_app @of_list_app : inf.

Hint Rewrite <- @of_list_cons @of_list_app : inf_finite.

Tactic Notation "simpl_inf" :=
  simpl of_list; autorewrite with inf using (simpl of_list);
  simpl of_list.
Tactic Notation "simpl_inf" "in" ident(H) :=
  simpl of_list in H; autorewrite with inf in H using (simpl of_list in H);
  simpl of_list in H.
Tactic Notation "regroup_finite" := autorewrite with inf_finite.
Tactic Notation "regroup_finite" "in" ident(H) :=
  autorewrite with inf_finite in H.
