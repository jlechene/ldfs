# Formalization of control dependence in the Coq proof assistant

## Compilation

The compilation has been tested with Coq 8.6.1 and
OCaml 4.05.0. The recommended installation process is with
[opam](https://opam.ocaml.org/).
Coq is also available at https://coq.inria.fr/.

You first need to compile and install the graph library in subdirectory
`concrete_graphs/`.
You can use:
```
cd concrete_graphs
make
make install
make html # optional, to generate the documentation
```
You can then compile the main development:
```
cd ..
make
make html # optional, to generate the documentation
```

Some extraction in OCaml is possible, using:
```
make extraction
```

## Testing

You can then play interactively with the extraction using `ocaml` or `utop`,
running `#use top.ml;;` as a first command.
For example,
```
test g_example [1;3]
```
computes the weak control-closure of `{1,3}` in `g_example`.
The result is `[0;1;2;3;4;6]` as expected.

## Files

There are two important files.
- `graph3.v` defines graph, control dependence on graphs,
  and some of its properties;
- `algo.v` gives the algorithm computing control dependence closures, and
  proves its correctness.

Some files are useful libraries.
- `MyFinite.v`, `Finite_facts.v` are handy results on finite sets;
- `last.v`, `last_f.v`, `listOtherResults.v` contain useful results on standard lists;
- `infList.v` defines infinite lists and their properties.

Other files are connected to the OCaml extraction.
- `cfg_test.v` gives concrete examples;
- `cfg_extraction.v` is the extraction itself;
- `top.ml` is a helper file to play with the extraction.
