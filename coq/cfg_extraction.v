(** Extraction file. Compile this file to extract most of the Coq development
    into OCaml.
*)

Require Import cfg_test.

Require Import ExtrOcamlBasic.
Require Import ExtrOcamlString.

Extract Inductive nat => "int" ["0" "(fun x -> x + 1)"]
  "(fun zero succ n -> match n with | 0 -> zero () | n -> succ (n-1))".

Extraction Blacklist List Nat set String.

(*
Extract Inductive bool => "bool" ["true" "false"].

Extract Inductive sumbool => "bool" ["true" "false"].

Extract Inductive list => "list" ["[]" "::"] "(fun n c l -> match l with | [] -> c() | a::l' -> c a l)".
*)

Extract Inductive String.string => "string" ["""""" " (fun (c, s) -> (String.make 1 c) ^ s)"]
  "(fun e c s -> match s with | """" -> e () | s1 -> c (s.[0]) (String.sub s 1 (String.length s - 1)))".

Extract Constant String.append => "(^)".
Recursive Extraction Library cfg_test.