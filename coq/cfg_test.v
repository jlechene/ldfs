(** Complete instantiation of [preCFG] in [algo], and example tests. *)

Require Import algo.
Require Import infList. Import infList.infListNotations.
Require List. Import List.ListNotations.
Require Import MyFinite.
Require Import Equalities.
Require Import Bool.
Require Import RelationClasses.

Require Import ConcreteGraph.DigraphMapImp.

Module V := OrderedTypeEx.Nat_as_OT.

Module Lab.
  Definition VertexLabel := nat.
  Definition EdgeLabel := nat.
End Lab.

Module C <: graph3.COMBINE_TYPE.
  Definition Label := option nat.
  Definition In (_ _ :Label) := True.
  Definition In_refl (_:Label) := I.
  Definition combine (x y:Label) := x.
  Definition combine_In (l1 l2 l : Label) :=
    conj (fun (_:True) => or_introl True I) (fun (_:True\/True) => I).
End C.

Module Import DG <: Constructive_DigraphInterface.Constructive_Directed_Graph V Lab
  := Directed_GraphMap V Lab.

Module Import Test := preCFG V Lab C DG.

Definition g :=
  let g := add_vertex 0 empty_graph None in
  let g := add_vertex 1 g None in
  let g := add_vertex 2 g None in
  let g := add_vertex 3 g None in
  let g := add_edge (0,1) g None in
  let g := add_edge (0,2) g None in
  let g := add_edge (1,3) g None in
  let g := add_edge (2,3) g None in
  let g := add_edge (3,0) g None in
  g.

Fixpoint seq n := match n with
  | 0 => []
  | S n' => seq n' ++ [n']
  end%list.

Definition g_example :=
  let g := add_vertex 0 empty_graph None in
  let g := add_vertex 1 g None in
  let g := add_vertex 2 g None in
  let g := add_vertex 3 g None in
  let g := add_vertex 4 g None in
  let g := add_vertex 5 g None in
  let g := add_vertex 6 g None in
  let g := add_edge (0, 1) g None in
  let g := add_edge (0, 2) g None in
  let g := add_edge (1, 3) g None in
  let g := add_edge (1, 4) g None in
  let g := add_edge (2, 3) g None in
  let g := add_edge (2, 4) g None in
  let g := add_edge (4, 3) g None in
  let g := add_edge (4, 5) g None in
  let g := add_edge (5, 6) g None in
  let g := add_edge (6, 0) g None in
  let g := add_edge (6, 4) g None in
  let g := add_edge (6, 5) g None in
  g.

Definition g_while_rand_1 :=
  let g := add_vertex 0 empty_graph None in
  let g := add_vertex 1 empty_graph None in
  let g := add_vertex 2 empty_graph None in
  let g := add_vertex 3 empty_graph None in
  let g := add_edge (1, 0) g None in
  let g := add_edge (2, 1) g None in
  let g := add_edge (3, 2) g None in
  let g := add_edge (3, 1) g None in
  g.

Definition test g l :=
  let s := List.fold_left (fun acc x => VertexSet.add x acc) l VertexSet.empty in
  VertexSet.elements (real_algo61 g s).
