(** Some results about standard lists involving [Exists], [find]... *)

Require List. Import List.ListNotations.
Local Open Scope list_scope.
Require last.
Require Import Sorted.
Require Import MyFinite.
Require Recdef.

Lemma list_nil_dec : forall {A} (l:list A),
  { l = [] } + { l <> [] }.
Proof.
  intros. destruct l; [left; reflexivity|right; discriminate].
Qed.

Lemma find_first : forall {A} (f:A->bool) (l:list A) a,
    List.find f l = Some a ->
    exists l1 l2, l = l1++a::l2 /\ List.Forall (fun x => f x = false) l1.
  Proof.
    intros A f l. induction l; intros.
    - discriminate.
    - simpl in H. destruct (f a) eqn:Heqgf.
      + inversion H. exists [], l. split; constructor.
      + apply IHl in H. destruct H as [l1 [l2 [H H0]]].
        exists (a::l1), l2. split.
        simpl. rewrite <- H. reflexivity.
        constructor; assumption.
  Qed.

  Lemma find_Exists : forall {A} (f:A->bool) (l:list A),
    List.find f l <> None <->
    List.Exists (fun x => f x = true) l.
  Proof.
    intros A f l. induction l; intros.
    - split; intros.
      + contradiction H. reflexivity.
      + inversion H.
    - split; intros.
      + simpl in H. destruct (f a) eqn:Heqf.
        * constructor; assumption.
        * apply List.Exists_cons_tl. apply IHl; assumption.
      + inversion H.
        * simpl. rewrite H1. discriminate.
        * simpl. destruct (f a); [discriminate|]. apply IHl; assumption.
  Qed.

  Definition find_last {A} (f:A->bool) (l:list A) := List.find f (List.rev l).

  Lemma find_last_some : forall {A} (f:A->bool) (l:list A) a,
    find_last f l = Some a -> List.In a l /\ f a = true.
  Proof.
    intros. unfold find_last in H. apply List.find_some in H.
    rewrite <- List.in_rev in H. assumption.
  Qed.

  Lemma find_last_last : forall {A} (f:A->bool) (l:list A) a,
    find_last f l = Some a ->
    exists l1 l2, l = l1 ++ a::l2 /\ List.Forall (fun x => f x = false) l2.
  Proof.
    intros. unfold find_last in H. apply find_first in H.
    destruct H as [l2 [l1 [H H0]]].
    exists (List.rev l1), (List.rev l2).
    split.
    - apply (f_equal (List.rev (A:=A))) in H.
      rewrite List.rev_involutive in H. rewrite List.rev_app_distr in H.
      simpl in H. rewrite <- List.app_assoc in H. assumption.
    - apply List.Forall_forall. intros. rewrite List.Forall_forall in H0.
      apply H0. rewrite <- List.in_rev in H1. assumption.
  Qed.

Section remove_cycles_NoDup.

  Context {A} (eq_dec : forall (a b : A), {a=b}+{a<>b}).

  Fixpoint find_last_prefix (a:A) (l:list A) :=
    match l with
    | [] => []
    | a'::l' => if List.in_dec eq_dec a l' then find_last_prefix a l'
                else if eq_dec a a' then (a'::l') else []
  end.

  Lemma find_last_prefix_decrease : forall a l,
    length (find_last_prefix a l) <= length l.
  Proof.
    intros. induction l.
    apply le_n.
    simpl. destruct (List.in_dec eq_dec a l).
    apply le_S. assumption.
    destruct (eq_dec a a0).
    apply le_n.
    apply le_0_n.
  Qed.

  Function remove_cycles (l:list A) {measure length} :=
    match l with
    | [] => []
    | a'::l' => if List.in_dec eq_dec a' l' then remove_cycles (find_last_prefix a' l')
                else a'::remove_cycles l'
    end.
  Proof.
    intros. simpl. apply le_n_S. apply find_last_prefix_decrease.
    intros. simpl. apply le_n.
  Qed.

  Lemma find_last_prefix_incl : forall a l,
    List.incl (find_last_prefix a l) l.
  Proof.
    intros. induction l.
    - apply List.incl_refl.
    - simpl. destruct (List.in_dec eq_dec a l).
      + apply List.incl_tl. assumption.
      + destruct (eq_dec a a0).
        * apply List.incl_refl.
        * intros b Hin. inversion Hin.
  Qed.

  Lemma remove_cycles_incl : forall l,
    List.incl (remove_cycles l) l.
  Proof. intros.
    functional induction (remove_cycles l).
    - apply List.incl_refl.
    - apply List.incl_tran with (m:=find_last_prefix a' l').
      assumption. apply List.incl_tl. apply find_last_prefix_incl.
    - apply List.incl_cons. left; reflexivity. apply List.incl_tl. assumption.
  Qed.

  Lemma remove_cycles_NoDup : forall l,
    List.NoDup (remove_cycles l).
  Proof.
    intros. functional induction (remove_cycles l).
    - constructor.
    - assumption.
    - constructor; [|apply IHl0]. intros contra. apply _x.
      apply remove_cycles_incl. assumption.
  Qed.

  Lemma find_last_prefix_decompose : forall a l,
    List.In a l ->
    exists l1, l = l1 ++ find_last_prefix a l.
  Proof.
    intros. induction l.
    - inversion H.
    - simpl. destruct (List.in_dec eq_dec a l).
      + apply IHl in i. destruct i as [l1 i]. exists (a0::l1). rewrite i at 1.
        reflexivity.
      + destruct (eq_dec a a0).
        * exists []. reflexivity.
        * destruct H; [|contradiction]. contradiction n0. subst a0. reflexivity.
  Qed.

  Lemma find_last_prefix_nil : forall a l,
    find_last_prefix a l = [] <-> ~ List.In a l.
  Proof.
    split; intros.
    - induction l.
      + intros contra; inversion contra.
      + intros contra. simpl in H.
        destruct (List.in_dec eq_dec a l).
        * apply IHl in H. contradiction.
        * destruct (eq_dec a a0). discriminate.
          destruct contra; [subst a0|contradiction].
          contradiction n0; reflexivity.
    - induction l.
      + reflexivity.
      + simpl. destruct (List.in_dec eq_dec a l).
        * contradiction H. right; assumption.
        * destruct (eq_dec a a0). contradiction H. left; subst a0; reflexivity.
          reflexivity.
  Qed.

  Lemma remove_cycles_nil : forall l,
    remove_cycles l = [] <-> l = [].
  Proof.
    split; intros.
    - functional induction (remove_cycles l).
      + reflexivity.
      + apply IHl0 in H. apply find_last_prefix_nil in H. contradiction.
      + inversion H.
    - subst; rewrite remove_cycles_equation; reflexivity.
  Qed.

  Lemma find_last_prefix_hd : forall a l a',
    List.In a l -> List.hd a' (find_last_prefix a l) = a.
  Proof.
    intros. induction l.
    - inversion H.
    - simpl. destruct (List.in_dec eq_dec a l).
      + apply IHl. assumption.
      + destruct (eq_dec a a0).
        * subst a0. reflexivity.
        * destruct H; [|contradiction]. contradiction n0. subst a0.
          reflexivity.
  Qed.

  Lemma remove_cycles_keeps_hd : forall a l,
    List.hd a (remove_cycles l) = List.hd a l.
  Proof.
    intros. functional induction (remove_cycles l).
    - reflexivity.
    - rewrite IHl0. apply find_last_prefix_hd; assumption.
    - reflexivity.
  Qed.

  Lemma find_last_prefix_last : forall a l a',
    List.In a l -> List.last (find_last_prefix a l) a' = List.last l a'.
  Proof.
    intros. induction l.
    - inversion H.
    - simpl find_last_prefix. destruct (List.in_dec eq_dec a l).
      + rewrite IHl by assumption. destruct l; [inversion i|reflexivity].
      + destruct (eq_dec a a0).
        * reflexivity.
        * destruct H; [|contradiction]. contradiction n0. subst a0. reflexivity.
  Qed.

  Lemma remove_cycles_keeps_last : forall a l,
    List.last (remove_cycles l) a = List.last l a.
  Proof.
    intros. functional induction (remove_cycles l).
    - reflexivity.
    - rewrite IHl0. rewrite last.last_cons_not_nil.
      apply find_last_prefix_last; assumption.
      intros contra. subst l'. inversion _x.
    - destruct (List.list_eq_dec eq_dec l' []).
      + rewrite e. rewrite remove_cycles_equation. reflexivity.
      + rewrite 2!last.last_cons_not_nil. assumption. assumption.
        intros contra. rewrite remove_cycles_nil in contra. contradiction.
  Qed.

  Lemma find_last_prefix_stays_sorted : forall a (l:list A) P,
    Sorted P l -> Sorted P (find_last_prefix a l).
  Proof.
    intros. induction l.
    - constructor.
    - simpl. destruct (List.in_dec eq_dec a l).
      + apply IHl. inversion H; assumption.
      + destruct (eq_dec a a0).
        * assumption.
        * constructor.
  Qed.

  Lemma remove_cycles_stays_sorted : forall (l:list A) P,
    Sorted P l -> Sorted P (remove_cycles l).
  Proof.
    intros. functional induction (remove_cycles l).
    - constructor.
    - apply IHl0. apply find_last_prefix_stays_sorted.
      inversion H; assumption.
    - constructor.
      + apply IHl0.
        inversion H; assumption.
      + destruct l'.
        * rewrite remove_cycles_equation. constructor.
        * destruct (remove_cycles (a::l')) eqn:Heqremove.
          constructor. pose proof (remove_cycles_keeps_hd a (a::l')).
          rewrite Heqremove in H0. simpl in H0. subst a0.
          constructor. inversion H. inversion H3; assumption.
  Qed.

End remove_cycles_NoDup.

Definition Ensemble_of_list {A} (l:list A) : Ensemble A :=
  fun a => List.In a l.

Definition common {A} (eq_dec : forall (a b:A), {a=b} + {a<>b})
  (l1 l2 : list A) :=
  List.filter (fun x => if List.In_dec eq_dec x l2 then true else false) l1.

Lemma common_spec : forall {A} (eq_dec : forall (a b:A), {a=b} + {a<>b})
  (l1 l2 : list A) a,
  List.In a (common eq_dec l1 l2)
    <-> Intersection (Ensemble_of_list l1) (Ensemble_of_list l2) a.
Proof.
  split; intros.
  - unfold common in H.
    apply List.filter_In in H.
    destruct H as [H H0].
    destruct (List.in_dec eq_dec a l2).
    + apply Intersection_intro; assumption.
    + discriminate.
  - unfold common. apply List.filter_In. apply Intersection_inv in H.
    destruct H as [H H0].
    split.
    + assumption.
    + destruct (List.in_dec eq_dec a l2).
      * reflexivity.
      * contradiction.
Qed.

Definition first_common {A} (eq_dec : forall (a b:A), {a=b} + {a<>b})
  (l1 l2 : list A) :=
  List.find
    (fun x =>
      if List.In_dec eq_dec x (common eq_dec l1 l2) then true else false)
    l1.

Definition last_common {A} (eq_dec : forall (a b:A), {a=b} + {a<>b})
  (l1 l2 : list A) :=
  find_last
    (fun x =>
      if List.In_dec eq_dec x (common eq_dec l1 l2) then true else false)
    l1.

Lemma first_common_some : forall {A} (eq_dec : forall (a b:A), {a=b} + {a<>b})
  (l1 l2 : list A) a,
  first_common eq_dec l1 l2 = Some a ->
  exists l11 l12 l21 l22,
  l1 = l11 ++ a::l12 /\ l2 = l21 ++ a::l22 /\
  Disjoint (Ensemble_of_list l11) (Ensemble_of_list l21).
Proof.
  intros. unfold first_common in H. pose proof H as H0.
  apply List.find_some in H. destruct H as [_ H].
  destruct (List.in_dec eq_dec a (common eq_dec l1 l2)); [|discriminate].
  apply find_first in H0.
  destruct H0 as [l11 [l12 [H0 H1]]].
  exists l11, l12.
  assert (List.find (fun x => if eq_dec x a then true else false) l2 <> None) as Ha.
  { apply find_Exists. apply List.Exists_exists. exists a.
    split; [|destruct (eq_dec a a); [reflexivity|contradiction n; reflexivity]].
    apply common_spec in i. apply Intersection_inv in i. apply i.
  }
  destruct (List.find _ l2) eqn:Heql2; [|contradiction Ha; reflexivity].
  pose proof Heql2 as Heql21. apply List.find_some in Heql2.
  destruct Heql2 as [_ Heql2]. destruct (eq_dec a0 a); [subst a0|discriminate].
  apply find_first in Heql21.
  destruct Heql21 as [l21 [l22 [Heql21 Heql22]]].
  exists l21, l22.
  split; [assumption|]. split; [assumption|].
  constructor. intros x contra.
  apply Intersection_inv in contra.
  unfold Ensemble_of_list, Ensembles.In in contra.
  destruct contra as [contra1 contra2].
  pose proof contra1 as contra11.
  rewrite List.Forall_forall in H1. apply H1 in contra11.
  destruct (List.in_dec eq_dec x (common eq_dec l1 l2)); [discriminate|].
  apply n. apply common_spec.
  apply Intersection_intro; unfold Ensemble_of_list, Ensembles.In.
  + rewrite H0. rewrite List.in_app_iff.
    left; assumption.
  + rewrite Heql21. rewrite List.in_app_iff.
    left; assumption.
Qed.

Lemma find_impl : forall {A} (f1 f2 : A -> bool) (l:list A),
  (forall a, f1 a = f2 a) ->
  List.find f1 l = List.find f2 l.
Proof.
  intros. induction l.
  - reflexivity.
  - simpl. rewrite H. rewrite IHl. reflexivity.
Qed.

Lemma last_common_some : forall {A} (eq_dec : forall (a b:A), {a=b} + {a<>b})
  (l1 l2 : list A) a,
  last_common eq_dec l1 l2 = Some a ->
  exists l11 l12 l21 l22,
  l1 = l11 ++ a::l12 /\ l2 = l21 ++ a::l22 /\
  Disjoint (Ensemble_of_list l12) (Ensemble_of_list l22).
Proof.
  intros.
  unfold last_common in H. unfold find_last in H.
  rewrite find_impl
    with (f2:=fun x =>
      if List.in_dec eq_dec x (common eq_dec (List.rev l1) (List.rev l2))
      then true else false)
    in H.
  apply first_common_some in H.
  destruct H as [l12 [l11 [l22 [l21 [H [H0 H1]]]]]].
  exists (List.rev l11), (List.rev l12), (List.rev l21), (List.rev l22).
  apply (f_equal (List.rev (A:=A))) in H.
  rewrite List.rev_involutive in H. rewrite List.rev_app_distr in H.
  simpl in H. rewrite <- List.app_assoc in H.
  apply (f_equal (List.rev (A:=A))) in H0.
  rewrite List.rev_involutive in H0. rewrite List.rev_app_distr in H0.
  simpl in H0. rewrite <- List.app_assoc in H0.
  split; [assumption|]. split; [assumption|].
  constructor. intros x contra. apply Intersection_inv in contra.
  destruct contra as [contra1 contra2].
  unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
  rewrite <- List.in_rev in contra1, contra2.
  inversion H1. apply (H2 x). apply Intersection_intro; assumption.
  intros x.
  destruct (List.in_dec eq_dec x (common eq_dec l1 l2)),
    (List.in_dec eq_dec x (common eq_dec (List.rev l1) (List.rev l2)));
    try reflexivity;
    rewrite common_spec in i, n; contradiction n; apply Intersection_inv in i;
    apply Intersection_intro; unfold Ensembles.In, Ensemble_of_list in *;
    rewrite <- !List.in_rev in *; apply i.
Qed.

Lemma first_common_some_strong :
  forall {A} (eq_dec : forall (a b:A), {a=b} + {a<>b})
  (l1 l2 : list A) a,
  first_common eq_dec l1 l2 = Some a ->
  exists l11 l12 l21 l22,
  l1 = l11 ++ a::l12 /\ l2 = l21 ++ a::l22 /\
  Disjoint (Ensemble_of_list l11) (Ensemble_of_list l2).
Proof.
  intros. unfold first_common in H. pose proof H as H0.
  apply List.find_some in H. destruct H as [_ H].
  destruct (List.in_dec eq_dec a (common eq_dec l1 l2)); [|discriminate].
  apply find_first in H0.
  destruct H0 as [l11 [l12 [H0 H1]]].
  exists l11, l12.
  assert (List.find (fun x => if eq_dec x a then true else false) l2 <> None) as Ha.
  { apply find_Exists. apply List.Exists_exists. exists a.
    split; [|destruct (eq_dec a a); [reflexivity|contradiction n; reflexivity]].
    apply common_spec in i. apply Intersection_inv in i. apply i.
  }
  destruct (List.find _ l2) eqn:Heql2; [|contradiction Ha; reflexivity].
  pose proof Heql2 as Heql21. apply List.find_some in Heql2.
  destruct Heql2 as [_ Heql2]. destruct (eq_dec a0 a); [subst a0|discriminate].
  apply find_first in Heql21.
  destruct Heql21 as [l21 [l22 [Heql21 Heql22]]].
  exists l21, l22.
  split; [assumption|]. split; [assumption|].
  constructor. intros x contra.
  apply Intersection_inv in contra.
  unfold Ensemble_of_list, Ensembles.In in contra.
  destruct contra as [contra1 contra2].
  pose proof contra1 as contra11.
  rewrite List.Forall_forall in H1. apply H1 in contra11.
  destruct (List.in_dec eq_dec x (common eq_dec l1 l2)); [discriminate|].
  apply n. apply common_spec.
  apply Intersection_intro; unfold Ensemble_of_list, Ensembles.In.
  + rewrite H0. rewrite List.in_app_iff.
    left; assumption.
  + assumption.
Qed.

Lemma last_common_some_strong : forall {A} (eq_dec : forall (a b:A), {a=b} + {a<>b})
  (l1 l2 : list A) a,
  last_common eq_dec l1 l2 = Some a ->
  exists l11 l12 l21 l22,
  l1 = l11 ++ a::l12 /\ l2 = l21 ++ a::l22 /\
  Disjoint (Ensemble_of_list l12) (Ensemble_of_list l2).
Proof.
  intros.
  unfold last_common in H. unfold find_last in H.
  rewrite find_impl
    with (f2:=fun x =>
      if List.in_dec eq_dec x (common eq_dec (List.rev l1) (List.rev l2))
      then true else false)
    in H.
  apply first_common_some_strong in H.
  destruct H as [l12 [l11 [l22 [l21 [H [H0 H1]]]]]].
  exists (List.rev l11), (List.rev l12), (List.rev l21), (List.rev l22).
  apply (f_equal (List.rev (A:=A))) in H.
  rewrite List.rev_involutive in H. rewrite List.rev_app_distr in H.
  simpl in H. rewrite <- List.app_assoc in H.
  apply (f_equal (List.rev (A:=A))) in H0.
  rewrite List.rev_involutive in H0. rewrite List.rev_app_distr in H0.
  simpl in H0. rewrite <- List.app_assoc in H0.
  split; [assumption|]. split; [assumption|].
  constructor. intros x contra. apply Intersection_inv in contra.
  destruct contra as [contra1 contra2].
  unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
  rewrite <- List.in_rev in contra1.
  inversion H1. apply (H2 x).
  apply Intersection_intro. assumption.
  unfold Ensembles.In, Ensemble_of_list. rewrite <- List.in_rev. assumption.
  intros x.
  destruct (List.in_dec eq_dec x (common eq_dec l1 l2)),
    (List.in_dec eq_dec x (common eq_dec (List.rev l1) (List.rev l2)));
    try reflexivity;
    rewrite common_spec in i, n; contradiction n; apply Intersection_inv in i;
    apply Intersection_intro; unfold Ensembles.In, Ensemble_of_list in *;
    rewrite <- !List.in_rev in *; apply i.
Qed.

Lemma first_common_none : forall {A} (eq_dec : forall (a b:A), {a=b} + {a<>b})
  (l1 l2 : list A),
  first_common eq_dec l1 l2 = None ->
  Disjoint (Ensemble_of_list l1) (Ensemble_of_list l2).
Proof.
  intros. unfold first_common in H. constructor.
  intros a contra.
  apply List.find_none with (x:=a) in H.
  destruct (List.in_dec eq_dec a (common eq_dec l1 l2)).
  - discriminate.
  - apply n. apply common_spec. assumption.
  - apply Intersection_inv in contra.
    apply contra.
Qed.

Lemma last_common_none : forall {A} (eq_dec : forall (a b:A), {a=b} + {a<>b})
  (l1 l2 : list A),
  last_common eq_dec l1 l2 = None ->
  Disjoint (Ensemble_of_list l1) (Ensemble_of_list l2).
Proof.
  intros. unfold last_common, find_last in H. constructor.
  intros a contra.
  apply List.find_none with (x:=a) in H.
  destruct (List.in_dec eq_dec a (common eq_dec l1 l2)).
  - discriminate.
  - apply n. apply common_spec. assumption.
  - apply Intersection_inv in contra. rewrite <- List.in_rev.
    apply contra.
Qed.

Lemma NoDup_Disjoint : forall {A} (l1 l2:list A),
  List.NoDup (l1++l2) -> Disjoint (Ensemble_of_list l1) (Ensemble_of_list l2).
Proof.
  intros A l1. induction l1; intros.
  - constructor. intros x contra. apply Intersection_inv in contra.
    destruct contra as [contra _]. contradiction.
  - inversion H. apply IHl1 in H3.
    constructor. intros x0 contra. apply Intersection_inv in contra.
    destruct contra as [[contra1|contra1] contra2].
    + subst x0. apply H2. apply List.in_app_iff. right; assumption.
    + inversion H3. apply (H4 x0). apply Intersection_intro; assumption.
Qed.

Lemma NoDup_app : forall {A} (l1 l2 : list A),
  List.NoDup (l1++l2) -> List.NoDup l1 /\ List.NoDup l2.
Proof.
  intros A l1. induction l1; intros.
  - simpl in H. split; [constructor|assumption].
  - inversion H. apply IHl1 in H3.
    split.
    + constructor. intros contra. apply H2.
      apply List.in_app_iff; left; assumption. apply H3.
    + apply H3.
Qed.

Lemma filter_NoDup : forall {A} f (l : list A),
  List.NoDup l -> List.NoDup (List.filter f l).
Proof.
  intros. induction H.
  - constructor.
  - simpl. destruct (f x).
    + constructor.
      * intros contra. apply List.filter_In in contra.
        destruct contra; contradiction.
      * assumption.
    + assumption.
Qed.

Lemma filter_StronglySorted : forall {A} (lt:A->A->Prop) f (l : list A),
  StronglySorted lt l -> StronglySorted lt (List.filter f l).
Proof.
  intros. induction H.
  - constructor.
  - simpl. destruct (f a).
    + constructor.
      * assumption.
      * rewrite List.Forall_forall in H0. apply List.Forall_forall.
        intros x ?. apply H0. rewrite List.filter_In in H1. apply H1.
    + assumption.
Qed.

Lemma filter_Sorted : forall {A} (lt:A->A->Prop)
  {lt_strorder : RelationClasses.StrictOrder lt}
  f (l : list A),
  Sorted lt l -> Sorted lt (List.filter f l).
Proof.
  intros.
  apply StronglySorted_Sorted.
  apply Sorted_StronglySorted in H.
  - apply filter_StronglySorted. assumption.
  - intros x y z ? ?. rewrite H0. assumption.
Qed.

Lemma find_app_none : forall {A} (f:A->bool) (l1 l2:list A),
  List.find f l1 = None -> List.find f (l1++l2) = List.find f l2.
Proof.
  intros A f l1. induction l1; intros.
  - reflexivity.
  - assert (f a = false).
    { apply List.find_none with (l:=a::l1). assumption. left; reflexivity. }
    simpl. rewrite H0. apply IHl1. simpl in H. rewrite H0 in H.
    assumption.
Qed.

Lemma first_common_app_none : forall {A}
  (eq_dec : forall a b : A, {a = b} + {a <> b}) (l1 l2 l : list A),
  Disjoint (Ensemble_of_list l1) (Ensemble_of_list l) ->
  first_common eq_dec (l1++l2) l = first_common eq_dec l2 l.
Proof.
  intros A eq_dec l1. induction l1; intros.
  - reflexivity.
  - unfold first_common. simpl.
    destruct (List.in_dec eq_dec a l).
    + inversion H. contradiction (H0 a).
      apply Intersection_intro. left; reflexivity. assumption.
    + destruct (List.in_dec eq_dec a (common eq_dec (l1 ++ l2) l)).
      * apply common_spec in i. apply Intersection_inv in i.
        destruct i as [_ i]. contradiction.
      * apply (IHl1 l2 l).
        constructor. intros x contra. apply Intersection_inv in contra.
        inversion H. apply (H0 x).
        apply Intersection_intro. right; apply contra. apply contra.
Qed.

Lemma Exists_first : forall {A} (P:A->Prop),
  (forall u, P u \/ ~ P u) ->
  forall l,
  List.Exists P l ->
  exists l1 u l2, l = l1 ++ u :: l2 /\
  P u /\ List.Forall (fun v => ~ P v) l1.
Proof.
  intros. induction l as [|a l].
  - inversion H0.
  - destruct (H a).
    + exists [], a, l. split; [reflexivity |]. split; [assumption|].
      constructor.
    + inversion H0; subst; [contradiction|].
      apply IHl in H3.
      destruct H3 as (l1 & u & l2 & IH1 & IH2 & IH3).
      exists (a::l1), u, l2. split.
      * simpl. rewrite <- IH1. reflexivity.
      * split; [assumption |]. constructor; assumption.
Qed.

Lemma Exists_last : forall {A} (P:A->Prop),
  (forall u, P u \/ ~ P u) ->
  forall l,
  List.Exists P l ->
  exists l1 u l2, l = l1 ++ u :: l2 /\
  P u /\ List.Forall (fun v => ~ P v) l2.
Proof.
  intros.
  destruct (Exists_first P H (List.rev l)) as (l1 & u & l2 & H1 & H2 & H3).
  - apply List.Exists_exists in H0. apply List.Exists_exists.
    destruct H0 as (x & H01 & H02). exists x. split; [|assumption].
    rewrite <- List.in_rev. assumption.
  - exists (List.rev l2), u, (List.rev l1).
    split.
    + apply (f_equal (List.rev (A:=_))) in H1. rewrite List.rev_app_distr in H1.
      rewrite List.rev_involutive in H1. simpl in H1.
      rewrite <- List.app_assoc in H1. assumption.
    + split; [assumption|]. apply List.Forall_forall.
      rewrite List.Forall_forall in H3.
      intros x ?. apply H3. apply List.in_rev. assumption.
Qed.

Lemma Forall_dec : forall {A : Type} (P : A -> Prop)
  (P_dec : forall x, P x \/ ~ P x) (l:list A),
  List.Forall P l \/ ~ List.Forall P l.
Proof.
  intros. induction l.
  - left. constructor.
  - destruct (P_dec a).
    + destruct IHl.
      * left; constructor; assumption.
      * right. intros contra; inversion contra; contradiction.
    + right. intros contra; inversion contra; contradiction.
Qed.

Lemma Exists_dec : forall {A : Type} (P : A -> Prop)
  (P_dec : forall x, P x \/ ~ P x) (l:list A),
  List.Exists P l \/ ~ List.Exists P l.
Proof.
  intros. induction l.
  - right. intros contra. inversion contra.
  - destruct (P_dec a).
    + left. apply List.Exists_cons_hd. assumption.
    + destruct IHl.
      * left. apply List.Exists_cons_tl. assumption.
      * right. intros contra; inversion contra; contradiction.
Qed.

Lemma Sorted_dec : forall {A:Type} (P:A->A->Prop)
  (P_dec : forall x y, P x y \/ ~ P x y) (l:list A),
  Sorted P l \/ ~ Sorted P l.
Proof.
  intros. induction l.
  - left. repeat constructor.
  - destruct l as [|b l].
    + left. repeat constructor.
    + destruct (P_dec a b).
      * { destruct IHl.
        - left. constructor.
          + assumption.
          + constructor; assumption.
        - right. intros contra. inversion contra; contradiction. }
      * right. intros contra. inversion contra; subst.
        inversion H3; contradiction.
Qed.
