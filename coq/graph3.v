(** All the results presented in this file come from Danicic et al., 2011,
    Theorical Computer Science.

    The main definitions are [Weakly_control_closed], the notion of set closed
    under control dependence, and [Weakly_deciding], which characterizes the
    interesting vertices to look at to compute the closure.
    Theorem [th54_explicit] states one way to compute the closure.
    It is the main justification for the algorithm proposed in [algo].
*)

Require Import MyFinite.
Require Import Finite_facts.
Require Import infList. Import infListNotations.
Import List.ListNotations.
Require Import listOtherResults.
Require last.
Require Import Equalities.

Open Scope list_scope.

(** * Signature for labels. *)
(** Experimental. To be checked later on. *)
Module Type COMBINE_TYPE.

  Parameter Label : Type.

  Parameter In : Label -> Label -> Prop.

  Axiom In_refl : forall l, In l l.

  Parameter combine : Label -> Label -> Label.

  Axiom combine_In : forall l1 l2 l,
    In l (combine l1 l2) <-> In l l1 \/ In l l2.

End COMBINE_TYPE.

(** * Signature for graphs with labels. *)
Module Type ABSTRACT_GRAPH (V : UsualEq) (Import C: COMBINE_TYPE).

(*  Definition Vertex := V.t. *)

  Parameter Graph : Type.

  Parameter Rel : Graph -> V.t -> Label -> V.t -> Prop.

  Parameter Support : Graph -> Ensemble V.t.

  Axiom Support_spec : forall g u l v,
    Rel g u l v -> Support g u /\ Support g v.

End ABSTRACT_GRAPH.

(** * Signature for graphs with labels. *)
Module Type ABSTRACT_FINITE_GRAPH (V : UsualEq) (Import C: COMBINE_TYPE).

  Include ABSTRACT_GRAPH V C.

  Axiom Support_finite : forall g, Finite (Support g).

  Axiom Rel_finite : forall g u,
    Finite (fun v => exists l, Rel g u l v).

End ABSTRACT_FINITE_GRAPH.

Module AbstractGraphProperties (V : UsualEq)
  (Import C : COMBINE_TYPE) (Import G : ABSTRACT_GRAPH V C).

  Definition Equal g1 g2 := Support g1 == Support g2 /\
    (forall u l v, Rel g1 u l v <-> Rel g2 u l v).

  Lemma Equal_refl : forall g, Equal g g.
  Proof.
    split; intros; reflexivity.
  Qed.

  Lemma Equal_sym : forall g1 g2, Equal g1 g2 -> Equal g2 g1.
  Proof.
    split; intros; symmetry; apply H.
  Qed.

  Lemma Equal_trans : forall g1 g2 g3, Equal g1 g2 -> Equal g2 g3 ->
    Equal g1 g3.
  Proof.
    split; intros.
    - destruct H. rewrite H. apply H0.
    - destruct H. rewrite H1. apply H0.
  Qed.

  Add Parametric Relation : (Graph) Equal
    reflexivity proved by Equal_refl
    symmetry proved by Equal_sym
    transitivity proved by Equal_trans
  as Equal_equiv.

  Add Parametric Morphism : Support
    with signature Equal ==> Same_set
  as Support_Equal.
  Proof.
    intros g1 g2 g_eqG.
    apply g_eqG.
  Qed.

  Add Parametric Morphism : Rel
    with signature Equal ==> V.eq ==> eq ==> V.eq ==> iff
  as Rel_Equal.
  Proof.
    intros g1 g2 g_eqG u1 u2 u_eq l v1 v2 v_eq.
    rewrite u_eq, v_eq.
    apply g_eqG.
  Qed.

  Definition Rel_w g u v := exists l, Rel g u l v.

  Add Parametric Morphism : Rel_w
    with signature Equal ==> V.eq ==> V.eq ==> iff
    as Rel_w_Equal.
  Proof.
    intros g1 g2 g_eqG u1 u2 u_eq v1 v2 v_eq.
    unfold Rel_w. setoid_rewrite u_eq. setoid_rewrite v_eq.
    setoid_rewrite g_eqG. reflexivity.
  Qed.

  Lemma Support_spec_w : forall g u v, Rel_w g u v ->
    Support g u /\ Support g v.
  Proof.
    intros. destruct H as (l & H). apply Support_spec in H. assumption.
  Qed.

  (** ** The notion of path in a graph *)
  Definition Path (g:Graph) p :=
    Forall (Support g) p /\
    Sorted (Rel_w g) p.

  Add Parametric Morphism : Path
    with signature Equal ==> inf_eq ==> iff as Path_inf_eq.
  Proof.
    intros g1 g2 g_eqG l1 l2 l_inf_eq. unfold Path.
    rewrite <- g_eqG, <- l_inf_eq. reflexivity.
  Qed.

  Lemma Path_cut : forall g p1 p2,
    Path g (p1++p2) -> Path g p1.
  Proof.
    repeat split; revert g p1 p2 H; cofix; intros.
    - destruct p1. constructor.
      destruct H. simpl_inf in H. inversion H.
      constructor. assumption. eapply Path_cut.
      split. eassumption. simpl_inf in H0. inversion H0. assumption.
    - destruct p1. constructor.
      simpl_inf in H. destruct H.
      constructor. eapply Path_cut. split. inversion H; eassumption.
      inversion H0; assumption.
      destruct p1; constructor. inversion H0. simpl_inf in H4.
      inversion H4; assumption.
  Qed.

  Lemma Path_finite_cut : forall g (p1:list V.t) (p2:infList V.t),
    Path g (p1++p2) -> Path g p2.
  Proof.
    intros. induction p1.
    - simpl_inf in H. assumption.
    - simpl_inf in H. apply IHp1. destruct H.
      split. inversion H; assumption. inversion H0; assumption.
  Qed.

  Lemma Path_finite_app : forall g (p1:list V.t) p2 u,
    Path g p1 -> Path g (u::p2) -> List.last p1 u = u ->
    Path g (p1++p2).
  Proof.
    intros. induction p1.
    - simpl_inf. apply Path_finite_cut with (p1:=[u]). simpl_inf. assumption.
    - simpl_inf.
      assert (Path g p1).
      { apply Path_finite_cut with (p1:=[a]). simpl_inf. assumption. }
      assert (List.last p1 u = u).
      { destruct p1. reflexivity. assumption. }
      specialize (IHp1 H2 H3).
      destruct H as [supp label].
      constructor.
      + constructor. inversion supp; assumption. apply IHp1.
      + constructor. apply IHp1. destruct p1.
        * destruct p2. simpl_inf. constructor.
          simpl_inf. constructor. destruct H0. inversion H0. inversion H7.
          simpl in H1. subst a. assumption.
        * simpl_inf. constructor. inversion label. inversion H6.
          assumption.
  Qed.

  (** ** The notion of walk in a graph. *)
  (** The last element is not required to have a meaningful label. *)
  Definition Walk (g:Graph) w :=
    Forall (Support g) (map fst w) /\
    Sorted (fun u v => exists l, In l (snd u) /\ Rel g (fst u) l (fst v)) w.

  Add Parametric Morphism : Walk
    with signature eq ==> inf_eq ==> iff as Walk_inf_eq.
  Proof.
    intros G l1 l2 H.
    split; intros; unfold Walk; [rewrite <- H|rewrite H]; assumption.
  Qed.

  Lemma walk_is_path : forall G w, Walk G w -> Path G (map fst w).
  Proof.
    intros. destruct H. split.
    assumption. clear H.

    (* Here begins another proof using coinduction *)
    revert dependent w.
    cofix. intros. destruct w.
    simpl_inf. constructor.
    simpl_inf. constructor. apply walk_is_path. inversion H0; assumption.
    destruct w. simpl_inf; constructor.
    inversion H0. inversion H3. simpl_inf. constructor.
    destruct H5 as [L [_ H5]].
    exists L. assumption.
  Qed.

  (** ** V-Intervals *)
  (** These are paths whose only end vertices are allowed to be in V. *)
  Definition Is_UVInterval G V u v (p:list V.t) :=
    Path G (u::p++[v])%inf /\ V u /\ V v /\
    Forall (Complement V) p.

  Definition UVInterval G V u v :=
    exists p, Is_UVInterval G V u v p.

  Definition UUVInterval G V u u' v :=
    exists p, Is_UVInterval G V u v p /\ List.hd v p = u'.

  Lemma UUVInterval_is_UVInterval : forall G V u u' v,
    UUVInterval G V u u' v -> UVInterval G V u v.
  Proof.
    intros. destruct H as [p H]. exists p. apply H.
  Qed.

  (** ** V-Paths *)
  (** These are paths whose only end vertex is allowed to be in V. *)
  Definition Is_VPath G V u v (p:list V.t) :=
    List.hd v p = u /\ Path G (p++[v]) /\ V v /\ Forall (Complement V) p.

  Add Parametric Morphism : Is_VPath
    with signature
      Equal ==> Same_set ==> V.eq ==> V.eq ==> eq ==> iff
    as Is_VPath_Equal.
  Proof.
    intros g1 g2 g_Equal V1 V2 V_Same u1 u2 u_eq v1 v2 v_eq p.
    split; intros; destruct H as (H1 & H2 & H3 & H4).
    - rewrite <- u_eq, <- v_eq. split; [assumption|].
      split.
      + rewrite <- g_Equal. assumption.
      + split.
        * apply V_Same. assumption.
        * rewrite <- V_Same. assumption.
    - rewrite u_eq, v_eq. split; [assumption|].
      split.
      + rewrite g_Equal. assumption.
      + split.
        * apply V_Same. assumption.
        * rewrite V_Same. assumption.
  Qed.

  Definition VPath G V u v :=
    exists p, Is_VPath G V u v p.

  Add Parametric Morphism : VPath
    with signature
      Equal ==> Same_set ==> V.eq ==> V.eq ==> iff
    as VPath_Equal.
  Proof.
    intros g1 g2 g_Equal V1 V2 V_Same u1 u2 u_eq v1 v2 v_eq.
    split; intros.
    - destruct H as (p & H). exists p.
      rewrite <- g_Equal, <- V_Same, <- u_eq, <- v_eq.
      assumption.
    - destruct H as (p & H). exists p.
      rewrite g_Equal, V_Same, u_eq, v_eq.
      assumption.
  Qed.

  Lemma Is_UVInterval_gives_Is_VPath : forall G V u u' v p,
    Is_UVInterval G V u v p /\ List.hd v p = u' ->
    Is_VPath G V u' v p.
  Proof.
    intros. destruct H as [[H [H0 [H1 H2]]] H3].
    split; [assumption|].
    split; [|split; assumption].
    apply Path_finite_cut with (p1:=[u]). simpl_inf. assumption.
  Qed.

  Lemma UUVInterval_gives_VPath : forall G V u u' v,
    UUVInterval G V u u' v -> VPath G V u' v.
  Proof.
    intros. destruct H as [p H]. exists p.
    apply Is_UVInterval_gives_Is_VPath in H. assumption.
  Qed.

  (** ** Weakly-committing vertices *)
  (** could be defined using uniqueness *)
  Definition Weakly_committing (g:Graph) (V:Ensemble V.t) u :=
    forall v1 v2, VPath g V u v1 -> VPath g V u v2 -> v1 = v2.

  Add Parametric Morphism : Weakly_committing
    with signature Equal ==> Same_set ==> V.eq ==> iff
    as Weakly_committing_Equal.
  Proof.
    intros g1 g2 g_Equal V1 V2 V_Same u1 u2 u_eq. rewrite <- u_eq.
    split; intros.
    - unfold Weakly_committing; intros. rewrite <- g_Equal, <- V_Same in H0, H1.
      eapply H; eassumption.
    - unfold Weakly_committing; intros. rewrite g_Equal, V_Same in H0, H1.
      eapply H; eassumption.
  Qed.

  (** ** Reachability from a vertex and from a set *)
  (** u is reachable from v in g *)
  Definition Reachable g u v := exists p:list V.t, Path g p /\
    List.hd u p = u /\ List.last p u = v.

  Add Parametric Morphism : Reachable
    with signature Equal ==> eq ==> eq ==> iff
    as Reachable_Equal.
  Proof.
    intros g1 g2 g_Equal u v.
    unfold Reachable. setoid_rewrite <- g_Equal. reflexivity.
  Qed.

  Lemma Reachable_refl : forall g u, Reachable g u u.
  Proof. exists []. repeat constructor.
  Qed.

  Lemma Reachable_trans : forall g u1 u2 u3, Reachable g u1 u2 ->
    Reachable g u2 u3 -> Reachable g u1 u3.
  Proof.
    intros. destruct H as [p1 H]. destruct H0 as [p2 H0].
    destruct p2 as [|v p2].
    - destruct H0 as [_ [_ H0]]. simpl in H0. subst u3. exists p1. assumption.
    - assert(p1=[] \/ p1 <>[]) as Ha.
      { destruct p1; [left; reflexivity | right; discriminate]. }
      destruct Ha.
      + subst p1. destruct H as [_ [_ H]]. simpl in H. subst u2.
        exists (v::p2). assumption.
      + exists (p1++p2). split.
        * simpl_inf. apply Path_finite_app with (u:=v).
          apply H. apply H0. destruct H0 as [_ [H0 _]].
          simpl in H0. subst u2. destruct H as [_ [_ H]].
          erewrite last.last_not_empty by assumption. eassumption.
        * { split.
          - destruct p1. contradiction H1; reflexivity.
            destruct H as [_ [H _]]. simpl in H. simpl. assumption.
          - destruct p2.
            + simpl in H0. destruct H0 as [_ [H01 H02]]. subst u2 u3.
              rewrite List.app_nil_r. apply H.
            + destruct H0 as [_ [_ H0]].
              rewrite 2!last.last_cons_change_default in H0.
              rewrite last.last_app_not_nil by discriminate.
              rewrite last.last_cons_change_default. assumption. }
  Qed.

  Lemma Reachable_In_Support : forall g u v,
    Reachable g u v -> u <> v -> Support g u /\ Support g v.
  Proof.
    intros. destruct H as (p & H1 & H2 & H3). destruct p as [|w p].
    - simpl in H3. contradiction.
    - simpl in H2; subst w. destruct H1 as (H1 & _).
      rewrite Forall_forall in H1. split; apply H1.
      + apply In_here.
      + apply In_finite. subst v. apply last.last_not_empty_In. discriminate.
  Qed.

  Lemma UVInterval_gives_Reachable : forall g V u v,
    UVInterval g V u v -> Reachable g u v.
  Proof.
    intros. destruct H as [p H]. exists (u::p++[v]). unfold Is_UVInterval in H.
    split.
    - simpl_inf. apply H.
    - split.
      + reflexivity.
      + rewrite List.app_comm_cons.
        rewrite last.last_app_not_nil by discriminate.
        reflexivity.
  Qed.

  Lemma UUVInterval_gives_Reachable_l : forall g V u u' v,
    UUVInterval g V u u' v -> Reachable g u v.
  Proof.
    intros. apply UUVInterval_is_UVInterval in H.
    apply UVInterval_gives_Reachable in H. assumption.
  Qed.

  Lemma UUVInterval_gives_Reachable_r : forall g V u u' v,
    UUVInterval g V u u' v -> Reachable g u' v.
  Proof.
    intros. destruct H as [p [HH1 HH2]]. unfold Is_UVInterval in HH1.
    unfold Reachable.
    exists (p++[v]). split.
    - apply Path_finite_cut with (p1:=[u]). simpl_inf. apply HH1.
    - split.
      + destruct p. simpl in HH2. subst u'. reflexivity. assumption.
      + rewrite last.last_app_not_nil by discriminate. reflexivity.
  Qed.

  Lemma VPath_gives_Reachable : forall g V u v, VPath g V u v ->
    Reachable g u v.
  Proof.
    intros. destruct H as [p H]. exists (p++[v]). unfold Is_VPath in H.
    split.
    - simpl_inf. apply H.
    - split.
      + destruct p; apply H.
      + rewrite last.last_app_not_nil by discriminate. reflexivity.
  Qed.

  (* v is reachable in g from a vertex in V *)
  Definition Reachable_from g V v := exists u, V u /\ Reachable g u v.

  Add Parametric Morphism : Reachable_from
    with signature Equal ==> Same_set ==> eq ==> iff
    as Reachable_from_Equal.
  Proof.
    intros g1 g2 g_Equal V1 V2 V_Same_set u.
    unfold Reachable_from.
    setoid_rewrite <- g_Equal. setoid_rewrite <- V_Same_set.
    reflexivity.
  Qed.

  Lemma Reachable_from_trans : forall g V u v,
    Reachable_from g V u -> Reachable g u v -> Reachable_from g V v.
  Proof.
    intros. destruct H as [u0 HH1].
    exists u0. split. apply HH1. apply Reachable_trans with (u2:=u).
    apply HH1. apply H0.
  Qed.

  (** ** Weakly control-closed set *)
  Definition Weakly_control_closed G V := forall u,
    Reachable_from G V u -> Weakly_committing G V u.

  Add Parametric Morphism : Weakly_control_closed
    with signature Equal ==> Same_set ==> iff
    as Weakly_control_closed_Equal.
  Proof.
    intros g1 g2 g_Equal V1 V2 V_Same.
    split; intros.
    - unfold Weakly_control_closed; intros. rewrite <- g_Equal, <- V_Same.
      rewrite <- g_Equal, <- V_Same in H0. apply H; assumption.
    - unfold Weakly_control_closed; intros. rewrite g_Equal, V_Same.
      rewrite g_Equal, V_Same in H0. apply H; assumption.
  Qed.

  (** ** g' is the induced graph by V from g *)
  Record induced g V g' : Prop := {
    induced_Rel : forall u l v,
      Rel g' u l v <->
      exists l' u', UUVInterval g V u u' v /\ Rel g u l' u' /\ In l' l;
    induced_Support : Support g' == Intersection V (Support g)
  }.

  Arguments induced_Rel {g V g'} _ _ _ _.
  Arguments induced_Support {g V g'} _.

  (** ** No_more_edges *)
  (** Another definition to express the same behavior. *)
  Definition No_more_edges g V := forall u u1 v1 u2 v2,
    UUVInterval g V u u1 v1 ->
    UUVInterval g V u u2 v2 ->
    u1 = u2 -> v1 = v2.

  Theorem th45_bis : forall g V,
    Weakly_control_closed g V ->
    No_more_edges g V.
  Proof.
    unfold No_more_edges.
    intros. subst u2.
    assert (Reachable_from g V u1).
    { exists u. destruct H0 as [p H0]. split. apply H0.
      exists [u; u1]. split; [|split; reflexivity].
      destruct H0 as [[H01 _] H02].
      apply Path_cut with (p2:=List.tl (p++[v1])).
      destruct p; simpl_inf in H01; simpl_inf; simpl in H02;
        subst u1; assumption.
    }
    apply UUVInterval_gives_VPath in H0. apply UUVInterval_gives_VPath in H1.
    eapply H; eassumption.
  Qed.

  Corollary th45_bis_negative : forall g V,
    Weakly_control_closed g V ->
    forall u u1 v1 u2 v2,
    UUVInterval g V u u1 v1 -> UUVInterval g V u u2 v2 ->
    v1 <> v2 -> u1 <> u2.
  Proof.
    intros. intros contra. apply H2. eapply th45_bis; eassumption.
  Qed.

  Theorem th45_ter : forall g V
    (In_dec : forall v, {V v} + { ~ V v}),
    No_more_edges g V ->
    Weakly_control_closed g V.
  Proof.
    intros. unfold Weakly_control_closed; intros.
    unfold Weakly_committing; intros.
    destruct H0 as [u0 [H01 H02]].
    destruct H02 as [p H02].
    destruct H1 as [p1 H1].
    destruct H2 as [p2 H2].

    (* if p1 is empty, p2 is empty too, so the result is trivial *)
    destruct p1 as [|u1 p1].
    - destruct H1 as [H11 [_ [H12 _]]]. simpl in H11. subst v1.
      destruct H2 as [H21 [_ [_ H22]]].
      destruct p2 as [|v p2]. simpl in H21. rewrite H21. reflexivity.
      inversion H22. simpl in H21. subst v. contradiction.
    - destruct p2 as [|u2 p2].
      + destruct H2 as [H21 [_ [H22 _]]]. simpl in H21. subst v2.
        destruct H1 as [H11 [_ [_ H12]]].
        inversion H12. simpl in H11. subst u1. contradiction.
      + (* the interesting case is when p1 and p2 are both non-empty *)
        destruct H1 as [H11 H12]. destruct H2 as [H21 H22].
        simpl in H11, H21. subst u1 u2.

        (* p cannot be nil either, because otherwise u is both in V and not in V *)
        destruct p as [|v p].
        * destruct H02 as [_ [_ H02]]. simpl in H02. subst u0.
          destruct H12 as [_ [_ H12]]. inversion H12; contradiction.
        * destruct H02 as [H021 [H022 H023]]. simpl in H022; subst v.

          (* to construct an interval, we need to find the last occurence of an
             element of (u0::p) in V ; we use List.find on the reverse list
             for that *)
          assert (find_last (fun v => if In_dec v then true else false) (u0::p) <> None)
            as Ha.
          { apply find_Exists. apply List.Exists_exists. exists u0.
            split. rewrite <- List.in_rev. left; reflexivity.
            destruct (In_dec u0); [reflexivity|contradiction].
          }

          destruct (find_last _ _) as [v|] eqn:Heqfind; [clear Ha|contradiction Ha; reflexivity].
          pose proof Heqfind as Heqfind2.
          apply find_last_some in Heqfind. destruct Heqfind as [_ Heqfind].
          destruct (In_dec v) as [Hindec|Hindec]; [clear Heqfind|discriminate Heqfind].
          apply find_last_last in Heqfind2.
          destruct Heqfind2 as [l1 [l2 [Heqfind1 Heqfind2]]].

          (* List.rev l1 is not nil, because v is in V *)
          destruct l2 as [|v' l2].
          rewrite Heqfind1 in H023.
          rewrite last.last_app_not_nil in H023 by discriminate.
          simpl in H023. subst v.
          destruct H12 as [_ [_ H12]]. inversion H12; contradiction.

          (* we can now use the hypothese that given two intervals, if the
             successors of the source are the same, then the destination are
             the same *)
          { apply H with (u:=v) (u1:=v') (u2:=v'); try reflexivity.
          - exists (v'::l2 ++ p1).
            split; [|reflexivity]. split.
            + simpl_inf.
              apply Path_finite_cut with (p1:=l1).
              rewrite 2!app_comm_cons. rewrite app_assoc.
              regroup_finite. rewrite <- Heqfind1.

              apply Path_finite_app with (u:=u). assumption. simpl_inf in H12.
              apply H12.
              rewrite last.last_cons_change_default.
              rewrite last.last_cons_change_default in H023. assumption.
            + split; [assumption|]. split; [apply H12|].
              apply Forall_forall; intros.
              simpl_inf in H0. rewrite app_comm_cons in H0.
              apply in_app in H0.
              destruct H0.
              * regroup_finite in H0.
                apply In_finite in H0.
                rewrite List.Forall_forall in Heqfind2.
                apply Heqfind2 in H0.
                destruct (In_dec x); [discriminate H0|assumption].
              * destruct H12 as [_ [_ H12]]. inversion H12.
                rewrite Forall_forall in H4. apply H4; assumption.
          - exists (v'::l2 ++ p2).
            split; [|reflexivity]. split.
            + simpl_inf.
              apply Path_finite_cut with (p1:=l1).
              rewrite 2!app_comm_cons. rewrite app_assoc.
              regroup_finite. rewrite <- Heqfind1.

              apply Path_finite_app with (u:=u). assumption. simpl_inf in H22.
              apply H22.
              rewrite last.last_cons_change_default.
              rewrite last.last_cons_change_default in H023. assumption.
            + split; [assumption|]. split; [apply H22|].
              apply Forall_forall; intros.
              simpl_inf in H0. rewrite app_comm_cons in H0.
              apply in_app in H0.
              destruct H0.
              * regroup_finite in H0.
                apply In_finite in H0.
                rewrite List.Forall_forall in Heqfind2.
                apply Heqfind2 in H0.
                destruct (In_dec x); [discriminate H0|assumption].
              * destruct H22 as [_ [_ H22]]. inversion H22.
                rewrite Forall_forall in H4. apply H4; assumption. }
  Qed.

  (** ** Rewrite of the statements using the idea of "observable node". *)
  Definition Observable g V u : Ensemble V.t := fun v => VPath g V u v.

  Definition At_most_one_observable g V := forall u,
    Has_at_most_one (Observable g V u).

  Lemma Weakly_committing_iff_Has_at_most_one_observable : forall g V u,
    Weakly_committing g V u /\ Finite (Observable g V u)
      <-> Has_at_most_one (Observable g V u).
  Proof.
    intros; apply uniqueness_cardinal.
  Qed.

  (** ** Weakly deciding *)
  (** A weakly deciding vertex must be added to build a weakly control-closed
      set *)
  Definition Weakly_deciding g V u :=
    exists v1 v2 p1 p2,
      Is_VPath g V u v1 (u::p1) /\
      Is_VPath g V u v2 (u::p2) /\
      Disjoint (Ensemble_of_list (p1++[v1])) (Ensemble_of_list (p2++[v2])).

  Add Parametric Morphism : Weakly_deciding
    with signature Equal ==> Same_set ==> V.eq ==> iff
    as Weakly_deciding_Equal.
  Proof.
    intros g1 g2 g_Equal V1 V2 V_Same u1 u2 u_eq. rewrite <- u_eq.
    split; intros.
    - destruct H as (v1 & v2 & p1 & p2 & H1 & H2 & H3).
      exists v1, v2, p1, p2. split.
      + rewrite <- g_Equal, <- V_Same. assumption.
      + split; [|assumption]. rewrite <- g_Equal, <- V_Same. assumption.
    - destruct H as (v1 & v2 & p1 & p2 & H1 & H2 & H3).
      exists v1, v2, p1, p2. split.
      + rewrite g_Equal, V_Same. assumption.
      + split; [|assumption]. rewrite g_Equal, V_Same. assumption.
  Qed.

  Lemma Weakly_deciding_not_In : forall g V u,
    Weakly_deciding g V u -> ~ V u.
  Proof.
    intros. destruct H as [v1 [v2 [l1 [l2 [H _]]]]].
    destruct H as [_ [_ [_ H]]].
    inversion H. assumption.
  Qed.

  Lemma Weakly_deciding_In_Support : forall g V u,
    Weakly_deciding g V u -> Support g u.
  Proof.
    intros.
    destruct H as (u1 & _ & p1 & _ & H & _).
    destruct H as (_ & (H & _) & _).
    simpl_inf in H.
    inversion H; assumption.
  Qed.

 (** ** Results *)
  Lemma lemma51 : forall g V,
    Weakly_control_closed g V ->
    forall u, Reachable_from g V u -> ~ Weakly_deciding g V u.
  Proof.
    intros. intros contra.
    destruct contra as [v1 [v2 [p1 [p2 [H11 [H12 H13]]]]]].
    apply H in H0.
    assert (VPath g V u v1) as Ha1. { exists (u::p1); assumption. }
    assert (VPath g V u v2) as Ha2. { exists (u::p2); assumption. }
    specialize (H0 _ _ Ha1 Ha2).
    destruct H13 as [H13]. specialize (H13 v1).
    apply H13.
    apply Intersection_intro.
    - apply List.in_app_iff. right. left. reflexivity.
    - apply List.in_app_iff. right. left. rewrite H0. reflexivity.
  Qed.

End AbstractGraphProperties.

Module AbstractGraphPropertiesDec (V : UsualDecidableType)
  (Import C : COMBINE_TYPE)
  (Import G : ABSTRACT_GRAPH V C).

  Include AbstractGraphProperties V C G.

  Lemma lemma51_reverse : forall g V,
    (forall u, Reachable_from g V u -> ~ Weakly_deciding g V u) ->
    Weakly_control_closed g V.
  Proof.
    intros. unfold Weakly_control_closed; intros. unfold Weakly_committing; intros.
    destruct H1 as [p1 H1]. destruct H2 as [p2 H2].
    unfold Is_VPath in H1, H2.
    (* as before, p1 and p2 are both empty or both non-empty *)
    destruct p1 as [|v p1].
    - destruct H1 as [H11 [_ [H12 _]]]. simpl in H11. subst v1.
      destruct p2 as [|v p2].
      + destruct H2 as [H2 _].
        simpl in H2. subst v2. reflexivity.
      + destruct H2 as [H21 [_ [_ H23]]]. simpl in H21. subst v.
        inversion H23; contradiction.
    - destruct p2 as [|v0 p2].
      + destruct H2 as [H21 [_ [H23 _]]]. destruct H1 as [H11 [_ [_ H12]]].
        simpl in H11, H21. subst v v2. inversion H12; contradiction.
      + destruct H1 as [H11 H1]. simpl in H11. subst v.
        destruct H2 as [H21 H2]. simpl in H21. subst v0.

        assert(last_common V.eq_dec (u::p1) (u::p2) <> None).
        { unfold last_common. apply find_Exists. apply List.Exists_exists.
          exists u. split.
          - rewrite <- List.in_rev. left. reflexivity.
          - destruct (List.in_dec V.eq_dec u (common V.eq_dec (u :: p1) (u :: p2)));
              [reflexivity|].
            contradiction n. apply common_spec. apply Intersection_intro;
              left; reflexivity.
        }
        destruct (last_common _ _) as [v|] eqn:Heqlast;
          [|contradiction H3; reflexivity].
        apply last_common_some in Heqlast.
        destruct Heqlast as [l11 [l12 [l21 [l22 [Heqlast1 [Heqlast2 Heqlast3]]]]]].

        assert(Reachable_from g V v) as Ha.
        { eapply Reachable_from_trans. eassumption.
          exists (l11++[v]). split.
          - apply Path_cut with (p2:=l12++[v1]). simpl_inf.
            rewrite app_comm_cons. rewrite app_assoc.
            regroup_finite. rewrite <- Heqlast1. apply H1.
          - split. destruct l11; inversion Heqlast1; reflexivity.
            rewrite last.last_app_not_nil by discriminate. reflexivity.
        }

        (* either v1 = v2 or v is V-weakly deciding *)
        destruct (V.eq_dec v1 v2); [assumption|].

        contradiction (H v).

        (* it remains to prove that v is really V-weakly deciding *)
        exists v1, v2, l12, l22.
        split; [|split].
        * { split; [reflexivity|]. split.
          - apply Path_finite_cut with (p1:=l11).
            rewrite app_assoc. regroup_finite. rewrite <- Heqlast1.
            simpl_inf. destruct H1 as [H1 _]. simpl_inf in H1. assumption.
          - split; [apply H1|]. apply Forall_forall. intros.
            destruct H1 as [_ [_ H1]]. rewrite Forall_forall in H1.
            apply H1. rewrite Heqlast1. simpl_inf. apply in_app_finite_r.
            assumption. }
        * { split; [reflexivity|]. split.
          - apply Path_finite_cut with (p1:=l21).
            rewrite app_assoc. regroup_finite. rewrite <- Heqlast2.
            destruct H2 as [H2 _]. simpl_inf. simpl_inf in H2.
            assumption.
          - split; [apply H2|]. apply Forall_forall. intros.
            destruct H2 as [_ [_ H2]]. rewrite Forall_forall in H2.
            apply H2. rewrite Heqlast2. simpl_inf. apply in_app_finite_r.
            assumption. }
        * constructor. intros u0. intros contra.
          apply Intersection_inv in contra.
          destruct contra as [contra1 contra2].
          unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
          rewrite List.in_app_iff in contra1, contra2.
          { destruct contra1 as [contra1|contra1], contra2 as [contra2|contra2].
          - (* the interesting case *)
            inversion Heqlast3. specialize (H4 u0). apply H4.
            apply Intersection_intro; assumption.
          - destruct contra2; [subst u0|contradiction].
            destruct H1 as [_ [_ H1]]. rewrite Forall_forall in H1.
            apply (H1 v2); [|apply H2]. rewrite Heqlast1. simpl_inf.
            apply in_app_finite_r. apply In_after. apply In_finite. assumption.
          - destruct contra1; [subst u0|contradiction].
            destruct H2 as [_ [_ H2]]. rewrite Forall_forall in H2.
            apply (H2 v1); [|apply H1]. rewrite Heqlast2. simpl_inf.
            apply in_app_finite_r. apply In_after. apply In_finite. assumption.
          - destruct contra1; [subst u0|contradiction].
            destruct contra2; [|contradiction].
            rewrite H4 in n. apply n; reflexivity. }
  Qed.

  Lemma lemma51_iff : forall (g : Graph) (V : V.t -> Prop),
    (forall u, Reachable_from g V u -> ~ Weakly_deciding g V u) <->
      Weakly_control_closed g V.
  Proof.
    intros. split; intros.
    apply lemma51_reverse; assumption.
    apply lemma51; assumption.
  Qed.

  Lemma find_last_prefix_stays_Path : forall g u (p:list V.t),
    Path g p -> Path g (find_last_prefix V.eq_dec u p).
  Proof.
    intros. destruct (List.In_dec V.eq_dec u p).
    - apply (find_last_prefix_decompose V.eq_dec) in i.
      destruct i as [l1 i].
      apply Path_finite_cut with (p1:=l1).
      regroup_finite. rewrite <- i. assumption.
    - apply (find_last_prefix_nil V.eq_dec) in n. rewrite n. repeat constructor.
  Qed.

  Lemma remove_cycles_stays_Path : forall g (p:list V.t),
    Path g p -> Path g (remove_cycles V.eq_dec p).
  Proof.
    intros. functional induction (remove_cycles V.eq_dec p); intros.
    - assumption.
    - apply IHl. apply find_last_prefix_stays_Path.
      apply Path_finite_cut with (p1:=[a']). simpl_inf. simpl_inf in H.
      assumption.
    - assert(Path g (remove_cycles V.eq_dec l')) as Ha.
      { apply IHl. apply Path_finite_cut with (p1:=[a']). simpl_inf.
        simpl_inf in H. assumption.
      }
      split.
      + simpl_inf. constructor; [|apply Ha]. destruct H as [H _].
        inversion H; assumption.
      + simpl_inf. constructor; [apply Ha|].
        destruct l' as [|v l'].
        * rewrite remove_cycles_equation. constructor.
        * destruct (remove_cycles V.eq_dec (v::l')) as [|v0 l] eqn:Heqremove.
          constructor. pose proof (remove_cycles_keeps_hd V.eq_dec v (v::l')).
          rewrite Heqremove in H0. simpl in H0. subst v0.
          constructor. destruct H as [_ H]. inversion H.
          inversion H3; assumption.
  Qed.

  Lemma remove_cycles_sub_stays_Path : forall g (p1 p2 p3:list V.t),
    Path g (p1++p2++p3) -> Path g (p1 ++ (remove_cycles V.eq_dec p2) ++ p3).
  Proof.
    intros. destruct p2 as [|u p2].
    - rewrite remove_cycles_equation. assumption.
    - pose proof (remove_cycles_keeps_hd V.eq_dec u (u::p2)).
      destruct (remove_cycles V.eq_dec (u::p2)) as [|v p2'] eqn:Heqremove.
      + rewrite remove_cycles_nil in Heqremove. discriminate.
      + simpl in H0. subst v.
        setoid_replace (p1++(u::p2')++p3)%inf with ((p1++[u])%list++p2'++p3)%inf
          by (simpl_inf; reflexivity).
        apply Path_finite_app with (u:=u).
        * setoid_replace (p1++(u::p2)++p3)%inf with ((p1++[u])%list++p2++p3)%inf
            in H by (simpl_inf; reflexivity).
          apply Path_cut in H. assumption.
        * apply Path_finite_cut in H.
          setoid_replace (u::p2'++p3)%inf with ((u::p2')%list++p3)%inf
            by (simpl_inf; reflexivity).
          rewrite <- Heqremove.
          destruct (List.exists_last (l:=remove_cycles V.eq_dec (u::p2)))
            as (p2'' & v & H0); [rewrite Heqremove; discriminate|].
          rewrite H0.
          destruct (List.exists_last (l:=u::p2)) as (p20 & v' & H1);
            [discriminate|].
          rewrite H1 in H, H0.
          pose proof (remove_cycles_keeps_last V.eq_dec u (p20++[v'])).
          rewrite H0 in H2.
          rewrite 2!last.last_app_not_nil in H2 by discriminate.
          simpl in H2. subst v'.
          { apply Path_finite_app with (u:=v).
          - apply Path_cut in H. rewrite <- H0.
            apply remove_cycles_stays_Path. assumption.
          - simpl_inf in H. apply Path_finite_cut in H. assumption.
          - rewrite last.last_app_not_nil by discriminate. reflexivity. }
        * rewrite last.last_app_not_nil by discriminate. reflexivity.
  Qed.

  (** *** Preliminary lemmas for lemma53 *)
  (** All lemma53* just prepare lemma53 *)

  Module Import lemma53_preresults.

    Lemma lemma53_aux : forall g V u v1 v2 p1 p2,
      Is_VPath g (Union V (Weakly_deciding g V)) u v1 (u :: p1) ->
      Is_VPath g (Union V (Weakly_deciding g V)) u v2 (u :: p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      V v1 -> V v2 -> Weakly_deciding g V u.
    Proof.
      intros. exists v1, v2, p1, p2.
      destruct H as [_ [HH1 [HH2 HH3]]]. destruct H0 as [_ [H01 [H02 H03]]].
      repeat (split; try assumption).
      apply Forall_forall. rewrite Forall_forall in HH3.
      intros v H. apply HH3 in H. intros contra. apply H.
      apply Union_introl. assumption.
      apply Forall_forall. rewrite Forall_forall in H03.
      intros v H. apply H03 in H. intros contra. apply H.
      apply Union_introl. assumption.
    Qed.

    Lemma lemma53_aux2_aux : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
      Path g ((u::p1)%list++[v1]) -> V v1 ->
      Forall (Complement V) (u::p1) ->
      Path g ((u::p2)%list++[v2]) ->
      Forall (Complement V) (u::p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      forall v21 v22 p21 p22,
      Is_VPath g V v2 v21 (v2 :: p21) ->
      Is_VPath g V v2 v22 (v2 :: p22) ->
      Disjoint (Ensemble_of_list (p21 ++ [v21])) (Ensemble_of_list (p22 ++ [v22])) ->
      forall l1 l2 q1 q2 v,
      p1 = l1 ++ v :: l2 -> p22 = q1 ++ v :: q2 ->
      Disjoint (Ensemble_of_list l1) (Ensemble_of_list p21) ->
      Disjoint (Ensemble_of_list (p2++[v2])) (Ensemble_of_list q2) ->
      Weakly_deciding g V u.
    Proof.
      intros.
      exists v21, v22, (p2++v2::p21), (l1++v::q2).
      split; [|split].
      - split; [reflexivity|]. split.
        + setoid_replace ((u :: p2 ++ v2 :: p21)%list ++ [v21])%inf
            with ((u::p2++[v2])%list ++ p21 ++ [v21])%inf
            by (simpl_inf; reflexivity).
          apply Path_finite_app with (u:=v2).
          * simpl_inf in H2. simpl_inf. assumption.
          * destruct H5 as [_ [H5 _]]. simpl_inf in H5. simpl_inf. assumption.
          * rewrite last.last_cons_change_default.
            rewrite last.last_app_not_nil by discriminate. reflexivity.
        + split; [apply H5|]. apply Forall_forall. intros v0 Hin contra.
          simpl_inf in Hin. rewrite app_comm_cons in Hin. apply in_app in Hin.
          destruct Hin.
          * rewrite Forall_forall in H3. apply (H3 v0); assumption.
          * destruct H5 as [_ [_ [_ H5]]]. rewrite Forall_forall in H5.
            apply (H5 v0); assumption.
      - split; [reflexivity|]. split.
        + setoid_replace ((u :: l1 ++ v :: q2)%list ++ [v22])%inf
            with ((u::l1++[v])%list ++ (q2 ++ [v22])%list)%inf
            by (simpl_inf; reflexivity).
          apply Path_finite_app with (u:=v).
          * apply Path_cut with (p2:=l2++[v1]). regroup_finite.
            rewrite List.app_assoc.
            simpl List.app.
            rewrite <- List.app_assoc with (m:=[v]). simpl List.app.
            rewrite <- H8.
            simpl_inf in H. simpl_inf. assumption.
          * apply Path_finite_cut with (p1:=v2::q1). regroup_finite.
            rewrite List.app_comm_cons.
            rewrite List.app_assoc.
            simpl List.app. rewrite <- H9.
            destruct H6 as [_ [H6 _]]. simpl_inf in H6. simpl_inf.
            assumption.
          * rewrite last.last_cons_change_default.
            rewrite last.last_app_not_nil by discriminate. reflexivity.
        + split; [apply H6|]. apply Forall_forall. intros v0 Hin contra.
          simpl_inf in Hin. rewrite app_comm_cons in Hin. apply in_app in Hin.
          destruct Hin.
          * rewrite Forall_forall in H1. apply (H1 v0).
            rewrite H8. simpl_inf. rewrite app_comm_cons.
            apply in_app_l. assumption. assumption.
          * destruct H6 as [_ [_ [_ H6]]]. rewrite Forall_forall in H6.
            apply (H6 v0). rewrite H9. simpl_inf.
            apply In_after. apply in_app_finite_r. assumption. assumption.
    - constructor. intros v0 contra. apply Intersection_inv in contra.
      destruct contra as [contra1 contra2].
      unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
      rewrite <- List.app_assoc in contra1, contra2.
      replace (v2::p21) with ([v2]++p21) in contra1 by reflexivity.
      replace (v::q2) with ([v]++q2) in contra2 by reflexivity.
      rewrite 2!List.app_assoc in contra1, contra2.
      rewrite <- List.app_assoc in contra1, contra2.
      rewrite List.in_app_iff in contra1, contra2.
      destruct contra1 as [contra1|contra1],
        contra2 as [contra2|contra2].
      + inversion H4. apply (H12 v0). apply Intersection_intro.
        * rewrite H8. apply List.in_app_iff. left.
          replace (v::l2) with ([v]++l2) by reflexivity.
          rewrite List.app_assoc.
          apply List.in_app_iff. left. assumption.
        * assumption.
      + rewrite List.in_app_iff in contra2.
        destruct contra2 as [contra2|contra2].
        * inversion H11. apply (H12 v0). apply Intersection_intro.
          assumption. assumption.
        * destruct contra2; [subst v0|contradiction].
          rewrite List.in_app_iff in contra1.
          destruct contra1 as [contra1|contra1].
          rewrite Forall_forall in H3. apply (H3 v22). apply In_after.
          apply In_finite. assumption. apply H6.
          destruct contra1; [subst v22|contradiction].
          destruct H5 as [_ [_ [_ H5]]].
          rewrite Forall_forall in H5. apply (H5 v2). apply In_here.
          apply H6.
      + rewrite List.in_app_iff in contra2.
        destruct contra2 as [contra2|contra2].
        * rewrite List.in_app_iff in contra1.
          { destruct contra1 as [contra1|contra1].
          - inversion H10. apply (H12 v0). apply Intersection_intro; assumption.
          - destruct contra1; [subst v0|contradiction].
            rewrite Forall_forall in H1. apply (H1 v21).
            rewrite H8. apply In_after. simpl_inf. apply in_app_l.
            apply In_finite. assumption. apply H5. }
        * destruct contra2; [subst v0|contradiction]. inversion H7.
           apply (H12 v). apply Intersection_intro.
           assumption.
           rewrite H9. apply List.in_app_iff. left.
           apply List.in_app_iff. right; left; reflexivity.
      + inversion H7. apply (H12 v0). apply Intersection_intro.
        assumption. rewrite H9. rewrite <- List.app_assoc.
        apply List.in_app_iff. right. right. assumption.
    Qed.

    Lemma lemma53_aux2_aux2 : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
      Path g ((u::p1)%list++[v1]) -> V v1 ->
      Forall (Complement V) (u::p1) ->
      Path g ((u::p2)%list++[v2]) ->
      Forall (Complement V) (u::p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      forall v21 v22 p21 p22,
      Is_VPath g V v2 v21 (v2 :: p21) ->
      Is_VPath g V v2 v22 (v2 :: p22) ->
      Disjoint (Ensemble_of_list (p21 ++ [v21])) (Ensemble_of_list (p22 ++ [v22])) ->
      v1 <> v22 ->
      Disjoint (Ensemble_of_list p1) (Ensemble_of_list p22) ->
      Weakly_deciding g V u.
    Proof.
      intros.
      exists v1, v22, p1, (p2++v2::p22).
      pose proof H1 as support. rewrite Forall_forall in support.
      split; [|split].
      * split; [reflexivity|]. split; [apply H|]. split; [assumption|].
        apply Forall_forall.
        intros v Hin. apply support in Hin. intros contra.
        apply Hin. assumption.
      * split; [reflexivity|]. { split.
        - simpl_inf.
          setoid_replace (u::p2++v2::p22++[v22])%inf
            with ((u::p2++[v2])%list++(p22++[v22]))%inf
            by (simpl_inf; reflexivity).
          apply Path_finite_app with (u:=v2).
          simpl_inf. simpl_inf in H2. assumption.
          destruct H6 as [_ [H6 _]]. simpl_inf in H6. apply H6.
          rewrite last.last_cons_change_default.
          rewrite last.last_app_not_nil by discriminate. reflexivity.
        - split; [apply H6|]. apply Forall_forall. intros v Hin contra.
          inversion Hin as [|v0 ? Hin2].
          + subst v. apply (support u).
            apply In_here. assumption.
          + simpl_inf in Hin2. apply in_app in Hin2. destruct Hin2 as [Hin2|Hin2].
            * rewrite Forall_forall in H3.
              apply (H3 v).
              simpl_inf. apply In_after. assumption.
              assumption.
            * destruct H6 as [_ [_ [_ H6]]].
              rewrite Forall_forall in H6. apply H6 in Hin2.
              contradiction. }
      * constructor. intros v contra.
        apply Intersection_inv in contra.
        destruct contra as [contra1 contra2].
        unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
        rewrite <- List.app_assoc in contra2.
        replace (v2::p22) with ([v2]++p22) in contra2 by reflexivity.
        rewrite 2!List.app_assoc in contra2.
        rewrite <- List.app_assoc in contra2.
        rewrite List.in_app_iff in contra2.
        { destruct contra2 as [contra2|contra2].
        - inversion H4. apply (H10 v). apply Intersection_intro; assumption.
        - rewrite List.in_app_iff in contra1, contra2.
          destruct contra1 as [contra1|contra1], contra2 as [contra2|contra2].
          + inversion H9.
            apply (H10 v). apply Intersection_intro; assumption.
          + destruct contra2; [subst v|contradiction].
            apply (support v22).
            simpl_inf. apply In_after. apply In_finite. assumption.
            apply H6.
          + destruct contra1; [subst v|contradiction].
            destruct H6 as [_ [_ [_ H6]]].
            rewrite Forall_forall in H6. apply (H6 v1).
            simpl_inf. apply In_after. apply In_finite. assumption.
            assumption.
          + destruct contra1; [subst v|contradiction].
            destruct contra2; [subst v22|contradiction].
            contradiction H8; reflexivity. }
    Qed.

    Lemma lemma53_aux2_aux3 : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
      Path g ((u::p1)%list++[v1]) -> V v1 ->
      Forall (Complement V) (u::p1) ->
      Path g ((u::p2)%list++[v2]) ->
      Forall (Complement V) (u::p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      forall v21 v22 p21 p22,
      Is_VPath g V v2 v21 (v2 :: p21) ->
      Is_VPath g V v2 v22 (v2 :: p22) ->
      Disjoint (Ensemble_of_list (p21 ++ [v21])) (Ensemble_of_list (p22 ++ [v22])) ->
      Disjoint (Ensemble_of_list (p2++[v2])) (Ensemble_of_list p21) ->
      Disjoint (Ensemble_of_list (p2++[v2])) (Ensemble_of_list p22) ->
      Weakly_deciding g V u.
    Proof.
      (* We removed some hypotheses from [lemma53_aux2_aux] and want to prove that
         the different configurations are special cases of it. *)
      intros.
      destruct (first_common V.eq_dec p1 p22) as [v|] eqn:Heqlast.
      - apply first_common_some_strong in Heqlast.
        destruct Heqlast as [l1 [l2 [q1 [q2 [Heqlast1 [Heqlast2 Heqlast3]]]]]].
        destruct (first_common V.eq_dec l1 p21) as [v0|] eqn:Heqlast0.
        + apply first_common_some in Heqlast0.
          destruct Heqlast0 as [l01 [l02 [q01 [q02 [Heqlast01 [Heqlast02 Heqlast03]]]]]].
          eapply lemma53_aux2_aux with (v1:=v1) (v22:=v21) (v:=v0) (l1:=l01);
            try eassumption.
          * constructor. intros u0 contra. apply Intersection_inv in contra.
            inversion H7. apply (H10 u0). split; apply contra.
          * rewrite Heqlast01 in Heqlast1. rewrite <- List.app_assoc in Heqlast1.
            simpl in Heqlast1. exact Heqlast1.
          * constructor. intros u0 contra. apply Intersection_inv in contra.
            destruct contra as [contra1 contra2].
            inversion Heqlast3. apply (H10 u0).
            apply Intersection_intro. rewrite Heqlast01.
            apply List.in_app_iff. left; assumption. assumption.
          * constructor. intros u0 contra. apply Intersection_inv in contra.
            destruct contra as [contra1 contra2].
            inversion H8. apply (H10 u0). apply Intersection_intro. assumption.
            rewrite Heqlast02. apply List.in_app_iff.
            right; right; assumption.
        + apply first_common_none in Heqlast0.
          eapply lemma53_aux2_aux with (v1:=v1) (v21:=v21);
            try eassumption.
          constructor. intros u0 contra. apply Intersection_inv in contra.
          destruct contra as [contra1 contra2].
          inversion H9. apply (H10 u0). apply Intersection_intro. assumption.
          rewrite Heqlast2. apply List.in_app_iff.
          right; right; assumption.
      - apply first_common_none in Heqlast.
        destruct (first_common V.eq_dec p1 p21) eqn:Heqlast0.
        + apply first_common_some_strong in Heqlast0.
          destruct Heqlast0 as [l1 [l2 [q1 [q2 [Heqlast1 [Heqlast2 Heqlast3]]]]]].
          eapply lemma53_aux2_aux with (v1:=v1) (v22:=v21); try eassumption.
          * constructor. intros u0 contra. apply Intersection_inv in contra.
            inversion H7. apply (H10 u0). split; apply contra.
          * constructor. intros u0 contra. apply Intersection_inv in contra.
            destruct contra as [contra1 contra2].
            inversion Heqlast. apply (H10 u0).
            apply Intersection_intro. rewrite Heqlast1.
            apply List.in_app_iff. left; assumption. assumption.
          * constructor. intros u0 contra. apply Intersection_inv in contra.
            destruct contra as [contra1 contra2].
            inversion H8. apply (H10 u0). apply Intersection_intro. assumption.
            rewrite Heqlast2. apply List.in_app_iff.
            right; right; assumption.
        + (* this is a case we could have handled earlier *)
          destruct (V.eq_dec v1 v21).
          * subst v21.
            eapply lemma53_aux2_aux2 with (v1:=v1) (v21:=v1) (v22:=v22);
              try eassumption.
            intros contra. subst v22. inversion H7.
            apply (H10 v1). apply Intersection_intro;
            apply List.in_app_iff; right; left; reflexivity.
          * apply first_common_none in Heqlast0.
            eapply lemma53_aux2_aux2 with (v1:=v1) (v22:=v21); try eassumption.
            constructor. intros v contra. apply Intersection_inv in contra.
            inversion H7. apply (H10 v). apply Intersection_intro; apply contra.
    Qed.

    Lemma lemma53_aux2_aux4 : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
      Path g ((u::p1)%list++[v1]) -> V v1 ->
      Forall (Complement V) (u::p1) ->
      Path g ((u::p2)%list++[v2]) ->
      Forall (Complement V) (u::p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      forall v21 v22 p21 p22,
      Is_VPath g V v2 v21 (v2 :: p21) ->
      Is_VPath g V v2 v22 (v2 :: p22) ->
      Disjoint (Ensemble_of_list (p21 ++ [v21])) (Ensemble_of_list (p22 ++ [v22])) ->
      List.NoDup (p2++[v2]) ->
      Disjoint (Ensemble_of_list (p2++[v2])) (Ensemble_of_list p21) ->
      Disjoint (Ensemble_of_list [v2]) (Ensemble_of_list p22) ->
      Weakly_deciding g V u.
    Proof.
      intros. destruct (last_common V.eq_dec p22 p2) as [v|] eqn:Heqlast.
      - apply last_common_some_strong in Heqlast.
        destruct Heqlast as [q1 [q2 [l1 [l2 [Heqlast1 [Heqlast2 Heqlast3]]]]]].
        eapply lemma53_aux2_aux3 with (v1:=v1) (v21:=v21) (v2:=v) (p2:=l1)
          (p21:=l2++v2::p21) (p22:=q2) (v22:=v22);
          try eassumption.
      + apply Path_cut with (p2:=l2++[v2]).
        setoid_replace (((u::l1)%list++[v])++(l2++[v2])%list)%inf
          with (u::(l1++v::l2)%list++[v2])%inf
          by (simpl_inf; reflexivity). rewrite <- Heqlast2.
        simpl_inf. simpl_inf in H2. assumption.
      + apply Forall_forall. intros u0 Hin contra. rewrite Forall_forall in H3.
        apply (H3 u0). rewrite Heqlast2. simpl_inf. rewrite app_comm_cons.
        apply in_app_l. assumption. assumption.
      + constructor. intros u0 contra. apply Intersection_inv in contra.
        inversion H4. apply (H11 u0).
        apply Intersection_intro. apply contra.
        rewrite Heqlast2. apply List.in_app_iff. left.
        replace (v::l2) with ([v]++l2) by reflexivity.
        rewrite List.app_assoc.
        apply List.in_app_iff. left. apply contra.
      + split; [reflexivity|]. split.
        * apply Path_finite_cut with (p1:=u::l1).
          setoid_replace ((u :: l1)%list ++ (v :: l2 ++ v2 :: p21)%list ++ [v21]%list)%inf
            with ((u::(l1++v::l2)++[v2])%list ++ p21++[v21])%inf
            by (simpl_inf; reflexivity).
          rewrite <- Heqlast2. apply Path_finite_app with (u:=v2).
          simpl_inf. simpl_inf in H2. assumption.
          destruct H5 as [_ [H5 _]]. simpl_inf. simpl_inf in H5. assumption.
          rewrite last.last_cons_change_default.
          rewrite last.last_app_not_nil by discriminate.
          reflexivity.
        * split; [apply H5|]. apply Forall_forall. intros u0 Hin contra.
          simpl_inf in Hin.
          rewrite app_comm_cons in Hin. apply in_app in Hin.
          destruct Hin as [Hin|Hin].
          rewrite Forall_forall in H3. apply (H3 u0). rewrite Heqlast2.
          simpl_inf.
          apply In_after. apply in_app_finite_r. assumption. assumption.
          destruct H5 as [_ [_ [_ H5]]]. rewrite Forall_forall in H5.
          apply (H5 u0). simpl_inf. assumption. assumption.
      + split; [reflexivity|]. split.
        * apply Path_finite_cut with (p1:=v2::q1).
          setoid_replace ((v2::q1)%list++(v::q2)%list++[v22])%inf
            with (v2::(q1++v::q2)%list++[v22])%inf
            by (simpl_inf; reflexivity). rewrite <- Heqlast1.
          simpl_inf. destruct H6 as [_ [H6 _]]. simpl_inf in H6. assumption.
        * split; [apply H6|]. apply Forall_forall. intros u0 Hin contra.
          destruct H6 as [_ [_ [_ H6]]]. rewrite Forall_forall in H6.
          apply (H6 u0). rewrite Heqlast1. simpl_inf. apply In_after.
          apply in_app_finite_r. assumption. assumption.
      + constructor. intros v0 contra. apply Intersection_inv in contra.
        destruct contra as [contra1 contra2].
        unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
        replace (v2::p21) with ([v2]++p21) in contra1 by reflexivity.
        rewrite List.app_assoc in contra1. rewrite <- List.app_assoc in contra1.
        rewrite List.in_app_iff in contra1.
        destruct contra1 as [contra1|contra1].
        * rewrite List.in_app_iff in contra1, contra2.
          { destruct contra1 as [contra1|contra1], contra2 as [contra2|contra2].
          - inversion Heqlast3. apply (H11 v0).
            apply Intersection_intro. assumption. rewrite Heqlast2.
            apply List.in_app_iff. right; right; assumption.
          - (* v22 in V *) destruct contra2; [subst v0|contradiction].
            rewrite Forall_forall in H3. apply (H3 v22).
            rewrite Heqlast2. simpl_inf. apply In_after. apply in_app_finite_r.
            apply In_after. apply In_finite. assumption. apply H6.
          - (* ?? *) inversion H10. apply (H11 v0). apply Intersection_intro.
            assumption. rewrite Heqlast1. apply List.in_app_iff.
            right; right; assumption.
          - (* v2 not in V, v22 in V *)
            destruct contra2; [subst v0|contradiction].
            destruct contra1; [subst v22|contradiction].
            destruct H5 as [_ [_ [_ H5]]]. rewrite Forall_forall in H5.
            apply (H5 v2). apply In_here. apply H6. }
        * (* p21 and p22 disjoint *) inversion H7. apply (H11 v0).
          apply Intersection_intro. assumption. rewrite Heqlast1.
          rewrite <- List.app_assoc. apply List.in_app_iff.
          right; right; assumption.
      + constructor. intros v0 contra. apply Intersection_inv in contra.
        destruct contra as [contra1 contra2].
        unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
        replace (v2::p21) with ([v2]++p21) in contra2 by reflexivity.
        rewrite List.app_assoc in contra2. rewrite List.in_app_iff in contra2.
        destruct contra2 as [contra2|contra2].
        * (* NoDup *) rewrite Heqlast2 in H8.
          replace (v::l2) with ([v]++l2) in H8 by reflexivity.
          rewrite List.app_assoc in H8. rewrite <- List.app_assoc in H8.
          apply NoDup_Disjoint in H8. inversion H8. apply (H11 v0).
          apply Intersection_intro; assumption.
        * (* p2 and p21 disjoint *)
          inversion H9. apply (H11 v0). apply Intersection_intro.
          rewrite Heqlast2. apply List.in_app_iff. left.
          replace (v::l2) with ([v]++l2) by reflexivity.
          rewrite List.app_assoc. rewrite List.in_app_iff. left; assumption.
          assumption.
      + (* Heqlast3 *)
        constructor. intros v0 contra. apply Intersection_inv in contra.
        inversion Heqlast3. apply (H11 v0). apply Intersection_intro.
        apply contra. rewrite Heqlast2.
        replace (v::l2) with ([v]++l2) by reflexivity.
        rewrite List.app_assoc. apply List.in_app_iff. left; apply contra.
    - apply last_common_none in Heqlast.
      eapply lemma53_aux2_aux3 with (v1:=v1) (v21:=v21); try eassumption.
      constructor. intros v contra. apply Intersection_inv in contra.
      destruct contra as [contra1 contra2].
      unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
      rewrite List.in_app_iff in contra1. destruct contra1.
      + inversion Heqlast. apply (H12 v). apply Intersection_intro; assumption.
      + inversion H10. apply (H12 v). apply Intersection_intro; assumption.
    Qed.

    Lemma lemma53_aux2_aux5 : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
      Path g ((u::p1)%list++[v1]) -> V v1 ->
      Forall (Complement V) (u::p1) ->
      Path g ((u::p2)%list++[v2]) ->
      Forall (Complement V) (u::p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      forall v21 v22 p21 p22,
      Is_VPath g V v2 v21 (v2 :: p21) ->
      Is_VPath g V v2 v22 (v2 :: p22) ->
      Disjoint (Ensemble_of_list (p21 ++ [v21])) (Ensemble_of_list (p22 ++ [v22])) ->
      List.NoDup (p2++[v2]) ->
      Disjoint (Ensemble_of_list (p2++[v2])) (Ensemble_of_list p21) ->
      Weakly_deciding g V u.
    Proof.
      intros. destruct (last_common V.eq_dec p22 [v2]) as [v|] eqn:Heqlast.
      - apply last_common_some_strong in Heqlast.
        destruct Heqlast as [q1 [q2 [l1 [l2 [Heqlast1 [Heqlast2 Heqlast3]]]]]].
        destruct l1; [|inversion Heqlast2; destruct l1; discriminate].
        simpl in Heqlast2.
        destruct l2; [|inversion Heqlast2]. inversion Heqlast2. subst v.
        eapply lemma53_aux2_aux4 with (v1:=v1) (v21:=v21) (p22:=q2) (v22:=v22);
          try eassumption.
        + split; [reflexivity|]. split.
          * apply Path_finite_cut with (v2::q1).
            setoid_replace ((v2::q1)%list++(v2::q2)%list++[v22])%inf
              with (v2::(q1++v2::q2)%list++[v22])%inf
              by (simpl_inf; reflexivity).
            rewrite <- Heqlast1.
            destruct H6 as [_ [H6 _]]. simpl_inf. simpl_inf in H6. assumption.
          * split; [apply H6|]. apply Forall_forall. intros v Hin contra.
            destruct H6 as [_ [_ [_ H6]]]. rewrite Forall_forall in H6.
            apply (H6 v). rewrite Heqlast1. simpl_inf. apply In_after.
            apply in_app_finite_r. assumption. assumption.
        + constructor. intros v contra. apply Intersection_inv in contra.
          inversion H7. apply (H10 v). apply Intersection_intro.
          apply contra. rewrite Heqlast1. rewrite <- List.app_assoc.
          apply List.in_app_iff. right; right; apply contra.
        + constructor. intros v contra. apply Intersection_inv in contra.
          inversion Heqlast3. apply (H10 v).
          apply Intersection_intro; apply contra.
      - apply last_common_none in Heqlast.
        eapply lemma53_aux2_aux4 with (v1:=v1) (v21:=v21); try eassumption.
        constructor. intros v contra. apply Intersection_inv in contra.
        inversion Heqlast. apply (H10 v). apply Intersection_intro; apply contra.
    Qed.

    Lemma lemma53_aux2_aux6 : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
      Path g ((u::p1)%list++[v1]) -> V v1 ->
      Forall (Complement V) (u::p1) ->
      Path g ((u::p2)%list++[v2]) ->
      Forall (Complement V) (u::p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      forall v21 v22 p21 p22,
      Is_VPath g V v2 v21 (v2 :: p21) ->
      Is_VPath g V v2 v22 (v2 :: p22) ->
      Disjoint (Ensemble_of_list (p21 ++ [v21])) (Ensemble_of_list (p22 ++ [v22])) ->
      List.NoDup (p2++[v2]) ->
      Disjoint (Ensemble_of_list [v2]) (Ensemble_of_list p21) ->
      Weakly_deciding g V u.
    Proof.
      intros. destruct (last_common V.eq_dec p21 p2) as [v|] eqn:Heqlast.
      - apply last_common_some_strong in Heqlast.
        destruct Heqlast as [q1 [q2 [l1 [l2 [Heqlast1 [Heqlast2 Heqlast3]]]]]].
        eapply lemma53_aux2_aux5 with (v1:=v1) (v21:=v21) (p2:=l1) (v2:=v)
          (p21:=q2) (p22:=l2++v2::p22) (v22:=v22);
          try eassumption.
      + apply Path_cut with (p2:=l2++[v2]).
        setoid_replace (((u::l1)%list++[v])++(l2++[v2])%list)%inf
          with (u::(l1++v::l2)%list++[v2])%inf
          by (simpl_inf; reflexivity). rewrite <- Heqlast2.
        simpl_inf. simpl_inf in H2. assumption.
      + apply Forall_forall. intros u0 Hin contra. rewrite Forall_forall in H3.
        apply (H3 u0). rewrite Heqlast2. simpl_inf. rewrite app_comm_cons.
        apply in_app_l. assumption. assumption.
      + constructor. intros u0 contra. apply Intersection_inv in contra.
        inversion H4. apply (H10 u0).
        apply Intersection_intro. apply contra.
        rewrite Heqlast2. apply List.in_app_iff. left.
        replace (v::l2) with ([v]++l2) by reflexivity.
        rewrite List.app_assoc.
        apply List.in_app_iff. left. apply contra.
      + split; [reflexivity|]. split.
        * apply Path_finite_cut with (p1:=v2::q1).
          setoid_replace ((v2::q1)%list++(v::q2)%list++[v21])%inf
            with (v2::(q1++v::q2)%list++[v21])%inf
            by (simpl_inf; reflexivity). rewrite <- Heqlast1.
          simpl_inf. destruct H5 as [_ [H5 _]]. simpl_inf in H5. assumption.
        * split; [apply H5|]. apply Forall_forall. intros u0 Hin contra.
          destruct H5 as [_ [_ [_ H5]]]. rewrite Forall_forall in H5.
          apply (H5 u0). rewrite Heqlast1. simpl_inf. apply In_after.
          apply in_app_finite_r. assumption. assumption.
      + split; [reflexivity|]. split.
        * apply Path_finite_cut with (p1:=u::l1).
          setoid_replace ((u :: l1)%list ++ (v :: l2 ++ v2 :: p22)%list ++ [v22]%list)%inf
            with ((u::(l1++v::l2)++[v2])%list ++ p22++[v22])%inf
            by (simpl_inf; reflexivity).
          rewrite <- Heqlast2. apply Path_finite_app with (u:=v2).
          simpl_inf. simpl_inf in H2. assumption.
          destruct H6 as [_ [H6 _]]. simpl_inf. simpl_inf in H6. assumption.
          rewrite last.last_cons_change_default.
          rewrite last.last_app_not_nil by discriminate.
          reflexivity.
        * split; [apply H6|]. apply Forall_forall. intros u0 Hin contra.
          simpl_inf in Hin.
          rewrite app_comm_cons in Hin. apply in_app in Hin.
          destruct Hin as [Hin|Hin].
          rewrite Forall_forall in H3. apply (H3 u0). rewrite Heqlast2.
          simpl_inf.
          apply In_after. apply in_app_finite_r. assumption. assumption.
          destruct H6 as [_ [_ [_ H6]]]. rewrite Forall_forall in H6.
          apply (H6 u0). simpl_inf. assumption. assumption.
      + constructor. intros v0 contra. apply Intersection_inv in contra.
        destruct contra as [contra1 contra2].
        unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
        replace (v2::p22) with ([v2]++p22) in contra2 by reflexivity.
        rewrite List.app_assoc in contra2. rewrite <- List.app_assoc in contra2.
        rewrite List.in_app_iff in contra2.
        destruct contra2 as [contra2|contra2].
        * rewrite List.in_app_iff in contra1, contra2.
          { destruct contra1 as [contra1|contra1], contra2 as [contra2|contra2].
          - inversion Heqlast3. apply (H10 v0).
            apply Intersection_intro. assumption. rewrite Heqlast2.
            apply List.in_app_iff. right; right; assumption.
          - (* ?? *) inversion H9. apply (H10 v0). apply Intersection_intro.
            assumption. rewrite Heqlast1. apply List.in_app_iff.
            right; right; assumption.
          - (* v21 in V *) destruct contra1; [subst v0|contradiction].
            rewrite Forall_forall in H3. apply (H3 v21).
            rewrite Heqlast2. simpl_inf. apply In_after. apply in_app_finite_r.
            apply In_after. apply In_finite. assumption. apply H5.
          - (* v2 not in V, v21 in V *)
            destruct contra1; [subst v0|contradiction].
            destruct contra2; [subst v21|contradiction].
            destruct H6 as [_ [_ [_ H6]]]. rewrite Forall_forall in H6.
            apply (H6 v2). apply In_here. apply H5. }
        * (* p21 and p22 disjoint *) inversion H7. apply (H10 v0).
          apply Intersection_intro. rewrite Heqlast1.
          rewrite <- List.app_assoc. apply List.in_app_iff.
          right; right; assumption. assumption.
      + apply (NoDup_app (l1++[v]) (l2++[v2])). rewrite List.app_assoc.
        rewrite <- List.app_assoc with (n:=l2). simpl.
        rewrite <- Heqlast2. assumption.
      + (* Heqlast3 *)
        constructor. intros v0 contra. apply Intersection_inv in contra.
        inversion Heqlast3. apply (H10 v0). apply Intersection_intro.
        apply contra. rewrite Heqlast2.
        replace (v::l2) with ([v]++l2) by reflexivity.
        rewrite List.app_assoc. apply List.in_app_iff. left; apply contra.
    - apply last_common_none in Heqlast.
      eapply lemma53_aux2_aux5 with (v1:=v1) (v21:=v21); try eassumption.
      constructor. intros v contra. apply Intersection_inv in contra.
      destruct contra as [contra1 contra2].
      unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
      rewrite List.in_app_iff in contra1. destruct contra1.
      + inversion Heqlast. apply (H11 v). apply Intersection_intro; assumption.
      + inversion H9. apply (H11 v). apply Intersection_intro; assumption.
    Qed.

    Lemma lemma53_aux2_aux7 : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
      Path g ((u::p1)%list++[v1]) -> V v1 ->
      Forall (Complement V) (u::p1) ->
      Path g ((u::p2)%list++[v2]) ->
      Forall (Complement V) (u::p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      forall v21 v22 p21 p22,
      Is_VPath g V v2 v21 (v2 :: p21) ->
      Is_VPath g V v2 v22 (v2 :: p22) ->
      Disjoint (Ensemble_of_list (p21 ++ [v21])) (Ensemble_of_list (p22 ++ [v22])) ->
      List.NoDup (p2++[v2]) ->
      Weakly_deciding g V u.
    Proof.
      intros. destruct (last_common V.eq_dec p21 [v2]) as [v|] eqn:Heqlast.
      - apply last_common_some_strong in Heqlast.
        destruct Heqlast as [q1 [q2 [l1 [l2 [Heqlast1 [Heqlast2 Heqlast3]]]]]].
        destruct l1; [|inversion Heqlast2; destruct l1; discriminate].
        simpl in Heqlast2.
        destruct l2; [|inversion Heqlast2]. inversion Heqlast2. subst v.
        eapply lemma53_aux2_aux6 with (v1:=v1) (v21:=v21) (p21:=q2);
          try eassumption.
        + split; [reflexivity|]. split.
          * apply Path_finite_cut with (v2::q1).
            setoid_replace ((v2::q1)%list++(v2::q2)%list++[v21])%inf
              with (v2::(q1++v2::q2)%list++[v21])%inf
              by (simpl_inf; reflexivity).
            rewrite <- Heqlast1.
            destruct H5 as [_ [H5 _]]. simpl_inf. simpl_inf in H5. assumption.
          * split; [apply H5|]. apply Forall_forall. intros v Hin contra.
            destruct H5 as [_ [_ [_ H5]]]. rewrite Forall_forall in H5.
            apply (H5 v). rewrite Heqlast1. simpl_inf. apply In_after.
            apply in_app_finite_r. assumption. assumption.
        + constructor. intros v contra. apply Intersection_inv in contra.
          inversion H7. apply (H9 v). apply Intersection_intro.
          rewrite Heqlast1. rewrite <- List.app_assoc.
          apply List.in_app_iff. right; right; apply contra. apply contra.
        + constructor. intros v contra. apply Intersection_inv in contra.
          inversion Heqlast3. apply (H9 v).
          apply Intersection_intro; apply contra.
      - apply last_common_none in Heqlast.
        eapply lemma53_aux2_aux6 with (v1:=v1) (v21:=v21); try eassumption.
        constructor. intros v contra. apply Intersection_inv in contra.
        inversion Heqlast. apply (H9 v). apply Intersection_intro; apply contra.
    Qed.

    Lemma lemma53_aux2_aux8 : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
      Path g ((u::p1)%list++[v1]) -> V v1 ->
      Forall (Complement V) (u::p1) ->
      Path g ((u::p2)%list++[v2]) ->
      Forall (Complement V) (u::p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      forall v21 v22 p21 p22,
      Is_VPath g V v2 v21 (v2 :: p21) ->
      Is_VPath g V v2 v22 (v2 :: p22) ->
      Disjoint (Ensemble_of_list (p21 ++ [v21])) (Ensemble_of_list (p22 ++ [v22])) ->
      Weakly_deciding g V u.
    Proof.
      intros.
      remember (remove_cycles V.eq_dec (p2++[v2])) as l.
      assert (exists l1, l = l1 ++ [v2]) as Ha.
      { destruct (List.rev l) as [|v l0] eqn:Heqrev.
        - apply (f_equal (List.rev (A:=V.t))) in Heqrev.
          rewrite List.rev_involutive in Heqrev. simpl in Heqrev. subst l.
          rewrite remove_cycles_nil in Heqrev. destruct p2; discriminate.
        - exists (List.rev l0).
          apply (f_equal (List.rev (A:=V.t))) in Heqrev.
          rewrite List.rev_involutive in Heqrev. simpl in Heqrev.
          pose proof (remove_cycles_keeps_last V.eq_dec v2 (p2++[v2])).
          rewrite <- Heql in H8. rewrite Heqrev in H8.
          rewrite 2!last.last_app_not_nil in H8 by discriminate.
          simpl in H8. subst v. assumption.
      }
      destruct Ha as [l1 Ha].
      eapply lemma53_aux2_aux7 with (v1:=v1) (v21:=v21) (p2:=l1);
        try eassumption.
      - setoid_replace ((u::l1)%list++[v2])%inf with (u::(l1++[v2])%list)%inf
          by (simpl_inf; reflexivity).
        rewrite <- Ha.
        assert(Path g l) as Ha2.
        { rewrite Heql. apply remove_cycles_stays_Path.
          apply Path_finite_cut with (p1:=[u]). simpl_inf.
          simpl_inf in H2. assumption.
        }
        destruct l as [|v l]; [destruct l1; discriminate|].
        setoid_replace ((u::p2)%list++[v2])%inf with (u::(p2++[v2])%list)%inf
          in H2 by (simpl_inf; reflexivity).
        destruct (p2++[v2]) as [|v0 ?] eqn:Heqp2; [destruct p2; discriminate|].
        pose proof (remove_cycles_keeps_hd V.eq_dec u (v0::l0)).
        rewrite <- Heql in H8. simpl in H8. subst v0.
        simpl_inf. simpl_inf in H2. split.
        + constructor.
          * destruct H2 as [H2 _].
            inversion H2; assumption.
          * apply Ha2.
        + constructor.
          * apply Ha2.
          * constructor. destruct H2 as [_ H2]. inversion H2. inversion H11.
            assumption.
      - assert(~ List.In v2 l1) as Ha2.
        { intros contra. pose proof (remove_cycles_NoDup V.eq_dec (p2++[v2])).
          rewrite <- Heql, Ha in H8.
          apply List.NoDup_remove in H8. destruct H8 as [_ H8].
          rewrite List.app_nil_r in H8. contradiction.
        }
        apply Forall_forall. intros v Hin contra.
        rewrite Forall_forall in H3. apply (H3 v); [|assumption]. inversion Hin.
        + apply In_here.
        + apply In_after. apply In_finite in H9. apply In_finite.
          assert(List.In v l) as Ha3.
          { rewrite Ha. apply List.in_app_iff. left; assumption. }
          rewrite Heql in Ha3. apply remove_cycles_incl in Ha3.
          rewrite List.in_app_iff in Ha3. destruct Ha3.
          assumption.
          destruct H11; [subst v|contradiction]. contradiction.
      - constructor. intros v contra. apply Intersection_inv in contra.
        destruct contra as [contra1 contra2].
        inversion H4. apply (H8 v). apply Intersection_intro. assumption.
        apply (remove_cycles_incl V.eq_dec). rewrite <- Heql. rewrite Ha.
        assumption.
      - rewrite <- Ha, Heql. apply remove_cycles_NoDup.
    Qed.

    Lemma lemma53_aux2 : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
      Path g ((u::p1)%list++[v1]) -> V v1 ->
      Forall (Complement V) (u::p1) ->
      Path g ((u::p2)%list++[v2]) -> Weakly_deciding g V v2 ->
      Forall (Complement V) (u::p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      Weakly_deciding g V u.
    Proof.
      intros. destruct H3 as [v21 [v22 [p21 [p22 [H31 [H32 H33]]]]]].
      eapply lemma53_aux2_aux8 with (v1:=v1) (p21:=p21); try eassumption.
    Qed.

    Lemma lemma53_aux3_aux : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
      Path g ((u::p1)%list++[v1]) -> Weakly_deciding g V v1 ->
      Forall (Complement (Union V (Weakly_deciding g V))) (u::p1) ->
      Path g ((u::p2)%list++[v2]) ->
      Forall (Complement V) (u::p2) ->
      Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
      forall v21 v22 p21 p22,
      Is_VPath g V v2 v21 (v2 :: p21) ->
      Is_VPath g V v2 v22 (v2 :: p22) ->
      Disjoint (Ensemble_of_list (p21 ++ [v21])) (Ensemble_of_list (p22 ++ [v22])) ->
      Disjoint (Ensemble_of_list p21) (Ensemble_of_list [v1]) ->
      Weakly_deciding g V u.
    Proof.
      intros.
      destruct (last_common V.eq_dec p1 p21) as [v|] eqn:Heqlast.
      - apply last_common_some in Heqlast.
        destruct Heqlast as [l11 [l12 [l21 [l22 [Heqlast1 [Heqlast2 Heqlast3]]]]]].
        assert(Weakly_deciding g V v) as Ha.
        { eapply lemma53_aux2 with (v1:=v21) (p1:=l22) (v2:=v1) (p2:=l12);
            try eassumption.
          - apply Path_finite_cut with (p1:=v2::l21).
            setoid_replace ((v2 :: l21)%list ++ (v :: l22)%list ++ [v21])%inf
              with (v2 :: (l21++v::l22)%list ++ [v21])%inf
              by (simpl_inf; reflexivity).
            rewrite <- Heqlast2. destruct H5 as [_ [H5 _]].
            simpl_inf. simpl_inf in H5. assumption.
          - apply H5.
          - destruct H5 as [_ [_ [_ H5]]]. apply Forall_forall.
            intros v0 Hin contra. rewrite Forall_forall in H5.
            apply (H5 v0).
            rewrite Heqlast2. simpl_inf. apply In_after. apply in_app_finite_r.
            assumption.
            assumption.
          - apply Path_finite_cut with (p1:=u::l11).
            setoid_replace ((u :: l11)%list ++ (v :: l12)%list ++ [v1])%inf
              with (u :: (l11++v::l12)%list ++ [v1])%inf
              by (simpl_inf; reflexivity).
            rewrite <- Heqlast1.
            simpl_inf. simpl_inf in H. assumption.
          - apply Forall_forall.
            intros v0 Hin contra. rewrite Forall_forall in H1.
            apply (H1 v0).
            rewrite Heqlast1. simpl_inf. apply In_after. apply in_app_finite_r.
            assumption.
            apply Union_introl. assumption.
          - constructor. intros v0 contra. apply Intersection_inv in contra.
            destruct contra as [contra1 contra2].
            unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
            rewrite List.in_app_iff in contra1, contra2.
            destruct contra1 as [contra1|contra1], contra2 as [contra2|contra2].
            + inversion Heqlast3. apply (H9 v0).
              apply Intersection_intro; assumption.
            + destruct contra2; [subst v0|contradiction]. inversion H8.
              apply (H9 v1). apply Intersection_intro.
              rewrite Heqlast2. apply List.in_app_iff. right; right; assumption.
              left; reflexivity.
            + destruct contra1; [subst v0|contradiction].
              rewrite Forall_forall in H1. apply (H1 v21).
              rewrite Heqlast1. simpl_inf. apply In_after. apply in_app_finite_r.
              apply In_after. apply In_finite. assumption.
              apply Union_introl. apply H5.
            + destruct contra1; [subst v0|contradiction].
              destruct contra2; [subst v21|contradiction].
              apply (Weakly_deciding_not_In _ _ _ H0). apply H5.
        }
        rewrite Forall_forall in H1. contradiction (H1 v).
        rewrite Heqlast1. simpl_inf. apply In_after. apply in_app_finite_r.
        apply In_here.
        apply Union_intror. assumption.
    - apply last_common_none in Heqlast.
      eapply lemma53_aux2 with (v1:=v21) (p1:=p2++v2::p21) (v2:=v1) (p2:=p1);
        try eassumption.
      + setoid_replace ((u::p2++v2::p21)%list++[v21])%inf
          with ((u::p2++[v2])%list ++ (p21++[v21]))%inf
          by (simpl_inf; reflexivity).
        apply Path_finite_app with (u:=v2).
        * simpl_inf in H2. simpl_inf. assumption.
        * destruct H5 as [_ [H5 _]]. simpl_inf. simpl_inf in H5. assumption.
        * rewrite last.last_cons_change_default.
          rewrite last.last_app_not_nil by discriminate.
          reflexivity.
      + apply H5.
      + apply Forall_forall. intros v Hin contra.
        simpl_inf in Hin. rewrite app_comm_cons in Hin. apply in_app in Hin.
        destruct Hin.
        * rewrite Forall_forall in H3. apply (H3 v); assumption.
        * destruct H5 as [_ [_ [_ H5]]]. rewrite Forall_forall in H5.
          apply (H5 v); assumption.
      + apply Forall_forall. intros v Hin contra. rewrite Forall_forall in H1.
        apply (H1 v). assumption. apply Union_introl. assumption.
      + constructor. intros v contra. apply Intersection_inv in contra.
        destruct contra as [contra1 contra2].
        unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
        replace (v2::p21) with ([v2]++p21) in contra1 by reflexivity.
        rewrite List.app_assoc in contra1. rewrite <- List.app_assoc in contra1.
        rewrite List.in_app_iff in contra1.
        destruct contra1 as [contra1|contra1].
        * inversion H4. apply (H9 v). apply Intersection_intro; assumption.
        * rewrite List.in_app_iff in contra1, contra2.
          { destruct contra1 as [contra1|contra1], contra2 as [contra2|contra2].
          - inversion Heqlast. apply (H9 v).
            apply Intersection_intro; assumption.
          - destruct contra2; [subst v|contradiction].
            inversion H8. apply (H9 v1). apply Intersection_intro.
            assumption. left; reflexivity.
          - destruct contra1; [subst v|contradiction].
            rewrite Forall_forall in H1. apply (H1 v21).
            apply In_after. apply In_finite. assumption.
            apply Union_introl. apply H5.
          - destruct contra1; [subst v|contradiction].
            destruct contra2; [subst v21|contradiction].
            apply (Weakly_deciding_not_In _ _ _ H0). apply H5. }
    Qed.

  Lemma lemma53_aux3 : forall g (V:Ensemble V.t) u v1 v2 p1 p2,
    Path g ((u::p1)%list++[v1]) -> Weakly_deciding g V v1 ->
    Forall (Complement (Union V (Weakly_deciding g V))) (u::p1) ->
    Path g ((u::p2)%list++[v2]) -> Weakly_deciding g V v2 ->
    Forall (Complement V) (u::p2) ->
    Disjoint (Ensemble_of_list (p1 ++ [v1])) (Ensemble_of_list (p2 ++ [v2])) ->
    Weakly_deciding g V u.
  Proof.
    intros. destruct H3 as [v21 [v22 [p21 [p22 [H31 [H32 H33]]]]]].
    destruct (List.in_dec V.eq_dec v1 p21).
    - eapply lemma53_aux3_aux with (v1:=v1) (v21:=v22) (v22:=v21);
        try eassumption.
      + constructor. intros v contra. apply Intersection_inv in contra.
        inversion H33. apply (H3 v). apply Intersection_intro; apply contra.
      + constructor. intros v contra. apply Intersection_inv in contra.
        destruct contra as [contra1 contra2].
        unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
        destruct contra2; [subst v|contradiction].
        inversion H33. apply (H3 v1).
        apply Intersection_intro; apply List.in_app_iff; left; assumption.
  - eapply lemma53_aux3_aux with (v1:=v1) (v21:=v21);
        try eassumption.
    constructor. intros v contra. apply Intersection_inv in contra.
    destruct contra as [contra1 contra2].
    unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
    destruct contra2; [subst v|contradiction]. contradiction.
  Qed.

  End lemma53_preresults.

  (** *** Lemma53 *)
  (** Finally... *)
  Lemma lemma53 : forall g V,
    Weakly_deciding g (Union V (Weakly_deciding g V)) == Empty_set.
  Proof.
    intros. split; [|apply Included_Empty]. unfold Included; intros u H.
    exfalso.
    destruct H as [v1 [v2 [l1 [l2 [HH1 [HH2 HH3]]]]]].
    pose proof HH1 as [_ [_ [_ support]]]. simpl_inf in support.
    inversion support as [|? ? support1 _]; subst.
    apply support1. apply Union_intror. clear support support1.

    pose proof HH1 as [_ [_ [union1 _]]]. pose proof HH2 as [_ [_ [union2 _]]].
    apply Union_inv in union1. apply Union_inv in union2.
    destruct union1 as [union1 | union1], union2 as [union2|union2].
    - eapply lemma53_aux with (v1:=v1); eassumption.
    - unfold Is_VPath in HH1, HH2. decompose [and] HH1. decompose [and] HH2.
      eapply lemma53_aux2 with (v1:=v1); try eassumption.
      + apply Forall_forall. rewrite Forall_forall in H3.
        intros v Hin contra. apply H3 in Hin. apply Hin.
        apply Union_introl; assumption.
      + apply Forall_forall. rewrite Forall_forall in H7.
        intros v Hin contra. apply H7 in Hin. apply Hin.
        apply Union_introl; assumption.
    - unfold Is_VPath in HH1, HH2. decompose [and] HH1. decompose [and] HH2.
      eapply lemma53_aux2 with (v2:=v1); try eassumption.
      + apply Forall_forall. rewrite Forall_forall in H7.
        intros v Hin contra. apply H7 in Hin. apply Hin.
        apply Union_introl; assumption.
      + apply Forall_forall. rewrite Forall_forall in H3.
        intros v Hin contra. apply H3 in Hin. apply Hin.
        apply Union_introl; assumption.
      + inversion HH3. constructor. intros v contra. apply (H6 v).
        apply Intersection_inv in contra. destruct contra.
        apply Intersection_intro; assumption.
    - unfold Is_VPath in HH1, HH2. decompose [and] HH1. decompose [and] HH2.
      eapply lemma53_aux3 with (v1:=v1); try eassumption.
      apply Forall_forall. rewrite Forall_forall in H7.
      intros v Hin contra. apply H7 in Hin. apply Hin.
      apply Union_introl; assumption.
  Qed.

End AbstractGraphPropertiesDec.

Module AbstractGraphPropertiesDecFinite (V : UsualDecidableType)
  (Import C : COMBINE_TYPE)
  (Import G : ABSTRACT_FINITE_GRAPH V C).

  Include AbstractGraphPropertiesDec V C G.

  Lemma Support_In_dec : forall g u, Support g u \/ ~ Support g u.
  Proof.
    intros. apply Finite_In_dec.
    exact V.eq_dec.
    exact (Support_finite g).
  Qed.

 Lemma lemma52 : forall g V1 V2
    (V2_In_dec : forall u, V2 u \/ ~ V2 u),
    Included V1 V2 -> Included (Weakly_deciding g V1)
                               (Union V2 (Weakly_deciding g V2)).
  Proof.
    intros.
    unfold Included; intros v Hin.
    destruct (V2_In_dec v) as [|not_in];
      [apply Union_introl; assumption|].
    apply Union_intror.
    unfold Ensembles.In, Weakly_deciding in Hin.
    destruct Hin as (v1 & v2 & p1 & p2 & Hin1 & Hin2 & Hin3).

    destruct (Exists_first V2 V2_In_dec (p1++[v1]))
      as (l11 & u1 & l12 & Heq1 & HinV21 & HForall1).
    - apply List.Exists_exists. exists v1. split.
      + apply List.in_app_iff. right; left; reflexivity.
      + apply H. apply Hin1.
    - destruct (Exists_first V2 V2_In_dec (p2++[v2]))
      as (l21 & u2 & l22 & Heq2 & HinV22 & HForall2).
      + apply List.Exists_exists. exists v2. split.
        * apply List.in_app_iff. right; left; reflexivity.
        * apply H. apply Hin2.
      + exists u1, u2, l11, l21.
        split; [|split].
        * split; [reflexivity|]. { split.
          - apply Path_cut with (p2:=l12). simpl_inf. regroup_finite.
            rewrite <- Heq1. unfold Is_VPath in Hin1. simpl_inf.
            simpl_inf in Hin1. apply Hin1.
          - split; [assumption|]. simpl_inf.
            constructor. assumption.
            apply Forall_forall. intros.
            rewrite List.Forall_forall in HForall1.
            apply In_finite in H0. apply HForall1 in H0.
            assumption. }
        * split; [reflexivity|]. { split.
          - apply Path_cut with (p2:=l22). simpl_inf. regroup_finite.
            rewrite <- Heq2. unfold Is_VPath in Hin2. simpl_inf.
            simpl_inf in Hin2. apply Hin2.
          - split; [assumption|]. simpl_inf.
            constructor. assumption.
            apply Forall_forall. intros.
            rewrite List.Forall_forall in HForall2.
            apply In_finite in H0. apply HForall2 in H0.
            assumption. }
        * constructor. intros u0 contra. apply Intersection_inv in contra.
          destruct contra as [contra1 contra2].
          unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
          rewrite List.in_app_iff in contra1, contra2.
          { destruct contra1 as [contra1|contra1], contra2 as [contra2|contra2].
          - inversion Hin3. apply (H0 u0). apply Intersection_intro.
            rewrite Heq1. apply List.in_app_iff. left; assumption.
            rewrite Heq2. apply List.in_app_iff. left; assumption.
          - destruct contra2; [subst u0|contradiction].
            rewrite List.Forall_forall in HForall1. apply HForall1 in contra1.
            contradiction.
          - destruct contra1; [subst u0|contradiction].
            rewrite List.Forall_forall in HForall2. apply HForall2 in contra2.
            contradiction.
          - destruct contra1; [subst u0|contradiction].
            destruct contra2; [subst u2|contradiction].
            inversion Hin3. apply (H0 u1).
            apply Intersection_intro.
            rewrite Heq1. apply List.in_app_iff. right; left; reflexivity.
            rewrite Heq2. apply List.in_app_iff. right; left; reflexivity. }
  Qed.

  Lemma Reachable_from_Included : forall g V,
    Included (Reachable_from g V) (Union V (Support g)).
  Proof.
    intros.
    unfold Included; intros v H0.
    destruct H0 as (u & H0 & H1).
    destruct H1 as (p & H1 & H2 & H3).
    destruct p.
    - left. simpl in H3. subst v. assumption.
    - right. destruct H1 as [H1 _].
      rewrite Forall_forall in H1. apply H1. apply In_finite.
      rewrite <- H3. apply last.last_not_empty_In.
      discriminate.
  Qed.

  Lemma Included_Reachable_from : forall g V1 V2,
    Included V1 V2 -> Included (Reachable_from g V1) (Reachable_from g V2).
  Proof.
    intros. intros x ?.
    destruct H0 as (u & H01 & H02).
    exists u. split; [apply H; assumption|]. assumption.
  Qed.

  Definition Reachable_n g n u v :=
    exists (p:list V.t), Path g p /\
      List.hd u p = u /\ List.last p u = v /\ List.length p <= n.

  Lemma Reachable_n_Sn : forall g n u v,
    Reachable_n g n u v -> Reachable_n g (S n) u v.
  Proof.
    intros. destruct H as (p & H1 & H2 & H3 & H4).
    exists p. split; [assumption|]. split; [assumption|]. split; [assumption|].
    apply le_S. assumption.
  Qed.

  Lemma Reachable_n_1_Sn : forall g n, n >= 1 -> forall u l u0 v,
    Rel g u l u0 -> Reachable_n g n u0 v -> Reachable_n g (S n) u v.
  Proof.
    intros.
    destruct H1 as (p & H11 & H12 & H13 & H14).
    destruct p as [|w p].
    - simpl in H12, H13. subst. exists [u;v].
      split.
      + split.
        * apply Support_spec in H0. constructor; [apply H0|].
          constructor; [apply H0|]. constructor.
        * repeat constructor. exists l; assumption.
      + split; [reflexivity|]. split; [reflexivity|]. simpl. apply le_n_S.
        assumption.
    - simpl in H12. subst w. exists (u::u0::p).
      split.
      + simpl_inf. split.
        * { constructor.
          - apply Support_spec in H0. apply H0.
          - apply H11. }
        * { constructor.
          - apply H11.
          - constructor. exists l; assumption. }
      + split; [reflexivity|]. split.
        * rewrite 2!last.last_cons_change_default.
          rewrite last.last_cons_change_default in H13.
          assumption.
        * simpl. apply le_n_S. assumption.
  Qed.

  Lemma Reachable_Sn_n : forall g n u v,
    Reachable_n g (S n) u v ->
      Reachable_n g n u v \/
      exists u0, (exists l, Rel g u l u0) /\ Reachable_n g n u0 v.
  Proof.
    intros.
    - destruct n.
      + left. destruct H as (p & H1 & H2 & H3 & H4).
        destruct p as [|w p].
        * simpl in H2, H3. subst v. exists [].
          split; [repeat constructor|]. split; [reflexivity|].
          split; [reflexivity|]. reflexivity.
        * simpl in H2. subst w. simpl in H4. apply le_S_n in H4. inversion H4.
          apply List.length_zero_iff_nil in H0. subst p.
          simpl in H3. subst v. exists [].
          split; [repeat constructor|]. split; [reflexivity|].
          split; [reflexivity|]. reflexivity.
      + destruct H as (p & H1 & H2 & H3 & H4).
        inversion H4.
        * right.
          destruct p as [|u' p]; [discriminate|].
          simpl in H2; subst u'.
          destruct p as [|u0 p]; [discriminate|].
          destruct H1 as [H11 H12]. inversion H12; subst.
          inversion H5; subst.
          exists u0. split; [assumption|].
          exists (u0::p). { split.
          - split.
            + inversion H11; assumption.
            + inversion H12; assumption.
          - split; [reflexivity|]. split.
            + rewrite 3!last.last_cons_change_default. reflexivity.
            + simpl in H4. apply le_S_n in H4. assumption. }
        * left. exists p. split; [assumption|]. split; [assumption|].
          split; assumption.
  Qed.

  Lemma NoDup_Finite_bound : forall {A:Type} (X:Ensemble A) n,
    cardinal X n -> forall (l:list A),
    List.Forall (fun x => X x) l ->
    List.NoDup l -> List.length l <= n.
  Proof.
    intros A X n HCard l. revert X n HCard. induction l; intros.
    - apply le_0_n.
    - inversion H; subst.
      destruct n.
      + inversion HCard; subst. apply H1 in H3.
        apply Noone_in_empty in H3. contradiction.
      + apply cardinal_Sn_Subtract with (x:=a) in HCard; try assumption.
        simpl. apply le_n_S. apply IHl with (X:=Subtract X a).
        * assumption.
        * rewrite List.Forall_forall. rewrite List.Forall_forall in H4.
          intros. specialize (H4 x H1).
          apply Subtract_intro. assumption.
          intros contra. subst x. inversion H0; subst. contradiction.
        * inversion H0; assumption.
  Qed.

  Lemma Reachable_iff_Reachable_n : forall g, exists n,
    forall u v, Reachable g u v <-> Reachable_n g n u v.
  Proof.
    intros.
    pose proof (finite_cardinal _ (Support_finite g)).
    destruct H as (n & H).
    exists n. split; intros.
    - destruct H0 as (p & H01 & H02 & H03).
      exists (remove_cycles V.eq_dec p).
      split; [apply remove_cycles_stays_Path; assumption|].
      split.
      + rewrite <- H02 at 2. apply listOtherResults.remove_cycles_keeps_hd.
      + split.
        * rewrite <- H03. apply listOtherResults.remove_cycles_keeps_last.
        * { apply NoDup_Finite_bound with (X:=Support g).
          - assumption.
          - destruct (remove_cycles_stays_Path g p H01) as (HSupp & _).
            rewrite List.Forall_forall. rewrite Forall_forall in HSupp.
            intros. apply HSupp. rewrite In_finite. assumption.
          - apply remove_cycles_NoDup. }
    - destruct H0 as (p & H01 & H02 & H03 & H04).
      exists p. split; [assumption|]. split; assumption.
  Qed.

  Lemma Reachable_n_dec : forall g n u v,
    Reachable_n g n u v \/ ~ Reachable_n g n u v.
  Proof.
    intros g n. induction n; intros.
    - destruct (V.eq_dec u v).
      + left. exists []. split.
        * repeat constructor.
        * split; [reflexivity|]. split; [assumption|reflexivity].
      + right. intros contra.
        destruct contra as (p & _ & _ & contra1 & contra2).
        inversion contra2. apply List.length_zero_iff_nil in H0. subst p.
        contradiction.
    - destruct n.
      + destruct (V.eq_dec u v).
        * subst v. left.
          exists []. split; [repeat constructor|]. split; [reflexivity|].
          split; [reflexivity|]. apply le_S; apply le_n.
        * right. intros contra.
          destruct contra as (p & _ & contra1 & contra2 & contra3).
          { destruct p as [|w p].
          - contradiction.
          - simpl in contra3. apply le_S_n in contra3. inversion contra3.
            apply List.length_zero_iff_nil in H0. subst p.
            simpl in contra1, contra2. subst. contradiction. }
      + destruct (Finite_exists_dec (Support g)
          (fun x => (exists l, Rel g u l x) /\ Reachable_n g (S n) x v)).
        * apply Support_finite.
        * intros. { destruct (Finite_In_dec V.eq_dec _ (Rel_finite g u) x).
          - destruct (IHn x v).
            + left; split; assumption.
            + right. intros contra. destruct contra; contradiction.
          - right. intros contra. destruct contra; contradiction. }
        * intros. destruct H as ((l & H0) & _). apply Support_spec in H0.
          apply H0.
        * destruct H as (x & (l & H1) & H2).
          left. eapply Reachable_n_1_Sn; try eassumption.
          apply le_n_S. apply le_0_n.
        * { destruct (IHn u v).
          - left. apply Reachable_n_Sn. assumption.
          - right. intros contra. apply Reachable_Sn_n in contra.
            destruct contra; contradiction. }
  Qed.

  Lemma Reachable_dec : forall g u v,
    Reachable g u v \/ ~ Reachable g u v.
  Proof.
    intros. destruct (Reachable_iff_Reachable_n g) as (n & H).
    rewrite H. apply Reachable_n_dec.
  Qed.

  Lemma Reachable_from_dec : forall g V
    (V_In_dec : forall u, V u \/ ~ V u),
    forall v,
    Reachable_from g V v \/ ~ Reachable_from g V v.
  Proof.
    intros. destruct (V_In_dec v).
    - left. exists v. split; [assumption|]. exists [].
      split; [repeat constructor|]. split; reflexivity.
    - apply Finite_exists_dec with (X:=Support g).
      + apply Support_finite.
      + intros u. apply Decidable.dec_and.
        * apply V_In_dec.
        * apply Reachable_dec.
      + intros u (H0 & p & H1 & H2 & H3).
        destruct p as [|u' p].
        * simpl in H3. subst u. contradiction.
        * simpl in H2. subst u'. destruct H1 as (H1 & _).
          inversion H1; assumption.
  Qed.

  Lemma stable_by_remove_cycles_dec : forall {A:Type}
    (eq_dec : forall (u v : A), {u=v} + {u<>v})
    (X:Ensemble A) (P:list A->Prop)
    (X_Fin:Finite X) (P_dec: forall x, P x \/ ~ P x)
    (HIncl : forall l, P l -> List.Forall X l)
    (HNoDup : forall l, P l -> P (remove_cycles eq_dec l)),
    (exists l, P l) \/ (~ exists l, P l).
  Proof.
    intros. apply finite_cardinal in X_Fin as X_Card.
    destruct X_Card as (n & X_Card).
    pose proof (list_n_Finite eq_dec X X_Fin n) as list_n_Fin.
    destruct (Finite_exists_dec (list_n X n)
      (fun l => list_n X n l /\ P (remove_cycles eq_dec l))
      list_n_Fin).
    - intros l. apply Decidable.dec_and.
      + apply (Finite_In_dec (List.list_eq_dec eq_dec)). assumption.
      + apply P_dec.
    - intros l ?. apply H.
    - destruct H as (l & H1 & H2). left. exists (remove_cycles eq_dec l).
      assumption.
    - right. intros contra. apply H. destruct contra as (l & contra).
      exists (remove_cycles eq_dec l). split.
      + split.
        * apply HIncl. apply HNoDup. assumption.
        * apply (NoDup_Finite_bound X n X_Card).
          apply HIncl. apply HNoDup. assumption.
          apply remove_cycles_NoDup.
      + apply HNoDup. apply HNoDup. assumption.
  Qed.

  Lemma Path_dec : forall g (p:list V.t), Path g p \/ ~ Path g p.
  Proof.
    intros. apply Decidable.dec_and.
    - unfold Decidable.decidable. rewrite Forall_finite.
      apply Forall_dec. apply Support_In_dec.
    - unfold Decidable.decidable. rewrite Sorted_finite.
      apply Sorted_dec. intros u.
      apply (Finite_In_dec V.eq_dec _ (Rel_finite g u)).
  Qed.

  Lemma Reachable_dec2 : forall g u v,
    Reachable g u v \/ ~ Reachable g u v.
  Proof.
    intros. apply (stable_by_remove_cycles_dec V.eq_dec (Support g)).
    - apply Support_finite.
    - intros l. apply Decidable.dec_and.
      + apply Path_dec.
      + apply Decidable.dec_and.
        * destruct (V.eq_dec (List.hd u l) u); [left|right]; assumption.
        * destruct (V.eq_dec (List.last l u) v); [left|right]; assumption.
    - intros. apply Forall_finite. apply H.
    - intros. destruct H as (H1 & H2 & H3).
      split.
      + apply remove_cycles_stays_Path. assumption.
      + split.
        * rewrite <- H2 at 2. apply remove_cycles_keeps_hd.
        * rewrite <- H3. apply remove_cycles_keeps_last.
  Qed.

  Lemma Is_VPath_dec : forall g V u v p
    (V_In_dec : forall x, V x \/ ~ V x),
    Is_VPath g V u v p \/ ~ Is_VPath g V u v p.
  Proof.
    intros. apply Decidable.dec_and.
    - destruct (V.eq_dec (List.hd v p) u); [left|right]; assumption.
    - apply Decidable.dec_and.
      + unfold Decidable.decidable.
        setoid_replace (of_list p ++ [v])%inf with (of_list (p++[v])%list)
          by (simpl_inf; reflexivity).
        apply Path_dec.
      + apply Decidable.dec_and.
        * apply V_In_dec.
        * unfold Decidable.decidable. rewrite Forall_finite.
          apply Forall_dec. intros w. apply Decidable.dec_not.
          apply V_In_dec.
  Qed.

  Lemma remove_cycles_stays_Is_VPath : forall g V u v p,
    Is_VPath g V u v p -> Is_VPath g V u v (remove_cycles V.eq_dec p).
  Proof.
    intros. destruct H as (H1 & H2 & H3 & H4). split.
    - rewrite <- H1. apply remove_cycles_keeps_hd.
    - split.
      + assert (p = [] \/ p <> []) as Ha
          by (destruct p; [left; reflexivity|right; discriminate]).
        destruct Ha.
        * subst p. rewrite remove_cycles_equation. assumption.
        * destruct (List.exists_last H) as (p' & w & Heqp). subst p.
          rewrite <- (remove_cycles_nil V.eq_dec) in H.
          destruct (List.exists_last H) as (rem & w' & Heqremove).
          pose proof (remove_cycles_keeps_last V.eq_dec w (p'++[w])).
          rewrite Heqremove in H0.
          rewrite 2!last.last_app_not_nil in H0 by discriminate.
          simpl in H0. subst w'.
          { apply Path_finite_app with (u:=w).
          - apply remove_cycles_stays_Path. apply Path_cut in H2. assumption.
          - simpl_inf in H2. apply Path_finite_cut in H2. assumption.
          - rewrite Heqremove. rewrite last.last_app_not_nil by discriminate.
            reflexivity. }
      + split; [assumption|]. apply Forall_forall. rewrite Forall_forall in H4.
        intros w ?. apply H4. apply In_finite in H.
        apply remove_cycles_incl in H. apply In_finite. assumption.
  Qed.

  Lemma VPath_dec : forall g V u v
    (V_In_dec : forall x, V x \/ ~ V x),
    VPath g V u v \/ ~ VPath g V u v.
  Proof.
    intros.
    apply (stable_by_remove_cycles_dec V.eq_dec (Support g)).
    - apply Support_finite.
    - intros. apply Is_VPath_dec. assumption.
    - intros p ?. destruct H as (_ & H & _).
      apply Path_cut in H. apply Forall_finite. apply H.
    - intros. apply remove_cycles_stays_Is_VPath. assumption.
  Qed.

  Lemma Finite_Disjoint_dec : forall {A}
    (eq_dec : forall u v : A, {u = v} + {u <> v})
    (X1 X2 : Ensemble A)
    (HFin : Finite X1) (X2_dec : forall a, X2 a \/ ~ X2 a),
    Disjoint X1 X2 \/ ~ Disjoint X1 X2.
  Proof.
    intros. induction HFin as [|X HFin ? a not_In].
    - left. constructor. intros a contra. apply Intersection_inv in contra.
      destruct contra. apply H in H0.
      apply Noone_in_empty in H0. contradiction.
    - destruct IHHFin.
      + destruct (X2_dec a).
        * right. intros contra. inversion contra. apply (H2 a).
          apply Intersection_intro; [|assumption]. apply H. apply Add_intro2.
        * left. constructor. intros a' contra. apply Intersection_inv in contra.
          destruct contra. apply H in H2.
          apply Add_inv in H2. { destruct H2.
          - inversion H0. apply (H4 a'). split; assumption.
          - subst a'. contradiction. }
      + right. intros contra. apply H0. constructor. intros a' contra2.
        apply Intersection_inv in contra2. destruct contra2.
        inversion contra as (contra3). apply (contra3 a'). split.
        * apply H. apply Add_intro1. assumption.
        * assumption.
  Qed.

  Lemma Weakly_deciding_dec : forall g V u
    (V_In_dec : forall x, V x \/ ~ V x),
    Weakly_deciding g V u \/ ~ Weakly_deciding g V u.
  Proof.
    intros. apply (Finite_exists_dec (Support g)).
    - apply Support_finite.
    - intros v1. apply (Finite_exists_dec (Support g)).
      + apply Support_finite.
      + intros v2. apply (stable_by_remove_cycles_dec V.eq_dec (Support g)).
        * apply Support_finite.
        * intros p1. { apply (stable_by_remove_cycles_dec V.eq_dec (Support g)).
          - apply Support_finite.
          - intros p2. apply Decidable.dec_and.
            + apply Is_VPath_dec. assumption.
            + apply Decidable.dec_and.
              * apply Is_VPath_dec. assumption.
              * { apply (Finite_Disjoint_dec V.eq_dec).
                - apply (Ensemble_of_list_finite V.eq_dec).
                - apply (Finite_In_dec V.eq_dec).
                  apply (Ensemble_of_list_finite V.eq_dec). }
          - intros p2 ?. destruct H as (_ & (_ & H & _) & _).
            apply Path_cut in H. destruct H as (H & _).
            apply Forall_finite in H. inversion H; assumption.
          - intros p2 ?. destruct H as (H1 & H2 & H3).
            split; [assumption|]. split.
            + split; [reflexivity|]. split.
              * setoid_replace ((u::remove_cycles V.eq_dec p2) ++ [v2])%inf
                  with ([u]%list++(remove_cycles V.eq_dec p2) ++[v2]%list)%inf
                  by (simpl_inf; reflexivity).
                apply remove_cycles_sub_stays_Path.
                simpl_inf. destruct H2 as (_ & H2 & _). simpl_inf in H2.
                assumption.
              * split; [apply H2|]. apply Forall_forall. intros w ?.
                destruct H2 as (_ & _ & _ & H2).
                rewrite Forall_forall in H2. apply H2.
                apply In_finite. apply In_finite in H.
                destruct H; [left; assumption|].
                right. apply remove_cycles_incl in H. assumption.
            + inversion H3. constructor. intros w contra.
              apply (H w). apply Intersection_inv in contra.
              destruct contra.
              apply Intersection_intro; [assumption|].
              apply List.in_app_iff.
              unfold Ensembles.In, Ensemble_of_list in H4.
              apply List.in_app_iff in H4. destruct H4.
              * apply remove_cycles_incl in H4. left; assumption.
              * right; assumption. }
        * intros p1 ?. destruct H as (_ & H & _). destruct H as (_ & H & _).
          apply Path_cut in H. destruct H as (H & _). apply Forall_finite in H.
          inversion H; assumption.
        * intros p1 ?. destruct H as (p2 & H1 & H2 & H3).
          exists p2. { split.
          - split; [reflexivity|]. split.
            + setoid_replace ((u::remove_cycles V.eq_dec p1) ++ [v1])%inf
                with ([u]%list++(remove_cycles V.eq_dec p1) ++[v1]%list)%inf
                by (simpl_inf; reflexivity).
              apply remove_cycles_sub_stays_Path.
              simpl_inf. destruct H1 as (_ & H1 & _). simpl_inf in H1.
              assumption.
            + split; [apply H1|].
              destruct H1 as (_ & _ & _ & H1).
              apply Forall_forall. rewrite Forall_forall in H1.
              intros w ?. apply H1. apply In_finite. apply In_finite in H.
              destruct H; [left; assumption|]. right.
              apply remove_cycles_incl in H. assumption.
          - split; [apply H2|]. inversion H3. constructor. intros w contra.
            apply (H w). apply Intersection_inv in contra.
            destruct contra as (contra1 & contra2).
            apply Intersection_intro; [|assumption].
            unfold Ensembles.In, Ensemble_of_list in contra1.
            apply List.in_app_iff in contra1. apply List.in_app_iff.
            destruct contra1 as [contra1|contra1]; [|right; assumption].
            left. apply remove_cycles_incl in contra1. assumption. }
      + intros v2 ?. destruct H as (_ & p2 & _ & (_ & H2 & _) & _).
        apply Path_finite_cut in H2.
        destruct H2 as (H2 & _). inversion H2; assumption.
    - intros v1 ?. destruct H as (_ & p1 & _ & H1 & _ & _).
      destruct H1 as (_ & H1 & _).
      apply Path_finite_cut in H1. destruct H1 as (H1 & _).
      inversion H1; assumption.
  Qed.

  (** X is the smallest set verifying P *)
  Definition Minimum {A} (P:Ensemble A->Prop) (X:Ensemble A) :=
    P X /\ (forall X', P X' -> Included X X').

  Lemma Minimum_unicity :
    forall (A : Type) (P : Ensemble A -> Prop) (X X' : Ensemble A),
    Minimum P X -> Minimum P X' -> X == X'.
  Proof.
    intros. split. apply H. apply H0. apply H0. apply H.
  Qed.

  Lemma Minimum_is_Minimum :
    forall (A : Type) (P : Ensemble A -> Prop) (X X' : Ensemble A),
    P X -> Included X X' -> Minimum P X' -> X == X'.
  Proof.
    intros. apply H1 in H. apply Same_set_iff. split; intros.
    - apply H0; assumption.
    - apply H; assumption.
  Qed.

  Lemma Reachable_from_Same_set : forall g V1 V2,
    V1 == V2 -> Same_set (Reachable_from g V1) (Reachable_from g V2).
  Proof.
    intros. split.
    - unfold Included; intros v ?. destruct H0 as (u & H0 & H1).
      exists u. split; [|assumption]. apply H; assumption.
    - unfold Included; intros v ?. destruct H0 as (u & H0 & H1).
      exists u. split; [|assumption]. apply H; assumption.
  Qed.

  Definition Minimal_Weakly_control_closed g V :=
    let W := fun u => Weakly_deciding g V u /\ Reachable_from g V u in
    Union V W.

  Lemma th54_explicit_Included : forall g V,
    Included V (Minimal_Weakly_control_closed g V).
  Proof.
    intros. apply Powerset.Union_increases_l.
  Qed.

  Lemma th54_explicit_dec : forall g V
    (V_In_dec : forall u, V u \/ ~ V u),
    forall u,
    Minimal_Weakly_control_closed g V u \/
    ~ Minimal_Weakly_control_closed g V u.
  Proof.
    intros.
    assert ((V u \/ Weakly_deciding g V u /\ Reachable_from g V u) \/
            ~ (V u \/ Weakly_deciding g V u /\ Reachable_from g V u)) as Ha.
    { apply Decidable.dec_or.
      - apply V_In_dec.
      - apply Decidable.dec_and.
        + apply Weakly_deciding_dec; assumption.
        + apply Reachable_from_dec; assumption.
    }
    destruct Ha.
    - destruct H.
      + left. apply Union_introl; assumption.
      + left. apply Union_intror; assumption.
    - right. intros contra. apply H.
      apply Union_inv in contra. destruct contra.
      + left; assumption.
      + right; assumption.
  Qed.

  Lemma th54_explicit_Weakly_control_closed : forall g V
    (V_In_dec : forall u, V u \/ ~ V u),
    Weakly_control_closed g (Minimal_Weakly_control_closed g V).
  Proof.
    intros.
    apply lemma51_reverse. intros u H contra.
    pose proof (Weakly_deciding_not_In _ _ _ contra) as contraIn.
    apply lemma52
      with (V2:=(Union V (Weakly_deciding g V)))
      in contra.
    - apply Union_inv in contra.
      destruct contra as [contra|contra].
      + (* u in V \cup WG (V) *)
        contradiction contraIn.
        apply Union_inv in contra. destruct contra as [contra|contra].
        * (* u not in V, because u in WG (V \cup W) *) left; assumption.
        * (* u not in WG(V) because reachable from V and not in W *)
          right. split; [assumption|].
          destruct H as (v & H & H0). apply Union_inv in H.
          { destruct H as [H|H].
          - (* definition of Reachable_from *) exists v. split; assumption.
          - (* W implies Reachable_from *)
            destruct H as (_ & H).
            apply Reachable_from_trans with (u:=v); assumption. }
      + (* u in WG (V \cup WG(V)), which is empty by lemma53 *)
        apply lemma53 in contra.
        eapply Noone_in_empty. eassumption.
    - intros v. destruct (V_In_dec v).
      + left. apply Union_introl. assumption.
      + destruct (Weakly_deciding_dec g V v V_In_dec).
        * left. right. assumption.
        * right. intros contra2. destruct contra2; contradiction.
    - intros v Hin. apply Union_inv in Hin. destruct Hin as [Hin|Hin].
      + apply Union_introl. assumption.
        + apply Union_intror. apply Hin.
  Qed.

  Theorem th54_explicit : forall g V
    (V_In_dec : forall u, V u \/ ~ V u),
    Minimum (fun X => Included V X /\
                      (forall u, X u \/ ~ X u) /\
                      Weakly_control_closed g X)
            (Minimal_Weakly_control_closed g V).
  Proof.
    intros.
    split.
    - split; [apply th54_explicit_Included|].
      split; [apply th54_explicit_dec; assumption|].
      apply th54_explicit_Weakly_control_closed; assumption.
    - intros V' (H & (* H0 & *) H1 & H2).
      intros v Hin.
      apply Union_inv in Hin.
      destruct Hin as [Hin|Hin]; [apply H; assumption|].
      destruct Hin as (Hin1 & Hin2).
      apply lemma52 with (V2:=V') in Hin1; [|assumption|assumption].
      apply Union_inv in Hin1.
      destruct Hin1 as [Hin1|Hin1]; [assumption|].
      contradiction (lemma51 _ _ H2 v).
      destruct Hin2 as [u Hin2].
      exists u. split; [apply H|]; apply Hin2.
  Qed.

  Theorem th54 : forall g V
    (V_In_dec : forall u, V u \/ ~ V u)
    (V_In_Support : Included V (Support g)),
    exists V',
    Minimum (fun X => Included V X /\
                      (forall u, X u \/ ~ X u) /\
                      Weakly_control_closed g X)
            V'.
  Proof.
    eexists. apply th54_explicit; assumption.
  Qed.

  Lemma Minimal_Weakly_control_closed_Included : forall g V,
    Included (Minimal_Weakly_control_closed g V) (Union V (Support g)).
  Proof.
    intros. intros u u_In.
    unfold Minimal_Weakly_control_closed in u_In.
    apply Union_inv in u_In. destruct u_In as [u_In|u_In].
    - apply Union_introl; assumption.
    - apply Union_intror. eapply Weakly_deciding_In_Support. apply u_In.
  Qed.

  Lemma Minimal_Weakly_control_closed_increasing : forall g V1 V2
    (In_V2_dec : forall u, V2 u \/ ~ V2 u),
    Included V1 V2 -> Included (Minimal_Weakly_control_closed g V1)
                               (Minimal_Weakly_control_closed g V2).
  Proof.
    intros. intros u u_In.
    apply Union_inv in u_In. destruct u_In as [u_In|u_In].
    - apply Union_introl. apply H; assumption.
    - destruct u_In as (u_dec & u_reach).
      apply lemma52 with (V2:=V2) in u_dec; try assumption.
      apply Union_inv in u_dec. destruct u_dec as [u_dec|u_dec].
      + apply Union_introl. assumption.
      + apply Union_intror. split.
        * assumption.
        * apply Included_Reachable_from with (V1:=V1); assumption.
  Qed.

  Lemma Weakly_control_closed_not_In_Support :
    forall g V, Disjoint (Support g) V ->
    Weakly_control_closed g V.
  Proof.
    intros. unfold Weakly_control_closed; intros.
    inversion H; subst. unfold Weakly_committing.
    intros.
    destruct H2 as (p1 & H2).
    destruct H2 as (_ & H2 & H22 & _).
    destruct H2 as [H2 _].
    rewrite Forall_forall in H2. exfalso.
    apply (H1 v1). split. apply H2. apply in_app_finite_r. left; reflexivity.
    assumption.
  Qed.

  Lemma Weakly_committing_not_In_support : forall g V u, ~ (Support g) u ->
    Reachable_from g V u -> Weakly_committing g V u.
  Proof.
    intros. unfold Weakly_committing. intros.
    destruct H1 as (p1 & H11 & (H12 & _) & _).
    destruct p1 as [|u1 p1].
    - simpl in H11. subst v1. simpl_inf in H12. inversion H12; contradiction.
    - simpl in H11. subst u1. simpl_inf in H12. inversion H12; contradiction.
  Qed.

  Lemma not_Weakly_control_closed : forall g V
    (V_In_dec : forall u, V u \/ ~ V u),
    ~ Weakly_control_closed g V ->
    exists u, Weakly_deciding g V u /\ Reachable_from g V u.
  Proof.
    intros. rewrite <- lemma51_iff in H.
    apply Finite_not_all_ex_not with (X:=Support g) in H.
    - destruct H as (u & H). exists u. apply Decidable.not_imp in H.
      + destruct H. split; [|assumption]. apply Decidable.not_not.
        * apply Weakly_deciding_dec. apply V_In_dec.
        * assumption.
      + apply Reachable_from_dec. assumption.
    - apply V.eq_dec.
    - intros u. apply Decidable.dec_imp.
      + apply Reachable_from_dec. assumption.
      + apply Decidable.dec_not. apply Weakly_deciding_dec. assumption.
    - apply Support_finite.
    - intros u ? ?. intros contra. apply Weakly_deciding_In_Support in contra.
      contradiction.
  Qed.

  Lemma Weakly_control_closed_dec : forall g V
    (V_In_dec : forall u, V u \/ ~ V u),
    Weakly_control_closed g V \/ ~ Weakly_control_closed g V.
  Proof.
    intros. rewrite <- lemma51_iff.
    apply Finite_forall_dec with (X:=Support g).
    - apply V.eq_dec.
    - intros u. apply Decidable.dec_imp.
      + apply Reachable_from_dec. assumption.
      + apply Decidable.dec_not. apply Weakly_deciding_dec. assumption.
    - apply Support_finite.
    - intros u ? ?. intros contra. apply Weakly_deciding_In_Support in contra.
      contradiction.
  Qed.

  Lemma Observable_finite : forall g V u
    (V_In_dec : forall u, V u \/ ~ V u),
    Finite (Observable g V u).
  Proof.
    intros.
    apply (Finite_downward_closed V.eq_dec (Support g) (Support_finite g)).
    - intros v. apply VPath_dec. assumption.
    - intros v ?. destruct H as (p & _ & (H & _) & _).
      rewrite Forall_forall in H. apply H. apply in_app_finite_r. constructor.
  Qed.

  Definition Critical_candidate g V u v :=
    Rel_w g u v /\
    (exists n,
    cardinal (Observable g V u) n /\ n >= 2) /\
    cardinal (Observable g V v) 1.

  Definition Critical_edge g V u v :=
    Critical_candidate g V u v /\
    Reachable_from g V u.

  Lemma Weakly_deciding_Critical_edge : forall g V u
    (V_In_dec : forall u, V u \/ ~ V u),
    Weakly_deciding g V u ->
    exists u' v, Critical_candidate g V u' v /\ Reachable g u u'.
  Proof.
    intros. destruct H as (v1 & v2 & p1 & p2 & Hp1 & Hp2 & HDisj).
    destruct (Exists_first
      (fun x => cardinal (Observable g V x) 1))
      with (l:=u::p1++[v1]) as (p11 & w1 & p12 & HExists).
    - intros v. apply (Finite_cardinal_dec V.eq_dec (Support g)).
      + apply Support_finite.
      + intros v'. apply VPath_dec. assumption.
      + intros v' ?. destruct H as (p & _ & (H & _) & _ & _).
        rewrite Forall_forall in H. apply H. apply in_app_finite_r.
        apply In_here.
    - apply List.Exists_exists. exists v1.
      split.
      + right. apply List.in_app_iff. right; left; reflexivity.
      + apply card_add with (X:=Empty_set) (x:=v1).
        * apply card_empty. reflexivity.
        * apply Noone_in_empty.
        * { split.
          - intros v ?. destruct H as (p & H).
            destruct p as [|w p].
            + destruct H as (H & _). simpl in H; subst v. apply Add_intro2.
            + destruct H as (HH1 & _ & _ & HH2). simpl in HH1; subst w.
              inversion HH2; subst. contradiction H1. apply Hp1.
          - intros v ?. apply Add_inv in H. destruct H.
            + apply Noone_in_empty in H. contradiction.
            + subst v. exists []. split; [reflexivity|]. split.
              * destruct Hp1 as (_ & Hp1 & _). apply Path_finite_cut in Hp1.
                simpl_inf. assumption.
              * split; [apply Hp1|]. constructor. }
    - destruct HExists as (HExists1 & HExists2 & HExists3).
      destruct (list_nil_dec p11) as [Heqp11|Heqp11].
      + subst p11. inversion HExists1. subst w1.
        inversion HExists2; subst. inversion H0; subst.
        inversion HDisj. contradiction (H1 v1).
        apply Intersection_intro.
        * apply List.in_app_iff. right; left; reflexivity.
        * assert (Observable g V u v1) by (exists (u::p1); assumption).
          assert (Observable g V u v2) by (exists (u::p2); assumption).
          rewrite H in H4. rewrite Singleton_Add in H4.
          apply H4 in H3. apply H4 in H5.
          apply Singleton_inv in H3; subst. apply Singleton_inv in H5.
          apply List.in_app_iff. right; left. symmetry; assumption.
      + destruct (List.exists_last Heqp11) as (p10 & w & H).
        subst p11.
        exists w, w1.
        destruct (finite_cardinal _ (Observable_finite g V w V_In_dec))
          as (n & HObsw).
        split.
        * { split.
          - destruct Hp1 as (_ & Hp1 & _).
            setoid_replace ((u::p1)%list++[v1])%inf with (u :: p1 ++ [v1]) in Hp1
              by (simpl_inf; reflexivity).
            rewrite HExists1 in Hp1. simpl_inf in Hp1.
            apply Path_finite_cut in Hp1. destruct Hp1 as (_ & Hp1).
            inversion Hp1. inversion H2; assumption.
          - split.
            + exists n. split; [assumption|].
              destruct n.
              * inversion HObsw; subst. contradiction (Noone_in_empty v1).
                rewrite <- H. { destruct (list_nil_dec p12).
                - subst p12.
                  rewrite List.app_comm_cons in HExists1.
                  apply List.app_inj_tail in HExists1.
                  destruct HExists1 as [HExists1 ?]. subst w1.
                  exists ([w]). split; [reflexivity|].
                  split.
                  + apply Path_finite_cut with (p1:=p10).
                    setoid_replace (p10 ++ [w]%list ++ [v1])%inf
                      with ((p10 ++ [w]) ++ [v1]) by (simpl_inf; reflexivity).
                    rewrite <- HExists1.
                    destruct Hp1 as (_ & Hp1 & _). simpl_inf in Hp1. simpl_inf.
                    assumption.
                  + split; [apply Hp1|]. destruct Hp1 as (_ & _ & _ & Hp1).
                    rewrite Forall_forall in Hp1. constructor; [|constructor].
                    apply Hp1. rewrite HExists1.
                    simpl_inf. apply in_app_finite_r. constructor.
                - destruct (List.exists_last n) as (p12' & v1' & Heqp12).
                  subst p12.
                  rewrite 2!List.app_comm_cons in HExists1.
                  rewrite List.app_assoc in HExists1.
                  apply List.app_inj_tail in HExists1.
                  destruct HExists1 as [HExists1 ?]. subst v1'.
                  exists (w::w1::p12').
                  split; [reflexivity|]. split.
                  + apply Path_finite_cut with (p1:=p10).
                    setoid_replace (p10 ++ (w :: w1 :: p12')%list ++ [v1])%inf
                      with (((p10 ++ [w]) ++ w1 :: p12') ++ [v1])
                      by (simpl_inf; reflexivity).
                    rewrite <- HExists1.
                    destruct Hp1 as (_ & Hp1 & _). simpl_inf in Hp1. simpl_inf.
                    assumption.
                  + split; [apply Hp1|]. destruct Hp1 as (_ & _ & _ & Hp1).
                    rewrite Forall_forall in Hp1. rewrite Forall_forall.
                    intros z ?. apply Hp1. rewrite HExists1.
                    simpl_inf. apply in_app_finite_r. assumption. }
              * { destruct n.
                - rewrite List.Forall_forall in HExists3.
                  contradiction (HExists3 w). rewrite List.in_app_iff.
                  right; left; reflexivity.
                - apply le_n_S. apply le_n_S. apply le_0_n. }
            + assumption. }
        * destruct Hp1 as (_ & Hp1 & _).
          { destruct p10 as [|u' p10].
          - simpl in HExists1. inversion HExists1. subst w. exists [].
            repeat constructor.
          - simpl in HExists1. inversion HExists1. subst u'.
            exists (u::p10++[w]). split.
            + apply Path_cut with (p2:=w1 :: p12).
              setoid_replace ((u :: p10 ++ [w])%list ++ (w1 :: p12)%list)%inf
                with (u :: (p10 ++ [w]) ++ w1 :: p12)
                by (simpl_inf; reflexivity).
              rewrite <- HExists1. simpl_inf. simpl_inf in Hp1. assumption.
            + split; [reflexivity|]. rewrite last.last_cons_change_default.
              rewrite last.last_app_not_nil by discriminate. reflexivity. }
  Qed.

  Lemma lemma60 : forall g V
    (V_In_dec : forall u, V u \/ ~ V u),
    ~ Weakly_control_closed g V ->
    exists u v, Critical_edge g V u v.
  Proof.
    intros.
    apply not_Weakly_control_closed in H; [|assumption].
    destruct H as (u & H1 & H2).
    apply Weakly_deciding_Critical_edge in H1; try assumption.
    destruct H1 as (u' & v & H11 & H12).
    exists u', v. split; [assumption|].
    eapply Reachable_from_trans; eassumption.
  Qed.

  Lemma Is_VPath_Observable_Included : forall g V u v p,
    Is_VPath g V u v p ->
    Forall (fun w => Included (Observable g V w) (Observable g V u)) (p++[v]).
  Proof.
    intros.
    setoid_replace (p ++ [v])%inf with (p++[v]) by (simpl_inf; reflexivity).
    rewrite Forall_finite. rewrite List.Forall_forall.
    intros w Hin. rewrite List.in_app_iff in Hin. destruct Hin as [Hin|Hin].
    - { intros x x_Obs.
    destruct x_Obs as (p' & x_Obs).
    apply List.in_split in Hin. destruct Hin as (p1 & p2 & Hin).
    destruct p' as [|w' p'].
    - destruct x_Obs as (x_Obs1 & _ & x_Obs2 & _). simpl in x_Obs1. subst x.
      destruct H as (_ & _ & _ & H). rewrite Forall_forall in H.
      contradiction (H w).
      rewrite Hin. simpl_inf. apply in_app_finite_r. apply In_here.
    - pose proof x_Obs as (x_Obs1 & _). simpl in x_Obs1. subst w'.
      exists (p1 ++ w :: p').
      split.
      + destruct H as (H & _). rewrite Hin in H. destruct p1; assumption.
      + split.
        * setoid_replace ((p1++w::p')%list++[x])%inf
            with ((p1++[w])%list++(p'++[x]))%inf
            by (simpl_inf; reflexivity).
          { apply Path_finite_app with (u:=w).
          - destruct H as (_ & H & _). apply Path_cut in H.
            rewrite Hin in H.
            simpl_inf in H.
            setoid_replace (p1++w::p2)%inf with ((p1++[w])++p2)%inf in H
              by (simpl_inf; reflexivity).
            apply Path_cut in H. simpl_inf; assumption.
          - destruct x_Obs as (_ & x_Obs & _). simpl_inf in x_Obs. assumption.
          - rewrite last.last_app_not_nil by discriminate. reflexivity. }
        * split; [apply x_Obs|]. apply Forall_forall.
          intros x' Hin'. simpl_inf in Hin'. apply in_app in Hin'.
          { destruct Hin' as [Hin'|Hin'].
          - destruct H as (_ & _ & _ & H). rewrite Forall_forall in H.
            apply H. rewrite Hin. simpl_inf. apply in_app_l. assumption.
          - destruct x_Obs as (_ & _ & _ & x_Obs).
            rewrite Forall_forall in x_Obs.
            apply x_Obs. simpl_inf. assumption. }
  }
  - destruct Hin; [|contradiction]. subst w. intros x H0.
    destruct H0 as (p' & H0). destruct p' as [|w p'].
    + destruct H0 as (H0 & _). simpl in H0. subst x. exists p. assumption.
    + destruct H0 as (H01 & _ & _ & H02). simpl in H01; subst w.
      inversion H02; subst. contradiction H2. apply H.
  Qed.

  Lemma In_V_Observable : forall g (V:Ensemble V.t) u
    (In_Support : Support g u),
    V u -> Observable g V u == Singleton u.
  Proof.
    intros. split; intros v H0.
    - destruct H0 as (p & H01 & _ & H02 & H03).
      destruct p as [|w p].
      + simpl in H01. subst v. apply In_singleton.
      + simpl in H01. subst w. rewrite Forall_forall in H03.
        contradiction (H03 u). apply In_here.
  - apply Singleton_inv in H0. subst v. exists [].
    split; [reflexivity|].
    split; [simpl_inf; repeat constructor; assumption|].
    split; [assumption|]. constructor.
  Qed.

  Lemma Critical_candidate_Weakly_deciding_aux : forall g V u v
    (u_v_Rel_w : Rel_w g u v),
    forall v' u2
    (v_singleton : Observable g V v == Singleton v')
    (HObsu2 : Observable g V u u2),
    u <> u2 ->
    v' <> u2 -> Weakly_deciding g V u.
  Proof.
    intros.
    exists v', u2.
    destruct HObsu2 as (p2 & HObsu2).
    destruct p2 as [|w p2].
    - destruct HObsu2 as (HObsu21 & _ & HObsu22 & _).
      simpl in HObsu21; subst u2. contradiction H. reflexivity.
    - pose proof HObsu2 as (HObsu21 & _). simpl in HObsu21. subst w.
      pose proof (In_singleton _ v') as HObsv. rewrite <- v_singleton in HObsv.
      destruct HObsv as (p' & HObsv).
      exists p', p2. split.
      + split; [reflexivity|]. split.
        * { destruct p' as [|w p'].
          - destruct HObsv as (HObsv & _). simpl in HObsv. subst v'.
            split.
            + destruct u_v_Rel_w as (l & u_v_Rel_w).
              pose proof (Support_spec g u l v u_v_Rel_w) as HSupp.
              destruct HSupp. simpl_inf; repeat constructor; assumption.
            + simpl_inf; repeat constructor; assumption.
          - pose proof HObsv as (HObsv1 & _). simpl in HObsv1. subst w.
            setoid_replace ((u::v::p')%list ++ [v'])%inf
              with ([u;v]%list++(p'++[v']))%inf
              by (simpl_inf; reflexivity).
            apply Path_finite_app with (u:=v).
            + split.
              * destruct u_v_Rel_w as (l & u_v_Rel_w).
                pose proof (Support_spec g u l v u_v_Rel_w) as HSupp.
                destruct HSupp. simpl_inf; repeat constructor; assumption.
              * simpl_inf; repeat constructor; assumption.
            + destruct HObsv as (_ & HObsv & _). simpl_inf in HObsv.
              assumption.
            + reflexivity. }
        * { split; [apply HObsv|]. simpl_inf. constructor.
          - destruct HObsu2 as (_ & _ & _ & HObsu2).
            simpl_inf in HObsu2. inversion HObsu2; assumption.
          - apply HObsv. }
      + split; [assumption|]. constructor. intros w contra.
        apply Intersection_inv in contra.
        destruct contra as (contra1 & contra2).
        unfold Ensembles.In, Ensemble_of_list in contra1, contra2.
        * assert (Included (Observable g V w) (Observable g V v)) as Ha.
          { pose proof (Is_VPath_Observable_Included g V v v' p' HObsv).
            rewrite Forall_forall in H1. apply H1.
            setoid_replace (p'++[v'])%inf with (p'++[v'])%list
              by (simpl_inf; reflexivity).
            rewrite In_finite; assumption.
          }
          assert (Observable g V w u2) as Ha2.
          { apply List.in_split in contra2.
            destruct contra2 as (p21 & p22 & contra2).
            destruct (list_nil_dec p22).
            - subst p22. apply List.app_inj_tail in contra2.
              destruct contra2 as (_ & contra2). subst w.
              rewrite List.in_app_iff in contra1. destruct contra1.
              + destruct HObsv as (_ & _ & _ & HObsv).
                rewrite Forall_forall in HObsv.
                contradiction (HObsv u2).
                * rewrite In_finite. assumption.
                * apply HObsu2.
              + destruct H1; contradiction.
            - destruct (List.exists_last n) as (p22' & w2 & Heqp22).
              rewrite Heqp22 in contra2.
              rewrite List.app_comm_cons in contra2.
              rewrite List.app_assoc in contra2.
              apply List.app_inj_tail in contra2.
              destruct contra2 as (contra2 & ?). subst w2.
              exists (w::p22').
              split; [reflexivity|].
              split.
              + apply Path_finite_cut with (p1:=u::p21).
                destruct HObsu2 as (_ & HObsu2 & _).
                rewrite contra2 in HObsu2.
                simpl_inf in HObsu2.
                simpl_inf. assumption.
              + split; [apply HObsu2|].
                destruct HObsu2 as (_ & _ & _ & HObsu2).
                rewrite Forall_forall in HObsu2.
                apply Forall_forall. intros x ?. apply HObsu2.
                rewrite contra2. rewrite In_finite. right.
                apply List.in_app_iff. right. rewrite In_finite in H1.
                assumption.
          }
          apply Ha in Ha2. rewrite v_singleton in Ha2.
          apply Singleton_inv in Ha2. contradiction.
  Qed.

  Lemma Critical_candidate_Weakly_deciding : forall g V u v,
    Critical_candidate g V u v ->
    Weakly_deciding g V u.
  Proof.
    intros. destruct H as (u_v_Rel_w & (n & u_card & n_gt_2) & v_card).
    assert (exists v', Observable g V v v') as (v' & HObsv).
    { inversion v_card; subst. exists x. apply H3. apply Add_intro2. }
    assert (exists u1 u2, u1 <> u2 /\ Observable g V u u1 /\
                                      Observable g V u u2)
      as (u1 & u2 & u_neq & HObsu1 & HObsu2).
    { inversion u_card; subst.
      - inversion n_gt_2.
      - inversion H; subst.
        + inversion n_gt_2; inversion H4.
        + exists x, x0. split.
          * intros contra. subst x0. apply H0. rewrite H4. apply Add_intro2.
          * { split.
            - apply H1. apply Add_intro2.
            - apply H1. rewrite H4. apply Add_intro1. apply Add_intro2. }
    }
    destruct (V.eq_dec v' u1).
    - subst u1.
      apply (Critical_candidate_Weakly_deciding_aux g V u v u_v_Rel_w v' u2).
      + apply cardinal_1_Singleton in v_card.
        destruct v_card as (a & v_card). apply v_card in HObsv.
        apply Singleton_inv in HObsv. subst a. assumption.
      + assumption.
      + intros contra. subst u2.
        destruct HObsu2 as (_ & _ & _ & HObsu2 & _).
        apply (In_V_Observable g V u) in HObsu1.
        * apply Singleton_inv in HObsu1. symmetry in HObsu1. contradiction.
        * apply Support_spec_w in u_v_Rel_w. apply u_v_Rel_w.
        * assumption.
      + assumption.
    - apply (Critical_candidate_Weakly_deciding_aux g V u v u_v_Rel_w v' u1).
      + apply cardinal_1_Singleton in v_card.
        destruct v_card as (a & v_card). apply v_card in HObsv.
        apply Singleton_inv in HObsv. subst a. assumption.
      + assumption.
      + intros contra. subst u1.
        destruct HObsu1 as (_ & _ & _ & HObsu1 & _).
        apply (In_V_Observable g V u) in HObsu2.
        * apply Singleton_inv in HObsu2. contradiction.
        * apply Support_spec_w in u_v_Rel_w. apply u_v_Rel_w.
        * assumption.
      + assumption.
  Qed.

  Lemma Reachable_Weakly_deciding_In_Weakly_control_closed : forall g V u
    (u_reach : Reachable_from g V u)
    (u_wdec : Weakly_deciding g V u),
    forall V'
    (V_V'_Incl : Included V V')
    (V'_In_dec : forall u, V' u \/ ~ V' u)
    (V'_closed : Weakly_control_closed g V'),
    V' u.
  Proof.
    intros.
    apply lemma52 with (V2:=V') in u_wdec; try assumption.
    apply Union_inv in u_wdec. destruct u_wdec; [assumption|].
    contradiction (lemma51 g V' V'_closed u).
    eapply Included_Reachable_from; eassumption.
  Qed.

  Lemma lemma60_reverse : forall g V,
    forall u v,
    Critical_edge g V u v ->
    forall V'
    (V_V'_Incl : Included V V')
    (V'_In_dec : forall u, V' u \/ ~ V' u)
    (V'_closed : Weakly_control_closed g V'), V' u.
  Proof.
    intros.
    destruct (V'_In_dec u); [assumption|].
    eapply Reachable_Weakly_deciding_In_Weakly_control_closed; try eassumption.
    - apply H.
    - eapply Critical_candidate_Weakly_deciding; apply H.
  Qed.

  Lemma lemma60_reverse_other : forall g V
    (V_In_dec : forall u, V u \/ ~ V u),
    forall u v,
    Critical_edge g V u v ->
    Minimal_Weakly_control_closed g V u.
  Proof.
    intros. eapply lemma60_reverse; try eassumption.
    - apply Union_increases_l.
    - apply th54_explicit_dec; assumption.
    - apply th54_explicit; assumption.
  Fail Fail Qed. (* the proof is finished *)
  Abort.

  Lemma lemma60_reverse_other : forall g V
    (V_In_dec : forall u, V u \/ ~ V u),
    forall u v,
    Critical_edge g V u v ->
    Minimal_Weakly_control_closed g V u.
  Proof.
    intros. destruct (V_In_dec u).
    - left; assumption.
    - right. split; [|apply H].
      eapply Critical_candidate_Weakly_deciding; apply H.
  Qed.

  Lemma Weakly_control_closed_closed : forall g V
    (V_In_dec : forall u, V u \/ ~ V u),
    Weakly_control_closed g V ->
    Minimal_Weakly_control_closed g V == V.
  Proof.
    intros. eapply Minimum_unicity.
    - apply th54_explicit. assumption.
    - split.
      + split.
        * intros u ?; assumption.
        * split; assumption.
      + intros V' (H1 & H2 & H3).
        assumption.
  Qed.

  (* replace >= 2 and 1 with n and n' and condition n' < n ? *)

  Lemma Weakly_deciding_at_least_2 : forall g V u
    (V_In_dec : forall u, V u \/ ~ V u),
    Weakly_deciding g V u ->
    exists n, cardinal (Observable g V u) n /\ 2 <= n.
  Proof.
    intros. pose proof (Observable_finite g V u V_In_dec) as u_card.
    apply finite_cardinal in u_card. destruct u_card as (n & u_card).
    exists n. split; [assumption|].
    destruct H as (u1 & u2 & p1 & p2 & Hp1 & Hp2 & HDisj).
    assert (Observable g V u u1) as HObsu1.
    { exists (u::p1). assumption. }
    assert (Observable g V u u2) as HObsu2.
    { exists (u::p2). assumption. }
    destruct n.
    - inversion u_card; subst. apply H in HObsu1.
      apply Noone_in_empty in HObsu1. contradiction.
    - destruct n.
      + apply cardinal_1_Singleton in u_card. destruct u_card as (w & u_card).
        apply u_card in HObsu1. apply u_card in HObsu2.
        apply Singleton_inv in HObsu1. apply Singleton_inv in HObsu2.
        inversion HDisj. contradiction (H w). apply Intersection_intro;
          apply List.in_app_iff; right; left; symmetry; assumption.
      + do 2 apply le_n_S. apply le_0_n.
  Qed.

  Lemma Is_VPath_In_Suppport : forall g V u v p,
    Is_VPath g V u v p -> Support g v.
  Proof.
    intros. destruct H as (_ & (H & _) & _ & _).
    rewrite Forall_forall in H. apply H. apply in_app_finite_r. apply In_here.
  Qed.

  Lemma VPath_In_Suppport : forall g V u v,
    VPath g V u v -> Support g v.
  Proof.
    intros. destruct H as (p & H). apply Is_VPath_In_Suppport in H.
    assumption.
  Qed.

End AbstractGraphPropertiesDecFinite.
