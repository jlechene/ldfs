(** A modern implementation of the [Coq.Sets.Finite_sets] module from the
    standard library.
    It includes:
    - implicit arguments
    - setoid equality
*)

Require Export Constructive_sets.
Require Export Finite_sets.
Require Export Classical_sets.
Require Export Image.
Require List. Import List.ListNotations.
(* Require Finite_sets_facts. *)

Require Import Setoid.

Arguments Same_set {_} _ _.
Arguments In {_} _ _.
Arguments Included {_} _ _.
Arguments Singleton {_} _ _.
Arguments Union {_} _ _ _.
Arguments Intersection {_} _ _ _.
Arguments Add {_} _ _ _.
Arguments Subtract {_} _ _ _.
Arguments Empty_set {_} _.
Arguments Im {_} {_} _ _ _.
Arguments Ensembles.Complement {_} _ _.
Arguments Disjoint {_} _ _.
Arguments Setminus {_} _ _ _.

Lemma Same_set_refl : forall {A} (X:Ensemble A),
  Same_set X X.
Proof.
  split; unfold Included; intros; assumption.
Qed.

Lemma Same_set_sym : forall {A} (X1 X2:Ensemble A),
  Same_set X1 X2 -> Same_set X2 X1.
Proof.
  intros. split; intros; apply H; intros.
Qed.

Lemma Same_set_trans : forall {A} (X1 X2 X3:Ensemble A),
  Same_set X1 X2 -> Same_set X2 X3 -> Same_set X1 X3.
Proof.
  intros. split.
  - unfold Included; intros. apply H0, H. assumption.
  - unfold Included; intros. apply H, H0. assumption.
Qed.

Add Parametric Relation {A} : (Ensemble A) Same_set
  reflexivity proved by Same_set_refl
  symmetry proved by Same_set_sym
  transitivity proved by Same_set_trans
as Same_set_equiv.

Lemma Same_set_iff : forall {A} (X Y:Ensemble A), Same_set X Y <->
  (forall x, X x <-> Y x).
Proof.
  split; intros.
  - split; intros; apply H; assumption.
  - split; intros; intros x ?.
    + rewrite <- H; assumption.
    + rewrite H; assumption.
Qed.

(* To allow rewrite on goals like [In X x] *)
Instance Same_set_pointwise {A:Type} :
  subrelation Same_set (Morphisms.pointwise_relation A iff).
Proof.
  split; intros; apply H; assumption.
Qed.

Notation "X == Y" := (Same_set X Y) (at level 70).

Add Parametric Morphism {A} : (@In A)
  with signature Same_set ==> eq ==> iff as In_Same_set.
Proof.
  intros X1 X2 X_Same x.
  split; intros; apply X_Same; assumption.
Qed.

Add Parametric Morphism {A} : (@Included A)
  with signature Same_set ==> Same_set ==> iff as Included_Same_set.
Proof.
  intros X1 X2 X_Same Y1 Y2 Y_Same.
  split; intros.
  - unfold Included; intros. apply Y_Same. apply H. apply X_Same. assumption.
  - unfold Included; intros. apply Y_Same. apply H. apply X_Same. assumption.
Qed.

Add Parametric Morphism {A} : (@Singleton A)
  with signature eq ==> Same_set as Singleton_Same_set.
Proof.
  intros x. reflexivity.
Qed.

Add Parametric Morphism {A} : (@Union A)
  with signature Same_set ==> Same_set ==> Same_set as Union_Same_set.
Proof.
  intros X1 X2 X_Same Y1 Y2 Y_Same.
  split.
  - unfold Included; intros.
    apply Union_inv in H. destruct H.
    + apply Union_introl. apply X_Same. assumption.
    + apply Union_intror. apply Y_Same. assumption.
  - unfold Included; intros.
    apply Union_inv in H. destruct H.
    + apply Union_introl. apply X_Same. assumption.
    + apply Union_intror. apply Y_Same. assumption.
Qed.

Add Parametric Morphism {A} : (@Intersection A)
  with signature Same_set ==> Same_set ==> Same_set as Intersection_Same_set.
Proof.
  intros X1 X2 X_Same Y1 Y2 Y_Same.
  split.
  - unfold Included; intros.
    apply Intersection_inv in H. destruct H.
    apply Intersection_intro.
    + apply X_Same. assumption.
    + apply Y_Same. assumption.
  - unfold Included; intros.
    apply Intersection_inv in H. destruct H.
    apply Intersection_intro.
    + apply X_Same. assumption.
    + apply Y_Same. assumption.
Qed.

Add Parametric Morphism {A} : (@Add A)
  with signature Same_set ==> eq ==> Same_set as Add_Same_set.
Proof.
  intros X1 X2 X_Same x. unfold Add. rewrite X_Same.
  reflexivity.
Qed.

Add Parametric Morphism {A} : (@Subtract A)
  with signature Same_set ==> eq ==> Same_set as Subtract_Same_set.
Proof.
  intros X1 X2 X_Same x.
  split.
  - unfold Included; intros.
    apply Subtract_inv in H. destruct H.
    apply Subtract_intro; [|assumption].
    apply X_Same; assumption.
  - unfold Included; intros.
    apply Subtract_inv in H. destruct H.
    apply Subtract_intro; [|assumption].
    apply X_Same; assumption.
Qed.

Add Parametric Morphism {A B} : (@Im A B)
  with signature Same_set ==> eq ==> Same_set as Im_Same_set.
Proof.
  intros X1 X2 X_Same f.
  split; intros b ?.
  - apply Im_inv in H. destruct H as (a & H1 & H2).
    apply Im_intro with (x:=a).
    + apply X_Same. assumption.
    + symmetry; assumption.
  - apply Im_inv in H. destruct H as (a & H1 & H2).
    apply Im_intro with (x:=a).
    + apply X_Same. assumption.
    + symmetry; assumption.
Qed.

Add Parametric Morphism {A} : (@Ensembles.Complement A)
  with signature Same_set ==> Same_set as Complement_Same_set.
Proof.
  intros X1 X2 X_Same.
  unfold Ensembles.Complement.
  split; intros x ?; unfold In in *.
  - rewrite <- X_Same. assumption.
  - rewrite X_Same. assumption.
Qed.

Add Parametric Morphism {A} : (@Ensembles.Disjoint A)
  with signature Same_set ==> Same_set ==> iff as Disjoint_Same_set.
Proof.
  intros X1 X2 X_Same Y1 Y2 Y_Same.
  split; intros.
  - inversion H. constructor. intros x contra. apply (H0 x).
    rewrite X_Same, Y_Same. assumption.
  - inversion H. constructor. intros x contra. apply (H0 x).
    rewrite <- X_Same, <- Y_Same. assumption.
Qed.

Add Parametric Morphism {A} : (@Ensembles.Setminus A)
  with signature Same_set ==> Same_set ==> Same_set as Setminus_Same_set.
Proof.
  intros X1 X2 X_Same Y1 Y2 Y_Same. rewrite Same_set_iff. intros.
  unfold Setminus. rewrite <- X_Same, Y_Same. reflexivity.
Qed.

Inductive Finite {A:Type} : Ensemble A -> Prop :=
| Empty_is_finite : forall X, X == Empty_set -> Finite X
| Union_is_finite : forall X, Finite X -> forall x, ~ X x ->
    forall Y, Y == Add X x -> Finite Y.

Add Parametric Morphism {A} : (@Finite A)
  with signature Same_set ==> iff as Finite_Same_set.
Proof.
  intros X1 X2 HSame. split; [intros X1_Fin|intros X2_Fin].
  - destruct X1_Fin as [X1|X1 X1_Fin x ? Y ?]; intros.
    + apply Empty_is_finite. rewrite <- HSame. assumption.
    + apply (Union_is_finite X1 X1_Fin x H).
      rewrite <- HSame. assumption.
  - destruct X2_Fin as [X2|X2 ? x ? Y ?]; intros.
    + apply Empty_is_finite. rewrite HSame. assumption.
    + apply (Union_is_finite X2 X2_Fin x H).
      rewrite HSame. assumption.
Qed.

Inductive cardinal {A : Type} : Ensemble A -> nat -> Prop :=
| card_empty : forall X, X == Empty_set -> cardinal X 0
| card_add : forall (X : Ensemble A) (n : nat), cardinal X n ->
    forall x, ~ X x -> forall Y, Y == Add X x -> cardinal Y (S n).

Add Parametric Morphism {A} : (@cardinal A)
  with signature Same_set ==> eq ==> iff as cardinal_Same_set.
Proof.
  intros X1 X2 HSame n. split; [intros X1_card|intros X2_card].
  - destruct X1_card as [X1|X1 n X1_card x ? Y ?]; intros.
    + apply card_empty. rewrite <- HSame. assumption.
    + apply (card_add X1 n X1_card x H).
      rewrite <- HSame. assumption.
  - destruct X2_card as [X2|X2 n X2_card x ? Y ?]; intros.
    + apply card_empty. rewrite HSame. assumption.
    + apply (card_add X2 n X2_card x H).
      rewrite HSame. assumption.
Qed.

Definition Strict_Included {A:Type} (U1 U2 : Ensemble A) :=
  Included U1 U2 /\ exists u, ~ U1 u /\ U2 u.

Definition Strict_Included_finite {A:Type} (U1 U2 : Ensemble A) :=
  Finite U2 /\ (forall u, U1 u \/ ~ U1 u) /\
  Included U1 U2 /\ exists u, ~ U1 u /\ U2 u.

Add Parametric Morphism A : (@Strict_Included A)
  with signature Same_set ==> Same_set ==> iff
  as Strict_Included_Same.
Proof.
  intros U1 U2 U_Same W1 W2 W_Same.
  unfold Strict_Included. setoid_rewrite U_Same. setoid_rewrite W_Same.
  reflexivity.
Qed.

Add Parametric Morphism A : (@Strict_Included_finite A)
  with signature Same_set ==> Same_set ==> iff
  as Strict_Included_finite_Same.
Proof.
  intros U1 U2 U_Same W1 W2 W_Same.
  unfold Strict_Included_finite. setoid_rewrite U_Same. setoid_rewrite W_Same.
  reflexivity.
Qed.

Arguments Noone_in_empty {_} _ _.

Definition Ensemble_of_list {A} (l:list A) : Ensemble A :=
  fun a => List.In a l.
