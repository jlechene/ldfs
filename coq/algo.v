(** Defines the [preCFG] functor, which provides an algorithm to compute the
    minimal weakly-control closed set containing a given set.
    [algo61] computes one step of the algorithm, while [real_algo61] is
    the complete algorithm.
    [real_algo61_correct] provides the proof of correctness of [real_algo61].
*)

Require Import graph3.
Require Import infList. Import infListNotations.
Require Import MyFinite.
Require Import Finite_facts.
Require Import String.

Require Import ConcreteGraph.Constructive_DigraphInterface.
Require Import ConcreteGraph.Graph_Properties2.
Require Import ConcreteGraph.Constructive_Graph_Properties.
Require Import ConcreteGraph.Dijkstra.

Require Import SetoidList.
Require Import Setoid.
Require Import RelationClasses.
Require Import Bool.
Require List. Import List.ListNotations.
Require Import Arith.
Require Import Wellfounded.

Module preCFG (Vertex:OrderedTypeEx.UsualOrderedType)
              (Lab:Labels.Labels)
              (C : COMBINE_TYPE with Definition Label := option Lab.EdgeLabel)
              (Import DG : Constructive_Directed_Graph Vertex Lab).

  Module V <: Equalities.UsualDecidableType.
    Include Vertex.
    Definition eq_equiv : RelationClasses.Equivalence eq := eq_equivalence.
  End V.

  Module Export GP := GraphProps V Lab DG.
  Module Export CGP := ConstructiveGraphProps V Lab DG.

  Module G <: ABSTRACT_FINITE_GRAPH V C.
    Definition Graph := t.
    Definition Rel g u l v := In_graph_labeled_edge (u, v) g l.
    Definition Support g u := In_graph_vertex u g.
    Lemma Support_spec : forall (g : Graph) (u : V.t) l (v : V.t),
         Rel g u l v -> Support g u /\ Support g v.
    Proof.
      intros. unfold Rel in H.
      apply In_graph_labeled_edge_spec in H.
      apply EMapFacts.MapsTo_In in H.
      apply In_graph_edge_spec in H.
      apply In_graph_edge_in_ext in H. apply H.
    Qed.

    Lemma Support_finite : forall g : Graph, Finite (Support g).
    Proof.
      intros.
      apply (Finite_downward_closed eq_dec
        (Ensemble_of_list (List.map fst (VertexMap.elements (V g))))).
      - apply (Ensemble_of_list_finite V.eq_dec).
      - intros u. destruct (In_graph_vertex_dec u g).
        + left; assumption.
        + right; assumption.
      - intros u ?. apply In_graph_vertex_spec in H.
        apply MapFacts.elements_in_iff in H.
        destruct H as (l & H).
        apply SetoidList.InA_alt in H.
        destruct H as ((v & l') & H1 & H2).
        destruct H1. simpl in H, H0. subst v l'.
        apply List.in_map_iff.
        exists (u, l). split; [reflexivity|]. assumption.
    Qed.

    Lemma Rel_finite : forall (g : Graph) (u : V.t),
      Finite (fun v : V.t => exists l, Rel g u l v).
    Proof.
      intros.
      apply (Finite_downward_closed eq_dec
        (Ensemble_of_list (List.map fst
          (VertexMap.elements (successors u g))))).
      - apply (Ensemble_of_list_finite V.eq_dec).
      - intros v. destruct (In_graph_edge_dec (u, v) g).
        + apply In_graph_edge_strong in i. destruct i as (l & i).
          left. exists l. assumption.
        + right. intros contra. apply n. destruct contra as (l & contra).
          apply In_graph_labeled_edge_weak in contra. assumption.
      - intros v ?. destruct H as (l & H).
        apply mapsto_edge_equiv_succ in H.
        apply MapFacts.elements_mapsto_iff in H.
        apply SetoidList.InA_alt in H.
        destruct H as ((v' & l') & H1 & H2).
        destruct H1 as (H11 & H12). simpl in H11, H12.
        subst v' l'.
        apply List.in_map_iff.
        exists (v, l). split; [reflexivity|]. assumption.
    Qed.
  End G.

  Include G.

  Module Lab_with_weight <: Labels.Labels_with_weight.
    Include Lab.
    Definition weight (_:EdgeLabel) : nat := 1.
  End Lab_with_weight.

  Module Graph := AbstractGraphPropertiesDecFinite V C G.
  Include Graph.
  Module Import Dijkstra := Shortest_path V Lab_with_weight DG.
  Import Dijkstra.P.

  Lemma V_Equal_iff_Same_Support :
    forall g1 g2, VertexMap.Equal (V g1) (V g2) -> Support g1 == Support g2.
  Proof.
    intros. apply Same_set_iff. intros u.
    rewrite MapFacts.Equal_mapsto_iff in H. unfold Support.
    rewrite 2!In_graph_vertex_spec.
    specialize (H u).
    split; intros.
    - apply VMapFacts.In_MapsTo in H0. destruct H0 as (l & H0).
      apply H in H0. apply VMapFacts.MapsTo_In in H0. assumption.
    - apply VMapFacts.In_MapsTo in H0. destruct H0 as (l & H0).
      apply H in H0. apply VMapFacts.MapsTo_In in H0. assumption.
  Qed.

  Lemma E_Equal_iff_Rel : forall g1 g2,
    EdgeMap.Equal (E g1) (E g2) <->
    (forall u l v, Rel g1 u l v <-> Rel g2 u l v).
  Proof.
    intros. rewrite EMapFacts.Equal_mapsto_iff. unfold Rel.
    setoid_rewrite In_graph_labeled_edge_spec. split; intros.
    - apply H.
    - destruct k; apply H.
  Qed.

  Instance eq_Equal : subrelation eq Equal.
  Proof.
    intros g1 g2 H.
    split.
    - apply V_Equal_iff_Same_Support. apply H.
    - apply E_Equal_iff_Rel. apply H.
  Qed.

  Lemma Rel_w_In_graph_edge : forall g u v,
    Rel_w g u v <-> In_graph_edge (u,v) g.
  Proof.
    intros. unfold Rel_w, Rel. split; intros.
    - destruct H as (l & H). apply In_graph_labeled_edge_weak in H. assumption.
    - apply In_graph_edge_strong. assumption.
  Qed.

  Definition reachable g v v' :=
    let pathmap := Dijkstra g v in
    let vs := VertexMap.find v' (map pathmap) in
    match vs with
    | None | Some Unreached => false
    | Some (Reached _ l _) => true
    end.

  Lemma path_Reachable : forall l u v g n,
    path l u v g n -> Reachable g u v.
  Proof.
    intros. induction H.
    - exists []. subst v2. repeat constructor.
    - apply Reachable_trans with (u2:=v2); [assumption|].
      exists [v2;v3]. split.
      + split.
        * apply Support_spec in H0. destruct H0.
          repeat (constructor; try assumption).
        * constructor; [repeat constructor|].
          constructor. exists w. apply H0.
      + split; reflexivity.
  Qed.

  Lemma Reachable_path : forall g u v,
    Reachable g u v -> exists p n, path p u v g n.
  Proof.
    intros. destruct H as (p & H1 & H2 & H3). revert u v H2 H3.
    induction p; intros.
    - exists [], 0. constructor. assumption.
    - simpl in H2. subst a. rewrite last.last_cons_change_default in H3.
      destruct p as [|w p].
      + simpl in H3. exists [], 0. constructor. assumption.
      + pose proof H1 as H11.
        setoid_replace (u :: w :: p)%inf with ([u]%list ++ w :: p)%inf in H11
          by (simpl_inf; reflexivity).
        apply Path_finite_cut in H11.
        apply IHp with (u:=w) (v:=v) in H11.
        * destruct H11 as (l & n & H11).
          simpl_inf in H1. inversion H1. inversion H0.
          inversion H6. destruct H8 as (n' & H8).
          exists ((u,  w)::l), (eval n'+n).
          replace ((u, w)::l) with ([(u,w)]++l) by reflexivity.
          { apply path_trans with (v2:=w).
          - rewrite <- PeanoNat.Nat.add_0_l.
            rewrite <- List.app_nil_l with (l:=[(u,w)]).
            apply path_post with (v2:=u).
            + constructor. reflexivity.
            + apply H8.
            + apply Edge.eq_refl.
          - assumption. }
        * reflexivity.
        * rewrite last.last_cons_change_default.
          rewrite last.last_cons_change_default in H3. assumption.
  Qed.

  Lemma reachable_spec : forall g u v,
    reachable g u v = true <-> Reachable g u v.
  Proof.
    split; intros.
    - unfold reachable in H.
      destruct (VertexMap.find v (map (Dijkstra g u))) eqn:HDij.
      + destruct v0.
        * apply Dijkstra_path in HDij. apply path_Reachable in HDij. assumption.
        * discriminate.
      + discriminate.
    - unfold reachable.
      destruct (V.eq_dec u v).
      + rewrite <- e. pose proof root_find (Dijkstra g u).
        rewrite Dijkstra_root in H0. rewrite H0. reflexivity.
      + destruct (VertexMap.find v (map (Dijkstra g u))) eqn:HDij.
        * destruct v0; [reflexivity|].
          apply Reachable_In_Support in H as H2; [|assumption].
          destruct H2 as (_ & H2).
          pose proof (completeness_2 g u v).
          rewrite Dijkstra_graph, Dijkstra_root in H0.
          apply Reachable_path in H. specialize (H0 H2 H).
          contradiction.
        * apply Reachable_In_Support in H; [|assumption].
          destruct H as (_ & H).
          pose proof (in_map_in_graph (Dijkstra g u) v).
          rewrite Dijkstra_graph in H0.
          specialize (H0 H).
          apply MapFacts.not_find_in_iff in HDij. contradiction.
  Qed.

  Module VertexSet := FSetAVL.Make V.

  Definition Ensemble_of_set V : Ensemble V.t := fun x => VertexSet.In x V.

  Definition reachable_from g V u :=
    VertexSet.exists_ (fun v => reachable g v u) V.

  Lemma reachable_from_correct : forall g U u,
    reachable_from g U u = true <->
    Reachable_from g (Ensemble_of_set U) u.
  Proof.
    split; intros.
    - unfold reachable_from in H. apply VertexSet.exists_2 in H.
      + destruct H as (v & H1 & H2).
        exists v. split; [assumption|]. apply reachable_spec. assumption.
      + unfold compat_bool; typeclasses eauto.
    - apply VertexSet.exists_1.
      + unfold compat_bool; typeclasses eauto.
      + destruct H as (v & H1 & H2).
        exists v. split; [assumption|]. apply reachable_spec. assumption.
  Qed.

  Definition filter_edges (f:Edge.t -> bool) g :=
    fold_edges (fun _ e l h => if f e then h
                                else remove_edge e h)
               g g.

  Lemma filter_edges_vertex : forall (f:_->bool) g u l,
    In_graph_labeled_vertex u (filter_edges f g) l <->
    In_graph_labeled_vertex u g l.
  Proof.
    intros. unfold filter_edges. rewrite fold_edges_spec.
    rewrite EdgeMap.fold_1. apply fold_invariant.
    - reflexivity.
    - intros. destruct (f (fst x)).
      + assumption.
      + rewrite In_graph_remove_edge_vertex. assumption.
  Qed.

  Lemma filter_edges_edge : forall (f:_->bool) g e l, f e = true ->
    In_graph_labeled_edge e (filter_edges f g) l <->
    In_graph_labeled_edge e g l.
  Proof.
    intros. unfold filter_edges. rewrite fold_edges_spec.
    rewrite EdgeMap.fold_1. apply fold_invariant.
    - reflexivity.
    - intros. destruct (f (fst x)) eqn:Heqf.
      + assumption.
      + rewrite In_graph_remove_edge_edge.
        * assumption.
        * intros contra. destruct contra.
          destruct (fst x), e; simpl in *; subst. rewrite H in Heqf.
          discriminate.
  Qed.

  Lemma fold_becomes_true :
    forall {A B : Type} (eqA : A -> A -> Prop) {eq_equiv : Equivalence eqA}
    (f : A -> B -> A)
    (f_assoc : forall (y z : B) (s : A), eqA (f (f s y) z) (f (f s z) y))
    (f_compat : Morphisms.Proper (eqA ==> Logic.eq ==> eqA) f)
    (P : A -> Prop) (P_compat : Morphisms.Proper (eqA ==> iff) P)
    (l : list B) (x : A),
    List.Exists (fun a => forall b, P (f b a)) l -> P (List.fold_left f l x).
  Proof.
    intros. revert dependent x. induction l; intros.
    - inversion H.
    - inversion H; subst.
      + rewrite fold_left_assoc.
        * apply H1.
        * reflexivity.
        * intros. transitivity y; assumption.
        * intros. rewrite f_assoc. reflexivity.
        * intros. rewrite H0. reflexivity.
      + apply IHl. assumption.
  Qed.

  Lemma fold_left_assoc_NoDup :
    forall {A B : Type} (eqA : A -> A -> Prop) {eq_equiv : Equivalence eqA}
    (eqB : B -> B -> Prop)
    (f : A -> B -> A)
    (f_assoc : forall (y z : B) (s : A),
      ~ eqB y z -> eqA (f (f s y) z) (f (f s z) y))
    (f_compat : Morphisms.Proper (eqA ==> Logic.eq ==> eqA) f)
    (l : list B) (l_NoDup : NoDupA eqB l) (x : A) a (a_not_In : ~ InA eqB a l),
    eqA (fold_left f (a :: l) x) (f (fold_left f l x) a).
  Proof.
    intros. revert x a a_not_In. induction l; intros.
    - simpl. reflexivity.
    - simpl. simpl in IHl. inversion l_NoDup. rewrite !IHl; try assumption.
      rewrite f_assoc. reflexivity.
      intros contra. apply a_not_In. left. assumption.
      intros contra. apply a_not_In; right; assumption.
  Qed.

  Lemma fold_becomes_true_NoDup : 
    forall {A B : Type} (eqA : A -> A -> Prop) {eq_equiv : Equivalence eqA}
    (eqB : B -> B -> Prop) {eqB_equiv : Equivalence eqB}
    (f : A -> B -> A)
    (f_assoc : forall (y z : B) (s : A),
      ~ eqB y z -> eqA (f (f s y) z) (f (f s z) y))
    (f_compat : Morphisms.Proper (eqA ==> Logic.eq ==> eqA) f)
    (P : A -> Prop) (P_compat : Morphisms.Proper (eqA ==> iff) P)
    (l : list B) (l_NoDup : NoDupA eqB l) (x : A),
    List.Exists (fun b => forall l', incl l' l -> ~ InA eqB b l' ->
      P (f (fold_left f l' x) b)) l ->
    P (List.fold_left f l x).
  Proof.
    intros. revert dependent x. induction l; intros.
    - inversion H.
    - inversion H; subst.
      + rewrite fold_left_assoc_NoDup; try assumption.
        * { apply H1.
          - intros a' ?. right; assumption.
          - inversion l_NoDup; assumption. }
        * apply f_assoc.
        * inversion l_NoDup; assumption.
        * inversion l_NoDup; assumption.
      + apply IHl.
        * inversion l_NoDup; assumption.
        * apply List.Exists_exists in H1. destruct H1 as (b & H11 & H12).
          apply List.Exists_exists. exists b. split; [assumption|].
          intros.
          replace (fold_left f l' (f x a)) with (fold_left f (a::l') x)
            by reflexivity. { apply H12.
          - intros a' ?. destruct H2; [left|right; apply H0]; assumption.
          - intros contra. inversion contra; subst; [|contradiction].
            inversion l_NoDup; subst. rewrite <- H3 in H5. apply H5.
            apply In_InA; assumption. }
  Qed.

  Lemma fold_left_assoc_NoDup_eqB :
    forall {A B : Type} (eqA : A -> A -> Prop) {eq_equiv : Equivalence eqA}
    (eqB : B -> B -> Prop) {eqB_equiv : Equivalence eqB}
    (f : A -> B -> A)
    (f_assoc : forall (y z : B) (s : A),
      ~ eqB y z -> eqA (f (f s y) z) (f (f s z) y))
    (f_compat : Morphisms.Proper (eqA ==> eqB ==> eqA) f)
    (l : list B) (l_NoDup : NoDupA eqB l) (x : A) a (a_not_In : ~ InA eqB a l),
    eqA (fold_left f (a :: l) x) (f (fold_left f l x) a).
  Proof.
    intros. revert x a a_not_In. induction l; intros.
    - simpl. reflexivity.
    - simpl. simpl in IHl. inversion l_NoDup. rewrite !IHl; try assumption.
      rewrite f_assoc. reflexivity.
      intros contra. apply a_not_In. left. assumption.
      intros contra. apply a_not_In; right; assumption.
  Qed.

  Lemma fold_becomes_true_NoDup_eqB : 
    forall {A B : Type} (eqA : A -> A -> Prop) {eq_equiv : Equivalence eqA}
    (eqB : B -> B -> Prop) {eqB_equiv : Equivalence eqB}
    (f : A -> B -> A)
    (f_assoc : forall (y z : B) (s : A),
      ~ eqB y z -> eqA (f (f s y) z) (f (f s z) y))
    (f_compat : Morphisms.Proper (eqA ==> eqB ==> eqA) f)
    (P : A -> Prop) (P_compat : Morphisms.Proper (eqA ==> iff) P)
    (l : list B) (l_NoDup : NoDupA eqB l) (x : A),
    List.Exists (fun b => forall l', incl l' l -> ~ InA eqB b l' ->
      P (f (fold_left f l' x) b)) l ->
    P (List.fold_left f l x).
  Proof.
    intros. revert dependent x. induction l; intros.
    - inversion H.
    - inversion H; subst.
      + rewrite fold_left_assoc_NoDup_eqB; try eassumption.
        * { apply H1.
          - intros a' ?. right; assumption.
          - inversion l_NoDup; assumption. }
        * inversion l_NoDup; assumption.
        * inversion l_NoDup; assumption.
      + apply IHl.
        * inversion l_NoDup; assumption.
        * apply List.Exists_exists in H1. destruct H1 as (b & H11 & H12).
          apply List.Exists_exists. exists b. split; [assumption|].
          intros.
          replace (fold_left f l' (f x a)) with (fold_left f (a::l') x)
            by reflexivity. { apply H12.
          - intros a' ?. destruct H2; [left|right; apply H0]; assumption.
          - intros contra. inversion contra; subst; [|contradiction].
            inversion l_NoDup; subst. rewrite <- H3 in H5. apply H5.
            apply In_InA; assumption. }
  Qed.

  Lemma filter_edges_edge_2 : forall (f:_->bool) g e, f e = false ->
    ~ In_graph_edge e (filter_edges f g).
  Proof.
    intros. unfold filter_edges. rewrite fold_edges_spec.
    rewrite EdgeMap.fold_1.
    destruct (In_graph_edge_dec e g).
    - apply (fold_becomes_true eq).
      + intros. destruct (f (fst z)) eqn:Heqfz; [reflexivity|].
        destruct (f (fst y)) eqn:Heqfy; [reflexivity|].
        apply remove_edge_comm.
      + intros g1 g2 g_eqG e1 e2 Heq. subst e2.
        destruct (f (fst e1)); rewrite g_eqG; reflexivity.
      + intros g1 g2 g_eqG. rewrite g_eqG. reflexivity.
      + apply List.Exists_exists. apply In_graph_edge_strong in i.
        destruct i as (l & i).
        exists (e, l). split.
        * apply In_graph_labeled_edge_spec in i.
          apply EMapFacts.elements_mapsto_iff in i.
          apply InA_alt in i.
          destruct i as ((e', l') & ((i1 & i2) & i3) & i4).
          destruct e, e'; simpl in i1, i2, i3; subst; assumption.
        * intros h. simpl. rewrite H. apply In_graph_remove_edge_edge_2.
    - apply fold_invariant.
      + assumption.
      + intros (e', l) h. intros. destruct (f (fst (e', l))).
        * assumption.
        * { destruct (Edge.eq_dec e' e).
          - rewrite e0. apply In_graph_remove_edge_edge_2.
          - intros contra. apply In_graph_edge_strong in contra.
            destruct contra as (l' & contra).
            rewrite In_graph_remove_edge_edge in contra by assumption.
            apply In_graph_labeled_edge_weak in contra. contradiction. }
  Qed.

  Definition copy g := fold_vertices (fun g u l h => add_vertex u h l) g empty_graph.

  Lemma In_graph_copy_vertex : forall g u l,
    In_graph_labeled_vertex u (copy g) l <-> In_graph_labeled_vertex u g l.
  Proof.
    split.
    - unfold copy. rewrite fold_vertices_spec. rewrite VertexMap.fold_1.
      apply fold_invariant.
      + intros contra. apply In_graph_labeled_vertex_weak in contra.
        apply empty_graph_empty in contra. contradiction.
      + intros x h. intros. apply In_add_vertex in H1. destruct H1.
        * apply In_graph_labeled_vertex_spec.
          apply VMapFacts.elements_mapsto_iff. apply InA_alt. exists x.
          split. split. apply H1. symmetry. apply H1.
          assumption.
        * apply H0; assumption.
    - intros. unfold copy.
      rewrite fold_vertices_spec. rewrite VertexMap.fold_1.
      apply (fold_becomes_true_NoDup eq (VertexMap.eq_key (elt:=_))).
      + intros x1 x2 h x_neq.
        apply add_vertex_comm. intros contra. symmetry in contra. contradiction.
      + intros g1 g2 g_eqG x1 x2 x_eq. subst x2. rewrite <- g_eqG.
        reflexivity.
      + intros u1 u2 u_eq. setoid_rewrite <- u_eq. reflexivity.
      + apply VertexMap.elements_3w.
      + apply List.Exists_exists. exists (u, l). split.
        * apply In_graph_labeled_vertex_spec in H.
          apply VMapFacts.elements_mapsto_iff in H. apply InA_alt in H.
          destruct H as (x & (H1 & H2) & H3).
          destruct x; simpl in H1, H2; subst; assumption.
        * simpl. intros.
          rewrite In_add_vertex.
          left. { split.
          - apply fold_invariant.
            + apply empty_graph_empty.
            + intros. intros contra. apply In_graph_vertex_strong in contra.
              destruct contra as (l'' & contra).
              rewrite In_add_vertex in contra. destruct contra.
              * apply H1. destruct H4 as (_ & H41 & H42). apply InA_alt.
                exists x. split; [|assumption]. apply H41.
              * apply In_graph_labeled_vertex_weak in H4. contradiction.
          - split; reflexivity. }
  Qed.

  Lemma In_graph_copy_edge : forall g u l,
    ~ In_graph_labeled_edge u (copy g) l.
  Proof.
    intros. unfold copy. rewrite fold_vertices_spec. rewrite VertexMap.fold_1.
    apply fold_invariant.
    - intros contra. apply In_graph_labeled_edge_weak in contra.
      apply In_graph_edge_In_ext in contra. destruct contra as (contra & _).
      apply empty_graph_empty in contra. contradiction.
    - intros x g'. intros. rewrite In_add_vertex_edge. assumption.
  Qed.

  Definition map f g :=
    let g' := copy g in
    fold_edges (fun _ e l h => add_edge (f e) h l) g' empty_graph.

  Definition copy_without_vertices g V :=
    filter_edges
      (fun e => if (VertexSet.mem (fst e) V) then false else true)
      g.

  Lemma In_graph_copy_without_vertices_vertex : forall g U u l,
    In_graph_labeled_vertex u (copy_without_vertices g U) l <->
    In_graph_labeled_vertex u g l.
  Proof.
    intros.
    apply filter_edges_vertex.
  Qed.

  Lemma In_graph_copy_without_vertices_vertex_2 : forall g U u,
    In_graph_vertex u (copy_without_vertices g U) <->
    In_graph_vertex u g.
  Proof.
    split; intros.
    - apply In_graph_vertex_strong in H. destruct H as (l & H).
      apply In_graph_copy_without_vertices_vertex in H.
      apply In_graph_labeled_vertex_weak in H. assumption.
    - apply In_graph_vertex_strong in H. destruct H as (l & H).
      apply In_graph_labeled_vertex_weak with (val:=l).
      apply In_graph_copy_without_vertices_vertex. assumption.
  Qed.

  Lemma In_graph_copy_without_vertices_edge : forall g U e l,
    In_graph_labeled_edge e (copy_without_vertices g U) l <->
    In_graph_labeled_edge e g l /\ ~ VertexSet.In (fst e) U.
  Proof.
    intros. unfold copy_without_vertices.
    destruct (VertexSet.mem (fst e) U) eqn:Hmem.
    - split; intros.
      + apply In_graph_labeled_edge_weak in H. apply filter_edges_edge_2 in H.
        * contradiction.
        * rewrite Hmem. reflexivity.
      + apply VertexSet.mem_2 in Hmem. destruct H; contradiction.
    - rewrite filter_edges_edge.
      + split; intros.
        * split; [assumption|]. rewrite <- not_true_iff_false in Hmem.
          intros contra. apply Hmem. apply VertexSet.mem_1. assumption.
        * apply H.
      + rewrite Hmem. reflexivity.
  Qed.

  Lemma In_graph_copy_without_vertices_edge_2 : forall g U e,
    In_graph_edge e (copy_without_vertices g U) <->
    In_graph_edge e g /\ ~ VertexSet.In (fst e) U.
  Proof.
    split; intros.
    - apply In_graph_edge_strong in H. destruct H as (l & H).
      apply In_graph_copy_without_vertices_edge in H. destruct H as (H1 & H2).
      split; [|assumption]. apply In_graph_labeled_edge_weak in H1. assumption.
    - destruct H as (H1 & H2). apply In_graph_edge_strong in H1.
      destruct H1 as (l & H1). apply In_graph_labeled_edge_weak with (val:=l).
      apply In_graph_copy_without_vertices_edge. split; assumption.
  Qed.

  Lemma copy_without_vertices_idempotent : forall g U,
    copy_without_vertices (copy_without_vertices g U) U =G
    copy_without_vertices g U.
  Proof.
    intros. apply eq_In_graph.
    split; intros.
    - rewrite In_graph_copy_without_vertices_vertex. reflexivity.
    - rewrite 2!In_graph_copy_without_vertices_edge.
      split; intros; split; apply H.
  Qed.

  Definition theta_set g V u := 
    VertexSet.filter (fun x => reachable g u x) V.

  Lemma theta_set_spec : forall g V u v,
    VertexSet.In v (theta_set g V u) <-> VertexSet.In v V /\ Reachable g u v.
  Proof.
    intros. unfold theta_set. split; intros.
    - split.
      + apply VertexSet.filter_1 in H; [assumption|].
        unfold compat_bool. typeclasses eauto.
      + apply VertexSet.filter_2 in H. apply reachable_spec in H. assumption.
        unfold compat_bool. typeclasses eauto.
    - apply VertexSet.filter_3.
      + unfold compat_bool. typeclasses eauto.
      + apply H.
      + apply reachable_spec; apply H.
  Qed.

  Lemma theta_set_spec_no_edge : forall g U
    (no_edge_from_U : forall e, In_graph_edge e g -> ~ VertexSet.In (fst e) U)
    u (In_Support : Support g u) v,
    VertexSet.In v (theta_set g U u) <-> Observable g (Ensemble_of_set U) u v.
  Proof.
    intros.
    rewrite theta_set_spec. split; intros.
    - destruct H as (H1 & H2). destruct H2 as (p & H21 & H22 & H23).
      destruct (listOtherResults.list_nil_dec p).
      + subst p. simpl in H23. subst v. exists [].
        split.
        * reflexivity.
        * { split.
          - simpl_inf. repeat constructor. assumption.
          - split; [assumption|]. constructor. }
      + destruct (List.exists_last n) as (p' & w & n1).
        subst p. rewrite last.last_app_not_nil in H23 by discriminate.
        simpl in H23. subst w.
        exists p'. split.
        * destruct p'; assumption.
        * { split.
          - simpl_inf in H21. assumption.
          - split; [assumption|]. rewrite infList.Forall_forall.
            intros w H contra. rewrite In_finite in H.
            apply in_split in H.
            destruct H as (p1 & p2 & H). destruct p2 as [|w' p2].
            + apply (no_edge_from_U (w, v)).
              * rewrite H in H21. simpl_inf in H21.
                apply Path_finite_cut in H21. destruct H21 as (_ & H21).
                inversion H21. inversion H4.
                apply Rel_w_In_graph_edge; assumption.
              * assumption.
            + apply (no_edge_from_U (w, w')).
              * rewrite H in H21. simpl_inf in H21.
                apply Path_finite_cut in H21. destruct H21 as (_ & H21).
                inversion H21. inversion H4.
                apply Rel_w_In_graph_edge; assumption.
              * assumption. }
    - destruct H as (p & H1 & H2 & H3 & H4). split; [assumption|].
      exists (p++[v]). split.
      + simpl_inf. assumption.
      + split.
        * destruct p; assumption.
        * rewrite last.last_app_not_nil by discriminate. reflexivity.
  Qed.

  Lemma InA_eq : forall {A:Type} a (l:list A), InA Logic.eq a l <-> In a l.
  Proof.
    split; intros.
    - apply InA_alt in H. destruct H as (a' & H1 & H2). subst a'. assumption.
    - apply In_InA; [typeclasses eauto|]. assumption.
  Qed.

  Lemma StronglySorted_equivlist_eqlist : forall {A:Type}
    (lt:A->A->Prop),
    StrictOrder lt -> forall (l1 l2 :list A),
    StronglySorted lt l1 -> StronglySorted lt l2 ->
    equivlistA Logic.eq l1 l2 ->
    l1 = l2.
  Proof.
    intros A lt ? l1. induction l1; intros.
    - destruct l2.
      + reflexivity.
      + assert (InA Logic.eq a []) as Ha.
        { apply H2. left; reflexivity. }
        inversion Ha.
    - destruct l2.
      + assert (InA Logic.eq a []) as Ha.
        { apply H2. left; reflexivity. }
        inversion Ha.
      + assert (a = a0) as Ha.
        { assert (In a (a0::l2)) as Ha.
          { rewrite <- InA_eq. apply H2. left; reflexivity. }
          inversion Ha; subst.
          - reflexivity.
          - assert (In a0 (a::l1)) as Ha2.
            { rewrite <- InA_eq. apply H2. left; reflexivity. }
            inversion Ha2; subst.
            + reflexivity.
            + inversion H0; subst. rewrite Forall_forall in H8.
              apply H8 in H4.
              inversion H1; subst. rewrite Forall_forall in H10.
              apply H10 in H3.
              rewrite H3 in H4. contradiction (irreflexivity H4).
        }
        subst a0. f_equal. apply IHl1.
        * inversion H0; assumption.
        * inversion H1; assumption.
        * { split; intros.
          - assert (In x (a::l2)) as Ha.
            { rewrite <- InA_eq. apply H2. right; assumption. }
            inversion Ha; subst.
            + inversion H0; subst.
              rewrite Forall_forall in H7. rewrite InA_eq in H3.
              contradiction (irreflexivity (H7 _ H3)).
            + rewrite InA_eq. assumption.
          - assert (In x (a::l1)) as Ha.
            { rewrite <- InA_eq. apply H2. right; assumption. }
            inversion Ha; subst.
            + inversion H1; subst.
              rewrite Forall_forall in H7. rewrite InA_eq in H3.
              contradiction (irreflexivity (H7 _ H3)).
            + rewrite InA_eq. assumption.
          }
  Qed.

  Lemma Sorted_equivlist_eqlist : forall {A:Type}
    (lt:A->A->Prop),
    StrictOrder lt -> forall (l1 l2 :list A),
    Sorted lt l1 -> Sorted lt l2 ->
    equivlistA Logic.eq l1 l2 ->
    l1 = l2.
  Proof.
    intros. eapply StronglySorted_equivlist_eqlist; try eassumption.
    - apply Sorted_StronglySorted.
      + intros x y z ? ?. rewrite H3. assumption.
      + assumption.
    - apply Sorted_StronglySorted.
      + intros x y z ? ?. rewrite H3. assumption.
      + assumption.
  Qed.

  Lemma filter_elements : forall f V,
    VertexSet.elements (VertexSet.filter f V) = filter f (VertexSet.elements V).
  Proof.
(*     EA.NoDup_sorted_equiv_eq  *)
    intros f U.
    assert (equivlistA Logic.eq (VertexSet.elements (VertexSet.filter f U))
                                (filter f (VertexSet.elements U))).
    { split; intros.
      - rewrite InA_eq. apply filter_In.
        apply VertexSet.elements_2 in H. split.
        + rewrite <- InA_eq. apply VertexSet.elements_1.
          apply VertexSet.filter_1 in H; [assumption|].
          unfold compat_bool; typeclasses eauto.
        + apply VertexSet.filter_2 in H; [assumption|].
          unfold compat_bool; typeclasses eauto.
      - rewrite InA_eq in H. apply filter_In in H. destruct H as (H1 & H2).
        rewrite <- InA_eq in H1. apply VertexSet.elements_2 in H1.
        apply VertexSet.elements_1. apply VertexSet.filter_3.
        + unfold compat_bool; typeclasses eauto.
        + assumption.
        + assumption.
    }
    apply Sorted_equivlist_eqlist with (lt:=V.lt) in H.
    - assumption.
    - typeclasses eauto.
    - apply VertexSet.elements_3.
    - apply listOtherResults.filter_Sorted; [typeclasses eauto|].
      apply VertexSet.elements_3.
  Qed.

  Definition theta g V u :=
    VertexSet.fold (fun x acc =>
      if reachable g u x then S acc else acc) V 0.

  Lemma theta_length_theta_set : forall g V u,
    theta g V u = VertexSet.cardinal (theta_set g V u).
  Proof.
    intros g U u. unfold theta, theta_set. rewrite VertexSet.fold_1.
    rewrite VertexSet.cardinal_1.
    rewrite filter_elements.
    induction (VertexSet.elements U); intros.
    - simpl. reflexivity.
    - rewrite fold_left_assoc.
      + simpl.
        destruct (reachable g u a) eqn:Hreach.
        * simpl. apply f_equal. assumption.
        * assumption.
      + reflexivity.
      + intros. rewrite H; assumption.
      + intros. destruct (reachable g u z); [|reflexivity].
        destruct (reachable g u y); reflexivity.
      + intros. subst; reflexivity.
  Qed.

  Lemma NoDupA_eq : forall {A} (l:list A),
    NoDupA Logic.eq l <-> NoDup l.
  Proof.
    intros. induction l.
    - split; intros; constructor.
    - split; intros.
      + inversion H; subst. constructor.
        * rewrite InA_eq in H2. assumption.
        * apply IHl; assumption.
      + inversion H; subst. constructor.
        * rewrite InA_eq. assumption.
        * apply IHl; assumption.
  Qed.

  Lemma Ensemble_of_set_of_list : forall U,
    Ensemble_of_set U == Ensemble_of_list (VertexSet.elements U).
  Proof.
    intros.
    rewrite Same_set_iff.
    intros u. unfold Ensemble_of_set, Ensemble_of_list.
    rewrite <- InA_eq. split; intros.
    - apply VertexSet.elements_1. assumption.
    - apply VertexSet.elements_2. assumption.
  Qed.

  Lemma Ensemble_of_set_finite : forall U,
    Finite (Ensemble_of_set U).
  Proof.
    intros. rewrite Ensemble_of_set_of_list. apply Ensemble_of_list_finite.
    apply V.eq_dec.
  Qed.

  Lemma cardinal_spec : forall U n,
    VertexSet.cardinal U = n <->
    cardinal (Ensemble_of_set U) n.
  Proof.
    split; intros.
    - rewrite <- H. rewrite VertexSet.cardinal_1.
      rewrite Ensemble_of_set_of_list.
      apply Ensemble_of_list_cardinal. rewrite <- NoDupA_eq.
      apply VertexSet.elements_3w.
    - assert (NoDup (VertexSet.elements U)) as Ha.
      { rewrite <- NoDupA_eq. apply VertexSet.elements_3w. }
      pose proof (Ensemble_of_list_cardinal (VertexSet.elements U) Ha).
      rewrite <- Ensemble_of_set_of_list in H0.
      rewrite VertexSet.cardinal_1.
      eapply cardinal_unicity; eassumption.
  Qed.

  Lemma theta_spec : forall g U
    (no_edge_from_U : forall e, In_graph_edge e g -> ~ VertexSet.In (fst e) U)
    u (In_Support : Support g u) n,
    theta g U u = n <->
      cardinal (Observable g (Ensemble_of_set U) u) n.
  Proof.
    intros. rewrite theta_length_theta_set.
    rewrite cardinal_spec.
    assert (Ensemble_of_set (theta_set g U u) ==
            Observable g (Ensemble_of_set U) u)
      as Ha.
    { apply Same_set_iff. intros v. unfold Ensemble_of_set.
      rewrite theta_set_spec_no_edge by assumption. reflexivity.
    }
    rewrite Ha. reflexivity.
  Qed.

  Definition is_critical_candidate g U e :=
    (theta g U (snd e) =? 1) && (2 <=? theta g U (fst e)).

  Add Parametric Morphism : theta_set
    with signature eq ==> VertexSet.Equal ==> Vertex.eq ==> VertexSet.Equal
    as theta_set_eqG.
  Proof.
    intros g1 g2 g_eqG U1 U2 U_Equal u1 u2 u_eq. intros u.
    rewrite 2!theta_set_spec.
    rewrite <- U_Equal, <- u_eq, <- g_eqG. reflexivity.
  Qed.

  Add Parametric Morphism : Ensemble_of_set
    with signature VertexSet.Equal ==> Same_set as Ensemble_of_set_Equal.
  Proof.
    intros U1 U2 U_Equal.
    rewrite Same_set_iff. assumption.
  Qed.

  Add Parametric Morphism : VertexSet.cardinal
    with signature VertexSet.Equal ==> Logic.eq as cardinal_Equal.
  Proof.
    intros U1 U2 U_Equal.
    assert (cardinal (Ensemble_of_set U1) (VertexSet.cardinal U1)) as Ha1.
    { apply cardinal_spec; reflexivity. }
    assert (cardinal (Ensemble_of_set U2) (VertexSet.cardinal U2)) as Ha2.
    {  apply cardinal_spec; reflexivity. }
    rewrite <- U_Equal in Ha2 at 1.
    eapply cardinal_unicity; eassumption.
  Qed.

  Add Parametric Morphism : theta
    with signature (eq ==> VertexSet.Equal ==> Vertex.eq ==> Logic.eq)
    as theta_eqG.
  Proof.
    intros g1 g2 g_eqG U1 U2 U_Equal u1 u2 u_eq.
    rewrite 2!theta_length_theta_set.
    rewrite <- g_eqG, <- u_eq, <- U_Equal. reflexivity.
  Qed.

  Add Parametric Morphism : is_critical_candidate
    with signature (eq ==> VertexSet.Equal ==> Edge.eq ==> Logic.eq)
    as is_critical_candidate_eqG.
  Proof.
    intros g1 g2 g_eqG U1 U2 U_Equal e1 e2 e_eq. unfold is_critical_candidate.
    rewrite <- g_eqG, <- U_Equal, <- e_eq. reflexivity.
  Qed.

  Add Parametric Morphism : reachable
    with signature eq ==> Vertex.eq ==> Vertex.eq ==> Logic.eq
    as reachable_eqG.
  Proof.
    intros g1 g2 g_eqG u1 u2 u_eq v1 v2 v_eq.
    destruct (reachable g2 u2 v2) eqn:Hreach.
    - apply reachable_spec in Hreach.
      apply reachable_spec.
      rewrite g_eqG, u_eq, v_eq. assumption.
    - apply not_true_iff_false in Hreach.
      apply not_true_iff_false. intros contra. apply Hreach.
      apply reachable_spec in contra. apply reachable_spec.
      rewrite <- g_eqG, <- u_eq, <- v_eq. assumption.
  Qed.

  Add Parametric Morphism : reachable_from
    with signature eq ==> VertexSet.Equal ==> Vertex.eq ==> Logic.eq
    as reachable_from_eqG.
  Proof.
    intros g1 g2 g_eqG U1 U2 U_Equal v1 v2 v_eq.
    destruct (reachable_from g2 U2 v2) eqn:Hreach.
    - apply reachable_from_correct in Hreach.
      apply reachable_from_correct.
      rewrite g_eqG, U_Equal, v_eq. assumption.
    - apply not_true_iff_false in Hreach.
      apply not_true_iff_false. intros contra. apply Hreach.
      apply reachable_from_correct in contra. apply reachable_from_correct.
      rewrite <- g_eqG, <- U_Equal, <- v_eq. assumption.
  Qed.

  Add Parametric Morphism : copy_without_vertices
    with signature eq ==> VertexSet.Equal ==> eq
    as copy_without_vertices_eqG.
  Proof.
    intros g1 g2 g_eqG U1 U2 U_Equal.
    apply eq_In_graph. split.
    - intros. rewrite 2!In_graph_copy_without_vertices_vertex.
      rewrite <- g_eqG. reflexivity.
    - intros. rewrite 2!In_graph_copy_without_vertices_edge.
      rewrite <- g_eqG, <- U_Equal.
      reflexivity.
  Qed.

  Lemma is_critical_candidate_correct : forall g U e
    (e_In_graph : In_graph_edge e g)
    (no_edge_from_U : forall e, In_graph_edge e g -> ~ VertexSet.In (fst e) U),
    is_critical_candidate g U e = true <->
    Critical_candidate g (Ensemble_of_set U) (fst e) (snd e).
  Proof.
    split; intros.
    - unfold is_critical_candidate in H. rewrite andb_true_iff in H.
      destruct H as (H1 & H2).
      rewrite Nat.eqb_eq in H1.
      rewrite Nat.leb_le in H2.
      split.
      + apply Rel_w_In_graph_edge. rewrite <- Edge.edge_eq. assumption.
      + split.
        * exists (theta g U (fst e)). { split.
          - apply theta_spec.
            + assumption.
            + apply In_graph_edge_In_ext in e_In_graph. apply e_In_graph.
            + reflexivity.
          - assumption. }
        * {  apply theta_spec.
          - assumption.
          - apply In_graph_edge_In_ext in e_In_graph. apply e_In_graph.
          - assumption. }
  - apply andb_true_iff. split.
    + apply Nat.eqb_eq. apply theta_spec.
      * assumption.
      * apply In_graph_edge_In_ext in e_In_graph. apply e_In_graph.
      * apply H.
    + apply Nat.leb_le. destruct H as (_ & (n & H1 & H2) & _).
      apply theta_spec in H1.
      * rewrite H1. assumption.
      * assumption.
      * apply In_graph_edge_In_ext in e_In_graph. apply e_In_graph.
  Qed.

  Lemma copy_without_vertices_Path_Included : forall g U p,
    Path (copy_without_vertices g U) p -> Path g p.
  Proof.
    intros. split.
    - destruct H as (H & _).
      rewrite infList.Forall_forall in H. apply infList.Forall_forall; intros.
      apply H in H0. apply In_graph_vertex_strong in H0.
      destruct H0 as (l & H0).
      apply In_graph_copy_without_vertices_vertex in H0.
      apply In_graph_labeled_vertex_weak in H0. assumption.
    - destruct H as (_ & H). revert p H. cofix; intros. inversion H; subst.
      + constructor.
      + constructor.
        * apply copy_without_vertices_Path_Included; assumption.
        * { inversion H1; subst.
          - constructor.
          - constructor.
            destruct H2 as (l & H2).
            apply In_graph_copy_without_vertices_edge in H2.
            exists l; apply H2. }
  Qed.

  Lemma Path_Included_copy_without_vertices : forall g U p
    (Forall_not : infList.Forall (Complement (Ensemble_of_set U)) p),
    Path g p -> 
    Path (copy_without_vertices g U) p.
  Proof.
    intros. split.
    - destruct H as (H & _).
      rewrite infList.Forall_forall in H. apply infList.Forall_forall; intros.
      apply H in H0. apply In_graph_vertex_strong in H0.
      destruct H0 as (l & H0).
      apply In_graph_copy_without_vertices_vertex with (U:=U) in H0.
      apply In_graph_labeled_vertex_weak in H0. assumption.
    - destruct H as (_ & H). revert dependent p. cofix; intros.
      inversion H; subst.
      + constructor.
      + constructor.
        * apply Path_Included_copy_without_vertices; [|assumption].
          inversion Forall_not; assumption.
        * { inversion H1; subst.
          - constructor.
          - constructor. destruct H2 as (l & H2). exists l.
            apply In_graph_copy_without_vertices_edge.
            split.
            + assumption.
            + inversion Forall_not; assumption. }
  Qed.

  Lemma copy_without_vertices_preserves_Is_VPath : forall g U u v p,
    Is_VPath (copy_without_vertices g U) (Ensemble_of_set U) u v p <->
    Is_VPath g (Ensemble_of_set U) u v p.
  Proof.
    split; intros.
    - destruct H as (H1 & H2 & H3 & H4). split; [assumption|].
      split.
      + apply copy_without_vertices_Path_Included in H2. assumption.
      + split; assumption.
    - destruct H as (H1 & H2 & H3 & H4). split; [assumption|].
      split.
      + destruct (listOtherResults.list_nil_dec p).
        * subst p. simpl_inf. repeat constructor. simpl_inf in H2.
          destruct H2 as (H2 & _). inversion H2; subst.
          apply In_graph_vertex_strong in H5.
          destruct H5 as (l & H5).
          apply In_graph_labeled_vertex_weak with (val:=l).
          apply In_graph_copy_without_vertices_vertex. assumption.
        * destruct (List.exists_last n) as (p' & w & n').
          subst p.
          { apply Path_finite_app with (u:=w).
          - apply Path_Included_copy_without_vertices; [assumption|].
            apply Path_cut in H2. assumption.
          - simpl_inf in H2. apply Path_finite_cut in H2. repeat constructor.
            + destruct H2 as (H2 & _). inversion H2; subst.
              apply In_graph_vertex_strong in H5.
              destruct H5 as (l & H5).
              apply In_graph_labeled_vertex_weak with (val:=l).
              apply In_graph_copy_without_vertices_vertex. assumption.
            + destruct H2 as (H2 & _). inversion H2; subst. inversion H6; subst.
              apply In_graph_vertex_strong in H1.
              destruct H1 as (l & H1).
              apply In_graph_labeled_vertex_weak with (val:=l).
              apply In_graph_copy_without_vertices_vertex. assumption.
            + destruct H2 as (_ & H2). inversion H2; subst.
              inversion H6; subst. destruct H0 as (l & H0).
              exists l. apply In_graph_copy_without_vertices_edge.
              split; [assumption|]. rewrite infList.Forall_forall in H4.
              apply H4. simpl_inf. apply in_app_finite_r. apply In_here.
          - rewrite last.last_app_not_nil by discriminate. reflexivity. }
      + split; assumption.
  Qed.

  Lemma copy_without_vertices_preserves_VPath : forall g U u v,
    VPath (copy_without_vertices g U) (Ensemble_of_set U) u v <->
    VPath g (Ensemble_of_set U) u v.
  Proof.
    split; intros.
    - destruct H as (p & H).
      rewrite copy_without_vertices_preserves_Is_VPath in H.
      exists p. assumption.
    - destruct H as (p & H).
      rewrite <- copy_without_vertices_preserves_Is_VPath in H.
      exists p. assumption.
  Qed.

  Lemma copy_without_vertices_preserves_Observable :
    forall g U u,
    Observable (copy_without_vertices g U) (Ensemble_of_set U) u ==
    Observable g (Ensemble_of_set U) u.
  Proof.
    intros. apply Same_set_iff. intros v.
    apply copy_without_vertices_preserves_VPath.
  Qed.

  Lemma theta_spec_2 :
    forall (g : t) (U : VertexSet.t),
    forall u : Vertex.t,
    Support g u ->
    forall n : nat, theta (copy_without_vertices g U) U u = n <->
    cardinal (Observable g (Ensemble_of_set U) u) n.
  Proof.
    intros. rewrite theta_spec.
    - rewrite copy_without_vertices_preserves_Observable.
      reflexivity.
    - intros. apply In_graph_copy_without_vertices_edge_2 in H0. apply H0.
    - apply In_graph_copy_without_vertices_vertex_2. assumption.
  Qed.

  Lemma copy_without_vertices_preserves_Critical_candidate : forall g U u v,
    Critical_candidate (copy_without_vertices g U) (Ensemble_of_set U) u v <->
    Critical_candidate g (Ensemble_of_set U) u v.
  Proof.
    split; intros.
    - destruct H as (u_v_Rel_w & (n & u_card & n_gt_2) & v_card).
      split.
      + apply Rel_w_In_graph_edge in u_v_Rel_w.
        apply In_graph_copy_without_vertices_edge_2 in u_v_Rel_w.
        apply Rel_w_In_graph_edge. apply u_v_Rel_w.
      + split.
        * exists n. split; [|assumption].
          rewrite copy_without_vertices_preserves_Observable in u_card.
          assumption.
        * rewrite copy_without_vertices_preserves_Observable in v_card.
          assumption.
    - destruct H as (u_v_Rel_w & (n & u_card & n_gt_2) & v_card).
      split.
      + apply Rel_w_In_graph_edge.
        apply In_graph_copy_without_vertices_edge_2.
        apply Rel_w_In_graph_edge in u_v_Rel_w. split; [assumption|].
        intros contra. simpl in contra.
        apply (In_V_Observable g (Ensemble_of_set U) u) in contra.
        * pose proof (Singleton_cardinal_1 u) as contra2.
          rewrite <- contra in contra2.
          pose proof (cardinal_unicity _ _ _ u_card contra2). subst n.
          apply le_not_lt in n_gt_2. apply n_gt_2. apply le_n.
        * apply In_graph_edge_In_ext in u_v_Rel_w. apply u_v_Rel_w.
      + split.
        * exists n. split; [|assumption].
          rewrite copy_without_vertices_preserves_Observable. assumption.
        * rewrite copy_without_vertices_preserves_Observable. assumption.
  Qed.

  Lemma copy_without_vertices_preserves_Weakly_deciding : forall g U u,
    Weakly_deciding (copy_without_vertices g U) (Ensemble_of_set U) u <->
    Weakly_deciding g (Ensemble_of_set U) u.
  Proof.
    split; intros.
    - destruct H as (u1 & u2 & p1 & p2 & Hp1 & Hp2 & HDisj).
      exists u1, u2, p1, p2.
      split; [apply copy_without_vertices_preserves_Is_VPath; assumption|].
      split; [apply copy_without_vertices_preserves_Is_VPath; assumption|].
      assumption.
    - destruct H as (u1 & u2 & p1 & p2 & Hp1 & Hp2 & HDisj).
      exists u1, u2, p1, p2.
      split; [apply copy_without_vertices_preserves_Is_VPath; assumption|].
      split; [apply copy_without_vertices_preserves_Is_VPath; assumption|].
      assumption.
  Qed.

  Lemma is_critical_candidate_correct_2 : forall g U e
    (e_In_graph : In_graph_edge e g),
    is_critical_candidate (copy_without_vertices g U) U e = true <->
    Critical_candidate g (Ensemble_of_set U) (fst e) (snd e).
  Proof.
    split; intros.
    - rewrite is_critical_candidate_correct in H.
      + rewrite copy_without_vertices_preserves_Critical_candidate in H.
        assumption.
      + apply In_graph_copy_without_vertices_edge_2. split; [apply e_In_graph|].
        apply andb_true_iff in H. destruct H as (_ & H).
        intros contra.
        apply (In_V_Observable g (Ensemble_of_set U) (fst e)) in contra.
        * pose proof (Singleton_cardinal_1 (fst e)) as contra2.
          rewrite <- contra in contra2. { apply theta_spec_2 in contra2.
          - rewrite contra2 in H. discriminate.
          - apply In_graph_edge_In_ext in e_In_graph. apply e_In_graph. }
        * apply In_graph_edge_In_ext in e_In_graph. apply e_In_graph.
      + intros. apply In_graph_copy_without_vertices_edge_2 in H0. apply H0.
    - rewrite is_critical_candidate_correct.
      + rewrite copy_without_vertices_preserves_Critical_candidate.
        assumption.
      + apply In_graph_copy_without_vertices_edge_2. split; [apply e_In_graph|].
        destruct H as (_ & (n & H1 & H2) & _).
        intros contra.
        apply (In_V_Observable g (Ensemble_of_set U) (fst e)) in contra.
        * pose proof (Singleton_cardinal_1 (fst e)) as contra2.
          rewrite <- contra in contra2.
          pose proof (cardinal_unicity _ _ _ H1 contra2). subst n.
          apply le_not_lt in H2. apply H2. apply le_n.
        * apply In_graph_edge_In_ext in e_In_graph. apply e_In_graph.
      + intros. apply In_graph_copy_without_vertices_edge_2 in H0. apply H0.
  Qed.

  Definition is_critical_edge g U e :=
    is_critical_candidate (copy_without_vertices g U) U e &&
    reachable_from g U (fst e).

  Add Parametric Morphism : is_critical_edge
    with signature eq ==> VertexSet.Equal ==> Edge.eq ==> Logic.eq
    as is_critical_edge_eqG.
  Proof.
    intros g1 g2 g_eqG U1 U2 U_Equal e1 e2 e_eq.
    unfold is_critical_edge.
    rewrite <- g_eqG, <- U_Equal, <- e_eq. reflexivity.
  Qed.

  Lemma is_critical_edge_correct : forall g U e
    (e_In_graph : In_graph_edge e g),
    is_critical_edge g U e = true <->
    Critical_edge g (Ensemble_of_set U) (fst e) (snd e).
  Proof.
    intros. unfold is_critical_edge. rewrite andb_true_iff.
    rewrite is_critical_candidate_correct_2 by assumption.
    rewrite reachable_from_correct. reflexivity.
  Qed.

  Definition algo61 g (U: VertexSet.t) : bool * VertexSet.t :=
    fold_edges (fun g' e l (res:_*_) =>
                if is_critical_edge g' U e
                then (true, VertexSet.add (fst e) (snd res))
                else res)
               g (false, U).

  Lemma algo61_spec : forall g U,
    Included (Ensemble_of_set (snd (algo61 g U)))
             (Minimal_Weakly_control_closed g (Ensemble_of_set U)).
  Proof.
    intros.
    unfold algo61. rewrite fold_edges_spec. rewrite EdgeMap.fold_1.
    apply fold_invariant.
    - apply Union_increases_l.
    - intros x (b, U') x_In HIncl u u_In.
      destruct (is_critical_edge g U (fst x))
        eqn:x_crit.
      + destruct (Finite_In_dec V.eq_dec _ (Ensemble_of_set_finite U) u).
        * apply Union_introl. assumption.
        * simpl in u_In. { destruct (V.eq_dec (fst (fst x)) u).
          - rewrite <- e. apply Union_intror. apply andb_true_iff in x_crit.
            destruct x_crit as (x_dec & x_reach). split.
            + apply is_critical_candidate_correct_2 in x_dec.
              * eapply Critical_candidate_Weakly_deciding; eassumption.
              * apply In_graph_labeled_edge_weak with (val:=snd x).
                apply In_graph_labeled_edge_spec.
                apply EMapFacts.MapFacts.elements_mapsto_iff.
                apply InA_alt. exists x.
                split; [repeat constructor|]. assumption.
            + apply reachable_from_correct. assumption.
          - apply VertexSet.add_3 in u_In; [|assumption].
            apply HIncl; assumption. }
      + apply HIncl; assumption.
  Qed.

  Lemma algo61_spec_2 : forall g U,
    Included (Ensemble_of_set U) (Ensemble_of_set (snd (algo61 g U))).
  Proof.
    intros. unfold algo61. rewrite fold_edges_spec. rewrite EdgeMap.fold_1.
    apply fold_invariant.
    - simpl. intros u ?; assumption.
    - intros x (b, U') x_In HIncl u u_In.
      destruct (is_critical_edge g U (fst x))
        eqn:x_crit.
      + simpl. apply VertexSet.add_2. apply HIncl. assumption.
      + apply HIncl. assumption.
  Qed.

  Lemma algo61_false_same : forall g U,
    fst (algo61 g U) = false ->
    VertexSet.Equal (snd (algo61 g U)) U.
  Proof.
    intros g U. unfold algo61. rewrite fold_edges_spec.
    rewrite EdgeMap.fold_1. apply fold_invariant; intros.
    - reflexivity.
    - destruct (is_critical_edge g U (fst x))
        eqn:x_crit.
      + discriminate.
      + apply H0; assumption.
  Qed.

  Definition eq_bool_set (bU1 bU2 : bool * VertexSet.t) := fst bU1 = fst bU2 /\
    VertexSet.Equal (snd bU1) (snd bU2).

  Lemma eq_bool_set_refl : forall bU1, eq_bool_set bU1 bU1.
  Proof.
    intros. split; reflexivity.
  Qed.

  Lemma eq_bool_set_sym : forall bU1 bU2, eq_bool_set bU1 bU2 ->
    eq_bool_set bU2 bU1.
  Proof.
    intros. destruct H as (H1 & H2).
    split; symmetry; assumption.
  Qed.

  Lemma eq_bool_set_trans : forall bU1 bU2 bU3, eq_bool_set bU1 bU2 ->
    eq_bool_set bU2 bU3 -> eq_bool_set bU1 bU3.
  Proof.
    intros. destruct H as (H1 & H2). destruct H0 as (H01 & H02).
    split.
    - rewrite H1. assumption.
    - rewrite H2. assumption.
  Qed.

  Add Parametric Relation : (bool*VertexSet.t) eq_bool_set
    reflexivity proved by eq_bool_set_refl
    symmetry proved by eq_bool_set_sym
    transitivity proved by eq_bool_set_trans
    as eq_bool_set_equiv.

  Lemma algo61_true : forall g U,
    fst (algo61 g U) = true ->
    exists u, VertexSet.In u (snd (algo61 g U)) /\ ~ VertexSet.In u U.
  Proof.
    intros g U. unfold algo61. rewrite fold_edges_spec.
    rewrite EdgeMap.fold_1. apply fold_invariant; intros.
    - discriminate.
    - destruct (is_critical_edge g U (fst x))
        eqn:x_crit.
      + exists (fst (fst x)). split.
        * apply VertexSet.add_1. reflexivity.
        * apply andb_true_iff in x_crit. destruct x_crit as (x_dec & _).
          { apply is_critical_candidate_correct_2 in x_dec.
          - apply Critical_candidate_Weakly_deciding in x_dec.
            apply Weakly_deciding_not_In in x_dec. assumption.
          - apply In_graph_edge_spec.
            apply EMapFacts.MapFacts.elements_in_iff.
            exists (snd x). apply InA_alt. exists x.
            split; [repeat constructor|]. assumption. }
      + apply H0; assumption.
  Qed.

  Lemma add_comm : forall U u1 u2,
    VertexSet.Equal (VertexSet.add u1 (VertexSet.add u2 U))
                    (VertexSet.add u2 (VertexSet.add u1 U)).
  Proof.
    intros. split; intros.
    - destruct (V.eq_dec u1 a).
      + apply VertexSet.add_2. apply VertexSet.add_1. assumption.
      + apply VertexSet.add_3 in H; [|assumption].
        destruct (V.eq_dec u2 a).
        * apply VertexSet.add_1. assumption.
        * apply VertexSet.add_3 in H; [|assumption].
          do 2 apply VertexSet.add_2. assumption.
    - destruct (V.eq_dec u2 a).
      + apply VertexSet.add_2. apply VertexSet.add_1. assumption.
      + apply VertexSet.add_3 in H; [|assumption].
        destruct (V.eq_dec u1 a).
        * apply VertexSet.add_1. assumption.
        * apply VertexSet.add_3 in H; [|assumption].
          do 2 apply VertexSet.add_2. assumption.
  Qed.

  Lemma algo61_not_Weakly_control_closed : forall g U,
    ~ Weakly_control_closed g (Ensemble_of_set U) ->
    fst (algo61 g U) = true.
  Proof.
    intros.
    apply lemma60 in H.
    + destruct H as (u & v & H).
      unfold algo61. rewrite fold_edges_spec.
      rewrite EdgeMap.fold_1. apply (fold_becomes_true eq_bool_set).
      * intros x1 x2 (b1, U1).
        { destruct (is_critical_edge g U (fst x2)).
        - destruct (is_critical_edge g U (fst x1)).
          + simpl. split; [reflexivity|]. rewrite add_comm. reflexivity.
          + reflexivity.
        - reflexivity. }
      * intros (b1, U1) (b2, U2) (b_eq & U_eq) x1 x2 x_eq. simpl in *.
        subst b2 x2.
        destruct (is_critical_edge g U (fst x1)); (split; [reflexivity|]);
          rewrite U_eq; reflexivity.
      * intros (b1, U1) (b2, U2) (b_eq & U_eq). simpl in b_eq. subst b2.
        reflexivity.
      * apply List.Exists_exists.
        pose proof H as ((u_v_Rel_w & _) & _).
        destruct u_v_Rel_w as (l & u_v_Rel).
        exists ((u, v), l). { split.
        - apply In_graph_labeled_edge_spec in u_v_Rel.
          apply EMapFacts.elements_mapsto_iff in u_v_Rel.
          apply InA_alt in u_v_Rel.
          destruct u_v_Rel as (((u', v'), l') & u_v_Rel1 & u_v_Rel2).
          destruct u_v_Rel1 as ((u_v_Rel11 & u_v_Rel12) & u_v_Rel13).
          simpl in u_v_Rel11, u_v_Rel12, u_v_Rel13. subst u' v' l'.
          assumption.
        - intros (b1, U1).
          simpl. apply is_critical_edge_correct with (e:=(u, v)) in H.
          + rewrite H. reflexivity.
          + apply In_graph_labeled_edge_weak in u_v_Rel. assumption. }
    + apply Finite_In_dec.
      * apply V.eq_dec.
      * apply Ensemble_of_set_finite.
  Qed.

  Lemma algo61_false : forall g U,
    fst (algo61 g U) = false ->
    Ensemble_of_set (snd (algo61 g U)) ==
    Minimal_Weakly_control_closed g (Ensemble_of_set U).
  Proof.
    intros. eapply Minimum_is_Minimum; [| |apply th54_explicit].
    - split.
      + apply algo61_spec_2.
      + split.
        * apply Finite_In_dec. apply V.eq_dec. apply Ensemble_of_set_finite.
        * rewrite algo61_false_same by assumption.
          apply not_true_iff_false in H.
          { destruct (Weakly_control_closed_dec g
                       (Ensemble_of_set U)).
          - apply Finite_In_dec. apply V.eq_dec. apply Ensemble_of_set_finite.
          - assumption.
          - contradiction H. apply algo61_not_Weakly_control_closed in H0.
            assumption. }
    - apply algo61_spec.
    - apply Finite_In_dec. apply V.eq_dec. apply Ensemble_of_set_finite.
  Qed.

  Lemma Strict_Included_finite_wf : forall {A:Type}
    (eq_dec : forall (u v:A), {u=v} + {u<>v}),
    well_founded (@Strict_Included_finite A).
  Proof.
    intros A eq_dec U2. constructor. intros U1 H.
    pose proof H as (U2_Fin & _). apply finite_cardinal in U2_Fin.
    destruct U2_Fin as (n & U2_card).
    revert U1 U2 H U2_card. induction n; intros.
    - inversion U2_card; subst. destruct H as (_ & _ & _ & (x & _ & H)).
      apply H0 in H. apply Noone_in_empty in H. contradiction.
    - assert (Finite U1) as Ha.
      { eapply Finite_downward_closed; try apply H. apply eq_dec. }
      apply finite_cardinal in Ha. destruct Ha as (m & U1_card).
      assert (m < S n) as Ha2.
      { eapply Strict_Included_cardinal; try eassumption. apply H. }
      inversion Ha2; subst.
      + constructor. intros. eapply IHn; eassumption.
      + pose proof H as (_ & _ & _ & (x & x_not_In & x_In)).
        assert (Included U1 (Subtract U2 x)) as Ha.
        { intros u u_In. apply Subtract_intro.
          - apply H; apply u_In.
          - intros ocntra. subst u. contradiction.
        }
        eapply Included_cardinal_lt in Ha.
        * { apply IHn with (U2:=Subtract U2 x).
          - split.
            + apply Finite_Subtract.
              * apply eq_dec.
              * apply cardinal_finite in U2_card.
                assumption.
            + split; [apply H|]. assumption.
          - apply cardinal_Sn_Subtract; assumption. }
        * eassumption.
        * apply cardinal_Sn_Subtract; eassumption.
        * assumption.
  Qed.

  Definition Strict_Included_finite_Setminus g U1 U2 :=
    Strict_Included_finite (Setminus (Support g) (Ensemble_of_set U1))
                           (Setminus (Support g) (Ensemble_of_set U2)).

  Function real_algo61 (g:t) (U:VertexSet.t)
    {wf (Strict_Included_finite_Setminus g) U} :=
    match algo61 g U with
    | (false, U1) => U1
    | (true, U1) => real_algo61 g U1
    end.
  Proof.
    - intros.
      split.
      + apply Setminus_finite.
        * apply Support_finite.
        * { apply Finite_In_dec.
          - apply V.eq_dec.
          - apply Ensemble_of_set_finite. }
      + split.
        * { apply Finite_In_dec.
          - apply V.eq_dec.
          - apply Setminus_finite.
            + apply Support_finite.
            + apply Finite_In_dec.
              * apply V.eq_dec.
              * apply Ensemble_of_set_finite. }
        * { split.
          - intros u u_In. destruct u_In as (u_In & u_not_In).
            split; [assumption|]. intros contra. apply u_not_In.
            apply (f_equal snd) in teq. simpl in teq. rewrite <- teq.
            apply algo61_spec_2. assumption.
          - apply (f_equal snd) in teq as teq'. simpl in teq'.
            apply (f_equal fst) in teq. simpl in teq. apply algo61_true in teq.
            destruct teq as (u & teq1 & teq2).
            exists u.
            split.
            + intros contra. destruct contra as (_ & contra2).
              rewrite <- teq' in contra2. contradiction.
            + split; [|assumption].
              apply algo61_spec in teq1.
              apply Minimal_Weakly_control_closed_Included in teq1.
              apply Union_inv in teq1.
              destruct teq1; [contradiction|assumption]. }
    - intros. apply wf_inverse_image with (R:=Strict_Included_finite).
      apply Strict_Included_finite_wf.
      apply V.eq_dec.
  Defined.

  Lemma real_algo61_correct : forall g U,
    Ensemble_of_set (real_algo61 g U) ==
    Minimal_Weakly_control_closed g (Ensemble_of_set U).
  Proof.
    intros. functional induction (real_algo61 g U).
    - apply (f_equal fst) in e as e1. simpl in e1.
      apply (f_equal snd) in e. simpl in e. subst U1.
      apply algo61_false. assumption.
    - apply (f_equal fst) in e as e1. simpl in e1.
      apply (f_equal snd) in e. simpl in e. subst U1.
      rewrite IHt0.
      apply Same_set_iff.
      intros u; split; intros.
      + apply Minimal_Weakly_control_closed_increasing
          with (V2:=Minimal_Weakly_control_closed g (Ensemble_of_set U)) in H.
        * { rewrite Weakly_control_closed_closed in H.
          - assumption.
          - apply th54_explicit_dec. apply Finite_In_dec; [apply V.eq_dec|].
            apply Ensemble_of_set_finite.
          - apply th54_explicit_Weakly_control_closed.
            apply Finite_In_dec; [apply V.eq_dec|].
            apply Ensemble_of_set_finite. }
        * apply th54_explicit_dec. apply Finite_In_dec; [apply V.eq_dec|].
          apply Ensemble_of_set_finite.
        * apply algo61_spec.
      + eapply Minimal_Weakly_control_closed_increasing.
        * apply Finite_In_dec; [apply V.eq_dec|]. apply Ensemble_of_set_finite.
        * apply algo61_spec_2.
        * assumption.
  Qed.

End preCFG.
